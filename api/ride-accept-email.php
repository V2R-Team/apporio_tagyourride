<?php
function email($email,$user_name,$car_type_name,$pickup_location,$drop_location,$driver_name,$driver_phone,$driver_image,$car_type_name)
{
    $date = date('Y-m-d');

    $subject = 'Ride Confirmation';
    $from = 'info@tagyourride.co.za';
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: '.$from."\r\n".
        'Reply-To: '.$from."\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $message = '<div id=":ko" class="ii gt adP adO"><div id=":kn" class="a3s aXjCH m15c35b5f208eae4c"><div class="gmail_quote"><div class="adM">
  </div><div dir="ltr"><div class="adM">
    </div><div class="gmail_quote"><div class="adM">
      <br>                                   
      </div><div style="margin:0;padding:0"><div class="adM">   
        </div><table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
             
          </tbody>
        </table>  
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
            <tr>         
              <td bgcolor="#EEEEEE" align="center" style="padding:0 15px 0 15px" class="m_8956238122359854557tracking-space">             
                <table border="0" cellpadding="0" cellspacing="0" style="margin:15px" width="660" class="m_8956238122359854557responsive-table">                 
                  <tbody>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tbody><tr>
			          <td style="text-align:left;padding:10px"> '.$date.' </td>
			          <td style="text-align:right;padding:10px"> Tag Your Ride</td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center;padding:20px 0px 0px 0px;font-size:46px;font-weight:bold">Ride Confirm</td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center;color:#000;border-bottom:3px solid #eee;padding:20px 0px 5px 0px;font-size:14px">'.$user_name.' Your ride has been assigned successfully</td>
			        </tr>
			    </tbody></table>
			 </td>
                    </tr>
                    <tr>         
                      <td>                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">                             
                          <tbody>
                            <tr>                                 
                              <td bgcolor="#ffffff" valign="top" style="padding-top:28px;padding-bottom:0;padding-left:0px;padding-right:0px" class="m_8956238122359854557ride-detail">                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" align="left" class="m_8956238122359854557responsive-table"> 
                                  <tbody>
                                    <tr>                                             
                                      <td style="padding-left:14px;padding-right:14px" class="m_8956238122359854557ride-detail-padding">       
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold">Ride Details</td>
                                            </tr>                                                                                                  
                                            <tr>                                                        
                                              <td style="padding:9px 0 0 0">                              
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">       
                                                  <tbody>
                                                    <tr>                        
                                                      <td valign="top" class="m_8956238122359854557trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:10px;padding-left:14px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="m_8956238122359854557left-space-col">                                                                                 
                                                                <img height="50" style="height:88px" src="http://www.apporiotaxi.com/'.$driver_image.'" alt="" class="m_8956238122359854557CToWUd CToWUd">
                                                              </td>             
                                                              <td align="left" style="padding-left:16px">                                     
                                                                <table>                                  
                                                                  <tbody>
                                                                    <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$driver_name.'</td>  
																	  </tr>
																	   <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$driver_phone.'</td>  
																	  </tr><tr>
<td style="color:#000000;font-size:16px;line-height:18px">'.$car_type_name.'</td> </tr><tr> 					
<td style="color:#000000;font-size:16px;line-height:18px">'.$car_number.'</td>												  
                                                                    </tr>                                                      
                                                                  </tbody>
                                                                </table>                  
                                                              </td>                                                                            
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                 
                                                                                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                                                              
                                            <tr>
                                            	<td height="20px"></td>
                                            </tr>
											<tr>
												<td>
                                                	<table align="center" cellpadding="0" height="60" cellspacing="0" width="95%">
                                                    	<tr>
                                                        	<td width="45%">
                                                              <strong style="height:8px; display:block">Pickup Location</strong><br />
                                                            	'.$pickup_location.'
                                                            </td>
                                                            <td width="10%"></td>
                                                            <td width="45%">
                                                              <strong style="height:8px; display:block">Drop Location</strong><br />
                                                            	'.$drop_location.'
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
											</tr>
                                            <tr>
                                            	<td height="20px"></td>
                                            </tr>
                                                                                             
                                          </tbody>
                                        </table>                                             
                                      </td>                                         
                                    </tr>                                     
                                  </tbody>
                                </table>                                                                                           
                              </td>                     
                            </tr>                
                          </tbody>
                        </table>             
                      </td>         
                    </tr>
                    <tr>       
                    </tr>                                                  
                  </tbody>
                </table> 
                
              </td> 
            </tr> 
          </tbody>
        </table>             
        <p>&nbsp;
          <br>
        </p><div class="yj6qo"></div><div class="adL"> 
      </div></div><div class="adL">  
    </div></div><div class="adL">
    <br>
  </div></div><div class="adL">
</div></div><div class="adL">
</div></div></div>';
    mail($email, $subject, $message, $headers);
}


?>
