<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');


$query = "select * from table_normal_ride_rating INNER JOIN user ON table_normal_ride_rating.user_id=user.user_id INNER JOIN driver ON table_normal_ride_rating.driver_id=driver.driver_id ORDER BY rating_id DESC";
$result = $db->query($query);
$list = $result->rows;
?>

<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Ratings And Reviews</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Ride Code</th>
                                        <th>User </th>
                                        <th>User Rating Star</th>
                                        <th>User Comment</th>
                                        <th>Driver</th>
                                        <th>Driver Rating Star</th>
                                        <th>Driver Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $j= 1;
                                    foreach($list as $ratings){
                                        ?>
                                        <tr>

                                            <td><?php echo $j;?></td>

                                            <td>
                                                <?php
                                                $ride_id=$ratings['ride_id'];
                                                echo $ride_id;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $user_name = $ratings['user_name'];
                                                echo $user_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $user_rating_star = $ratings['user_rating_star'];
                                                $wholeStars = floor( $user_rating_star );
                                                $halfStar = round( $user_rating_star * 2 ) % 2;
                                                $HTML = "";
                                                for( $i=0; $i<$wholeStars; $i++ ){
                                                    $HTML .= "<img src=star@13.png alt='Whole Star'>";
                                                }
                                                if( $halfStar ){
                                                    $HTML .= "<img src=halfstar.png alt='Half Star'>";
                                                }
                                                print $HTML;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $user_comment = $ratings['user_comment'];
                                                if($user_comment == ""){
                                                    echo "No Comment";
                                                }else{
                                                    echo $user_comment;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $driver_name = $ratings['driver_name'];
                                                echo $driver_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $driver_rating_star = $ratings['driver_rating_star'];
                                                $wholeStars = floor($driver_rating_star );
                                                $halfStar = round( $driver_rating_star * 2 ) % 2;
                                                $HTML = "";
                                                for( $i=0; $i<$wholeStars; $i++ ){
                                                    $HTML .= "<img src=star@13.png alt='Whole Star'>";
                                                }
                                                if( $halfStar ){
                                                    $HTML .= "<img src=halfstar.png alt='Half Star'>";
                                                }
                                                print $HTML;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $driver_comment = $ratings['driver_comment'];
                                                if($driver_comment == ""){
                                                    echo "No Comment";
                                                }else{
                                                    echo $driver_comment;
                                                }
                                                ?>
                                            </td>

                                        </tr>
                                        <?php
                                        $j++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>
</section>
</body></html>