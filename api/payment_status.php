<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");
$id = $_REQUEST['id'];
if ($id != "")
{
    $responseData = request($id);
    $rep_array = json_decode($responseData);
    $code = $rep_array->result->code;
    $description = $rep_array->result->description;
    $details_array = array('code'=>$code,'description'=>$description);
    $re = array('result'=> 1,'msg'=> $description,'details'=>$details_array);
}else{
    $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
function request($id) {
    $url = "https://test.oppwa.com/v1/checkouts/{$id}/payment";
    $url .= "?authentication.userId=8a8294175d18c9b1015d1cf8d44b1209";
    $url .= "&authentication.password=pDtAgMPE6d";
    $url .= "&authentication.entityId=8a8294175d18c9b1015d1cfa7ccb1213";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if(curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    return $responseData;
}

echo json_encode($re, JSON_PRETTY_PRINT);
?>