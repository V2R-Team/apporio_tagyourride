<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$query = "select * from done_ride INNER JOIN payment_confirm ON done_ride.done_ride_id=payment_confirm.order_id INNER JOIN ride_table ON done_ride.ride_id=ride_table.ride_id INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id";
$result = $db->query($query);
$list=$result->rows;
if(!empty($list)){
    require_once 'PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'User Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Total Amount');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Company Cut');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Driver Cut');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Waiting Charges');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Time Charges');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Peak Time Charges');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Night Time Charge');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Coupon');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Payment Mode');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Payment Date');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Status');
    $row = 2;
    foreach($list as $value)
    {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['user_name']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, "R ".$value['payment_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, "R ".$value['company_payment']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, "R ".$value['driver_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, "R ".$value['waiting_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, "R ".$value['ride_time_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, "R ".$value['peak_time_charge']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, "R ".$value['night_time_charge']);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, "R ".$value['coupan_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['payment_method']);
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $value['pay_date']);
        $objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $value['payment_status']);
        $row++;
    }
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=Transactions.xlsx");
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');

}else{
    echo '<script type="text/javascript">alert("No Data For Export")</script>';
}
?>