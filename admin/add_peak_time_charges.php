<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="select * from city";
$result = $db->query($query);
$list=$result->rows;
if(isset($_POST['Peak']))
{
    $day = $_POST['extra_charges_day'];
    $slot_one_starttime = $_POST["slot_one_starttime"];
    $slot_one_endtime = $_POST["slot_one_endtime"];
    $slot_two_starttime = $_POST["slot_two_starttime"];
    $slot_two_endtime = $_POST["slot_two_endtime"];
    $payment_type = $_POST["payment_type"];
    $slot_price = $_POST["slot_price"];
    $city_id = $_POST['city_id'];
    $query="select * from extra_charges WHERE city_id='$city_id' AND extra_charges_day='$day'";
    $result = $db->query($query);
    $list=$result->row;
    if(empty($list)){
        $query2="INSERT INTO extra_charges (city_id,extra_charges_type,extra_charges_day,slot_one_starttime,slot_one_endtime,slot_two_starttime,slot_two_endtime,payment_type,slot_price) 
    VALUES ('$city_id','1','$day','$slot_one_starttime','$slot_one_endtime','$slot_two_starttime','$slot_two_endtime','$payment_type','$slot_price')";
        $db->query($query2);
        $msg = "Peak Time Charges Save Successfully";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    }else{
        echo '<script type="text/javascript">alert("Peak Time Charges Already")</script>';
    }
    $db->redirect("home.php?pages=add-peak-time-charges");
}

?>

<script>
    function validatelogin() {
        if(document.getElementById('city_id').value == "") { alert("Select City"); return false; }
        var extra_charges_day = document.forms["peak"]["extra_charges_day"].value;
        if(extra_charges_day == "") { alert("Select Day"); return false; }
        var slot_one_starttime = document.forms["peak"]["slot_one_starttime"].value;
        if(slot_one_starttime == "") { alert("Select Slot One Start Time"); return false; }
        var slot_one_endtime = document.forms["peak"]["slot_one_endtime"].value;
        if(slot_one_endtime == "") { alert("Select Slot One End Time"); return false; }
        var slot_two_starttime = document.forms["peak"]["slot_two_starttime"].value;
        if(slot_two_starttime == "") { alert("Select Slot Two Start Time"); return false; }
        var slot_two_endtime = document.forms["peak"]["slot_two_endtime"].value;
        if(slot_two_endtime == "") { alert("Select Slot two End Time"); return false; }
        var payment_type = document.forms["peak"]["payment_type"].value;
        if(payment_type == "") { alert("Select Payment Type"); return false; }
        var slot_price = document.forms["peak"]["slot_price"].value;
        if(slot_price == "") { alert("Enter Amount"); return false; }
    }

</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Peak time charges</h3>
        <span class="tp_rht">
         <a href="home.php?pages=view-rate-card" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
    </div>

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                    <div class="form" >
                        <form class="cmxform form-horizontal tasi-form" name="peak" onSubmit="return validatelogin()" method="post" >
                            <div class="row">
                                <div class="col-sm-12">
                                        <div class="panel-body">
                                            <div class="form" >

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Choose City*</label>
                                                    <div class="col-lg-6">
                                                        <select class="form-control" name="city_id" id="city_id">
                                                            <option value="">--Please Select City Name--</option>
                                                            <?php foreach($list as $cityname): ?>
                                                                <option id="<?php echo $cityname['city_id'];?>"  value="<?php echo $cityname['city_id'];?>"><?php echo $cityname['city_name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Select Day*</label>
                                                    <div class="col-lg-6">
                                                        <select class="form-control" name="extra_charges_day" id="extra_charges_day">
                                                            <option value="">--Select Day--</option>
                                                            <option value="Monday">Monday</option>
                                                            <option value="Tuesday">Tuesday</option>
                                                            <option value="Wednesday">Wednesday</option>
                                                            <option value="Thursday">Thursday</option>
                                                            <option value="Friday">Friday</option>
                                                            <option value="Saturday">Saturday</option>
                                                            <option value="Sunday">Sunday</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Slot One Start Time</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Slot One Start Time" name="slot_one_starttime"  id="timepicker" >
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Slot One End Time</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Slot One End Time" name="slot_one_endtime"  id="timepicker1" >
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Slot Two Start Time</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Slot Two Start Time" name="slot_two_starttime"  id="timepicker1">
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Slot Two End Time</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Slot Two End Time" name="slot_two_endtime"  id="timepicker1">
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Charges Type*</label>
                                                    <div class="col-lg-6">
                                                        <select class="form-control" name="payment_type" id="payment_type">
                                                            <option value="">Select Payment</option>
                                                            <option value="1">Nominal</option>
                                                            <option value="2">Multiplier</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Charges</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Enter Charges" name="slot_price"  id="slot_price">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="Peak" value="Add Peak Time Charge" >
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="clear"></div>
            </div>
        </div>
    </div>

</div>
<script>
    jQuery(document).ready(function() {
        var timeAnswers =$('#timepicker, #timepicker1');
        $(timeAnswers).each(function(){
            $(this).timepicki({
                overflow_minutes:true,
                increase_direction:'up',
                disable_keyboard_mobile: true
            });
        });
    });
</script>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
