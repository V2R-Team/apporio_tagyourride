<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Demo_Account extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Useraccountmodel');
    }

    function Signup_post()
    {
        $first_name = "Demo";
        $lastname =  "User";
        $user_password = "Apporiotaxi";
        $user_phone = $this->post('user_phone');
        $user_email = $this->post('user_email');
        $unique_number = $this->post('unique_number');
        if($first_name != "" && $lastname != "" && $user_password != "" && $unique_number != ""){
            $text = $this->Useraccountmodel->check_unique_number($unique_number);
            if (!empty($text))
            {
                $this->response([
                            'result' => 1,
                            'message' => "Signup Succusfully!!",
                            'details' => $text
                        ], REST_Controller::HTTP_CREATED);
            }else{
                $user_name = $first_name." ".$lastname;
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data=$dt->format('M j');
                $day=date("l");
                $date=$day.", ".$data ;
                $user_signup_date = date("Y-m-d");
                if (empty($user_phone))
                {
                    $user_phone = "User Phone Number";
                }
                if (empty($user_email))
                {
                    $user_phone = "User Email Number";
                }
                $data = array(
                    'user_name' => $user_name,
                    'user_phone' => $user_phone,
                    'user_password' => $user_password,
                    'user_email'=>$user_email,
                    'register_date'=>$date,
                    'user_type'=>2,
                    'unique_number'=>$unique_number,
                    'user_signup_date'=>$user_signup_date
                );
                $text = $this->Useraccountmodel->signup($data);
                $this->response([
                    'result' => 1,
                    'message' => "Signup Succusfully!!",
                    'details' => $text
                ], REST_Controller::HTTP_CREATED);
            }
        }
        else{
            $this->response([
                'result' => 0,
                'message' => "Required fields missing!!"
            ], REST_Controller::HTTP_CREATED);
        }
    }
	
	public function Upload_File_post()
    {
        $file_name = $this->post('file_name');
		$user_id = $this->post('user_id');
        if(!empty($user_id) && !empty($file_name) && !empty($_FILES['file']['name']))
        {
                $config = [
                    'upload_path' => './test_docs/',
                    'allowed_types' => '*'
                ];
                $this->load->library('upload', $config);
                if($this->upload->do_upload('file') == TRUE)
                {
                    $data = $this->upload->data();
                    $image = base_url("test_docs/" . $data['raw_name'] . $data['file_ext']);
                    $this->Useraccountmodel->Upload_File($image,$file_name,$user_id);
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Your File Upload Succusfully'  
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'Image Not Uploaded'
                    ], REST_Controller::HTTP_CREATED);
                }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }
	
	public function View_Upload_File_post()
    {
        $file_name = $this->post('file_name');
        if(!empty($file_name))
        {
                $file_name = json_decode($file_name);
				foreach($file_name as $login){
					$file = $login['file_name'];
					$data = $this->Useraccountmodel->View_Upload_File($file);
					if(!empty($data)){
						$c[]= $data;
					}
				}
				if(!empty($c)){
					$this->set_response([
                        'status' => 1,
                        'message' => 'File Paths',
                        'details'=>$c
                    ], REST_Controller::HTTP_CREATED);
				}else{
					$this->set_response([
									'status' => 0,
									'message' => 'No File Paths Found'
								], REST_Controller::HTTP_CREATED);
				}
                
                
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    
}