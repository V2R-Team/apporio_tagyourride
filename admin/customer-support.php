<?php
include_once '../apporioconfig/start_up.php';
if (!isset($_SESSION['ADMIN']['ID'])) {
    $db->redirect("index.php");
}
include('common.php');

$query = "select * from customer_support ORDER BY customer_support_id DESC";
$result = $db->query($query);
$list = $result->rows;

if(isset($_POST['closeQuery'])){
    $ID=$_POST['closeQuery'];
    $sql="update customer_support set query_status = 1 WHERE customer_support_id = $ID ";
    $db->query($sql);
    $db->redirect('home.php?pages=customer-support');
}
?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Customer Support</h3>
    </div>
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
            <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                <tr>
                    <th width="5%">Sr.No.</th>
                    <th> Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Query</th>
                    <th>Query Status</th>
                    <th>Application</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $j = 1;
                foreach ($list as $support) {
                    ?>
                    <tr>
                        <td><?php echo $j; ?></td>
                        <td>
                            <?php
                            $name = $support['name'];
                            echo $name;
                            ?>
                        </td>
                        <td>
                            <?php
                            $email = $support['email'];
                            if ($email == "") {
                                echo "------";
                            } else {
                                echo $email;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            $phone = $support['phone'];
                            echo $phone;
                            ?>
                        </td>

                        <td>
                            <?php
                            $query = $support['query'];
                            echo $query;

                            ?>

                        </td>
                        <td>
                            <?php
                            $query = $support['query_status'];
                            if ($query == 0) {
                                echo "<label style='color: #ff0000; font-size: 16px;'>Active</label>";
                            } else {
                                echo "<label style='color: #00CC00; font-size: 16px;'>Closed</label>";
                            }

                            ?>

                        </td>


                        <td><?php
                            $application = $support['application'];
                            switch ($application) {
                                case "1":
                                    echo "Customer";
                                    break;
                                case "2":
                                    echo "Driver";
                                    break;
                                default:
                                    echo "------";
                            }
                            ?></td>
                        <td><?php
                            $date = $support['date'];
                            echo $date;
                            ?></td>
                        <td><?php
                            echo "<form method='post'><button class='btn btn-success' name='closeQuery' value='".$support['customer_support_id']."'>Close</button></form>";
                            ?></td>
                    </tr>
                    <?php $j++;
                }
                ?>
                </tbody>

            </table>
        </div>


    </div>
    <!-- End row -->

</div>


</section>

</body></html>