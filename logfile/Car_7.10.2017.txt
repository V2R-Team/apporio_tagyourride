Rental - Car Type Api: October 7, 2017, 10:51 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Sedan
            [car_type_image] => uploads/car/editcar_28.png
            [city_id] => 56
            [base_fare] => 5 Per 5 Miles
            [ride_mode] => 1
            [currency_iso_code] => R
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Luxury
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 56
            [base_fare] => 5 Per 5 Miles
            [ride_mode] => 1
            [currency_iso_code] => R
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Van
            [car_type_image] => uploads/car/editcar_30.png
            [city_id] => 56
            [base_fare] => 5 Per 5 Miles
            [ride_mode] => 1
            [currency_iso_code] => R
            [currency_unicode] => 0
        )

)

city_name: Gurugram
latitude: 28.412218492717617
longitude: 77.0432299375534
-------------------------
