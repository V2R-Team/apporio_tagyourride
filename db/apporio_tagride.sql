-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:23 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_tagride`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(32, 'Arcel', 'Dred', 'Dred', '', '', 'DRE988$d', '0736721260', 'dredarsenic@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(33, 'David', 'Satande', 'david', '', '', 'tatenda', '0835156147', 'davidtatenda@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(34, 'Rogerant', 'Tshibangu', 'Rogerant', '', '', 'kapinga12', '0614950420', 'rtshbangu', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 2),
(35, 'Roger', 'Tshibanguw', 'Roger', '', '', 'kapinga12', '0614950420', 'rtshbangu', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 2),
(36, 'Rogerant', 'Tshibangu', 'rtshibangu', '', '', 'apporio123', '0614950420', 'rtshibangu@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(37, 'Roy', 'Makumborenga', 'roym', '', '', 'P@ssword01', '0786022243', 'roy.m@tagyourride.co.za', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'Tag Your Ride', 'uploads/logo/logo_59ee1917a5ed6.png', 'hello@apporio.com', 'Pietermaritzburg, KwaZulu-Natal, South Africa', 'AIzaSyBXzLu3YdYG27dQu7UZzmwk5TSnPYpdJX8', '-29.6006068', '30.3794118', 'tagyourride-6bf4e');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1),
(13, 'Customer not reachable', '', 0, 2, 1),
(15, 'Bad weather', '', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(1, '8a82944a5f5892d0015f7c7aed072d43', 1),
(2, '8a8294495f587757015f7c7af92f1ffd', 1),
(3, '8a82944a5f5892d0015f7c7b63ad2da3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/editcar_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1),
(13, 'yamaha', 'uploads/car/car_13.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 2),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi A3', '', '', '', '', '', '', 'uploads/car/editcar_11.png', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', 'uploads/car/editcar_19.png', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 2),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1),
(43, 'Camry', '', '', 'Ferrari', '', '', '', 'uploads/car/editcar_43.png', 29, 2),
(44, 'EcoSport', '', '', 'Suzuki', '', '', '', '', 30, 1),
(45, 'Polo Vivo', '', '', 'BMW', '', '', '', 'uploads/car/editcar_45.png', 28, 1),
(46, 'Ford Ranger', '', '', 'BMW', '', '', '', '', 28, 1),
(47, 'Hilux', '', '', 'BMW', '', '', '', 'uploads/car/editcar_47.png', 29, 2),
(48, 'Figo', '', '', 'BMW', '', '', '', 'uploads/car/editcar_48.png', 30, 1),
(49, '3 Series', '', '', 'BMW', '', '', '', '', 31, 1),
(50, 'Hilux', '', '', 'BMW', '', '', '', 'uploads/car/editcar_50.png', 32, 1),
(51, 'S-Class', '', '', 'Mercedes', '', '', '', 'uploads/car/editcar_51.png', 32, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(28, 'Sedan', '', 'Sedan', 'uploads/car/editcar_28.png', '5102', 1, 1),
(32, 'Luxury', '', ' ', 'uploads/car/editcar_32.png', '4349', 1, 1),
(33, 'Luxury', '', 'Luxury', 'uploads/car/car_33.png', '', 1, 1),
(34, 'Van', '', '', 'uploads/car/car_34.png', '6542', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', 'R', 'R', '0', 'Miles', '', '', 1),
(121, 'Pietermaritzburg', '-29.6006068', '30.3794118', 'R', 'R', '0', 'Km', '', '', 1),
(122, 'Harare', '-17.8251657', '31.03351', '$', 'AUD', '0024', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(28, 'Erva', 'ervaerva@gmail.com', '251525152515', 'Mudanya/Bursa, TÃ¼rkiye', 'TURKEY', 77, 'erva erva', 'rikapimobil', '123456', '', 1),
(29, 'mazen', 'wkobessy@hotmail.com', '+966542961615', '11422 Riyadh,, Saudi Arabia', 'SAUDI ARABIA', 79, 'Waleed Abdelrahman', '123456', '', '', 1),
(30, 'MIZZO', 'WWWW@YYYY.COM', 'WKOBESSY', 'BAKOS', 'EGYPT', 81, 'Waleed Abdelrahman', '12345678', '133', '', 2),
(31, 'Take me ', 'ronyprevot@gmail.com', '3052006714', '18101 NW 7 ave miami gardens, fl 33169', 'UNITED STATES', 71, 'Peter', 'comcast1', '2323232', 'uploads/company/company_31.jpg', 1),
(32, 'company test', 'test@test.retest', '0406070809', 'info93390@yahoo.fr', 'FRANCE', 0, 'admin', '123456', '1234567', 'uploads/company/company_32.jpg', 1),
(33, 'kljlk', 'asdfgh@gmail.com', '0975687658', 'infolabs', 'VIET NAM', 85, 'jkjh', '12345678', '2', 'uploads/company/company_33.jpg', 1),
(34, 'iuyoiu', 'zxcvbn@gmail.com', '09587587', 'HÃ  Ná»™i, Viá»‡t Nam', 'BAHAMAS', 84, 'bnvnv', '12345678', '1', '', 1),
(35, 'FCT', 'trustfastcare@gmail.com', '6025548447', '3001 West Indian School Road, Phoenix, AZ, United States', 'UNITED STATES', 0, 'Ernie', 'Isudana', '', '', 1),
(36, 'jact', 'infolabs', '0722517766', '10511 00100 nairobi kenya', 'KENYA', 0, 'john', 'apporio7788', '', '', 1),
(37, 'Nashmi', 'jordanians.nashmi@gmail.com', '0799007537', 'Amman Governorate, Jordan', 'JORDAN', 3, 'Wajdi', '123456', '', 'uploads/company/company_37.png', 1),
(38, 'Apporio Infolabs Pvt Ltd', 'apporio@infolabs.com', '9592096200', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', 'ANTIGUA AND BARBUDA', 71, 'keshav Goyal', '123456', '12', '', 1),
(39, 'MP', 'stephan@mobility.rocks', '004917685400042', 'stephan@mobility.rocks', 'GERMANY', 77, 'Volker', 'pactaol2112', '', '', 1),
(40, 'Infosec systems', 'info@infosecsystems.co.ke', '+254700160125', 'Ngara Ngara Road', 'KENYA', 104, 'Sam Mwas', 'administrator2017', 'PO288999U', '', 1),
(41, 'Mrt', 'murtada.omer@hotmail.com', '0912381921', 'sudan', 'SUDAN', 3, 'murtada', '78787878', '7878', '', 1),
(42, 'MOTO TAXI SP JÃ ', 'ANDREFREITASALVES2017@GMAIL.COM', '5511958557088', 'Anisio Da Silveira Machado - Rua AnÃ­sio da Silveira Machado - Jardim Robru, SÃ£o Paulo - SP, Brasil', 'BRAZIL', 113, 'ANDRE', '14127603', '1976', 'uploads/company/company_42.jpg', 1),
(43, 'exeer', 'exeer@gmail.com', '0111111111111', 'ÙAl-Waha, Omdurman, Khartoum, Sudan', 'SUDAN', 112, 'exeery', 'exeer', '5', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 63, 'MOTO TAXI SP JÃ', 1, 2, 1, 2, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com2', '', 'tawsila', '', 'tawsila', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(74, 'APPORIO', '10', 0, 0, '2017-08-14', '2017-09-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2017-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2017-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-13', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1),
(108, 'Manual', '10', 10, 10, '2017-09-19', '2017-09-22', 'Nominal', 1),
(109, 'PROMO123', '2', 2, 2, '2017-10-03', '2017-10-04', 'Nominal', 1),
(110, 'TAG4432y7R', '75', 1, 1, '2017-10-27', '2018-10-27', 'Nominal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`) VALUES
(1, 'Doller', '&#36', '&#36'),
(2, 'POUND', '&#163', '&#163'),
(3, 'Indian Rupees', '&#8377', '&#8377');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL,
  `query_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`, `query_status`) VALUES
(1, 1, 'ananna', 'anaknak', '939293', 'kaksak', 'Saturday, Aug 26, 01:05 PM', 0),
(2, 2, 'xhxhxhhx', 'jcjcjcjcjc45@t.com', '1234567990', 'xhlduohldgkzDJtisxkggzk', 'Saturday, Aug 26, 01:06 PM', 0),
(3, 2, 'dyhddh', 'hxhxjccj57@y.com', '3664674456', 'chicxgxhxyhccphxoh', 'Saturday, Aug 26, 01:10 PM', 0),
(4, 2, 'bzxhjx', 'xhcjlbgkog46@t.com', '488668966', 'hxcjycpots gkkcDuizgk g kbclh', 'Saturday, Aug 26, 01:12 PM', 0),
(5, 1, 'hdxhxh', 'pcijcjcjc56@y.com', '63589669', 'bhxcjn tfg nzgogxoxgxl', 'Saturday, Aug 26, 01:46 PM', 0),
(6, 2, 'fbfbfbfb', 'fbfbfbfb', 'hdhdhhdhdhdh', 'udydhdhdg', 'Saturday, Aug 26, 06:20 PM', 0),
(7, 2, 'd xrcr', 'd xrcr', 'wzxxwxwxe', 'ssxscecececrcrcrcrcrcrcrc', 'Saturday, Aug 26, 06:25 PM', 0),
(8, 2, 'Shilpa', 'Shilpa', '9865321478', 'hi ', 'Sunday, Aug 27, 09:25 AM', 0),
(9, 2, 'hsjhhshh', 'hsjhhshh', '9969569', 'vvvvvvgg', 'Monday, Aug 28, 11:17 AM', 0),
(10, 1, 'Shivani', 'fxuxyuguhco@gmail.com', 'trafsyxtuxfuxfu', 'Fyfzyhf hf', 'Monday, Aug 28, 11:25 AM', 0),
(11, 2, 'zfjzgjzjgzgj', 'jfititig@gmail.com', 'dghvvch', 'Dfhxxhhxhc', 'Monday, Aug 28, 11:26 AM', 0),
(12, 2, 'dhjsjz', 'dhjsjz', '76979797979', 'zhhzhzhzhhz', 'Monday, Aug 28, 05:28 PM', 0),
(13, 1, 'Halmat ', 'LuxuryRidesLimo@gmail.com', '8477645466', 'Hey this is Halmat \nCan you call me or email me \nNeed to talk to you\n\nThanks ', 'Wednesday, Sep 6, 08:38 PM', 0),
(14, 2, 'anuuuuuu', 'anuuuuuu', '8874531856', 'jftjffugjvvd', 'Thursday, Sep 7, 10:45 AM', 0),
(15, 2, 'André', 'André', '1195855708870', 'seja bem-vindo ', 'Tuesday, Sep 12, 05:38 AM', 0),
(16, 2, 'anurag', 'anurag', '880858557585', 'zfgfgfgxxfdcghdgc', 'Tuesday, Sep 12, 11:20 AM', 0),
(17, 2, 'MOHAMED ', 'MOHAMED ', '8639206529', 'taxi app', 'Wednesday, Sep 13, 11:17 PM', 0),
(18, 2, 'amrit', 'amrit', '0686566855', 'ucjcjyjbkvffvkb', 'Thursday, Sep 14, 10:56 AM', 0),
(19, 2, 'ndufu', 'ndufu', '27342438', 'ududcucuc', 'Thursday, Sep 14, 11:06 AM', 0),
(20, 2, '15666666666', '15666666666', '15666666666', '15666666666', 'Friday, Sep 15, 05:55 AM', 0),
(21, 2, '15666666666', '15666666666', '15666666666', '15666666666', 'Saturday, Sep 16, 12:08 AM', 0),
(22, 2, 'Adamah', 'adamah.welbeck@gmail.com', '0208219759', 'My system keep going off. Please try to help me', 'Wednesday, Sep 20, 12:29 AM', 1),
(23, 2, 'dred', 'dred', '0736721260', 'can\'t navigate ', 'Wednesday, Oct 11, 09:47 PM', 1),
(24, 2, 'David', 'davidtatenda@gmail.com', '0835156147', 'Test query', 'Wednesday, Oct 25, 04:58 PM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.4120880738336', '77.0432801295571', '28.412080666942', '77.0432798459306', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:18:17 PM', '02:18:21 PM', '02:18:26 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(2, 2, '28.4120919231934', '77.0433016341939', '28.4120928777848', '77.0433041267486', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:45:39 AM', '07:45:52 AM', '07:46:07 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(3, 3, '28.412093041892', '77.0432342313308', '28.4120774338745', '77.0432583639921', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:50:16 AM', '07:50:31 AM', '07:51:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(4, 4, '28.4120981351223', '77.0432269936879', '28.4120981351223', '77.0432269936879', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:30:34 AM', '10:30:38 AM', '10:30:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(5, 5, '-26.1232715030509', '28.0348194249302', '-26.1232438290558', '28.0346163374317', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '12:46:11 PM', '12:46:22 PM', '12:50:07 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '4', '60', 1, '', 1, 0, '0000-00-00'),
(6, 6, '-26.1232879152962', '28.0347041407109', '-26.1232839967738', '28.0347815818072', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '01:09:34 PM', '01:10:08 PM', '01:10:58 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '1', '60', 1, '', 1, 1, '0000-00-00'),
(7, 9, '-26.1230758019035', '28.034825641688', '-26.123098349223', '28.0347957182937', '0B 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0B 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:22:50 PM', '03:23:04 PM', '03:23:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '0', '60', 1, '', 1, 1, '0000-00-00'),
(8, 12, '-26.0608131316789', '28.0623029132542', '-26.0686073374159', '28.0531375855207', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '11 North Rd, Morningside, Sandton, 2057, South Africa', '05:11:02 PM', '05:11:12 PM', '05:11:28 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '0', '60', 1, '', 1, 1, '0000-00-00'),
(9, 13, '-26.0608355964566', '28.0622789502018', '-26.0607355960679', '28.0626268442581', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:15:10 PM', '05:15:20 PM', '05:15:37 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '0', '60', 1, '', 1, 1, '0000-00-00'),
(10, 18, '-26.0608387846703', '28.0623131743902', '-26.0627738695698', '28.06131798774', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 4th Ave, Edenburg, Sandton, 2128, South Africa', '07:13:04 PM', '07:13:30 PM', '07:14:57 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '1', '60', 1, '', 1, 1, '0000-00-00'),
(11, 20, '-26.0614992174137', '28.0625528119625', '-26.063818066301', '28.0606735870242', '8-10 5th Ave, Edenburg, Sandton, 2128, South Africa', '315 Rivonia Rd, Morningside, Sandton, 2057, South Africa', '12:16:36 AM', '12:16:51 AM', '12:17:48 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '1', '60', 1, '', 0, 0, '0000-00-00'),
(12, 24, '-26.0610750736911', '28.0623566732072', '-26.0608063724038', '28.0622313473232', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '08:57:38 AM', '08:58:22 AM', '08:59:58 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '2', '60', 1, '', 1, 1, '0000-00-00'),
(13, 26, '-26.0610899515693', '28.0624047015124', '-26.0541809125658', '28.0615791908079', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '24 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '04:15:50 PM', '04:15:58 PM', '04:19:29 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '714.879189288158', '4', '60', 1, '', 1, 1, '0000-00-00'),
(14, 28, '-26.0608770112941', '28.0623516798032', '-26.0609711991667', '28.0624288125319', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:22:52 PM', '05:23:07 PM', '05:23:22 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '0', '60', 1, '', 0, 0, '0000-00-00'),
(15, 34, '-26.0609245702208', '28.0623880261404', '-26.0609090511834', '28.0623779398324', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:44:07 PM', '05:45:04 PM', '05:45:44 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '1', '60', 1, '', 1, 1, '0000-00-00'),
(16, 35, '-26.0435617575664', '28.0602626223378', '-26.0314093809817', '28.0620857700967', '67 Wessel Rd, Rivonia, Sandton, 2128, South Africa', '0B Leeuwkop Rd, Sunninghill, Sandton, 2157, South Africa', '08:30:06 AM', '08:30:13 AM', '08:36:17 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '1585.02265796449', '6', '60', 1, '', 1, 1, '0000-00-00'),
(17, 36, '-26.0362309490543', '28.0504757273578', '-26.0610530711953', '28.0623849202209', '23 Witkoppen Rd, Sandton, Johannesburg, 2054, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '09:23:38 AM', '09:23:59 AM', '09:31:34 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '3675.75281230916', '8', '60', 1, '', 1, 0, '0000-00-00'),
(18, 38, '-26.0610321249793', '28.0624694563877', '-26.060874511791', '28.0623495207805', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:12:08 PM', '05:12:35 PM', '05:13:10 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 6, '60', '6.00', '54.00', '60.00', '0.00', '0.00 Km', '0.0', '1', '60', 1, '', 1, 1, '0000-00-00'),
(19, 45, '-26.0608914608336', '28.0619647353888', '-26.0609604007355', '28.0624199443139', '2-4 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:30:18 PM', '07:41:46 PM', '07:42:01 PM', '11', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '20', '2.00', '18.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 1, '0000-00-00'),
(20, 46, '28.4120878950026', '77.04327712774', '28.4120878950026', '77.04327712774', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:49:19 AM', '10:49:36 AM', '10:49:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(21, 47, '28.4121167026632', '77.0432607087679', '28.4120982616544', '77.0432560815665', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:04:28 AM', '11:04:36 AM', '11:05:11 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '1', '178', 1, '', 1, 1, '0000-00-00'),
(22, 48, '-26.1303472286334', '28.0343836639338', '-26.1303734220808', '28.0344700813555', '11 North Rd, Dunkeld West, Randburg, 2196, South Africa', '12 North Rd, Dunkeld West, Randburg, 2196, South Africa', '03:23:18 PM', '03:23:44 PM', '03:24:07 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(23, 49, '-26.1303194155944', '28.0344611790826', '-26.1304271081706', '28.0345026031398', '12 North Rd, Dunkeld West, Randburg, 2196, South Africa', '12 North Rd, Dunkeld West, Randburg, 2196, South Africa', '03:31:45 PM', '03:31:59 PM', '03:32:15 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(24, 50, '-26.060960987847', '28.0624201611956', '-26.0609603095205', '28.062419903193', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '04:26:14 PM', '04:26:22 PM', '04:26:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(25, 51, '-26.06096038', '28.06241993', '-26.06096038', '28.06241993', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '04:38:40 PM', '04:38:54 PM', '04:39:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(26, 52, '-26.06096038', '28.06241993', '-26.0607645042683', '28.0622643527554', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '08:13:23 PM', '08:14:05 PM', '08:15:27 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.00', '0.00', '5.00', '0', '0.00 Km', '0.0', '1', '5', 1, 'Insufficent Balance', 1, 1, '0000-00-00'),
(27, 53, '-26.0443762690071', '28.0604520533495', '-26.0465136537861', '28.0611220525233', '61 Wessel Rd, Woodmead, Sandton, 2191, South Africa', '45A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '01:14:26 PM', '01:14:54 PM', '01:17:12 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '210.15165121522', '2', '5', 1, '', 1, 1, '0000-00-00'),
(28, 54, '-26.0517786572208', '28.06143937587', '-26.0616198717507', '28.0592831715198', '32 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4 De La Rey Rd, Edenburg, Sandton, 2128, South Africa', '01:21:26 PM', '01:21:35 PM', '01:26:18 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '1145.46792794656', '5', '5', 1, '', 1, 1, '0000-00-00'),
(29, 55, '-26.0616260259573', '28.0591280475921', '-26.0616235896576', '28.0591894573559', '4 De La Rey Rd, Edenburg, Sandton, 2128, South Africa', '4 De La Rey Rd, Edenburg, Sandton, 2128, South Africa', '01:31:34 PM', '01:33:06 PM', '01:34:07 PM', '1', '5.00', '00.00', '0.00', '0.00', '0.00', 11, '10', '0.50', '9.50', '5.00', '0.00', '0.00 Km', '0.0', '1', '10', 1, '', 1, 1, '0000-00-00'),
(30, 56, '-26.0616202309524', '28.0592741174249', '-26.0610579746087', '28.0624146759771', '4 De La Rey Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '01:35:28 PM', '01:35:39 PM', '01:40:11 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '627.401158035812', '5', '5', 1, '', 1, 1, '0000-00-00'),
(31, 57, '-26.0597437760104', '28.0606381315999', '-26.0610639257599', '28.0624266620987', '332 Rivonia Blvd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '11:28:19 AM', '11:28:26 AM', '11:31:36 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '446.926993404676', '3', '5', 1, '', 1, 1, '0000-00-00'),
(32, 58, '-26.0612154705693', '28.0623949785047', '-26.0607731420359', '28.0623417733419', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '12:00:12 PM', '12:00:39 PM', '12:01:33 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(33, 64, '-26.0606385368142', '28.0624421558402', '-26.0606385368142', '28.0624421558402', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '06:09:20 AM', '06:09:28 AM', '06:12:35 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '3', '5', 1, '', 1, 1, '0000-00-00'),
(34, 66, '28.4121084540399', '77.0433549130731', '28.4120872663599', '77.043316857961', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:57:46 AM', '06:58:03 AM', '06:58:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(35, 67, '-26.1232828224944', '28.0347302913249', '-26.1232813280803', '28.0347767295833', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:37:35 PM', '02:37:49 PM', '02:38:00 PM', '0', '0.00', '00.00', '0.00', '0.00', '2', 11, '3', '0.15', '2.85', '5.00', '0.00', '0.00 Km', '0.0', '0', '3', 1, '', 1, 1, '0000-00-00'),
(36, 68, '28.4121513879191', '77.0433492959263', '28.4121496172727', '77.0433432628476', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:57:26 PM', '02:57:31 PM', '02:57:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(37, 69, '-26.123265100087', '28.0347175743704', '-26.1232719861217', '28.0347768758214', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:08:41 PM', '03:09:00 PM', '03:09:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '2', 11, '3', '0.15', '2.85', '5.00', '0.00', '0.00 Km', '0.0', '0', '3', 1, '', 1, 1, '0000-00-00'),
(38, 69, '-26.123265100087', '28.0347175743704', '-26.1232719861217', '28.0347768758214', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:08:41 PM', '03:09:00 PM', '03:09:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '2', 11, '3', '0.15', '2.85', '5.00', '0.00', '0.00 Km', '0.0', '0', '3', 1, '', 1, 1, '0000-00-00'),
(39, 71, '-26.0607723299957', '28.0623486299701', '-26.0607723299957', '28.0623486299701', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:06:01 PM', '05:06:24 PM', '05:07:16 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(40, 76, '-26.0607723299957', '28.0623486299701', '-26.0818182290436', '28.0422659218311', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '46 Coleraine Dr, Riverclub, Sandton, 2191, South Africa', '05:32:27 PM', '05:32:40 PM', '05:32:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(41, 78, '-26.0607723299957', '28.0623486299701', '-26.0597385267213', '28.0610792740285', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', 'Mutual Rd, Edenburg, Sandton, 2128, South Africa', '05:48:57 PM', '05:49:14 PM', '05:53:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '0.39 Km', '0.0', '4', '5', 1, '', 1, 1, '0000-00-00'),
(42, 79, '-26.059882664146', '28.0609664507472', '-26.0544797092933', '28.0606310250858', 'Mutual Rd, Edenburg, Sandton, 2128, South Africa', '26-28 10th Ave, Edenburg, Sandton, 2128, South Africa', '06:00:17 PM', '06:00:27 PM', '06:04:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '1.03 Km', '463.191141559797', '4', '5', 1, '', 1, 1, '0000-00-00'),
(43, 80, '-26.0544789949256', '28.0606480330927', '-26.0610370198507', '28.062390536096', '26 10th Ave, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '06:09:21 PM', '06:09:39 PM', '06:13:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '0.84 Km', '852.066958973175', '4', '5', 1, '', 1, 1, '0000-00-00'),
(44, 81, '-26.0607791547809', '28.062276934697', '-26.0607791547809', '28.062276934697', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '06:58:46 PM', '06:59:03 PM', '07:03:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '5', '5', 1, '', 1, 1, '0000-00-00'),
(45, 82, '-26.0607791547809', '28.062276934697', '-26.0607791547809', '28.062276934697', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:28:19 PM', '07:28:26 PM', '07:29:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(46, 83, '-26.0607791547809', '28.062276934697', '-26.0610545873144', '28.0623767617596', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:51:47 PM', '07:52:01 PM', '07:54:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '3', '20', 1, '', 0, 1, '0000-00-00'),
(47, 85, '-26.0610545873146', '28.0623767617597', '-26.0607811885767', '28.0622774284472', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:58:20 PM', '07:58:38 PM', '08:02:05 PM', '0', '0.00', '5.00', '0.00', '0.00', '0.00', 17, '25', '1.25', '23.75', '20.00', '0.00', '0.00 Km', '0.0', '3', '25', 1, '', 0, 0, '0000-00-00'),
(48, 86, '-26.0613623634223', '28.0592562910431', '-26.1306909120541', '28.0315633446957', '4 De La Rey Rd, Edenburg, Sandton, 2128, South Africa', '2-4 Bompas Rd, Dunkeld West, Randburg, 2196, South Africa', '07:22:03 AM', '07:24:25 AM', '07:48:28 AM', '2', '4.00', '110.00', '0.00', '0.00', '0.00', 17, '228.3', '11.42', '216.88', '114.30', '0.00', '10.43 Km', '9356.40781140361', '24', '228.3', 1, '', 1, 1, '0000-00-00'),
(49, 87, '-26.1232972799059', '28.034744040703', '-26.1232949467619', '28.0347377155237', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:41:11 PM', '02:41:18 PM', '02:41:35 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(50, 93, '28.4120438', '77.0432797', '28.4120438', '77.0432797', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:33:53 PM', '12:34:13 PM', '12:34:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 28, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(51, 94, '28.4122167', '77.0432123', '28.4122167', '77.0432123', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:36:18 PM', '12:36:30 PM', '12:36:40 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 28, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(52, 95, '28.4122167', '77.0432123', '28.4122167', '77.0432123', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:00:38 PM', '02:00:46 PM', '02:00:53 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 30, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(53, 96, '28.4122244', '77.0432212', '28.4122167', '77.0432123', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:31:33 PM', '02:31:37 PM', '02:31:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 30, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(54, 98, '28.4122184', '77.0432299', '28.4122184', '77.0432301', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:36:43 PM', '02:36:52 PM', '02:37:01 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 30, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(55, 99, '-26.0612909406507', '28.0620088371401', '-26.0607730938598', '28.0623466428067', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:28:37 PM', '05:28:45 PM', '05:29:29 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.07 Km', '66.5893586049523', '1', '20', 1, '', 1, 1, '0000-00-00'),
(56, 100, '28.4122186', '77.0432316', '28.4122185', '77.0432302', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:21:57 AM', '06:22:08 AM', '06:22:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(57, 101, '28.4311842', '77.0338251', '28.4311962', '77.0338312', 'N Block, JMD Gardens, SH13, Sector 33, Gurugram, Haryana 122022, India', 'N Block, JMD Gardens, SH13, Sector 33, Gurugram, Haryana 122022, India', '08:33:57 AM', '08:34:03 AM', '08:34:11 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(58, 102, '28.4346785', '77.0355937', '28.4346785', '77.0355938', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '09:04:47 AM', '09:04:51 AM', '09:04:55 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(59, 103, '28.4346784', '77.0355946', '28.4346784', '77.0355946', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '09:06:20 AM', '09:06:23 AM', '09:06:27 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(60, 104, '28.4346783', '77.0355947', '28.4346783', '77.0355947', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '09:06:58 AM', '09:07:01 AM', '09:07:06 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(61, 105, '28.4346781', '77.0355944', '28.4346781', '77.0355945', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '09:11:31 AM', '09:11:35 AM', '09:11:39 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(62, 106, '28.4346782', '77.0355937', '28.4346782', '77.0355939', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '09:12:37 AM', '09:12:41 AM', '09:12:46 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(63, 107, '28.4122183814997', '77.0435400745599', '28.4121256884889', '77.0433273280577', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:40:49 AM', '10:41:00 AM', '10:41:10 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 0, 1, '0000-00-00'),
(64, 108, '28.4121893424433', '77.043476207231', '28.4121765772471', '77.0434201147196', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:43:23 PM', '01:43:29 PM', '01:43:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 1, '0000-00-00'),
(65, 110, '28.412137278409', '77.0433495955764', '28.4121195460842', '77.0433741774529', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:45:05 PM', '01:45:10 PM', '01:45:17 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(66, 111, '28.4121401410092', '77.0434120069191', '28.428317146293', '77.0667864382267', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '416, Orchid Island, Sector 51, Gurugram, Haryana 122022, India', '02:05:02 PM', '02:05:06 PM', '02:05:12 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 0, 1, '0000-00-00'),
(67, 113, '28.4121952540422', '77.0433780247982', '28.4121828573218', '77.0433755313984', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:14:52 PM', '02:15:02 PM', '02:15:09 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(68, 114, '28.4122076624973', '77.0434832671004', '28.4121604247335', '77.0434613933365', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:20:15 PM', '02:20:19 PM', '02:20:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(69, 115, '28.4120482', '77.0433247', '28.4122244', '77.0432212', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:04:28 AM', '11:04:32 AM', '11:04:42 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(70, 116, '28.4122189', '77.0432347', '28.4122167', '77.0432123', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:23:25 AM', '11:23:34 AM', '11:23:49 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(71, 117, '28.4120482', '77.0433247', '28.4120482', '77.0433247', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:35:32 AM', '11:35:37 AM', '11:35:42 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(72, 118, '28.4120482', '77.0433247', '28.4120482', '77.0433247', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:36:38 AM', '11:36:41 AM', '11:36:46 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(73, 119, '28.4120482', '77.0433247', '28.4120482', '77.0433247', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:15:39 PM', '12:15:49 PM', '12:16:03 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(74, 120, '28.4121520871856', '77.0432544414363', '28.4121520871856', '77.0432544414363', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:21:24 PM', '12:21:27 PM', '12:21:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(75, 121, '28.4121860201185', '77.0434616057394', '28.4121860201185', '77.0434616057394', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:43:06 PM', '12:43:09 PM', '12:43:14 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(76, 122, '28.4120279', '77.0433472', '28.4120279', '77.0433472', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '05:35:17 AM', '05:35:20 AM', '05:35:24 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 31, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(77, 123, '-26.060781759996', '28.0622793100239', '-26.074752508788237', '28.042486868798733', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4 Umgeni Rd, Riverclub, Sandton, 2191, South Africa', '05:58:29 AM', '05:58:44 AM', '05:59:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(78, 124, '-29.6214776', '30.4071338', '-29.6214776', '30.4071338', 'N3, Scottsville, Pietermaritzburg, 3201, South Africa', 'N3, Scottsville, Pietermaritzburg, 3201, South Africa', '06:09:42 AM', '06:11:24 AM', '06:11:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(79, 125, '-26.06078176', '28.06227931', '-26.1240632', '28.034747099999997', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '55 6th Rd, Hyde Park, Sandton, 2196, South Africa', '06:57:40 AM', '06:57:57 AM', '06:58:54 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(80, 126, '-33.921116', '18.5857356', '-33.9211148', '18.585734', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '07:02:18 AM', '07:02:24 AM', '07:03:07 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 34, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(81, 130, '-26.1231626645932', '28.034394852728', '-26.1230777514553', '28.034263732373', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '60 Hyde Cl, Hyde Park, Sandton, 2196, South Africa', '09:51:43 AM', '09:52:36 AM', '09:53:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(82, 130, '-26.1231626645932', '28.034394852728', '-26.1230777514553', '28.034263732373', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '60 Hyde Cl, Hyde Park, Sandton, 2196, South Africa', '09:51:58 AM', '09:52:36 AM', '09:53:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(83, 131, '-26.1233048986575', '28.0346008471404', '-26.1232981925668', '28.0345734156278', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '10:00:04 AM', '10:00:19 AM', '10:00:32 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(84, 132, '-29.7363368', '31.061679', '-29.7363492', '31.0616958', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '10:54:04 AM', '10:54:12 AM', '10:55:48 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 32, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '2', '20', 1, '', 1, 1, '0000-00-00'),
(85, 133, '-26.1233172055629', '28.0347755387896', '-26.1232990327639', '28.0345817842362', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '11:02:00 AM', '11:02:21 AM', '11:06:28 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '4', '20', 1, '', 1, 0, '0000-00-00'),
(86, 134, '-26.1233131276589', '28.0346698861517', '-26.1233121193508', '28.034647083726', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '11:09:44 AM', '11:10:12 AM', '11:10:34 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(87, 138, '-29.7415257', '31.0538719', '-29.7237886', '31.0642223', '2 ILALA Dr, Umhlanga, 4051, South Africa', '2 Jubilee Grove, Umhlanga Ridge, Umhlanga, 4319, South Africa', '11:23:37 AM', '11:24:17 AM', '11:29:13 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 32, '39.4', '0.79', '38.61', '39.40', '0.00', '2.94 Km', '2530.0', '5', '39.4', 1, '', 1, 1, '0000-00-00'),
(88, 139, '-26.123302479268', '28.0345845676057', '-26.123302878511', '28.0345858018987', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '11:24:05 AM', '11:24:50 AM', '11:25:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(89, 140, '', '', '', '', '', '', '11:32:32 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 17, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(90, 141, '-26.1233263744845', '28.0346667102002', '-26.1233263744858', '28.0346667101122', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '11:39:32 AM', '11:40:06 AM', '11:40:26 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(91, 142, '-26.1232380589541', '28.0345267870135', '-26.1232521466894', '28.0345494141633', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:12:35 PM', '02:12:46 PM', '02:13:21 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(92, 143, '-26.1233194282586', '28.0346641628034', '-26.1232369622425', '28.0345775180974', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:16:02 PM', '02:16:14 PM', '02:16:41 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(93, 144, '-26.1233095', '28.0344698', '-26.1233047', '28.0344927', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:38:14 PM', '02:38:33 PM', '02:38:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(94, 145, '-26.1233047', '28.0344927', '-26.1233047', '28.0344927', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:44:01 PM', '02:44:11 PM', '02:44:19 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(95, 146, '-26.1233047', '28.0344927', '-26.1233047', '28.0344927', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:59:16 PM', '02:59:20 PM', '02:59:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(96, 147, '-26.1233305840648', '28.0347756495413', '-26.1233306596099', '28.0347742909212', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:13:44 PM', '03:13:59 PM', '03:14:19 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(97, 148, '-29.7363275', '31.0616597', '-29.7363343', '31.0616635', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '03:30:04 PM', '03:30:36 PM', '03:31:01 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(98, 150, '-29.7362467', '31.0614539', '-29.7362467', '31.0614539', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '03:33:22 PM', '03:33:28 PM', '03:33:34 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(99, 151, '-26.1233349213483', '28.0347772039465', '-26.1231497302895', '28.0344800558202', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '04:23:59 PM', '04:24:05 PM', '04:24:45 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(100, 152, '-26.0747507354487', '28.046493837636', '-26.0610427614544', '28.0623574275785', '91 Coleraine Dr, Riverclub, Sandton, 2191, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:01:55 PM', '05:02:13 PM', '05:07:22 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 17, '36.4', '1.82', '34.58', '36.40', '0.00', '2.64 Km', '2188.39079323083', '5', '36.4', 1, '', 1, 1, '0000-00-00'),
(101, 153, '-26.0607817600181', '28.0622793099535', '-26.0607817600219', '28.0622793099439', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:16:04 PM', '07:16:54 PM', '07:17:22 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '20', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(102, 154, '-33.8972496', '18.6283756', '-33.897238', '18.6284204', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '08:17:07 PM', '08:17:22 PM', '08:18:26 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 34, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 0, 0, '0000-00-00'),
(103, 155, '-33.8972369', '18.6282365', '-33.8971997', '18.6278281', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '08:25:29 PM', '08:25:32 PM', '08:29:33 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 34, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '4', '20', 1, '', 1, 0, '0000-00-00'),
(104, 156, '', '', '', '', '', '', '08:39:18 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 17, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(105, 157, '-26.0607817600001', '28.0622793099996', '-26.0607817600001', '28.0622793099996', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '08:45:36 PM', '08:46:08 PM', '08:46:27 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(106, 160, '-33.8970549', '18.6286412', '-33.8970428', '18.6286036', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '08:51:48 PM', '08:51:54 PM', '08:52:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(107, 169, '28.4121211620934', '77.0433022930521', '28.4121211620934', '77.0433022930521', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:53:19 AM', '07:53:28 AM', '07:53:37 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(108, 170, '-33.9178543', '18.5879033', '-33.9123833', '18.5933149', '80 Connaught Rd, Parow Valley, Cape Town, 7500, South Africa', '69 Mark St, Parow Valley, Cape Town, 7500, South Africa', '08:26:58 AM', '08:26:59 AM', '08:29:04 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 34, '21.1', '0.42', '20.68', '21.10', '0.00', '1.11 Km', '1090.0', '2', '21.1', 1, '', 1, 1, '0000-00-00'),
(109, 171, '28.4121038191431', '77.0433093435551', '28.4121699283637', '77.0434485839579', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:29:49 AM', '09:29:57 AM', '09:30:14 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(110, 171, '28.4121038191431', '77.0433093435551', '28.4121699283637', '77.0434485839579', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:29:52 AM', '09:29:57 AM', '09:30:14 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(111, 172, '-29.6255526', '30.4037686', '-29.6255526', '30.4037686', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '09:30:02 AM', '09:30:18 AM', '09:30:35 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(112, 173, '-26.1232978', '28.0344844', '-26.1232964', '28.0344838', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '01:16:43 PM', '01:17:15 PM', '01:17:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(113, 176, '-29.6255526', '30.4037686', '-29.6255526', '30.4037686', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '01:41:01 PM', '01:41:15 PM', '01:42:06 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 33, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(114, 180, '28.4121446', '77.0432202', '28.4121465', '77.0432179', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:55:40 PM', '02:55:45 PM', '02:55:51 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 40, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(115, 181, '-29.6228018', '30.4052542', '-29.6228598', '30.4051867', '3 Blackburrow Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '3 Blackburrow Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '03:18:23 PM', '03:18:38 PM', '03:18:59 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(116, 184, '', '', '', '', '', '', '03:23:30 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 17, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(117, 185, '-26.1233058', '28.0344912', '-26.1233058', '28.0344912', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:25:03 PM', '03:25:14 PM', '03:25:30 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(118, 186, '-26.123302', '28.0344905', '-26.1233023', '28.0344907', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:27:33 PM', '03:27:44 PM', '03:27:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(119, 187, '-29.7363486', '31.0616415', '-29.8187617', '31.0120596', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '03:30:22 PM', '03:30:48 PM', '03:54:09 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 32, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '13240.0', '23', '0.00', 1, '', 1, 1, '0000-00-00'),
(120, 191, '-26.123286796645', '28.034691807448', '-26.1231496464704', '28.0346293375157', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:35:48 PM', '03:36:05 PM', '03:36:32 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(121, 191, '-26.123286796645', '28.034691807448', '-26.1231496464704', '28.0346293375157', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:35:48 PM', '03:36:05 PM', '03:36:32 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(122, 193, '-26.1232993', '28.0344961', '-26.123299', '28.0344954', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:48:15 PM', '03:48:26 PM', '03:48:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(123, 195, '-29.8662605', '30.9993966', '-29.8605896', '31.0252564', '264 Umbilo Rd, Bulwer, Berea, 4083, South Africa', '6 Dorothy Nyembe St, Durban Central, Durban, 4000, South Africa', '04:36:49 PM', '04:36:55 PM', '04:37:25 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 33, '41', '0.82', '40.18', '41.00', '0.00', '3.10 Km', '0.0', '1', '41', 1, '', 1, 1, '0000-00-00');
INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(124, 197, '-29.8615495', '31.0234421', '-29.8909463', '30.9794314', '106 Margaret Mncadi Ave, Durban Central, Durban, 4001, South Africa', '471-475 Bartle Rd, Umbilo, Berea, 4075, South Africa', '04:38:38 PM', '04:38:43 PM', '04:58:34 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 33, '97.9', '1.96', '95.94', '97.90', '0.00', '8.79 Km', '10100.0', '20', '97.9', 1, '', 1, 1, '0000-00-00'),
(125, 199, '-29.8908833', '30.9791072', '-29.8908833', '30.9791072', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '06:14:21 PM', '06:14:40 PM', '06:15:02 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '0.40', '19.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(126, 202, '-29.8627665', '30.9837461', '-29.8487863', '30.9992354', '5 Princess Anne Pl, Westridge, Berea, 4091, South Africa', '115 Musgrave Rd, Musgrave, Berea, 4001, South Africa', '06:38:19 PM', '06:38:27 PM', '06:46:15 PM', '0', '0.00', '5.60', '0.00', '0.00', '0.00', 32, '40.65', '0.81', '39.84', '35.05', '0.00', '3.15 Km', '2690.0', '8', '40.65', 1, '', 1, 1, '0000-00-00'),
(127, 206, '-29.8740373', '30.9824276', '-29.8584867', '31.010425', '71 Elgie Rd, Umbilo, Berea, 4075, South Africa', '100 Julius Nyerere St, Greyville, Berea, 4001, South Africa', '11:22:57 AM', '11:23:00 AM', '11:29:06 AM', '0', '0.00', '4.20', '0.00', '0.00', '0.00', 33, '45.83', '9.17', '36.66', '41.63', '0.00', '4.09 Km', '2930.0', '6', '45.83', 1, '', 1, 0, '0000-00-00'),
(128, 208, '-29.8568528', '31.0266602', '-29.8901461', '30.9794538', '157-161 Monty Naicker Rd, Durban Central, Durban, 4001, South Africa', '453 Bartle Rd, Umbilo, Berea, 4075, South Africa', '11:58:41 AM', '11:59:13 AM', '12:16:45 PM', '0', '0.00', '12.60', '0.00', '0.00', '0.00', 33, '86.29', '17.26', '69.03', '73.69', '0.00', '8.67 Km', '6480.0', '18', '86.29', 1, '', 1, 1, '0000-00-00'),
(129, 209, '-26.0611827', '28.0625748', '-26.0611818', '28.0625709', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '11:59:50 AM', '12:00:12 PM', '12:01:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 41, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Km', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(130, 210, '-26.0608229460437', '28.0625010933988', '-26.0608213953917', '28.0625031888746', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '12:18:42 PM', '12:18:58 PM', '12:20:45 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 17, '21.4', '4.28', '17.12', '20.00', '0.00', '0.00 Km', '0.0', '2', '21.4', 1, '', 1, 1, '0000-00-00'),
(131, 211, '-26.0606914263313', '28.0623485382316', '-26.0606914263313', '28.0623485382316', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '03:28:05 PM', '03:28:19 PM', '03:29:37 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 17, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 1, 1, '0000-00-00'),
(132, 213, '-26.0606898448723', '28.0623589355169', '-26.0606914263313', '28.0623485382316', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '03:34:13 PM', '03:34:40 PM', '03:35:49 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 17, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 0, 0, '0000-00-00'),
(133, 217, '-26.074444879802', '28.0445903074257', '-26.0608558852352', '28.0622725023086', '99 Ballyclare Dr, Riverclub, Sandton, 2191, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '01:27:50 PM', '01:27:57 PM', '01:36:09 PM', '0', '0.00', '5.60', '0.00', '0.00', '0.00', 17, '38.97', '7.79', '31.18', '33.37', '0.00', '2.91 Km', '2497.96224576677', '8', '38.97', 1, '', 1, 1, '0000-00-00'),
(134, 231, '28.4121196516542', '77.0432414652806', '28.4120647390603', '77.0432618966775', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:15:11 AM', '11:15:16 AM', '11:15:32 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '5', '0.25', '4.75', '5.00', '0.00', '1.73 Miles', '1950.65481705381', '0', '5', 1, '', 1, 1, '0000-00-00'),
(135, 234, '28.4121688', '77.0431904', '28.4120859', '77.0433249', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:26:44 PM', '03:26:54 PM', '03:27:08 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(136, 235, '28.4122352267062', '77.0432904934923', '28.4423137547207', '77.0948691666126', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Khatu Shyam Rd, Parsvnath Exotica, DLF Phase 5, Sector 53, Gurugram, Haryana 122003, India', '03:27:22 PM', '03:27:38 PM', '03:27:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(137, 236, '28.4345833', '77.0354157', '28.4345833', '77.0354157', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '05:09:16 PM', '05:09:20 PM', '05:09:28 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 40, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(138, 237, '28.4345833', '77.0354157', '28.4347713', '77.0350702', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '05:10:51 PM', '05:10:59 PM', '05:11:04 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 40, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(139, 239, '28.4345833', '77.0354157', '28.4345833', '77.0354157', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '05:15:24 PM', '05:15:28 PM', '05:15:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 40, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(140, 240, '', '', '', '', '', '', '06:35:10 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 40, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(141, 244, '28.4121438', '77.043231', '28.4121451', '77.043229', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:54:45 AM', '06:54:49 AM', '06:54:54 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 40, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(142, 245, '28.4121967179505', '77.0432421614916', '28.4121875126692', '77.0432426106139', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:05:14 AM', '09:05:19 AM', '09:05:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '5', '0.25', '4.75', '5.00', '0.00', '0.36 Miles', '255.20538511461', '0', '5', 1, '', 1, 1, '0000-00-00'),
(143, 246, '-29.5720856', '30.3566606', '-29.5720856', '30.3566606', '23 Mc Carthy Dr, Chase Valley Downs, Pietermaritzburg, 3201, South Africa', '23 Mc Carthy Dr, Chase Valley Downs, Pietermaritzburg, 3201, South Africa', '12:02:56 PM', '12:03:10 PM', '12:03:32 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 0, '0000-00-00'),
(144, 250, '-26.0607993973405', '28.0623361855185', '-26.0607995096724', '28.062336138659', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '09:23:26 AM', '09:24:13 AM', '09:24:38 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(145, 252, '-26.0607931695707', '28.0622814596473', '-26.0607931600755', '28.0622814798393', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '02:19:16 PM', '02:19:22 PM', '02:19:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(146, 253, '-26.0608077294855', '28.0623037798316', '-26.0607954138038', '28.0622849296375', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '02:23:13 PM', '02:23:18 PM', '02:23:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 1, '0000-00-00'),
(147, 254, '28.4120698', '77.0433194', '28.41212', '77.0432762', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '05:49:06 AM', '05:49:10 AM', '05:49:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 29, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(148, 255, '28.4121561124188', '77.0432995496243', '28.4143737115337', '77.0460873400211', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1002, Sispal Vihar Internal Rd, Block K, South City II, Sector 49, Gurugram, Haryana 122018, India', '06:51:30 AM', '06:52:10 AM', '06:53:51 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 46, '5', '0.25', '4.75', '5.00', '0.00', '0.26 Miles', '374.803710343373', '2', '5', 1, '', 1, 0, '0000-00-00'),
(149, 258, '28.4121873', '77.0432195', '24.43434', '74.43234', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Udaipur - Bari Sadri - Bansi Rd, Ranawaton Ka Khera, Rajasthan 312403, India', '07:38:45 AM', '07:38:52 AM', '11:50:36 AM', '0', '0.00', '3765.00', '0.00', '0.00', '0.00', 47, '3865', '193.25', '3671.75', '100.00', '0.00', '0.00 Miles', '', '252', '3865', 1, '', 1, 0, '0000-00-00'),
(150, 260, '28.4121776', '77.0432347', '28.4121781', '77.0432338', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:55:44 AM', '07:55:49 AM', '07:57:14 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 47, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(151, 262, '28.412114', '77.0434259', '28.4121943', '77.0432123', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:37:51 AM', '10:37:55 AM', '10:38:00 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 48, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(152, 263, '28.4120867', '77.0433176', '28.4120867', '77.0433176', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:39:32 AM', '10:39:34 AM', '10:39:36 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 48, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(153, 264, '28.4121788', '77.0432353', '', '', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '', '10:41:17 AM', '10:41:21 AM', '', '0', '0', '0', '0.00', '0.00', '0.00', 47, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(154, 265, '28.4121364', '77.0432638', '28.4121391', '77.0432617', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:43:36 AM', '10:43:44 AM', '10:43:52 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 47, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(155, 266, '28.4121503', '77.0432585', '28.4121447', '77.0432677', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:44:39 AM', '10:44:41 AM', '10:44:49 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 47, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 1, '0000-00-00'),
(156, 267, '28.4121446', '77.0433101', '28.4121563', '77.0432802', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:24:42 AM', '11:24:57 AM', '11:25:10 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 46, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(157, 268, '-26.0863226233293', '28.0391527992003', '-26.0610995069389', '28.0623357184493', '7 Coleraine Dr, Moodie Hill, Sandton, 2057, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '04:25:14 PM', '04:25:25 PM', '04:39:02 PM', '0', '0.00', '9.80', '0.00', '0.00', '0.00', 17, '51.71', '10.34', '41.37', '41.91', '0.00', '4.13 Km', '3605.60812153485', '14', '51.71', 1, '', 1, 1, '0000-00-00'),
(158, 271, '-29.8908833', '30.9791072', '-29.8908833', '30.9791072', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '06:02:32 PM', '06:02:46 PM', '06:03:02 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(159, 272, '-33.8970335', '18.6275803', '-33.8970718', '18.6272949', '2 Bloem St, Bosbell, Cape Town, 7530, South Africa', '1 Bloem St, Bosbell, Cape Town, 7530, South Africa', '06:56:13 PM', '06:57:21 PM', '06:57:47 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(160, 273, '-33.897213', '18.6285386', '-33.8971854', '18.6285046', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '07:08:49 PM', '07:09:19 PM', '07:10:23 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 34, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '0.00', 1, '', 1, 0, '0000-00-00'),
(161, 274, '-33.8974804', '18.628614', '-33.8971733', '18.6285028', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '07:20:12 PM', '07:20:53 PM', '07:24:31 PM', '0', '0.00', '2.80', '0.00', '0.00', '0.00', 34, '22.8', '4.56', '18.24', '20.00', '0.00', '0.00 Km', '0.0', '4', '22.8', 1, '', 1, 0, '0000-00-00'),
(162, 275, '-29.8908833', '30.9791072', '-29.8908833', '30.9791072', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '07:24:23 PM', '07:24:43 PM', '07:25:49 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 53, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 1, 0, '0000-00-00'),
(163, 279, '-29.8908811', '30.9791069', '-29.8908807', '30.9791069', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '07:28:49 PM', '07:29:02 PM', '07:29:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(164, 278, '-33.8969914', '18.62855', '-33.8969914', '18.62855', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '07:31:43 PM', '07:31:47 PM', '07:31:50 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 0, '0000-00-00'),
(165, 286, '-26.0608578968819', '28.0625953794084', '-26.0608715733231', '28.0626451224133', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '07:36:37 PM', '07:36:52 PM', '07:37:09 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(166, 287, '-33.8973114', '18.6285345', '-33.897361', '18.6285463', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '07:37:13 PM', '07:37:19 PM', '07:37:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(167, 288, '-29.8189104', '31.0116054', '-29.8189104', '31.0116054', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '07:37:32 PM', '07:37:50 PM', '07:38:21 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 32, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '0.00', 1, '', 1, 0, '0000-00-00'),
(168, 291, '-26.0609485', '28.0623972', '-26.0609485', '28.0623972', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:39:22 PM', '07:39:36 PM', '07:39:50 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 45, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Insufficent Balance', 1, 1, '0000-00-00'),
(169, 293, '-29.8189104', '31.0116054', '-29.8189104', '31.0116054', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '07:41:41 PM', '07:41:59 PM', '07:42:27 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(170, 298, '-29.8189808', '31.0117013', '-29.8189808', '31.0117013', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '07:50:35 PM', '07:50:42 PM', '07:51:10 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(171, 300, '-33.8970513', '18.6280374', '-33.8968161', '18.6279131', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '07:57:04 PM', '07:57:38 PM', '07:58:30 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 34, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 1, 0, '0000-00-00'),
(172, 301, '-26.0609485', '28.0623972', '-26.0609485', '28.0623972', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:57:30 PM', '07:58:01 PM', '07:58:08 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(173, 308, '-33.8970384', '18.6282032', '-33.8969826', '18.6282251', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '08:05:26 PM', '08:05:40 PM', '08:06:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(174, 311, '-26.0608496140384', '28.062320443198', '-26.0608496140384', '28.062320443198', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '08:11:00 PM', '08:11:05 PM', '08:11:25 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Insufficent Balance', 0, 1, '0000-00-00'),
(175, 319, '-26.0607646899995', '28.0623501900002', '-26.0607646899995', '28.0623501900001', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '08:49:59 PM', '08:50:14 PM', '08:50:34 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(176, 320, '-29.890883', '30.9791072', '-29.8908831', '30.9791072', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '08:54:06 PM', '08:54:22 PM', '08:54:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(177, 325, '-29.8908833', '30.9791072', '-29.8908833', '30.9791072', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '09:18:19 PM', '09:18:29 PM', '09:18:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 53, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(178, 329, '-33.8970336', '18.6284397', '-33.921108', '18.5857529', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '09:43:26 PM', '09:44:59 PM', '07:35:16 AM', '0', '0.00', '595.00', '0.00', '0.00', '0.00', 34, '661.9', '132.38', '529.52', '66.90', '0.00', '7.70 Km', '0.0', '850', '661.9', 1, '', 1, 1, '0000-00-00'),
(179, 330, '-26.0609485', '28.0623972', '-26.0579196', '28.0336818', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '9 St James Cres, Bryanston, Sandton, 2191, South Africa', '09:48:29 PM', '09:50:54 PM', '09:53:31 PM', '2', '2.00', '2.10', '0.00', '0.00', '0.00', 17, '42.65', '8.53', '34.12', '38.55', '0.00', '3.65 Km', '2910.0', '3', '42.65', 1, '', 1, 0, '0000-00-00'),
(180, 332, '-26.0609435', '28.0624318', '-26.0609435', '28.0624318', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '06:48:52 AM', '06:49:02 AM', '06:49:20 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(181, 333, '-26.0591466', '28.0743667', '-26.0591466', '28.0743667', '19-21 Hampton Ct Rd, Gallo Manor, Sandton, 2052, South Africa', '19-21 Hampton Ct Rd, Gallo Manor, Sandton, 2052, South Africa', '06:52:58 AM', '06:53:13 AM', '06:53:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(182, 334, '-26.060767327479', '28.0623492661601', '-26.0607650196713', '28.0623500745248', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '06:57:20 AM', '06:57:34 AM', '06:57:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(183, 336, '-26.0607646901468', '28.0623501899486', '-26.0607646900263', '28.0623501899908', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:00:39 AM', '07:00:58 AM', '07:01:12 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '20', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(184, 337, '-29.6207957', '30.3967564', '-29.6205721', '30.3967902', 'Science Building, Scottsville, Pietermaritzburg, 3209, South Africa', 'Science Building, Scottsville, Pietermaritzburg, 3209, South Africa', '09:08:05 AM', '09:08:28 AM', '09:08:59 AM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 33, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 1, 1, '0000-00-00'),
(185, 339, '-29.6254284', '30.403611', '-29.6254228', '30.4036335', '17 Shores Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '17 Shores Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '09:39:59 AM', '09:40:14 AM', '09:41:10 AM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 33, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 1, 1, '0000-00-00'),
(186, 340, '28.4122918', '77.0434354', '28.4122884', '77.0434361', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:38:30 AM', '11:38:38 AM', '11:38:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 21, '5', '0.00', '0.00', '5.00', '0', '0.00 Miles', '0.0', '0', '5', 1, 'Insufficent Balance', 1, 0, '0000-00-00'),
(187, 341, '28.4122937', '77.0434392', '28.412289', '77.0434339', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:40:54 AM', '11:40:58 AM', '11:41:07 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 21, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(188, 343, '28.4123092', '77.043421', '28.4123057', '77.0434221', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:05:07 PM', '12:05:15 PM', '12:05:41 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 21, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(189, 344, '', '', '', '', '', '', '12:51:43 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 21, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(190, 345, '28.4122711', '77.0434322', '28.4122672', '77.0434337', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:02:14 PM', '01:02:19 PM', '01:02:24 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 27, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'Insufficent Balance', 1, 0, '0000-00-00'),
(191, 347, '28.4122993', '77.0434435', '28.4123008', '77.0434419', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:18:15 PM', '01:18:19 PM', '01:18:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 27, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '1', '0.00', 1, '', 1, 0, '0000-00-00'),
(192, 350, '28.4120649866538', '77.0432179330991', '28.4122838041123', '77.043370124585', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:35:12 PM', '01:35:51 PM', '01:36:03 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(193, 350, '28.4120649866538', '77.0432179330991', '28.4122838041123', '77.043370124585', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:35:37 PM', '01:35:51 PM', '01:36:03 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(194, 359, '28.4123263', '77.0434085', '28.4123263', '77.0434085', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:56:39 PM', '01:56:43 PM', '01:56:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 27, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(195, 362, '28.412296', '77.0434444', '28.4122951', '77.0434457', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '02:06:51 PM', '02:06:54 PM', '02:07:00 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 60, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(196, 363, '-29.7363647', '31.0615071', '-29.7364048', '31.0614094', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '02:13:37 PM', '02:14:09 PM', '02:14:26 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(197, 364, '-33.9210999', '18.585749', '-33.9211007', '18.5857529', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '02:38:55 PM', '02:39:13 PM', '02:40:06 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 34, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 1, 0, '0000-00-00'),
(198, 365, '-33.921103', '18.5857638', '-33.9211076', '18.5857532', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '02:51:34 PM', '02:51:45 PM', '02:52:29 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 34, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '1', '20.7', 1, '', 0, 0, '0000-00-00'),
(199, 370, '-26.0614529298861', '28.0592888966464', '-26.0608006425667', '28.0622826841238', '4 De La Rey Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '04:38:47 PM', '04:39:40 PM', '04:46:10 PM', '0', '0.00', '4.90', '0.00', '0.00', '0.00', 11, '24.9', '1.25', '23.65', '20.00', '0.00', '0.60 Km', '581.314058639556', '7', '24.9', 1, '', 1, 0, '0000-00-00'),
(200, 372, '-29.8910479', '30.9795329', '-29.8907173', '30.9791142', '2 Meller Cres, Umbilo, Berea, 4075, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '09:04:31 PM', '09:04:39 PM', '09:04:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 53, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(201, 376, '28.4123461', '77.0433922', '28.4123338', '77.0433809', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:16:10 AM', '06:16:22 AM', '06:16:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 66, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(202, 378, '28.4122282', '77.0434484', '28.4123259', '77.0434147', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:20:27 AM', '06:20:31 AM', '06:20:42 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 66, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(203, 387, '28.4123461', '77.0433922', '28.4123461', '77.0433922', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:08:33 AM', '10:08:39 AM', '10:08:45 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 71, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(204, 388, '28.4121442041806', '77.0432461588042', '28.4121630387645', '77.0431692526462', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:57:57 AM', '11:58:18 AM', '11:58:34 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 67, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(205, 390, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '01:29:20 PM', '01:29:24 PM', '01:29:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 67, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(206, 391, '28.4122853', '77.0434596', '28.4123259', '77.0434147', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '02:49:52 PM', '02:51:03 PM', '02:52:33 PM', '1', '5.00', '00.00', '0.00', '0.00', '0.00', 72, '10', '0.50', '9.50', '5.00', '0.00', '0.00 Miles', '0.0', '2', '10', 1, '', 1, 0, '0000-00-00'),
(207, 392, '28.4123259', '77.0434147', '28.4123259', '77.0434147', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '02:54:54 PM', '02:59:03 PM', '02:59:08 PM', '4', '20.00', '00.00', '0.00', '0.00', '0.00', 72, '25', '1.25', '23.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '25', 1, '', 1, 0, '0000-00-00'),
(208, 395, '-29.8907173', '30.9791142', '-29.8907173', '30.9791142', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '03:28:15 PM', '03:28:27 PM', '03:28:47 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 0, '0000-00-00'),
(209, 396, '-26.1232784267871', '28.0346997829805', '-26.1232764758854', '28.0349500345939', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:53:05 PM', '03:53:17 PM', '03:55:02 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 11, '21.4', '1.07', '20.33', '20.00', '0.00', '0.00 Km', '0.0', '2', '21.4', 1, '', 1, 0, '0000-00-00'),
(210, 398, '-26.0608158499856', '28.0623322699902', '-26.07786758544384', '28.051768653094772', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '17 Acacia Rd, Duxberry, Sandton, 2191, South Africa', '05:11:32 PM', '05:11:43 PM', '05:12:02 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(211, 399, '-26.060921517225', '28.0624507181608', '-26.0609682044257', '28.0624193698429', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:27:15 PM', '05:27:32 PM', '05:27:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(212, 400, '-26.0609682044257', '28.0624193698429', '-26.060937359022', '28.0624561663978', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:29:23 PM', '05:29:34 PM', '05:31:57 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 11, '21.4', '1.07', '20.33', '20.00', '0.00', '0.00 Km', '0.0', '2', '21.4', 1, '', 1, 0, '0000-00-00'),
(213, 401, '-26.0608077469056', '28.0623643212517', '-26.0608162836026', '28.0623306131149', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '07:18:39 PM', '07:19:11 PM', '07:19:33 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(214, 402, '-26.0607499221419', '28.0625893806369', '-26.0606985172553', '28.0630217942091', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '29B Stiglingh Rd, Edenburg, Sandton, 2128, South Africa', '07:21:06 PM', '07:21:11 PM', '07:21:30 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '20', '1.00', '19.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(215, 404, '', '', '', '', '', '', '07:04:35 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 33, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(216, 394, '-26.1232840239221', '28.0348235972721', '-26.1232834888048', '28.0348267006225', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '08:38:59 AM', '08:39:18 AM', '08:39:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(217, 406, '28.4122853', '77.0434596', '28.4122853', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:33:05 AM', '11:33:22 AM', '11:33:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 75, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(218, 407, '-33.8969775', '18.6286213', '-33.8969727', '18.6285272', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '08:19:44 PM', '08:20:13 PM', '08:20:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 0, '0000-00-00'),
(219, 409, '-29.8661694', '31.010394', '-29.8909186', '30.9794202', '14 Canal Rd, Maydon Wharf, Durban, 4001, South Africa', '471-475 Bartle Rd, Umbilo, Berea, 4075, South Africa', '02:28:40 PM', '02:29:00 PM', '02:41:46 PM', '0', '0.00', '9.10', '0.00', '0.00', '0.00', 33, '92.94', '18.59', '74.35', '83.84', '0.00', '10.12 Km', '11530.0', '13', '92.94', 1, '', 1, 0, '0000-00-00'),
(220, 410, '-26.1233216431235', '28.03481767888', '-26.1232122592871', '28.0344589334242', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:14:08 PM', '03:14:15 PM', '03:21:16 PM', '0', '0.00', '4.90', '0.00', '0.00', '0.00', 73, '24.9', '4.98', '19.92', '20.00', '0.00', '0.05 Km', '53.6294704461263', '7', '24.9', 1, '', 1, 0, '0000-00-00'),
(221, 413, '-26.1231682542955', '28.0346940458082', '-26.1236883513873', '28.034756826263', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '55 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:24:52 PM', '03:24:59 PM', '03:27:15 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 73, '21.4', '4.28', '17.12', '20.00', '0.00', '0.05 Km', '50.4870806381256', '2', '21.4', 1, '', 1, 1, '0000-00-00'),
(222, 418, '-26.1234992137422', '28.0344471149408', '-26.0610445216541', '28.0623868480586', '55 6th Ave, Hyde Park, Sandton, 2196, South Africa', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '04:43:48 PM', '04:44:02 PM', '05:12:29 PM', '0', '0.00', '19.60', '0.00', '0.00', '0.00', 73, '100.43', '20.09', '80.34', '80.83', '0.00', '9.69 Km', '9258.80671637024', '28', '100.43', 1, '', 1, 1, '0000-00-00'),
(223, 419, '-29.8543912', '31.0002651', '-29.8910118', '30.9796891', '337 King Dinuzulu Rd (South ), Bulwer, Berea, 4083, South Africa', '3-11 Meller Cres, Umbilo, Berea, 4075, South Africa', '05:33:03 PM', '05:33:06 PM', '05:46:42 PM', '0', '0.00', '9.80', '0.00', '0.00', '0.00', 33, '58.43', '11.69', '46.74', '48.63', '0.00', '5.09 Km', '4750.0', '14', '58.43', 1, '', 0, 0, '0000-00-00'),
(224, 420, '-29.8909376', '30.9795434', '-29.8712708', '30.9766753', '3 Meller Cres, Umbilo, Berea, 4075, South Africa', '288 King George V Ave, University, Berea, 4041, South Africa', '07:59:53 AM', '08:00:05 AM', '08:05:44 AM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 33, '20.7', '4.14', '16.56', '20.00', '0.00', '0.00 Km', '0.0', '6', '20.7', 1, '', 1, 0, '0000-00-00'),
(225, 421, '-29.8741206', '30.9784902', '-29.8741206', '30.9784902', '11 Convent Cl, Umbilo, Berea, 4075, South Africa', '11 Convent Cl, Umbilo, Berea, 4075, South Africa', '08:46:38 AM', '08:47:03 AM', '08:47:29 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(226, 422, '-29.8829632', '30.9838231', '-29.8830127', '30.9837111', '95 Teignmouth Rd, Umbilo, Berea, 4075, South Africa', '95 Teignmouth Rd, Umbilo, Berea, 4075, South Africa', '01:00:01 PM', '01:00:27 PM', '01:00:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(227, 428, '28.4121839516129', '77.0432831627103', '28.4121839516129', '77.0432831627103', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:35:41 PM', '02:35:46 PM', '02:35:50 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 76, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(228, 428, '28.4121839516129', '77.0432831627103', '28.4121839516129', '77.0432831627103', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:35:41 PM', '02:35:46 PM', '02:35:50 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 76, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(229, 429, '-29.8907173', '30.9791142', '-29.8907173', '30.9791142', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '06:54:31 PM', '06:54:43 PM', '06:55:33 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(230, 430, '-33.8971412', '18.6286889', '-33.8971418', '18.628682', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '06:59:28 PM', '07:00:15 PM', '07:00:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(231, 431, '-26.0607856200952', '28.0623118166843', '-26.103708020874347', '28.050084225833416', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '3 Sandown Valley Cres, Sandown, Sandton, 2031, South Africa', '07:06:22 PM', '07:06:37 PM', '07:06:58 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(232, 432, '28.4344827', '77.0354838', '28.4344827', '77.0354838', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '07:26:04 PM', '07:26:37 PM', '07:26:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 72, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(233, 434, '-29.8188665', '31.0117894', '-29.8188665', '31.0117894', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '07:47:54 PM', '07:48:19 PM', '07:48:30 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(234, 436, '-26.0607690234288', '28.0623516035113', '-26.092776885573432', '28.007782436907288', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '10 June Ave, Bordeaux, Randburg, 2194, South Africa', '07:51:23 PM', '07:51:39 PM', '07:53:56 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '2', '20', 1, '', 1, 0, '0000-00-00'),
(235, 439, '-29.8189608', '31.0112096', '-29.8190706', '31.0116638', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '07:56:24 PM', '07:56:51 PM', '07:57:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(236, 440, '-33.8969898', '18.6287969', '-33.8969874', '18.6287683', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '08:28:59 PM', '08:29:29 PM', '08:30:02 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(237, 441, '-33.896981', '18.6288049', '-33.8969815', '18.6288062', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '08:30:41 PM', '08:30:43 PM', '08:30:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(238, 442, '-29.8907148', '30.9791065', '-29.8907148', '30.9791065', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '09:00:51 PM', '09:01:05 PM', '09:01:20 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(239, 443, '', '', '', '', '', '', '09:28:58 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 34, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(240, 444, '-26.0612236848344', '28.0623452738189', '-26.0541343663017', '28.0237854289881', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '372 Main Rd, Bryanston, Sandton, 2191, South Africa', '08:20:45 AM', '08:20:52 AM', '08:31:47 AM', '0', '0.00', '4.20', '0.00', '0.00', '0.00', 17, '57.52', '11.50', '46.02', '53.32', '0.00', '5.76 Km', '5014.08107736683', '11', '57.52', 1, '', 1, 1, '0000-00-00'),
(241, 446, '-33.8972105', '18.6282246', '-33.8971446', '18.6284641', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '01:09:52 PM', '01:15:57 PM', '01:42:14 PM', '6', '12.00', '14.70', '0.00', '0.00', '0.00', 34, '46.7', '9.34', '37.36', '20.00', '0.00', '0.00 Km', '0.0', '26', '46.7', 1, '', 0, 0, '0000-00-00'),
(242, 451, '-33.8972162', '18.6284845', '-33.8970589', '18.6287634', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '02:18:57 PM', '02:19:00 PM', '02:21:37 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '3', '20', 1, '', 0, 0, '0000-00-00'),
(243, 453, '', '', '', '', '', '', '02:50:19 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 34, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(244, 454, '-29.8907148', '30.9791065', '-29.8907173', '30.9791142', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '03:04:58 PM', '03:05:12 PM', '03:05:18 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 0, '0000-00-00'),
(245, 456, '-33.897527', '18.6289728', '-33.8969644', '18.6288677', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '03:17:57 PM', '03:19:50 PM', '03:21:05 PM', '1', '2.00', '00.00', '0.00', '0.00', '0.00', 34, '22', '4.40', '17.60', '20.00', '0.00', '0.06 Km', '110.0', '1', '22', 1, '', 1, 0, '0000-00-00'),
(246, 457, '-26.060769982504', '28.062351230104', '-26.060769982504', '28.062351230104', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '06:00:49 PM', '06:01:02 PM', '06:01:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(247, 459, '-29.61679', '30.39278', '28.4126053892041', '77.043030783534', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '07:28:35 AM', '07:28:46 AM', '08:09:40 AM', '0', '0.00', '180.00', '0.00', '0.00', '0.00', 77, '185', '9.25', '175.75', '5.00', '0.00', '0.01 Miles', '12', '41', '185', 1, '', 0, 0, '0000-00-00'),
(248, 461, '28.4123229', '77.0434015', '28.4123229', '77.043401', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:59:57 AM', '10:00:02 AM', '10:00:06 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 80, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(249, 462, '28.4123033', '77.0434141', '28.412305', '77.0434132', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:04:42 AM', '10:04:45 AM', '10:04:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 80, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(250, 463, '28.4122928', '77.0434254', '28.4122725', '77.0434298', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:06:42 AM', '10:15:46 AM', '10:18:16 AM', '9', '45.00', '00.00', '0.00', '0.00', '0.00', 80, '50', '2.50', '47.50', '5.00', '0.00', '0.00 Miles', '0.0', '3', '50', 1, '', 0, 0, '0000-00-00'),
(251, 466, '28.4120790774551', '77.0432799732793', '28.4120847471338', '77.0432396730185', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:46:37 AM', '11:46:52 AM', '11:47:00 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00');
INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(252, 467, '-26.1233099084591', '28.034543003913', '-26.1232060985883', '28.0344895273708', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '01:33:55 PM', '01:34:15 PM', '02:00:15 PM', '0', '0.00', '14.70', '0.00', '0.00', '75', 17, '0.00', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '26', '0.00', 1, '', 1, 0, '0000-00-00'),
(253, 469, '28.4120730171245', '77.0432773791971', '28.4139801578161', '77.076452113688', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Golf Course Ext Rd, Sushant Lok III Extension, Sector 62, Gurugram, Haryana 122005, India', '12:15:42 PM', '12:15:47 PM', '12:15:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(254, 470, '28.4120730171245', '77.0432773791971', '28.4139801578161', '77.076452113688', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Golf Course Ext Rd, Sushant Lok III Extension, Sector 62, Gurugram, Haryana 122005, India', '12:18:18 PM', '12:18:32 PM', '12:18:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(255, 472, '28.4120860763407', '77.0432564985224', '28.4270425179841', '77.0693251490593', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '601, Orchid Island, Sector 51, Gurugram, Haryana 122003, India', '12:46:45 PM', '12:47:05 PM', '12:47:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '1', '5', 1, '', 1, 1, '0000-00-00'),
(256, 473, '28.4120578663038', '77.0432562461237', '28.4121848632382', '77.0432432940509', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:14:21 PM', '01:14:42 PM', '01:14:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(257, 474, '28.4121214734837', '77.0432678237557', '28.4120773437322', '77.0432948444655', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:23:23 PM', '01:24:26 PM', '01:24:34 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(258, 475, '28.4121056227278', '77.0432185382368', '28.4207107225325', '77.0516896247864', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '75, Vikas Marg, Malibu Town, Sector 47, Gurugram, Haryana 122018, India', '01:41:15 PM', '01:41:23 PM', '01:41:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(259, 476, '28.4120777224884', '77.0432236125068', '28.4120901313176', '77.0432218864057', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:55:11 PM', '01:55:16 PM', '01:55:33 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(260, 477, '-29.8190646', '31.0117739', '-29.8190646', '31.0117739', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '02:44:36 PM', '02:44:51 PM', '02:45:12 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(261, 479, '-29.8190174', '31.0116983', '-29.8190174', '31.0116983', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '03:15:30 PM', '03:15:45 PM', '03:16:00 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(262, 480, '', '', '', '', '', '', '03:38:19 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 32, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(263, 481, '', '', '', '', '', '', '03:38:53 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 32, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(264, 482, '-29.8642417', '30.9987112', '-29.8910132', '30.979409', '125 Esther Roberts Rd, Bulwer, Berea, 4083, South Africa', '461 Bartle Rd, Umbilo, Berea, 4075, South Africa', '03:41:38 PM', '03:41:41 PM', '03:49:03 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 33, '44.15', '8.83', '35.32', '42.75', '0.00', '4.25 Km', '3520.0', '7', '44.15', 1, '', 1, 0, '0000-00-00'),
(265, 484, '28.4346468', '77.0350361', '28.4346468', '77.0350361', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '04:21:45 PM', '04:21:50 PM', '04:21:53 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 80, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(266, 485, '28.434647', '77.0350356', '28.434647', '77.0350357', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '04:27:35 PM', '04:27:38 PM', '04:27:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 80, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(267, 487, '', '', '', '', '', '', '05:48:12 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 32, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(268, 488, '-26.0608541795085', '28.0623545587519', '-26.0607870040349', '28.0623118879182', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '05:51:06 PM', '05:51:21 PM', '05:51:40 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(269, 489, '-29.8188588', '31.0116753', '-29.8188588', '31.0116753', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '05:54:09 PM', '05:54:16 PM', '05:54:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(270, 490, '-29.818918', '31.0117913', '-29.818918', '31.0117913', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '05:58:38 PM', '05:58:45 PM', '05:58:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(271, 491, '-26.0607825632524', '28.0623110547337', '-26.2402112', '27.9556537', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '17198 Masheshane St, Diepkloof Zone 4, Diepkloof, 1862, South Africa', '06:01:20 PM', '06:01:26 PM', '06:01:43 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(272, 494, '-33.9216523', '18.5843497', '-33.9216523', '18.5843497', 'Francie Van Zijl Dr, Beaconvale, Cape Town, 7500, South Africa', 'Francie Van Zijl Dr, Beaconvale, Cape Town, 7500, South Africa', '06:32:05 AM', '06:32:26 AM', '06:32:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 0, 0, '0000-00-00'),
(273, 496, '28.412083536413', '77.0432539936873', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Block F, South City I, Sector 41, Gurugram, Haryana 122003, India', '07:19:36 AM', '07:19:42 AM', '07:19:54 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 81, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(274, 497, '-26.1233059689646', '28.0347396433615', '-26.1232788115983', '28.034688346114', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '07:28:21 AM', '07:28:43 AM', '07:31:51 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '3', '20', 1, '', 1, 1, '0000-00-00'),
(275, 498, '-26.1233340732925', '28.0348470474733', '-26.1233362898222', '28.0347931135472', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '07:46:13 AM', '07:46:34 AM', '07:48:13 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '2', '20', 1, '', 1, 1, '0000-00-00'),
(276, 499, '', '', '', '', '', '', '12:13:54 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 33, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(277, 500, '-34.4118121', '19.1985729', '', '', 'R43, Onrus River, Onrus, 7200, South Africa', '', '02:37:07 PM', '02:37:45 PM', '', '0', '0', '0', '0.00', '0.00', '0.00', 34, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(278, 501, '-26.1231776839365', '28.0346820596867', '-26.1231663683673', '28.0347072892152', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '03:00:31 PM', '03:01:01 PM', '03:01:18 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(279, 503, '-29.8578924', '31.0245845', '-29.8908022', '30.9792409', 'Dorothy Nyembe St, Durban Central, Durban, 4001, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '03:35:33 PM', '03:35:37 PM', '03:56:21 PM', '0', '0.00', '11.20', '0.00', '0.00', '0.00', 33, '78.66', '15.73', '62.93', '67.46', '0.00', '7.78 Km', '6470.0', '21', '78.66', 1, '', 1, 0, '0000-00-00'),
(280, 509, '-33.8972268', '18.628543', '-33.8972', '18.6285283', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '04:45:52 PM', '04:46:34 PM', '04:46:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(281, 510, '-26.0607618750595', '28.062350014968', '-26.0613748898066', '28.0624418923761', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '5-7 5th Ave, Edenburg, Sandton, 2128, South Africa', '05:48:03 PM', '05:48:17 PM', '05:48:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(282, 511, '-29.8700737', '30.9806638', '-29.8700634', '30.9805854', '303 King George V Ave, University, Berea, 4041, South Africa', '303 King George V Ave, University, Berea, 4041, South Africa', '05:57:32 PM', '05:57:36 PM', '05:57:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(283, 512, '-29.8908458', '30.9793082', '-29.8908197', '30.9793306', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '08:22:00 PM', '08:22:03 PM', '08:22:34 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(284, 513, '-26.0610907478501', '28.0624798871838', '-26.1233260177239', '28.0347917648336', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '06:15:19 AM', '06:15:27 AM', '07:05:29 AM', '0', '0.00', '31.50', '0.00', '0.00', '0.00', 17, '112.05', '22.41', '89.64', '80.55', '0.00', '9.65 Km', '7438.7485968313', '50', '112.05', 1, '', 1, 1, '0000-00-00'),
(285, 514, '-26.1233270272378', '28.0348352508386', '-26.1233286374235', '28.0347701540175', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '11:26:46 AM', '11:26:51 AM', '11:27:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(286, 515, '-29.6228664', '30.4051424', '-29.8909507', '30.9795322', '3 Blackburrow Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '3 Meller Cres, Umbilo, Berea, 4075, South Africa', '02:20:02 PM', '02:21:21 PM', '04:05:05 PM', '1', '2.00', '69.30', '0.00', '0.00', '0.00', 33, '841.07', '168.21', '672.86', '769.77', '0.00', '108.11 Km', '102360.0', '104', '841.07', 1, '', 1, 0, '0000-00-00'),
(287, 516, '-26.130635608012', '28.0315130297356', '-26.1306125996878', '28.0314847827219', '1 Bompas Rd, Dunkeld West, Randburg, 2196, South Africa', '1 Bompas Rd, Dunkeld West, Randburg, 2196, South Africa', '03:13:14 PM', '03:13:29 PM', '03:13:48 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(288, 520, '-26.092368657725', '28.0365497991704', '-26.060778348743', '28.0623469808772', '55 12th Ave, Parkmore, Sandton, 2196, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '03:35:40 PM', '03:48:38 PM', '04:01:14 PM', '12', '24.00', '5.60', '0.00', '0.00', '0.00', 17, '77.39', '15.48', '61.91', '47.79', '0.00', '4.97 Km', '304.496999533881', '13', '77.39', 1, '', 1, 1, '0000-00-00'),
(289, 521, '-26.0607955882728', '28.0622860138014', '-26.0608887413356', '28.0626500169813', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '07:19:07 PM', '07:19:29 PM', '07:27:34 PM', '0', '0.00', '2.10', '0.00', '0.00', '0.00', 17, '22.1', '4.42', '17.68', '20.00', '0.00', '0.00 Km', '0.0', '8', '22.1', 1, '', 1, 1, '0000-00-00'),
(290, 523, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:38:46 AM', '09:38:49 AM', '09:38:55 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(291, 524, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:57:22 AM', '09:57:25 AM', '09:57:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(292, 525, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:03:11 AM', '10:03:20 AM', '10:03:27 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(293, 526, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:14:32 AM', '10:14:35 AM', '10:14:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(294, 528, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:28:56 AM', '10:28:59 AM', '10:29:04 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(295, 529, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:47:25 AM', '10:47:28 AM', '10:47:32 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(296, 530, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:48:58 AM', '10:49:00 AM', '10:49:02 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(297, 531, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '11:13:46 AM', '11:13:50 AM', '11:13:57 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(298, 532, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '11:25:21 AM', '11:25:27 AM', '11:25:34 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(299, 533, '-29.8907062', '30.979098', '-29.8907065', '30.9790993', '470 Bartle Rd, Umbilo, Berea, 4075, South Africa', '470 Bartle Rd, Umbilo, Berea, 4075, South Africa', '03:51:15 PM', '03:51:21 PM', '03:51:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(300, 534, '-26.2444437481707', '27.9082298185936', '-26.060841624823', '28.0625248087697', '11870 Senokoanyana St, Orlando West, Soweto, 1804, South Africa', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '03:02:39 PM', '03:02:47 PM', '03:40:48 PM', '0', '0.00', '23.10', '0.00', '0.00', '0.00', 17, '43.1', '8.62', '34.48', '20.00', '0.00', '0.00 Km', '0.0', '38', '43.1', 1, '', 1, 1, '0000-00-00'),
(301, 535, '-26.0607592555981', '28.0627376840141', '-26.0607592555981', '28.0627376840141', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '03:43:41 PM', '03:43:58 PM', '03:44:20 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 17, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(302, 536, '-29.6255857', '30.4038811', '-29.6255855', '30.4038979', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '06:29:35 AM', '06:30:07 AM', '06:31:10 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(303, 538, '28.4122949', '77.0434332', '28.4123073', '77.0434291', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:32:11 AM', '06:32:14 AM', '06:32:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(304, 539, '28.4123194', '77.043436', '28.4122642', '77.0434236', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '07:29:44 AM', '07:30:20 AM', '07:34:36 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '4', '5', 1, '', 1, 0, '0000-00-00'),
(305, 540, '28.4123196', '77.043439', '28.4123186', '77.0434398', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '07:35:20 AM', '07:35:29 AM', '07:35:33 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(306, 545, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '07:58:02 AM', '07:58:06 AM', '07:58:19 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 1, '0000-00-00'),
(307, 546, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '07:59:31 AM', '07:59:34 AM', '07:59:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 0, '0000-00-00'),
(308, 548, '', '', '', '', '', '', '08:49:30 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 84, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(309, 553, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '08:55:10 AM', '08:55:15 AM', '08:55:36 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 1, '0000-00-00'),
(310, 554, '28.4123163', '77.0434459', '28.4123191', '77.0434458', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '08:55:18 AM', '08:55:21 AM', '08:55:25 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(311, 555, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '09:03:27 AM', '09:03:39 AM', '09:03:46 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(312, 556, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '09:06:32 AM', '09:06:37 AM', '09:06:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 0, '0000-00-00'),
(313, 559, '-26.0146114981518', '28.1004849282123', '-26.0146248633348', '28.1005743809779', 'Jukskei View Dr, Waterval 5-Ir, Midrand, 2090, South Africa', 'Jukskei View Dr, Waterval 5-Ir, Midrand, 2090, South Africa', '09:15:45 AM', '09:16:03 AM', '09:16:20 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '20', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(314, 559, '-26.0146114981518', '28.1004849282123', '-26.0146248633348', '28.1005743809779', 'Jukskei View Dr, Waterval 5-Ir, Midrand, 2090, South Africa', 'Jukskei View Dr, Waterval 5-Ir, Midrand, 2090, South Africa', '09:15:46 AM', '09:16:03 AM', '09:16:20 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '20', '0.00 Km', '0.0', '0', '20', 1, '', 1, 1, '0000-00-00'),
(315, 560, '28.4123275', '77.0434377', '28.4123275', '77.0434377', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:38:06 AM', '09:38:10 AM', '09:38:14 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0', '0.00 Miles', '0.0', '0', '100', 1, 'Insufficent Balance', 1, 0, '0000-00-00'),
(316, 561, '28.4123212', '77.0434338', '28.4123232', '77.0434184', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:55:34 AM', '09:55:38 AM', '09:55:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(317, 562, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '10:04:10 AM', '10:04:13 AM', '10:04:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.25', '4.75', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(318, 563, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '10:07:07 AM', '10:07:11 AM', '10:07:15 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 1, '0000-00-00'),
(319, 565, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '10:14:41 AM', '10:14:53 AM', '10:15:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 1, '0000-00-00'),
(320, 566, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '10:18:49 AM', '10:19:01 AM', '10:19:15 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 1, '0000-00-00'),
(321, 567, '28.4123256', '77.0434353', '28.4123264', '77.0434369', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:19:53 AM', '10:19:57 AM', '10:20:02 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(322, 568, '28.4123233', '77.0434415', '28.4123233', '77.0434415', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:28:08 AM', '10:28:10 AM', '10:28:13 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.25', '4.75', '5.00', '5', '0.00 Miles', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(323, 569, '28.4123209', '77.043436', '28.41233', '77.0434437', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:29:34 AM', '10:29:36 AM', '10:29:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(324, 570, '28.4123295', '77.0434372', '28.4123298', '77.0434381', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:32:08 AM', '10:32:12 AM', '10:32:21 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0', '0.00 Miles', '0.0', '0', '5', 1, 'Insufficent Balance', 1, 0, '0000-00-00'),
(325, 571, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '11:08:25 AM', '11:08:29 AM', '11:08:33 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0', '0.00 Miles', '0.0', '0', '5', 1, 'Insufficent Balance', 0, 1, '0000-00-00'),
(326, 572, '-34.0619348', '18.4332794', '-34.0652096', '18.453967', '10978 Tokai Rd, Steenberg Estate, Cape Town, 7945, South Africa', '31A Tokai Rd, Kirstenhof, Cape Town, 7945, South Africa', '02:15:54 PM', '02:16:38 PM', '02:19:44 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '26.72', '0.00', '0.00', '26.72', '0.00', '1.96 Km', '1880.0', '3', '26.72', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(327, 573, '-29.721974', '31.071009', '-29.7219315', '31.0709508', '19 Park Ln, Umhlanga, South Africa', '19 Park Ln, Umhlanga, South Africa', '02:17:09 PM', '02:17:21 PM', '02:17:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 32, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(328, 574, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '02:21:04 PM', '02:21:08 PM', '02:21:13 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 0, 1, '0000-00-00'),
(329, 575, '-26.1233195653577', '28.0346764890053', '-26.1233209067934', '28.0347853273754', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:21:20 PM', '02:21:33 PM', '02:21:47 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '20', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(330, 576, '-26.1233314499986', '28.0346699975889', '-26.1233170749863', '28.0347787868493', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '0C 6th Rd, Hyde Park, Sandton, 2196, South Africa', '02:23:02 PM', '02:23:19 PM', '02:23:41 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 0, 'You Have Select Credit Card', 0, 0, '0000-00-00'),
(331, 578, '-33.8970405', '18.6286781', '-33.8970881', '18.6286913', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '06:56:06 PM', '07:02:49 PM', '07:02:58 PM', '6', '12.00', '00.00', '0.00', '0.00', '0.00', 34, '32', '6.40', '25.60', '20.00', '0.00', '0.00 Km', '0.0', '0', '32', 1, '', 1, 0, '0000-00-00'),
(332, 580, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '08:48:31 AM', '08:48:34 AM', '08:48:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'Insufficent Balance', 1, 1, '0000-00-00'),
(333, 581, '-29.61679', '30.39278', '-29.61685', '30.39286', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '79 Leinster Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '09:00:00 AM', '09:00:04 AM', '09:00:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 84, '5', '0.00', '0.00', '5.00', '0', '0.00 Miles', '0.0', '0', '5', 1, 'Insufficent Balance', 1, 1, '0000-00-00'),
(334, 582, '-29.6255701', '30.4036614', '-29.6255661', '30.4036659', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '09:46:12 AM', '09:46:40 AM', '09:47:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(335, 585, '28.434563', '77.0351492', '28.4345927', '77.0351101', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '03:48:22 PM', '03:48:25 PM', '03:48:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 83, '5', '0.00', '0.00', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(336, 589, '-33.8703194', '18.5789051', '-33.8970712', '18.6288105', '3 Geelhout Cres, Plattekloof 3, Cape Town, 7500, South Africa', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '04:54:54 PM', '04:58:12 PM', '05:10:57 PM', '3', '6.00', '5.60', '0.00', '0.00', '0.00', 34, '80.32', '16.06', '64.26', '68.72', '0.00', '7.96 Km', '6300.0', '13', '80.32', 1, '', 1, 0, '0000-00-00'),
(337, 590, '-29.835783', '30.855195', '-29.8302936', '30.8386021', '32 Waterloo Cres, Pinelands, Pinetown, 3610, South Africa', '73 Pine St, Ashley, Pinetown, 3610, South Africa', '04:31:20 AM', '04:31:50 AM', '04:32:46 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(338, 592, '-29.6257094', '30.4032403', '-29.6257151', '30.4032311', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '10:05:48 AM', '10:05:57 AM', '10:06:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 0, '0000-00-00'),
(339, 594, '-29.8907128', '30.9791133', '-29.8907076', '30.9791072', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '470 Bartle Rd, Umbilo, Berea, 4075, South Africa', '07:30:34 PM', '07:30:40 PM', '07:30:48 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'You Have Select Credit Card', 0, 0, '0000-00-00'),
(340, 595, '-18.9789399644537', '32.6806429960138', '-18.97898702884', '32.6805864181674', 'Carrington Rd, Mutare, Zimbabwe', 'Carrington Rd, Mutare, Zimbabwe', '03:07:06 AM', '03:07:32 AM', '03:08:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(341, 596, '-29.6258105', '30.4030006', '-29.6258103', '30.402998', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '10:13:30 AM', '10:13:39 AM', '10:13:58 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 86, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(342, 597, '-29.8148243', '30.7697915', '-29.8505727', '30.9312413', 'Giba - Green Rte, Pinetown Rural, Clifton Canyon, 3610, South Africa', '3 Harry Gwala Rd, Dawncliffe, Westville, 3629, South Africa', '11:29:04 AM', '11:30:08 AM', '11:40:31 AM', '0', '0.00', '3.50', '0.00', '0.00', '0.00', 33, '23.5', '4.70', '18.80', '20.00', '0.00', '0.00 Km', '0.0', '10', '23.5', 1, '', 1, 0, '0000-00-00'),
(343, 600, '-33.8970381', '18.6284201', '-33.8970228', '18.6283632', '0A Durban Rd, Oakdale, Cape Town, 7530, South Africa', '0A Durban Rd, Oakdale, Cape Town, 7530, South Africa', '08:02:21 PM', '08:02:25 PM', '08:02:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(344, 601, '-33.8971429', '18.627855', '-33.8971339', '18.627836', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '16 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '08:03:05 PM', '08:03:07 PM', '08:03:10 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '0.00', '0.00', '20.00', '0', '0.00 Km', '0.0', '0', '20', 1, 'Insufficent Balance', 1, 0, '0000-00-00'),
(345, 602, '-33.8972741', '18.6285105', '-33.888515', '18.6307372', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '0D Carl Cronje Dr, Boston, Cape Town, 7530, South Africa', '08:37:25 PM', '08:37:28 PM', '08:44:21 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 34, '21.4', '4.28', '17.12', '20.00', '0.00', '0.00 Km', '0.0', '7', '21.4', 1, '', 1, 0, '0000-00-00'),
(346, 603, '-33.9210469', '18.5854109', '-33.9210685', '18.5854589', '35-37 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '35-37 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '06:40:47 AM', '06:41:12 AM', '06:41:37 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00'),
(347, 604, '-25.7572798244895', '28.3396279346462', '-25.7572046370759', '28.3395673928748', '901 Libertas Ave, Equestria, Pretoria, 0184, South Africa', '901 Libertas Ave, Equestria, Pretoria, 0184, South Africa', '06:54:23 AM', '06:55:06 AM', '06:56:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 73, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 1, 1, '0000-00-00'),
(348, 605, '', '', '', '', '', '', '10:23:28 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 33, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(349, 607, '-33.9211206', '18.5857276', '-33.9211202', '18.5857273', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '01:57:07 PM', '01:57:10 PM', '01:57:13 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(350, 608, '-33.9211219', '18.585729', '-33.9211219', '18.585729', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '02:05:41 PM', '02:05:42 PM', '02:05:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'You Have Select Credit Card', 1, 0, '0000-00-00'),
(351, 609, '-33.9211196', '18.5857264', '-33.9211206', '18.5857262', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '02:07:13 PM', '02:07:16 PM', '02:07:18 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '0.00', '0.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, 'Insufficent Balance', 1, 0, '0000-00-00'),
(352, 610, '-33.9211129', '18.5857184', '-33.9211183', '18.5857264', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '02:33:27 PM', '02:33:30 PM', '02:34:03 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 34, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '1', '20', 1, '', 0, 0, '0000-00-00'),
(353, 612, '-29.6011323', '30.3811999', '-29.6011323', '30.3811999', 'Henrietta St, Pietermaritzburg, 3201, South Africa', 'Henrietta St, Pietermaritzburg, 3201, South Africa', '10:19:31 AM', '10:19:49 AM', '10:20:10 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 33, '20', '4.00', '16.00', '20.00', '0.00', '0.00 Km', '0.0', '0', '20', 1, '', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `car_image` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'Your documents are under process.You will be notified soon.',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `car_image`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 5, 'driver', 'driver@f.com', '1234123456', '', 'qwerty', 'r7dQcPwBKBNdmO6u', '25540.75', '0', '3671.75', '012F77DFF441D9BBC51FBDC6B79C4D251BE6B0B62A8526B30C8C3CBC8D1DB6AD', 1, '', 3, 20, '0000', '', 56, 'Wednesday, Sep 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121080636601', '77.0432739913134', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '11:50', 'Wednesday, Sep 20, 2017', 8, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 3, 1, 0),
(2, 0, 5, 'driver', 'driver@gmail.com', '1592648731', '', 'qwerty', 'ckO6bvvgAHAxVnRA', '190', '0', '', 'A6C16FD1277A9A9A8357C0C656DCDB169FFB19F9545C3AECF1BDA2B274AAE517', 1, '2.25', 3, 2, '0000', '', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121080229713', '77.043243839804', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:51', 'Friday, Sep 22, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(3, 0, 4, 'create', 'create@h.com', '1234560000', '', 'qwerty', 'AlIImDweejrHWhh5', '0', '0', '', 'AEBA09150B9827CFE14D9DAD0806BA594405E06E0989B53E2644365CCE73DC30', 1, '', 2, 3, '0000', '', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121002581395', '77.043276655924', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '10:12', 'Friday, Sep 22, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(4, 0, 6, 'your', 'your@q.com', '1234567000', '', 'qwerty', '2HSjL1kixHuu0PEv', '167.32', '0', '', '3195F84E7DDCBD9EB58015EFA18E0EFBA7DD0EB6AF78974DB6BE205E814D50FA', 1, '', 4, 11, '0000', '', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122144900705', '77.0431876107312', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '10:32', 'Friday, Sep 22, 2017', 1, 0, 1, 2, 0, 2, 2, 0, '', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(5, 0, 4, 'demo', 'sonu26tanwar@gmail.com', '9865321478', '', '7vcpah', 'HXzlLaRQwfnAu4UJ', '0', '0', '', '3195F84E7DDCBD9EB58015EFA18E0EFBA7DD0EB6AF78974DB6BE205E814D50FA', 1, '', 2, 3, 'qwee', '', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120657082749', '77.0432844938777', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '10:34', 'Friday, Sep 22, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(6, 0, 10, 'David', 'davids@tagyourride.co.za', '0835156147', 'uploads/driver/1506208512driver_6.jpg', 'Tatenda', 'PVcurDV5GmYaxsdt', '666', '0', '', '99E6CA60CA489D6FDDF54675D0DDABC9AF5D8DD3A13C7312FC1513BF19EA790F', 1, '2.5', 4, 11, 'CW60ZCGP', '', 121, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.1383457854634', '28.0182065908127', '82 13th Street , Parkhurst, Randburg, 2193', '14:51', 'Friday, Sep 29, 2017', 13, 1, 2, 2, 0, 2, 2, 0, '', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(7, 0, 10, 'Faith ', 'faithc@tagyourride.co.za', '0624503508', '', 'Kundai', 'GEUhhGGvaYx2fN9j', '108', '0', '', '6B9CD6174318841CC677143E0B6B0C75B3303D71BF0D145E5E2067678292B647', 1, '4.75', 4, 11, 'DF08DEGP', '', 121, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.0617033129779', '28.0626564288919', '0B 5th Avenue , Edenburg, Sandton, 2128', '09:01', 'Sunday, Oct 1, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(8, 0, 6, 'driver', 'driver@g.com', '1234561237', 'uploads/driver/1506678461driver_8.jpg', 'qwerty', 'UlUVVWcYmNtitJfi', '334.64', '0', '', '6DBEE4CB98D7A107E5BD7D898384685E03187B10590A9036AC8B002F18A864F1', 1, '1.7', 4, 29, '0000', '', 56, 'Friday, Sep 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121603845689', '77.0432583944195', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:56', 'Friday, Sep 29, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(9, 0, 5, 'driv', 'driv@g.com', '1234512345', '', 'qwerty', '86R4G18RxXlgNHRu', '47.5', '0', '', '60917797A567D5393B0D68CEA0A4BCE977B0A4D6B9EEDF3BDBCF53384A9E361B', 1, '2.38461538462', 28, 19, '0000', '', 56, 'Friday, Sep 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.414248947716', '77.0461190428817', '1002, Sispal Vihar Internal Road, Block K, South City II, Sector 49 , Gurugram, Haryana 122018', '12:45', 'Tuesday, Oct 10, 2017', 10, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-09-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 6, 1, 0),
(10, 0, 5, 'driv', 'driv@h.com', '1234578960', '', 'qwerty', 'tizq2ZDApz66WurN', '0', '0', '', '4A5B7BEF87BE630CEDF7A8A7E94D8C208E18C7655B7FF16BCEA22C3FBAD3E0BE', 1, '', 28, 19, '0000', '', 56, 'Friday, Sep 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '06:02', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-10-03', 0, '', '2017-09-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your Are Reject By Admin', 6, 1, 0),
(11, 0, 5, 'Tatenda', 'tatenda@tagyourride.co.za', '0835156148', 'uploads/driver/1508793520driver_11.jpg', 'tatenda', 'OAMLEARfBFFrwFWE', '283.76', '0', '19', 'cnfvrSoE3sg:APA91bHtg2CzbFS2r7mtw_JEiyBuPkfzdH9HjrVJ_ag_U3snvJiYW5JGxWivRA0HEo0FwmUiULG3ZqH_BRSkF4gXinQCiQ6YrhwMFcQPwL7OiX9MHiXhemhWKjWXAtSY_cri5xuJOOvy', 2, '3.05172413793', 28, 45, 'DF78RTGP', '', 121, 'Friday, Sep 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.0607499221419', '28.0625893806369', '4A Wessel Road , Edenburg, Sandton, 2128', '02:45', 'Wednesday, Oct 25, 2017', 25, 10, 5, 2, 0, 2, 2, 0, '', 1, '', '2017-09-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(12, 0, 5, 'hook', 'h@h.com', '1234561234', '', 'qwerty', 'WWnfcYUVdiQ8J0P5', '0', '0', '', '4A5B7BEF87BE630CEDF7A8A7E94D8C208E18C7655B7FF16BCEA22C3FBAD3E0BE', 1, '', 29, 43, '0000', '', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122169502029', '77.0433046559209', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '11:12', 'Tuesday, Oct 3, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 6, 2, 0),
(13, 0, 5, 'atul', 'atul@apporio.com', '1234567891', 'uploads/driver/1507034722driver_13.jpg', 'qwerty', 'JxbDEob94tfNqPkS', '0', '0', '', '4A5B7BEF87BE630CEDF7A8A7E94D8C208E18C7655B7FF16BCEA22C3FBAD3E0BE', 1, '', 30, 44, '0000', 'uploads/driver/1507034263driver_13.jpg', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412124649648', '77.0433610305908', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '01:54', 'Tuesday, Oct 3, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 6, 2, 0),
(14, 0, 5, 'demo', 'demo1@gmail.com', '1234567812', '', 'qwerty', 'UmxVv5ywMDXf7Sa5', '0', '0', '', '60917797A567D5393B0D68CEA0A4BCE977B0A4D6B9EEDF3BDBCF53384A9E361B', 1, '', 28, 46, '0000', 'uploads/driver/1507035440driver_14.jpg', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '02:08', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(15, 0, 5, 'atul1', 'atul1@gmail.com', '1234564321', '', 'qwerty', '33sY8CU4y9aYPues', '0', '0', '', '60917797A567D5393B0D68CEA0A4BCE977B0A4D6B9EEDF3BDBCF53384A9E361B', 1, '', 29, 43, '0000', 'uploads/driver/1507036167driver_15.jpg', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '02:16', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(16, 0, 5, 'demo2', 'demo2@gmail.com', '1234554321', '', 'qwerty', 'OPkhahIIK56KwfLn', '0', '0', '', '0A893A17DF26ECC8DB9337AA02EABA67F7182350546488ED16038B65B7D6CD3B', 1, '', 28, 45, '0000', '', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '07:22', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(17, 0, 20, 'Dave', 'dave@tagyourride.co.za', '0835156149', 'uploads/driver/1508781365driver_17.jpg', 'tatenda', 'wDJBPYK4ZSIAldXy', '1441.1', '17.04', '86', '0B479C56781FF214A0000BFD831828DE8AA9CC2EC6F7BD8D0DE0273A3E1B2174', 1, '3.47014925373', 28, 45, 'CW60ZCGP', 'uploads/driver/1507046530driver_17.jpg', 121, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.0145799378054', '28.1005699629225', 'Jukskei View Drive , Waterval 5-Ir, Midrand, 2090', '09:12', 'Monday, Nov 6, 2017', 64, 17, 12, 1, 0, 1, 2, 0, '', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(18, 0, 5, 'demo12', 'demo12@g.com', '1234565321', '', 'qwerty', 'WRtMAflLYPrO2xAP', '0', '0', '', '60917797A567D5393B0D68CEA0A4BCE977B0A4D6B9EEDF3BDBCF53384A9E361B', 1, '', 28, 46, '0000', 'uploads/driver/1507106228driver_18.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '09:43', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(19, 0, 5, 'hdvsv', 'hahs@gmail.com', '5858585858', '', '123456', 'Vd1udp8cFryvEgDI', '0', '0', '', '60917797A567D5393B0D68CEA0A4BCE977B0A4D6B9EEDF3BDBCF53384A9E361B', 1, '', 28, 19, 'hash', 'uploads/driver/1507106652driver_19.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '10:35', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(20, 0, 5, 'cv', 'njb@gmail.com', '6464646464', 'uploads/driver/1507114742driver_20.jpg', '123456', 'eWZr2vK839o4Rq1U', '0', '0', '', 'f53nPBRzn4Y:APA91bE57DyCCrZLpaVoucyNUHPpARNV3LO4DojYL1Jt0UBR2KpEG-RAN-B3uhoF7HeTlCre6JYgUBPJp8J9zfwBYIbJxKhqNkZ5yJUB-sGf7bDXb7F3QvfYAVclwhJM8VPH1iZROLpz', 2, '', 28, 19, 'bsvz', 'uploads/driver/1507112967driver_20.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120793', '77.0434147', '----', '12:03', 'Wednesday, Oct 4, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 0),
(21, 0, 5, 'jsvb', 'zbz@gmail.com', '6060606060', '', '123456', 'Xy5PhmyC8rddfT57', '14.25', '0', '9.5', 'dj_jKim-yms:APA91bGClG8L8ufMnSZYieXTSKsL9QJD31xOav2FT6r3izqpmtnxvPD5dCiHlPw1hVKuQYyY6PnvOI1RKd_sB-R0D58lts2-fPVnY7KoNTYVwSQg5fxl2E7eA7OPs-tP7898GJ_7GNxl', 2, '1.25', 28, 19, 'jzbzbzz', 'uploads/driver/1507113382driver_21.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122635', '77.043441', '----', '12:58', 'Tuesday, Oct 24, 2017', 3, 0, 2, 2, 0, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(22, 0, 5, 'znznzn', 'jBN@gmail.com', '5252525252', '', '123456', 'ip0CXIfFG6ThKASu', '0', '0', '', 'ff4WPxAvmVY:APA91bHPcec7goLVZyHvrbNjpvP_50IJbRTItjyMrDwuNoOPosab1-UdHO68KxlCXN32ytFLIChuV68w4yZiykfKwJFZVxrOSdaUE2sQ29mIkiYLG0BxERNg7QabyAJEhJWZBJNjQnUI', 2, '', 28, 19, 'hhss', 'uploads/driver/1507116785driver_22.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122189', '77.0432347', '----', '03:39', 'Wednesday, Oct 4, 2017', 0, 0, 3, 2, 1, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(23, 0, 5, 'shsh', 'sysy@gmail.com', '1212121212', '', '123456', 'uYzruMhfrNjw9IRc', '0', '0', '', '', 0, '', 28, 19, '12345', 'uploads/driver/1507118025driver_23.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0),
(24, 0, 5, 'get', 'hz@gmail.com', '1515151515', '', '123456', 'FctoWRrI7rAIQzOJ', '0', '0', '', '', 0, '', 28, 19, 'dccf', 'uploads/driver/1507118222driver_24.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(25, 0, 5, 'hssh', 'hgg@gmail.com', '5454545454', '', '123456', '7O0RwtSfMH1bQIlk', '0', '0', '', 'd3sJT1zLpo8:APA91bFa76YyjD6RI3d-25zJNuOnCG14cg9aSugFthFjKDNUunxxr3iq-U4M1fQwEMYKH1zDPBf4XdWG0xLl20X-SG66SZLT_WKpVi6Z-hHmFWlu9Us2uLavDRh21qybq9DEYu9XYUd_', 2, '', 28, 19, 'usbssg', 'uploads/driver/1507118961driver_25.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122189', '77.0432347', '----', '01:13', 'Wednesday, Oct 4, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(26, 0, 5, 'hshz', 'javvdh@gmail.com', '1010101010', '', '123456', '1Wbe935FuuB6sTnp', '0', '0', '', 'doBwz5ALnbI:APA91bFb_mCGluamc2-ltlws5hG8S6r7g0acStg3rzprW2dU3DifjaMAZJqvjYW_-HC5igOrzUXhyuxrWdjAFhbeiPkk9yfsOu2q0oGWiFJQqF-YXSQPuzOgeT6DZlGMhSySAI2a-cq3', 2, '', 28, 19, 'vzvz', 'uploads/driver/1507123601driver_26.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121481', '77.0432875', '----', '06:24', 'Thursday, Oct 5, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(27, 0, 5, 'bzbz', 'lVf@gmail.com', '8080808080', '', '123456', 'CzuBfTsSxQIcysQV', '14.25', '0', '4.75', 'dj_jKim-yms:APA91bGClG8L8ufMnSZYieXTSKsL9QJD31xOav2FT6r3izqpmtnxvPD5dCiHlPw1hVKuQYyY6PnvOI1RKd_sB-R0D58lts2-fPVnY7KoNTYVwSQg5fxl2E7eA7OPs-tP7898GJ_7GNxl', 2, '1.6', 28, 19, 'jsbssb', 'uploads/driver/1507128041driver_27.jpg', 56, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123319', '77.0434022', '----', '01:59', 'Tuesday, Oct 24, 2017', 3, 0, 1, 2, 0, 2, 2, 0, '', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(28, 0, 5, 'Rahul', 'rahul1@gmail.com', '6767676767', '', '123456', 'OEj22CV0iJsSIyC2', '9.5', '0', '', 'cfPbaoYHMkE:APA91bEBqW1ZTX1z94MTonaRFHkwn13BJBpao8h52_iDF_GEUD7HSYXMAcj0zIQUco8Vh_JHEfEdvhDyD7H_jxCCLSnDOnq1HxwEKoxhf2lRfpKjOa1BJxX9OLaE7uStMgIKAORsrfw0', 2, '0', 28, 19, 'cggg', 'uploads/driver/1507288016driver_28.jpg', 56, 'Friday, Oct 6', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120482', '77.0433247', '----', '12:56', 'Friday, Oct 6, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-06', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(29, 0, 5, 'kaku', 'kaku@gmail.com', '5050505050', '', '123456', 'kKRcbi5d8zNAof8l', '4.75', '0', '', 'e5ff0rnAkI4:APA91bGTf18GBL8hWRISX9NBrXNikM8go19cgdBtScM3CSTKf3AN4yMDDnUCcC29VhEo7olhcbNHu1Ay9Dc-dR5l85R2aLdd0tXwfO0gkzmi608xUiLCdEl0G4VylkztVHcwHQT7gfXo', 2, '0', 28, 19, 'jshs', 'uploads/driver/1507292066driver_29.jpg', 56, 'Friday, Oct 6', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412102', '77.0432883', '----', '05:49', 'Monday, Oct 23, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-06', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(30, 0, 5, 'dadu', 'dadu@gmail.con', '8787878787', '', '123456', '1XjjR71vPq20mXGh', '14.25', '0', '', 'f7QZ08Iig_U:APA91bHTwaPfkGyV_Y_xnlSWAcR2gI88sg5rTKWlav6tP5qfg6ML-IRzNUAAQSftN7cRgY4PzAC0Dl1aF5mRdysWlwAUL45TK5pFCkatn4s5BFHkMQz-9V2QUFfKJ6yD2WK7RpJ2bL31', 2, '0', 28, 19, 'hdbd', 'uploads/driver/1507294662driver_30.jpg', 56, 'Friday, Oct 6', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121697', '77.0431898', '----', '07:46', 'Friday, Oct 13, 2017', 3, 5, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-06', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(31, 0, 5, 'vikash', 'vikash@gmail.com', '6868686868', '', '123456', '10O0z0KerqKLnG66', '61.75', '0', '', 'fQVOpiCKhZA:APA91bHaeDzuwPtclsGaF1EDSPAVSVQWvjOm4Xe0x59JjhqLasKBTA4n5WiRGekwa5FaKdVaOM-CRrhA7beNoiolMx0-Seb8szIUpYuve0xMHV2UschT9lc0xqWY4qXHIzsmAwHfLzCg', 2, '1.69047619048', 28, 19, '1212', 'uploads/driver/1507353558driver_31.jpg', 56, 'Saturday, Oct 7', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120279', '77.0433472', '----', '05:35', 'Thursday, Oct 12, 2017', 13, 0, 1, 2, 0, 2, 2, 0, '', 1, '', '2017-10-07', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(32, 0, 20, 'Roy', 'roymakg@gmail.com', '0786022243', 'uploads/driver/1508952099driver_32.jpg', 'P@ssword01', '4PlDNr1DUom62FHK', '333.41', '4', '123.36', 'eFACuunTXTA:APA91bEMaLkKI-4c--w_JZHFZ9aydaia4sy0weyN3v3vTzhLRBzlzTYs_1vCe8lmtdKx5n6WTer5SGiYkZAnvLg7Pe6gYi4nXu0v1alHuwobF3Tcg4jsv3N8rfdyeNgu-TzzebrcKxxd', 2, '3.11363636364', 28, 45, 'ND753778', 'uploads/driver/1507737439driver_32.jpg', 121, 'Wednesday, Oct 11', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.7216078', '31.0711926', '----', '10:57', 'Tuesday, Nov 28, 2017', 17, 1, 5, 1, 0, 1, 2, 0, '', 1, '', '2017-10-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(33, 0, 20, 'Roger', 'rtshibangu@gmail.com', '0614950420', '', 'DNL2F0', 'wEz3rQ50MydKv78K', '1667.36', '32.7', '0', 'dkElXnjejfI:APA91bHIkB144nP-nQzmrqS-t2iTPK2X9kbWLQvvqtLUCM6rqV_rnbjVYSdIhbFD_ZpnRS02FL9VbiwUDE5H1b93ClqwIalN3yGfzbuIhGW29RsWPQLFcdfq3hGvmfRO5wmrmQ6xlVoC', 2, '2.90217391304', 28, 46, 'ND658157', 'uploads/driver/1507737659driver_33.jpg', 121, 'Wednesday, Oct 11', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.5987635', '30.3760121', '', '10:21', 'Thursday, Nov 16, 2017', 38, 12, 8, 2, 0, 1, 2, 0, '', 1, '', '2017-10-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(34, 0, 20, 'La Dredure', 'dredarsenic@gmail.com', '0736721260', 'uploads/driver/1508853009driver_34.jpg', 'DRE988$d', 'J64XNTwhrGkkAF3s', '1184.4', '60.08', '32.56', 'fPnxcH1QLtA:APA91bGcW6SOhbH-cYBzzlkNKFDSvExYQk6_lLqgGELf3oyTypL-DVP30a5j6P8BqmZpJTRr0oS3RE8afgYHdqmr6ZUojZNrp3BnPVX16c5IyBVoLioc8O8iM7--ojQIkEMmdP2WPP-J', 2, '1.87179487179', 28, 45, 'FZ 08 SS GP', 'uploads/driver/1507737781driver_34.jpg', 121, 'Wednesday, Oct 11', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-33.9211201', '18.5857273', '----', '14:34', 'Tuesday, Nov 14, 2017', 35, 8, 10, 1, 0, 1, 2, 0, '', 1, '', '2017-10-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(35, 0, 5, 'rakesh', 'rakesh@gmail.com', '9090909090', '', '1234t6', 'KIMQ4UyRwpPElVW2', '0', '0', '', 'fLxmno5Btrs:APA91bFCv9xb6-nTFLB3prNac-X1mKRgp80MlAtgENuG51pmgZobJd7xbeSkPZpNOqvQH46nmlqwRazUY7bCsV6lP8oKZQl5rX_qM96pJzQZyIWYJNnBif5PUfrDIzA6zE8OB_fKUw2m', 2, '', 28, 19, '1212', 'uploads/driver/1507783190driver_35.jpg', 56, 'Thursday, Oct 12', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120279', '77.0433472', '----', '06:02', 'Thursday, Oct 12, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-12', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(41, 0, 5, 'Tatenda Satande', 'davidtatenda@gmail.com', '0835156123', 'uploads/driver/1507978634driver_41.jpg', 'tatenda', 'B9k8G7oVZG7Hz3XE', '4.75', '0', '', 'eF1s-zJGE7U:APA91bHOYLPiPcUuprKNOAZHgNKKGbtFj60AXDQ0wpQ9wSOladlgPVtHd88u_Wgg_6XpKC36Oz2meK0ti4_186dMGJtE7KFaVrVvEyzHAFVDCSvwLJdG3brhDOrHduL6nG5qsjSp6aP7', 2, '4', 29, 47, 'HB56TFGP', 'uploads/driver/1507978552driver_41.jpg', 121, 'Saturday, Oct 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.0611818', '28.0625709', '----', '12:01', 'Saturday, Oct 14, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(36, 0, 5, 'taps', 'timshosho@gmail.com', ' 263774302982', '', 'tapiwa', 'z4wjfwRNkpOVfuEC', '0', '0', '', '', 0, '', 29, 43, 'add788', 'uploads/driver/1507839043driver_36.jpg', 56, 'Thursday, Oct 12', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-12', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(39, 0, 5, 'viku', 'viku@gmail.c', '7070707070', '', '123456', 'BH1cuCGB4VIN8L39', '0', '0', '', 'dLVoi9oDGk8:APA91bGmLp29ZFEvP-19FubcjVARQ6ZHs317TuECOSsGC5fRlzSVtb3_igq6N1AJwbMrZeaZ-CI-MAKNA6fTHfraud_dY0qIO-wTQ0oZ2CxLdmvN2J-JWABW0q6LZ_ymiZK9aiLmeSd7', 2, '', 28, 19, '1212', 'uploads/driver/1507895386driver_39.jpg', 56, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121438', '77.0432311', '----', '08:48', 'Tuesday, Oct 17, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(37, 0, 5, 'driver1', 'driver1@9.com', '1234567123', 'uploads/driver/1507874562driver_37.jpg', 'qwerty', 'dF3kPndRkG4TwvEi', '0', '0', '', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '', 28, 46, '1234', 'uploads/driver/1507874453driver_37.jpg', 56, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121747496691', '77.0433062775913', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '01:18', 'Tuesday, Oct 24, 2017', 0, 0, 0, 2, 1, 2, 2, 0, '', 1, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(38, 0, 5, 'vikalp', 'vikalp@9.com', '9599031533', '', 'qwerty', 'wV8JA3r4vTuDsLd8', '23.75', '0', '', 'E572272F9BFE634CBF22B8CADA3D9AACEA70F23C330A04B93A7E17C27C518B1A', 1, '2.9', 30, 44, '1234', 'uploads/driver/1507877349driver_38.jpg', 56, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121909160013', '77.043276454217', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '06:24', 'Monday, Oct 23, 2017', 5, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(42, 0, 20, 'Boniswa', 'boniswamoto@gmail.com', '0837745275', '', 'moto123', 'Kj3rHuKgwCr7rumD', '0', '0', '', 'c9ot7upbviQ:APA91bGuvXt1E1dJkazDCtMmUIzS7F-iY3mrIgw5HwmjZqtQrhBeB1XVdmNMwPxGPFgTdoZNMot6bAH_v13rlvTqg-JhZlMrt3KwdeL3qEWWyYN1FOheOwKrK6YQZa5s4aDoH4vgi28T', 2, '', 28, 45, 'ND17629', 'uploads/driver/1508077863driver_42.jpg', 121, 'Sunday, Oct 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.8908833', '30.9791072', '----', '15:33', 'Sunday, Oct 15, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(40, 0, 5, 'keshav', 'keshav@gmail.com', '2525252525', '', '123456', 'A751hbd4L4p2gstO', '23.75', '0', '', 'dLVoi9oDGk8:APA91bGmLp29ZFEvP-19FubcjVARQ6ZHs317TuECOSsGC5fRlzSVtb3_igq6N1AJwbMrZeaZ-CI-MAKNA6fTHfraud_dY0qIO-wTQ0oZ2CxLdmvN2J-JWABW0q6LZ_ymiZK9aiLmeSd7', 2, '0.8', 28, 19, '121212', 'uploads/driver/1507902558driver_40.jpg', 56, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121281', '77.043257', '----', '07:00', 'Tuesday, Oct 17, 2017', 5, 0, 3, 2, 0, 2, 2, 0, '', 1, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 0),
(43, 0, 5, 'mxnxx', 'zbxb@gmail.com', '46494949494', '', 'vzzvzvzbz', 'Ml27g9XlEdCwk87e', '0', '0', '', 'fw4-c6kcuQo:APA91bElSL6vobfgTDDjUBjU6L3daH5cJDfm1c2rLQApdXQ2kKCJeI8lrSp_hzZagUHhftKaCrlw0fdCAW54d02G756HNm3TFzj4hzoXygSL5SR-G-fTf9Pry8YbwbLCPmJX3-hVYh4D', 2, '', 28, 19, ', , , b,', 'uploads/driver/1508159899driver_43.jpg', 56, 'Monday, Oct 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121753', '77.04319', '----', '14:21', 'Monday, Oct 16, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(77, 0, 5, 'igsfgdhzd', 'bxchhffhf35@t.com', '9696969696', '', '123456', 'hyl4RrpoOTYPSyDD', '175.75', '0', '', '0A893A17DF26ECC8DB9337AA02EABA67F7182350546488ED16038B65B7D6CD3B', 1, '', 28, 19, 'DL-10S-7555', 'uploads/driver/1509348270driver_77.jpg', 56, 'Monday, Oct 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120933613858', '77.0432469264884', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:51', 'Monday, Nov 6, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '2017-10-30', 1, '', '2017-10-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(44, 0, 5, 'xknx', 'hzzb@gmail.com', '679797999797', '', 'xbxvcx', 'w1tXRN2mApliamB3', '4.75', '0', '', 'dLVoi9oDGk8:APA91bGmLp29ZFEvP-19FubcjVARQ6ZHs317TuECOSsGC5fRlzSVtb3_igq6N1AJwbMrZeaZ-CI-MAKNA6fTHfraud_dY0qIO-wTQ0oZ2CxLdmvN2J-JWABW0q6LZ_ymiZK9aiLmeSd7', 2, '2', 28, 19, 'bxbxbxb', 'uploads/driver/1508161421driver_44.jpg', 56, 'Monday, Oct 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4345833', '77.0354157', '----', '04:53', 'Monday, Oct 16, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(45, 0, 20, 'Satande', 'satande@tyr.co.za', '0784512325', 'uploads/driver/1508781303driver_45.jpg', 'tatenda', 'KTw27SwJwOYXJ81L', '16', '0', '0', 'fMRnClhGxIw:APA91bFLaXfAoYKab_MeLR7nED-B580HyzNuLeBuH9xbl3j8nsKglcNozcXq18OpD80KnAfMFOx3DPd3iCosG7poyR8HMBh-YQHFbub1W8wn7NsDhXqj-HXQ8d3V29cshsBf7xfUzvKw', 2, '0', 32, 51, 'JH67DRGP', '', 121, 'Friday, Oct 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.0609464', '28.0624294', '----', '06:44', 'Tuesday, Oct 24, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '2017-10-20', 1, '', '2017-10-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 1),
(46, 0, 5, 'qwerty@gmail.com', 'qwerty@gmail.com', '1236547890', '', 'qwerty', 'CcN9KnhFaupcYGl0', '9.5', '0', '', 'ceNw3bu1hxY:APA91bFFkuoVLA7rJ_rFYPWw5DxMgpTbPkmnAVy0aijRGKSFfPbz-B-V_PxFho8E4bwMZJwgkYMpEd1BbF_GAmxyd94uUXvemuyNRJwXeSdHRwKTPlSAH2lL--bulQplPcpA48_zWQHI', 2, '1.66666666667', 30, 44, 'qwerty', 'uploads/driver/1508736966driver_46.jpg', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121841', '77.0432236', '----', '02:39', 'Monday, Oct 23, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '2017-10-23', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 5),
(47, 0, 5, 'ytrewq', 'ytrewq@gmail.com', '1236547410', '', 'ytrewq', 'kMeqm6axwLlIFucC', '19', '0', '', 'eONdXeFWPww:APA91bFDKwI4x0PVa3fPQgEfggmGab7pDSoMa2H2c9Mo3ZAylIoxZ5aakJCo6-mue3eeevjb2wnSH-dhzHD_Gv_OIu-TBrzNfEMzEvo-UF8e098aSN4n5LO-_TdCQOCkbWpcTJr-XrB7', 2, '1.66666666667', 30, 44, 'ytrewq', 'uploads/driver/1508737364driver_47.jpg', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121307', '77.0432941', '', '10:44', 'Monday, Oct 23, 2017', 4, 2, 2, 2, 0, 2, 2, 0, '2017-10-23', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 5),
(48, 0, 5, 'yhhhgh', 'ghhhh@gmail.com', '66999999999', '', 'vhhhhhhjj', 'zweBGxKIDWKcR7HV', '9.5', '0', '', 'cB6QuQDtal4:APA91bGBNOmBOVp8-WsgJooF-n5jkhqbu0VSXDmMRjAHNEt_i44WFc6xSzN9YFJNwmF4CjK7iYWt4qf6rUwAwEUv6R2zAyIcF3_SNiPVaHZAcrZInm4iO1Uce-t3D6Y4SfCtKX2SN4Km', 2, '0', 28, 19, 'ghhhjj', 'uploads/driver/1508751345driver_48.jpg', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412046', '77.0433022', '----', '12:55', 'Monday, Oct 23, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(49, 0, 5, 'we', '9@j.com', '1234567321', '', 'qwerty', 'PuMfB7COaRXaUKBG', '4.75', '0', '4.75', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '0', 30, 48, '1234', 'uploads/driver/1508751624driver_49.jpg', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120874159808', '77.0433091491385', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '14:02', 'Tuesday, Oct 24, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(50, 0, 5, 'Manish', 'manish@apporio.com', '1234554329', '', 'qwerty', 'qZdRjahkPkQYuOoB', '0', '0', '', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '', 30, 44, '1234', 'uploads/driver/1508765100driver_50.jpg', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '11:05', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(51, 0, 20, 'vhhhh', 'hhhh@g.com', '8855555555555', '', 'ygggggg', '78OHNB7SZxvTEeZb', '0', '0', '', '', 0, '', 28, 19, 'fggggggg', 'uploads/driver/1508766030driver_51.jpg', 121, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(52, 0, 5, 'bbzbz', 'ahaha@gmail.com', '67797979779797', '', 'gsgsgszvs', 'qdF8dVXfNWEHXsyl', '0', '0', '', 'eZItbQC_4F0:APA91bFhY-0nGRsXYPazXV2_z4Prgf12E-RfaherR_GtQ2NRmwDjb94cb-4-WonbQAbbAxEzq1lYfrb7y0HgwsO4tXylsFQ302g6N9_-0130DWY7wj1FXok9ZbFLXqcsNPnFtjodwQIN', 2, '', 28, 19, 'vzzvzzvzvzv', 'uploads/driver/1508778739driver_52.jpg', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4344827', '77.0354838', '----', '06:44', 'Tuesday, Oct 24, 2017', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(53, 0, 20, 'Andiswa', 'sindane92@gmail.com', '0731709711', '', 'ayabonga', 'Hknj74PkHUZ8RY3N', '48.56', '0', '', 'fxRTEpbX5B4:APA91bEvyonvDNO5VGUdmtWoaL2__ezZrVRcxVCnLQQzDgaK_nUxgE7q8Ddk27JwG3whLPylxvb6pawXBm-k6VV5fwpX24A026ZyHv1ZLxv2wc0oA4orYHETBmtsqFnFgG4ECaAeBYE3', 2, '2.33333333333', 28, 45, 'NP12349', 'uploads/driver/1508782876driver_53.jpg', 121, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.8907173', '30.9791142', '----', '21:08', 'Tuesday, Oct 24, 2017', 3, 3, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(66, 0, 5, 'Manisha sharma', 'manishsharma@apporio.com', '7015363199', '', 'XSPGC5', 'ZHiig6KGAruXsQN2', '9.5', '0', '4.75', 'eVv0yO-ZuU4:APA91bEg80nOZdcOf36peTe558YoO1zoE4ivUgB60StQFX0IghipZrPn6Jm8QYoiic82jbr1JaNxqlNLOUfPGdPPfq5Vp1PQqOtUGd2lApRdhfT52a_OU5k2bV8eac9bFoAT0joxwL9h', 2, '1.33333333333', 28, 19, '1212', 'uploads/driver/1508908239driver_66.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123461', '77.0433922', '----', '08:09', 'Wednesday, Oct 25, 2017', 2, 0, 1, 2, 0, 2, 2, 0, '2017-10-25', 1, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 5),
(58, 0, 5, 'jsbxbbx', 'bvzzv@gmail.com', '4040404040', '', '123456', 'tWJUkRaFB9k95fRS', '0', '0', '', '', 0, '', 28, 19, 'bnzbzzb', 'uploads/driver/1508848664driver_58.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-10-24', 0, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your Are Reject By Admin', 5, 2, 1),
(57, 38, 26, 'Testing', '2test@demo.com', '98987545665', '', 'n_6M</)$W*9%iBg<p=', '', '0', '0', '', '', 0, '', 28, 46, '2626', '', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-10-24', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 5),
(56, 0, 5, 'abzvvz', 'shzbsb@gmail.com', '79779977979997', '', '122212222222', 'V1lU9o5lzpDnMb3J', '0', '0', '', 'dj_jKim-yms:APA91bGClG8L8ufMnSZYieXTSKsL9QJD31xOav2FT6r3izqpmtnxvPD5dCiHlPw1hVKuQYyY6PnvOI1RKd_sB-R0D58lts2-fPVnY7KoNTYVwSQg5fxl2E7eA7OPs-tP7898GJ_7GNxl', 2, '', 28, 19, 'bzvz z zvvzbz', 'uploads/driver/1508841027driver_56.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '11:32', '', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 0),
(60, 0, 5, 'ghfhfu', 'jjjjjjjj@gmail.com', '00000022222', '', '3ffffgggg', 'f5hpAD4dIIXajkiZ', '4.75', '0', '', 'dj_jKim-yms:APA91bGClG8L8ufMnSZYieXTSKsL9QJD31xOav2FT6r3izqpmtnxvPD5dCiHlPw1hVKuQYyY6PnvOI1RKd_sB-R0D58lts2-fPVnY7KoNTYVwSQg5fxl2E7eA7OPs-tP7898GJ_7GNxl', 2, '0', 28, 19, 'hbbh', 'uploads/driver/1508850009driver_60.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122779', '77.0434424', '----', '02:37', 'Tuesday, Oct 24, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 0),
(59, 0, 5, 'xbxbxbx', 'hszbzbzsb@gmail.com', '76677676767', '', 'shshshs', 'bH7iInwfnN0dfaYI', '0', '0', '', '', 0, '', 28, 19, 'Ji bssbz', 'uploads/driver/1508848756driver_59.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(61, 0, 5, 'iOS', 'ios@gmail.com', '5555588888', '', 'qwerty', 'YxycLdsEjbXp3N9b', '0', '0', '', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '', 30, 44, '1234', 'uploads/driver/1508850302driver_61.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '02:20', '', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 0),
(62, 0, 5, 'Jai', 'jai@gmail.com', '6666666666', '', 'FZ2TLX', 'hrSCpkXUBDmFHpHL', '0', '0', '', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '', 30, 48, '1234', 'uploads/driver/1508851319driver_62.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121045920247', '77.0433188278302', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '08:50', 'Wednesday, Oct 25, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(63, 38, 12, 'Test ', 'test@demo.com', '23232434', '', '123', '', '0', '0', '', '', 0, '', 28, 19, '3131', '', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 0, 0, '', 0, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 0),
(64, 0, 5, 'dhinchik pooja', 'ashish@apporio.com', '5454545455', '', '123456', 'FzLtaqf5ty7np9nH', '0', '0', '', 'dXSQmHgkZNA:APA91bHfP5tntbltRWdcm6WhexBDVDCsa6JtmQpFq5ZBOErodL1qQ1Oz-6v0pM1SKoFtn1YkK0nSlNOtGp1wLo4M_274UzkSBrSO3aLbjHw3kvWHVygFCvNrhYwPzZ6vZJGsyUADfxvI', 2, '', 28, 19, '123123', 'uploads/driver/1508852378driver_64.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123359', '77.0434011', '----', '08:20', 'Wednesday, Oct 25, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '2017-10-24', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 5),
(65, 0, 5, 'manish', 'itsme.095manish@gmail.com', '9991617237', '', '123456', 'NbCjUZRrWAkEx2pM', '0', '0', '', 'eZItbQC_4F0:APA91bFhY-0nGRsXYPazXV2_z4Prgf12E-RfaherR_GtQ2NRmwDjb94cb-4-WonbQAbbAxEzq1lYfrb7y0HgwsO4tXylsFQ302g6N9_-0130DWY7wj1FXok9ZbFLXqcsNPnFtjodwQIN', 2, '', 28, 19, '1212', 'uploads/driver/1508867099driver_65.jpg', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4344827', '77.0354838', '----', '06:48', 'Tuesday, Oct 24, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '2017-10-24', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 5),
(67, 0, 5, 'Atul', 'Akshay@apporio.com', '1023456789', '', '54YIDJ', 'i06bsKI0v5YCbmRv', '9.5', '0', '', '0A893A17DF26ECC8DB9337AA02EABA67F7182350546488ED16038B65B7D6CD3B', 1, '2', 30, 44, '1234', 'uploads/driver/1508918027driver_67.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122729336695', '77.0432740036608', '322B, Sohna Road, Sector 49 , Gurugram, Haryana 122018', '02:26', 'Saturday, Oct 28, 2017', 2, 0, 1, 2, 0, 2, 2, 0, '2017-10-25', 1, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 2, 5),
(68, 0, 5, 'ETFs', 'gggg@9.com', '4567890321', '', 'qwerty', 'SZfTwWdfXN3ACGOz', '0', '0', '', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '', 28, 19, '1234', 'uploads/driver/1508918824driver_68.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '09:46', '', 0, 0, 0, 2, 0, 2, 3, 0, '', 0, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(69, 0, 5, 'hshshs', 'bsbsbsb@hdbs.com', '555554444', '', 'qwzerty', '9V3kMkzI9HGtiXAZ', '0', '0', '', 'dXSQmHgkZNA:APA91bHfP5tntbltRWdcm6WhexBDVDCsa6JtmQpFq5ZBOErodL1qQ1Oz-6v0pM1SKoFtn1YkK0nSlNOtGp1wLo4M_274UzkSBrSO3aLbjHw3kvWHVygFCvNrhYwPzZ6vZJGsyUADfxvI', 2, '', 28, 46, '1234', 'uploads/driver/1508921367driver_69.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123338', '77.0433809', '----', '09:55', 'Wednesday, Oct 25, 2017', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(70, 0, 5, 'hshshs', 'bsbsbsb@hdbs.com', '555554444', '', 'qwzerty', 'NKkUSLDmq2qdOUdF', '0', '0', '', '', 0, '', 28, 46, '1234', 'uploads/driver/1508921367driver_70.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 0),
(71, 0, 5, 'sam', 'sam@gmail.com', '2121212121', '', 'MCYNHS', 'DlXzfc9Grazarg5E', '4.75', '0', '', 'dXSQmHgkZNA:APA91bHfP5tntbltRWdcm6WhexBDVDCsa6JtmQpFq5ZBOErodL1qQ1Oz-6v0pM1SKoFtn1YkK0nSlNOtGp1wLo4M_274UzkSBrSO3aLbjHw3kvWHVygFCvNrhYwPzZ6vZJGsyUADfxvI', 2, '2', 28, 19, '1212', 'uploads/driver/1508921858driver_71.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123237', '77.0433922', '----', '11:46', 'Wednesday, Oct 25, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '2017-10-25', 1, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 5, 1, 5),
(72, 0, 5, 'sahil', 'sahil@gmail.com', '6363636333', '', '123456', '0VkbgCkdg17lc76C', '38', '0', '', 'dXSQmHgkZNA:APA91bHfP5tntbltRWdcm6WhexBDVDCsa6JtmQpFq5ZBOErodL1qQ1Oz-6v0pM1SKoFtn1YkK0nSlNOtGp1wLo4M_274UzkSBrSO3aLbjHw3kvWHVygFCvNrhYwPzZ6vZJGsyUADfxvI', 2, '1.7', 28, 19, 'wawaaaa', 'uploads/driver/1508939153driver_72.jpg', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4344827', '77.0354838', '----', '19:30', 'Saturday, Oct 28, 2017', 3, 0, 0, 2, 0, 2, 2, 0, '2017-10-25', 1, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(76, 0, 5, 'drt', 'det@9.com', '5656565656', '', 'qwerty', 'guvqAoSUfVZzDl8N', '4.75', '0', '', 'A0ED0E7D4A23BEECBF7DE948B23720299663100DCADEE7E5A2A6A357564AE1EA', 1, '5', 28, 46, '1234', 'uploads/driver/1509197319driver_76.jpg', 56, 'Saturday, Oct 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412133911651', '77.0432808995964', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '14:35', 'Saturday, Oct 28, 2017', 1, 2, 0, 2, 0, 2, 2, 0, '2017-10-28', 1, '', '2017-10-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(73, 0, 20, 'Mambo', 'mambo@tagyourride.co.za', '0789654125', 'uploads/driver/1509211624driver_73.jpg', 'tatenda', 'xIkrfShIiLmLGgME', '229.38', '-24', '32', '0B479C56781FF214A0000BFD831828DE8AA9CC2EC6F7BD8D0DE0273A3E1B2174', 1, '2.5', 28, 45, 'PMB1234', 'uploads/driver/1508955926driver_73.jpg', 121, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-26.1233011309576', '28.0347953318505', '57 6th Road , Hyde Park, Sandton, 2196', '14:45', 'Wednesday, Nov 15, 2017', 11, 2, 1, 1, 1, 1, 2, 0, '2017-10-25', 1, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 1, 1, 1),
(74, 0, 5, 'rohit', 'rohit@g.com', '9865321473', '', '123456', 'Q2l9ubCAPmbq7KTc', '0', '0', '', 'eUEM3WwZfoY:APA91bFD-F-Hp0_C1PSmjhZLDww0bR0BTuEKXTQgWqU9FK-OOWZLyTisZLARTgacoHKdnBdJ9US2C2brdqKn8qgsZEEv-LBtWzWYQjTf_zrZKqb5RNkbs37oclV0Sb_c0751wtNwqU-L', 2, '', 28, 45, '1234', 'uploads/driver/1509013575driver_74.jpg', 56, 'Thursday, Oct 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123259', '77.0434147', '----', '11:26', 'Thursday, Oct 26, 2017', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 0),
(75, 0, 5, 'demo', 'demo@gmail.com', '9865321475', '', '123456', 'cs4vCI5MMlCsvV8V', '4.75', '0', '', 'eUEM3WwZfoY:APA91bFD-F-Hp0_C1PSmjhZLDww0bR0BTuEKXTQgWqU9FK-OOWZLyTisZLARTgacoHKdnBdJ9US2C2brdqKn8qgsZEEv-LBtWzWYQjTf_zrZKqb5RNkbs37oclV0Sb_c0751wtNwqU-L', 2, '0', 28, 45, '12334', 'uploads/driver/1509013650driver_75.jpg', 56, 'Thursday, Oct 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122853', '77.0434596', '----', '11:33', 'Thursday, Oct 26, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '2017-10-26', 1, '', '2017-10-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(78, 0, 5, 'amir', 'bsz@gmail.com', '1234567888', '', '123456', '0OIVTTAd4WqhoPtm', '0', '0', '', 'eL7pHZ1aG9U:APA91bG2fxYtmifnZsz0H8qhxu1L3zaPnyVCs4uml3geYsBblWsunQrGwUrgWySGvoDTUNtgJREz_DD-K10liTGzJ0a1BMjZJciWhRNYj4EL-LmiMY92nkAptW75LPhETwc8ka-uhv1q', 2, '', 28, 19, '1212', 'uploads/driver/1509349757driver_78.jpg', 56, 'Monday, Oct 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123092', '77.0434267', '----', '09:14', 'Monday, Oct 30, 2017', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-10-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 0),
(79, 0, 5, 'vishal', 'nsbxbxb@gmail.com', '1234556788888', '', '123456', 'iYHtqeoDQKaSFe3m', '0', '0', '', '', 0, '', 28, 19, '1212', 'uploads/driver/1509354982driver_79.jpg', 56, 'Monday, Oct 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 3, 0, '', 0, '', '2017-10-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 0);
INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `car_image`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(80, 0, 5, 'harish', 'hzzbb@gmail.com', '123456111', '', '123456', '6VGNbU8cOjmPxEYA', '66.5', '0', '', 'eL7pHZ1aG9U:APA91bG2fxYtmifnZsz0H8qhxu1L3zaPnyVCs4uml3geYsBblWsunQrGwUrgWySGvoDTUNtgJREz_DD-K10liTGzJ0a1BMjZJciWhRNYj4EL-LmiMY92nkAptW75LPhETwc8ka-uhv1q', 2, '1.85714285714', 28, 19, '1212', 'uploads/driver/1509355169driver_80.jpg', 56, 'Monday, Oct 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4346472', '77.0350353', '----', '05:01', 'Tuesday, Oct 31, 2017', 5, 0, 0, 2, 0, 1, 2, 0, '2017-10-30', 1, '', '2017-10-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(81, 0, 5, 'fifi', 'fifi@g.com', '1234560987', '', 'qwerty', 'JnxSK0p0NmLxgstb', '61.75', '0', '', 'BD735B54FE018308B2E29FCE35BC89012AEC2F17028F3AD881B9E3C7E3E514B9', 1, '2.125', 28, 45, '1234', 'uploads/driver/1509359765driver_81.jpg', 56, 'Monday, Oct 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122406768048', '77.043204552273', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:42', 'Monday, Nov 6, 2017', 9, 1, 0, 2, 0, 2, 2, 0, '2017-10-30', 1, '', '2017-10-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(82, 0, 5, 'ggdg', 'zvvzzv@gmail.com', '6464646646', '', '123456', 'hknERtBqYZuC93uK', '0', '0', '', '', 0, '', 28, 19, '1212', 'uploads/driver/1509787833driver_82.jpg', 56, 'Saturday, Nov 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 0),
(83, 0, 5, 'sameer', 'sameer@gmail.com', '4454454456', '', '123456', 'zuAaK18bPAg8Gfzp', '85.5', '-50.5', '52.25', 'eL7pHZ1aG9U:APA91bG2fxYtmifnZsz0H8qhxu1L3zaPnyVCs4uml3geYsBblWsunQrGwUrgWySGvoDTUNtgJREz_DD-K10liTGzJ0a1BMjZJciWhRNYj4EL-LmiMY92nkAptW75LPhETwc8ka-uhv1q', 2, '1.82857142857', 28, 19, '1212', 'uploads/driver/1509788162driver_83.jpg', 56, 'Saturday, Nov 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4346477', '77.0350375', '----', '02:49', 'Tuesday, Nov 14, 2017', 20, 3, 1, 1, 0, 1, 2, 0, '2017-11-04', 1, '', '2017-11-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(84, 0, 5, 'zzzz', '9@9.com', '0000000000', '', 'qwerty', 'ADrgF8mGiZIFgKfn', '61.75', '-26.75', '28.5', '0A893A17DF26ECC8DB9337AA02EABA67F7182350546488ED16038B65B7D6CD3B', 1, '2.07142857143', 28, 46, '1234', 'uploads/driver/1509954786driver_84.jpg', 56, 'Monday, Nov 6', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120881045502', '77.0432561729821', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:00', 'Tuesday, Nov 7, 2017', 13, 1, 2, 1, 0, 1, 2, 0, '2017-11-06', 1, '', '2017-11-06', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 5, 1, 5),
(85, 0, 20, 'Leone', 'leooo@gmail.com', '0825047226', '', '565643', 'HVpuUFON0VqjbexT', '0', '0', '', 'dkElXnjejfI:APA91bHIkB144nP-nQzmrqS-t2iTPK2X9kbWLQvvqtLUCM6rqV_rnbjVYSdIhbFD_ZpnRS02FL9VbiwUDE5H1b93ClqwIalN3yGfzbuIhGW29RsWPQLFcdfq3hGvmfRO5wmrmQ6xlVoC', 2, '', 28, 19, 'mp1234', 'uploads/driver/1510394317driver_85.jpg', 121, 'Saturday, Nov 11', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.765518', '30.6751693', '----', '11:26', 'Saturday, Nov 11, 2017', 0, 0, 0, 2, 0, 2, 3, 0, '', 0, '', '2017-11-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 1, 1, 0),
(86, 0, 20, 'Sanelisiwe Maphumulo', 'sanelisiwe155@gmail.com', '0608483591', '', 'sane4061', 'uyRVNbufe51PnrLx', '16', '4', '', 'fVHxEBq9w7w:APA91bFJC0ya3ejDjBc4w1JMxMSdT-qRSZVClmg5fB2QHnWMPMPLxZFR0uic67gOQfb8HZh6Hwxtc_8uEILfciBKF855PgmnwAUNz5zdyKzBB9PCkh-J7scSJjFFwgDY49Ibe8S3aRs6', 2, '0', 28, 46, 'NP 10384', 'uploads/driver/1510394428driver_86.jpg', 121, 'Saturday, Nov 11', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.6258173', '30.402999', '----', '10:14', 'Saturday, Nov 11, 2017', 1, 0, 0, 2, 0, 1, 2, 0, '2017-11-11', 1, '', '2017-11-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 1, 2, 1),
(87, 0, 20, 'bongumusa', 'bungumusa2c2@gmail.com', '0725580519', '', '123456', 'k6nvPt5uKHr45TJb', '0', '0', '', 'eztfDxB6XNA:APA91bGSsQ-vfPUYzew7gQA0I6nQGRvyydgtyjasQOTRP6cMDbdOjAbALCLds9qRnuP59uQ0HJnLKn-lZXdA2t7sc1l7dtjgxE2OO_8j8XrogeXnfjsWk6lrj_5iFlnA38qpsl7pEL4_', 2, '', 32, 51, 'NP174513', 'uploads/driver/1510394534driver_87.jpg', 121, 'Saturday, Nov 11', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-29.6258072', '30.4029827', '----', '10:04', 'Saturday, Nov 11, 2017', 0, 0, 0, 2, 0, 1, 1, 0, '', 0, '', '2017-11-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 1, 1, 0),
(88, 0, 20, 'arcel', 'aaaaaa@gmail.com', '0795792956', '', 'DRE988$d', 'kCYPW9ZvE92BK8zC', '0', '0', '', '', 0, '', 32, 51, 'CA000888', 'uploads/driver/1511588625driver_88.jpg', 121, 'Saturday, Nov 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your documents are under process.You will be notified soon.', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '100', 1, '95.00', '5.00', '2017-09-20'),
(2, 2, '200', 2, '190', '10', '2017-09-22'),
(3, 4, '178', 1, '167.32', '10.68', '2017-09-22'),
(4, 6, '240', 4, '216', '24', '2017-09-22'),
(5, 7, '120', 2, '108', '12', '2017-09-22'),
(6, 6, '300', 5, '270', '30', '2017-09-24'),
(7, 6, '120', 2, '108', '12', '2017-09-25'),
(8, 6, '80', 2, '72', '8', '2017-09-28'),
(9, 8, '356', 2, '334.64', '21.36', '2017-09-29'),
(10, 11, '25', 6, '23.75', '-3.75', '2017-09-29'),
(11, 11, '25', 4, '23.75', '1.25', '2017-09-30'),
(12, 11, '10', 2, '9.5', '0.5', '2017-10-01'),
(13, 11, '11', 3, '10.45', '0.55', '2017-10-03'),
(14, 9, '10', 2, '9.5', '0.5', '2017-10-03'),
(15, 17, '80', 9, '76', '4', '2017-10-03'),
(16, 17, '248.3', 2, '235.88', '12.42', '2017-10-04'),
(17, 28, '10', 2, '9.5', '0.5', '2017-10-06'),
(18, 30, '15', 3, '14.25', '0.75', '2017-10-06'),
(19, 17, '20', 1, '19.00', '1.00', '2017-10-06'),
(20, 31, '5', 1, '4.75', '0.25', '2017-10-07'),
(21, 31, '45', 9, '23.75', '1.25', '2017-10-08'),
(22, 31, '5', 1, '4.75', '0.25', '2017-10-09'),
(23, 9, '50', 10, '23.75', '1.25', '2017-10-09'),
(24, 31, '40', 8, '14.25', '0.75', '2017-10-10'),
(25, 9, '20', 4, '9.5', '0.5', '2017-10-10'),
(26, 31, '15', 3, '14.25', '0.75', '2017-10-11'),
(27, 17, '396.4', 19, '351.58', '-35.18', '2017-10-12'),
(28, 33, '20', 1, '19.60', '0.40', '2017-10-12'),
(29, 34, '80', 4, '78.4', '1.6', '2017-10-12'),
(30, 32, '99.4', 4, '97.41', '-38.01', '2017-10-12'),
(31, 9, '5', 1, '4.75', '0.25', '2017-10-12'),
(32, 38, '10', 2, '9.5', '0.5', '2017-10-13'),
(33, 34, '21.1', 1, '20.68', '0.42', '2017-10-13'),
(34, 33, '218.9', 6, '214.52', '4.38', '2017-10-13'),
(35, 17, '100', 5, '80', '20', '2017-10-13'),
(36, 40, '5', 1, '4.75', '0.25', '2017-10-13'),
(37, 32, '60.65', 2, '59.44', '-18.79', '2017-10-13'),
(38, 32, '0.00', 1, '0.00', '0.00', '2017-10-14'),
(39, 33, '132.12', 2, '105.69', '26.43', '2017-10-14'),
(40, 41, '5', 1, '4.75', '0.25', '2017-10-14'),
(41, 17, '62.8', 3, '50.24', '12.56', '2017-10-14'),
(42, 17, '38.97', 1, '31.18', '7.79', '2017-10-15'),
(43, 38, '10', 2, '9.5', '0.5', '2017-10-16'),
(44, 44, '5', 1, '4.75', '0.25', '2017-10-16'),
(45, 40, '15', 3, '14.25', '0.75', '2017-10-16'),
(46, 40, '5', 1, '4.75', '0.25', '2017-10-17'),
(47, 38, '5', 1, '4.75', '0.25', '2017-10-17'),
(48, 33, '20', 1, '16.00', '4.00', '2017-10-18'),
(49, 17, '60', 3, '48', '12', '2017-10-21'),
(50, 29, '5', 1, '4.75', '0.25', '2017-10-23'),
(51, 46, '10', 2, '9.5', '0.5', '2017-10-23'),
(52, 47, '20', 4, '19', '1', '2017-10-23'),
(53, 48, '10', 2, '9.5', '0.5', '2017-10-23'),
(54, 17, '134.36', 4, '107.49', '26.87', '2017-10-23'),
(55, 33, '60', 3, '48', '12', '2017-10-23'),
(56, 34, '144.2', 9, '115.36', '-11.86', '2017-10-23'),
(57, 53, '40.7', 2, '32.56', '8.14', '2017-10-23'),
(58, 32, '60.7', 6, '48.56', '-48.56', '2017-10-23'),
(59, 45, '20', 2, '16', '-16', '2017-10-23'),
(60, 11, '40', 3, '38', '-18', '2017-10-23'),
(61, 17, '40', 2, '32', '-32', '2017-10-24'),
(62, 11, '64.9', 3, '61.65', '-16.75', '2017-10-24'),
(63, 34, '703.3', 3, '562.64', '140.66', '2017-10-24'),
(64, 33, '41.4', 2, '33.12', '8.28', '2017-10-24'),
(65, 21, '15', 6, '14.25', '-14.25', '2017-10-24'),
(66, 27, '15', 5, '14.25', '-9.25', '2017-10-24'),
(67, 49, '5', 1, '4.75', '-4.75', '2017-10-24'),
(68, 60, '5', 1, '4.75', '0.25', '2017-10-24'),
(69, 32, '20', 2, '16', '-16', '2017-10-24'),
(70, 53, '20', 1, '16.00', '4.00', '2017-10-24'),
(71, 66, '10', 2, '9.5', '-4.5', '2017-10-25'),
(72, 71, '5', 1, '4.75', '0.25', '2017-10-25'),
(73, 67, '10', 2, '9.5', '0.5', '2017-10-25'),
(74, 72, '35', 2, '33.25', '1.75', '2017-10-25'),
(75, 33, '20', 1, '16.00', '4.00', '2017-10-25'),
(76, 11, '122.8', 6, '116.66', '6.14', '2017-10-25'),
(77, 73, '20', 1, '16.00', '4.00', '2017-10-26'),
(78, 75, '5', 1, '4.75', '0.25', '2017-10-26'),
(79, 34, '20', 1, '16.00', '4.00', '2017-10-26'),
(80, 33, '151.37', 2, '121.09', '30.28', '2017-10-27'),
(81, 73, '146.73', 3, '117.38', '29.35', '2017-10-27'),
(82, 33, '100.7', 5, '80.56', '20.14', '2017-10-28'),
(83, 76, '5', 1, '4.75', '0.25', '2017-10-28'),
(84, 34, '60', 3, '48', '12', '2017-10-28'),
(85, 73, '40', 2, '32', '8', '2017-10-28'),
(86, 72, '5', 1, '4.75', '0.25', '2017-10-28'),
(87, 32, '40', 2, '32', '8', '2017-10-28'),
(88, 17, '77.52', 2, '62.02', '15.5', '2017-10-29'),
(89, 34, '88.7', 3, '70.96', '17.74', '2017-10-29'),
(90, 33, '20', 1, '16.00', '4.00', '2017-10-29'),
(91, 66, '0.00', 1, '0.00', '0.00', '2017-10-30'),
(92, 77, '185', 1, '175.75', '9.25', '2017-10-30'),
(93, 80, '60', 3, '57', '3', '2017-10-30'),
(94, 81, '5', 1, '4.75', '0.25', '2017-10-30'),
(95, 17, '0.00', 1, '0.00', '0.00', '2017-10-30'),
(96, 81, '55', 11, '52.25', '2.75', '2017-10-31'),
(97, 32, '80', 4, '64', '16', '2017-10-31'),
(98, 33, '44.15', 1, '35.32', '8.83', '2017-10-31'),
(99, 80, '10', 2, '9.5', '0.5', '2017-10-31'),
(100, 17, '40', 2, '32', '8', '2017-10-31'),
(101, 34, '40', 2, '32', '8', '2017-11-01'),
(102, 81, '5', 1, '4.75', '0.25', '2017-11-01'),
(103, 17, '80', 4, '64', '16', '2017-11-01'),
(104, 33, '197.25', 4, '157.8', '39.45', '2017-11-01'),
(105, 17, '229.44', 4, '183.55', '45.89', '2017-11-02'),
(106, 1, '26785', 7, '25445.75', '-2525.75', '2017-11-02'),
(107, 33, '841.07', 1, '672.86', '168.21', '2017-11-02'),
(108, 17, '22.1', 1, '17.68', '4.42', '2017-11-03'),
(109, 83, '25', 16, '23.75', '-48.75', '2017-11-04'),
(110, 33, '20', 1, '16.00', '4.00', '2017-11-04'),
(111, 17, '63.1', 2, '50.48', '12.62', '2017-11-05'),
(112, 83, '60', 20, '57', '-82', '2017-11-06'),
(113, 33, '20', 1, '16.00', '4.00', '2017-11-06'),
(114, 84, '50', 18, '47.5', '-67.5', '2017-11-06'),
(116, 32, '', 1, '0.00', '-20', '2017-11-06'),
(115, 73, '40', 3, '32', '-52', '2017-11-06'),
(117, 34, '58.72', 3, '46.98', '-14.98', '2017-11-06'),
(118, 84, '15', 5, '14.25', '-9.25', '2017-11-07'),
(119, 33, '20', 1, '16.00', '4.00', '2017-11-07'),
(120, 83, '5', 2, '4.75', '-9.75', '2017-11-07'),
(121, 34, '80.32', 1, '64.26', '16.06', '2017-11-08'),
(122, 33, '40', 2, '32', '8', '2017-11-09'),
(123, 32, '20', 1, '16.00', '4.00', '2017-11-09'),
(124, 33, '', 1, '0.00', '-20', '2017-11-10'),
(125, 73, '20', 1, '16.00', '4.00', '2017-11-11'),
(126, 33, '43.5', 2, '34.8', '8.7', '2017-11-11'),
(127, 86, '20', 1, '16.00', '4.00', '2017-11-11'),
(128, 34, '41.4', 4, '33.12', '-31.72', '2017-11-11'),
(129, 34, '20', 1, '16.00', '4.00', '2017-11-12'),
(130, 34, '20', 1, '16.00', '4.00', '2017-11-13'),
(131, 73, '20', 1, '16.00', '4.00', '2017-11-13'),
(132, 34, '60', 6, '48', '-48', '2017-11-14'),
(133, 33, '20', 1, '16.00', '4.00', '2017-11-16'),
(134, 34, '20', 1, '16.00', '4.00', '2017-11-17');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 1, 3, 1),
(2, 2, 3, 1),
(3, 4, 4, 1),
(4, 6, 45, 1),
(5, 7, 45, 1),
(6, 8, 47, 1),
(7, 11, 402, 1),
(8, 9, 180, 1),
(9, 17, 559, 1),
(10, 22, 91, 1),
(11, 27, 359, 1),
(12, 26, 358, 1),
(13, 28, 94, 1),
(14, 30, 166, 1),
(15, 31, 243, 1),
(16, 33, 612, 1),
(17, 34, 623, 1),
(18, 32, 624, 1),
(19, 37, 165, 1),
(20, 38, 245, 1),
(21, 39, 180, 1),
(22, 40, 244, 1),
(23, 41, 229, 1),
(24, 42, 381, 1),
(25, 44, 234, 1),
(26, 29, 346, 1),
(27, 47, 375, 1),
(28, 46, 267, 1),
(29, 48, 386, 1),
(30, 45, 291, 1),
(31, 21, 343, 1),
(32, 49, 350, 1),
(33, 60, 362, 1),
(34, 53, 373, 1),
(35, 66, 378, 1),
(36, 71, 387, 1),
(37, 67, 390, 1),
(38, 72, 432, 1),
(39, 73, 604, 1),
(40, 75, 406, 1),
(41, 76, 458, 1),
(42, 77, 544, 1),
(43, 80, 485, 1),
(44, 81, 542, 1),
(45, 83, 585, 1),
(46, 84, 588, 1),
(47, 86, 596, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', '');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 6, '', '-29.6137542268127', '30.3684371709824', 'Eugene Marais Road\nNapierville, Pietermaritzburg, 3201', '-26.1161626392265', '28.0482630059123', '66 Ian Place\nChislehurston, Sandton, 2196', '', '11:06:08', '11:06:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.6137542268127,30.3684371709824&markers=color:red|label:D|-26.1161626392265,28.0482630059123&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(2, 6, '', '-29.6137542268127', '30.3684371709824', 'Eugene Marais Road\nNapierville, Pietermaritzburg, 3201', '-26.1161626392265', '28.0482630059123', '66 Ian Place\nChislehurston, Sandton, 2196', '', '11:08:19', '11:08:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.6137542268127,30.3684371709824&markers=color:red|label:D|-26.1161626392265,28.0482630059123&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(3, 6, '', '-26.0608049677702', '28.0623011755758', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.068918744692', '28.0524884909391', '9A North Road\nMorningside, Sandton, 2057', '', '17:15:41', '05:15:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.0608049677702,28.0623011755758&markers=color:red|label:D|-26.068918744692,28.0524884909391&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(4, 6, '', '-26.060780954375', '28.0622758076147', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0607995985132', '28.0626520514488', '7 5th Avenue\nEdenburg, Sandton, 2128', 'Sunday, Oct 1', '19:35:18', '07:35:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.060780954375,28.0622758076147&markers=color:red|label:D|-26.0607995985132,28.0626520514488&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(5, 10, '', '28.411467697180143', '77.04328056424856', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '14:56:27', '02:56:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.411467697180143,77.04328056424856&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(6, 10, '', '28.411467697180143', '77.04328056424856', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '14:56:32', '02:56:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.411467697180143,77.04328056424856&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(7, 10, '', '28.411467697180143', '77.04328056424856', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '14:57:54', '02:57:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.411467697180143,77.04328056424856&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(8, 11, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '14:31:03', '02:31:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(9, 11, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.462932631979015', '77.08659756928682', '6101, DLF Phase IV, Sector 27, Gurugram, Haryana 122022, India', 'Friday, Oct 6', '14:32:42', '02:32:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.462932631979015,77.08659756928682&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, 1, 1, 0, 1, 0, 1),
(10, 16, '', '-29.736316458872068', '31.061659790575508', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '', '', 'Set your drop point', '', '10:51:00', '10:51:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.736316458872068,31.061659790575508&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 3, 1, 1),
(11, 16, '', '-29.736321699141012', '31.06166180223227', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.818767', '31.011423999999998', '253 Peter Mokaba Rd', 'Thursday, Oct 12', '11:00:30', '11:00:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.736321699141012,31.06166180223227&markers=color:red|label:D|-29.818767,31.011423999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(12, 16, '', '-29.736321699141012', '31.06166180223227', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.7259994', '31.065828500000002', 'Gateway', 'Thursday, Oct 12', '11:17:45', '11:17:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.736321699141012,31.06166180223227&markers=color:red|label:D|-29.7259994,31.065828500000002&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(13, 17, '', '-33.9145354155857', '18.420993760228157', 'Helen Suzman Blvd, De Waterkant, Cape Town, 8001, South Africa', '-33.913109999999996', '18.54276', '3 Voortrekker Rd, Townsend Estate', '', '20:15:16', '08:15:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.9145354155857,18.420993760228157&markers=color:red|label:D|-33.913109999999996,18.54276&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(14, 15, '', '-29.62555269229388', '30.403768569231033', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.626340000000003', '30.413919999999994', '19 Medwood Pl', '', '09:28:00', '09:28:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.62555269229388,30.403768569231033&markers=color:red|label:D|-29.626340000000003,30.413919999999994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(15, 25, '', '28.4120770596907', '77.0432814197413', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4201395626342', '77.0532282069325', '75, Vikas Marg, Malibu Town, Sector 47\nGurugram, Haryana 122018', 'Monday, Oct 16', '11:14:30', '11:14:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120770596907,77.0432814197413&markers=color:red|label:D|28.4201395626342,77.0532282069325&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, 1, 1, 0, 1, 0, 1),
(16, 27, '', '28.4121192634714', '77.043312233357', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4423137547207', '77.0948691666126', 'Khatu Shyam Road, Parsvnath Exotica, DLF Phase 5, Sector 53\nGurugram, Haryana 122003', '', '15:26:08', '03:26:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4121192634714,77.043312233357&markers=color:red|label:D|28.4423137547207,77.0948691666126&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, 1, 1, 0, 1, 0, 1),
(17, 27, '', '28.5323885084396', '77.1945407241583', '249/5 B,Aurbindo Marg, Sri Aurobindo Marg, Block C, Mehrauli\nNew Delhi, Delhi 110030', '28.5464776486512', '77.2144011408091', '209, Shahpur Jat, Siri Fort\nNew Delhi, Delhi 110049', 'Monday, Oct 23', '06:30:45', '06:30:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.5323885084396,77.1945407241583&markers=color:red|label:D|28.5464776486512,77.2144011408091&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, 1, 1, 0, 1, 0, 1),
(18, 27, '', '28.5323885084396', '77.1945407241583', '249/5 B,Aurbindo Marg, Sri Aurobindo Marg, Block C, Mehrauli\nNew Delhi, Delhi 110030', '28.5464776486512', '77.2144011408091', '209, Shahpur Jat, Siri Fort\nNew Delhi, Delhi 110049', 'Monday, Oct 23', '06:30:48', '06:30:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.5323885084396,77.1945407241583&markers=color:red|label:D|28.5464776486512,77.2144011408091&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, 1, 1, 0, 1, 0, 1),
(19, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.45328394385306', '18.726691156625748', '115 Arcadia St, Malmesbury, 7299, South Africa', '', '19:16:19', '07:16:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.45328394385306,18.726691156625748&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 32, 1, 1, 0, 1, 0, 1),
(20, 33, '', '-33.897084345844284', '18.627562075853348', '2 Bloem St, Bosbell, Cape Town, 7530, South Africa', '-33.91694940000001', '18.3875487', 'Sea Point', '', '20:01:22', '08:01:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.897084345844284,18.627562075853348&markers=color:red|label:D|-33.91694940000001,18.3875487&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(21, 35, '', '-29.736349647237457', '31.06161955744028', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.849106300000003', '30.9900032', '25 Peter Mokaba Ridge', '', '14:12:24', '02:12:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.736349647237457,31.06161955744028&markers=color:red|label:D|-29.849106300000003,30.9900032&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 3, 0, 1),
(22, 33, '', '-33.921107927716214', '18.585752807557583', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '', '', 'Set your drop point', '', '14:37:37', '02:37:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.921107927716214,18.585752807557583&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(23, 33, '', '-33.92109763378299', '18.585752807557583', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.91021374670537', '18.629888221621513', 'Caledon St, Transnet, Cape Town, 7505, South Africa', '', '14:52:11', '02:52:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.92109763378299,18.585752807557583&markers=color:red|label:D|-33.91021374670537,18.629888221621513&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(24, 6, '', '-26.1232819739976', '28.0348324214581', '57 6th Road\nHyde Park, Sandton, 2196', '-26.149320512585', '28.0077264457941', '51 Marico Road\nEmmarentia, Randburg, 2195', '', '15:03:29', '03:03:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1232819739976,28.0348324214581&markers=color:red|label:D|-26.149320512585,28.0077264457941&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(25, 6, '', '-26.1232819739976', '28.0348324214581', '57 6th Road\nHyde Park, Sandton, 2196', '-26.149320512585', '28.0077264457941', '51 Marico Road\nEmmarentia, Randburg, 2195', '', '15:03:34', '03:03:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1232819739976,28.0348324214581&markers=color:red|label:D|-26.149320512585,28.0077264457941&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(26, 6, '', '-26.1232819739976', '28.0348324214581', '57 6th Road\nHyde Park, Sandton, 2196', '-26.149320512585', '28.0077264457941', '51 Marico Road\nEmmarentia, Randburg, 2195', 'Wednesday, Oct 25', '15:03:51', '03:03:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232819739976,28.0348324214581&markers=color:red|label:D|-26.149320512585,28.0077264457941&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 32, 1, 1, 0, 1, 0, 1),
(27, 6, '', '-26.1232819739976', '28.0348324214581', '57 6th Road\nHyde Park, Sandton, 2196', '-26.149320512585', '28.0077264457941', '51 Marico Road\nEmmarentia, Randburg, 2195', 'Wednesday, Oct 25', '15:03:55', '03:03:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232819739976,28.0348324214581&markers=color:red|label:D|-26.149320512585,28.0077264457941&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 32, 1, 1, 0, 1, 0, 1),
(28, 6, '', '-26.1232819739976', '28.0348324214581', '57 6th Road\nHyde Park, Sandton, 2196', '-26.149320512585', '28.0077264457941', '51 Marico Road\nEmmarentia, Randburg, 2195', '', '15:04:56', '03:04:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1232819739976,28.0348324214581&markers=color:red|label:D|-26.149320512585,28.0077264457941&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(29, 15, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870922973658924', '30.98042272031307', 'Princess Alice Ave, University, Berea, 4041, South Africa', '', '15:25:59', '03:25:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.870922973658924,30.98042272031307&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(30, 15, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870922973658924', '30.98042272031307', 'Princess Alice Ave, University, Berea, 4041, South Africa', '', '15:26:50', '03:26:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.870922973658924,30.98042272031307&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(31, 6, '', '-29.5904753143522', '30.3839822486043', '493 Boom Street\nPietermaritzburg, 3201', '-29.6042821025668', '30.3844114020467', '290 Burger Street\nPietermaritzburg, 3201', '', '12:57:35', '12:57:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.5904753143522,30.3839822486043&markers=color:red|label:D|-29.6042821025668,30.3844114020467&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(32, 47, '', '-30.048844568271395', '30.89059688150883', '97 Beach Rd, Amanzimtoti, 4126, South Africa', '-29.858680399999994', '31.0218404', 'Durban', '', '14:58:44', '02:58:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-30.048844568271395,30.89059688150883&markers=color:red|label:D|-29.858680399999994,31.0218404&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(33, 47, '', '-30.048844568271395', '30.89059688150883', '97 Beach Rd, Amanzimtoti, 4126, South Africa', '-29.858680399999994', '31.0218404', 'Durban', '', '14:59:46', '02:59:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-30.048844568271395,30.89059688150883&markers=color:red|label:D|-29.858680399999994,31.0218404&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(34, 35, '', '-30.04620938440872', '30.889632627367973', 'N2, Athlone Park, Amanzimtoti, 4126, South Africa', '-29.858680399999994', '31.0218404', 'Durban', '', '14:59:52', '02:59:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-30.04620938440872,30.889632627367973&markers=color:red|label:D|-29.858680399999994,31.0218404&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(35, 33, '', '-26.703420399999995', '27.8076959', 'Vanderbijlpark', '-26.5969312', '27.901465400000003', 'Vereeniging', '', '21:33:01', '09:33:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.703420399999995,27.8076959&markers=color:red|label:D|-26.5969312,27.901465400000003&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(36, 6, 'TAG4432y7R', '-26.1233659698035', '28.0346426004152', '55 6th Avenue\nHyde Park, Sandton, 2196', '-26.1861865146916', '27.9745149984956', '27 Bradley Crescent\nWestbury, Johannesburg, 2093', '', '10:43:33', '10:43:33 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1233659698035,28.0346426004152&markers=color:red|label:D|-26.1861865146916,27.9745149984956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(37, 6, 'TAG4432y7R', '-26.1233659698035', '28.0346426004152', '55 6th Avenue\nHyde Park, Sandton, 2196', '-26.1861865146916', '27.9745149984956', '27 Bradley Crescent\nWestbury, Johannesburg, 2093', '', '10:43:38', '10:43:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1233659698035,28.0346426004152&markers=color:red|label:D|-26.1861865146916,27.9745149984956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(38, 6, '', '-26.1232984059434', '28.0344989150763', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1809459304474', '27.9802703484893', '7 Toby Street\nMartindale, Johannesburg, 2092', '', '10:47:11', '10:47:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1232984059434,28.0344989150763&markers=color:red|label:D|-26.1809459304474,27.9802703484893&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(39, 6, '', '-26.1248875177438', '28.0307853966951', '9 Portland Avenue\nCraighall Park, Randburg, 2196', '-26.1449360431111', '28.0271955952048', '30 7th Avenue\nRandburg, 2193', '', '11:40:15', '11:40:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.1248875177438,28.0307853966951&markers=color:red|label:D|-26.1449360431111,28.0271955952048&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(40, 33, '', '-33.89703787093072', '18.628573939204216', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.924868499999995', '18.4240553', 'Cape Town', '', '16:28:38', '04:28:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.89703787093072,18.628573939204216&markers=color:red|label:D|-33.924868499999995,18.4240553&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(41, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', '', '07:42:16', '07:42:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(42, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', '', '07:42:24', '07:42:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(43, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Monday, Nov 6', '07:56:12', '07:56:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(44, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '08:52:01', '08:52:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 2, 0, 1),
(45, 6, '', '-26.0146340554599', '28.1005698690848', 'Jukskei View Drive\nWaterval 5-Ir, Midrand, 2090', '-26.13032', '28.03148', '1 Bompas Rd', '', '09:05:35', '09:05:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.0146340554599,28.1005698690848&markers=color:red|label:D|-26.13032,28.03148&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(46, 15, '', '-29.624368827599778', '30.394553169608116', '11 Oribi Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.612060124256494', '30.403761193156242', '14 Sanders Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '', '09:13:41', '09:13:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.624368827599778,30.394553169608116&markers=color:red|label:D|-29.612060124256494,30.403761193156242&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(47, 15, '', '-29.624368827599778', '30.394553169608116', '11 Oribi Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.612060124256494', '30.403761193156242', '14 Sanders Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '', '09:15:46', '09:15:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.624368827599778,30.394553169608116&markers=color:red|label:D|-29.612060124256494,30.403761193156242&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(48, 15, '', '-29.625822280413907', '30.403006821870804', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.615095322565363', '30.410727895796295', '7 Hesketh Dr, Hayfields, Pietermaritzburg, 3201, South Africa', '', '09:43:39', '09:43:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.625822280413907,30.403006821870804&markers=color:red|label:D|-29.615095322565363,30.410727895796295&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(49, 15, '', '-29.6244183741608', '30.403072871267792', '25 Shores Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.61404425011876', '30.40676660835743', 'N3 & R56 & N3, Scottsville, Pietermaritzburg, 3201, South Africa', '', '10:06:22', '10:06:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.6244183741608,30.403072871267792&markers=color:red|label:D|-29.61404425011876,30.40676660835743&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(50, 15, '', '-29.792557294126123', '30.740168541669846', '450 Kassier Rd, Summerveld, Outer West Durban, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', '', '11:39:57', '11:39:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-29.792557294126123,30.740168541669846&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(51, 33, '', '-33.921083444845976', '18.585704527795315', '34 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.92050253104291', '18.58679685741663', '33 Glenhurst St, Beaconvale, Cape Town, 7500, South Africa', '', '06:40:03', '06:40:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-33.921083444845976,18.585704527795315&markers=color:red|label:D|-33.92050253104291,18.58679685741663&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(52, 33, '', '-26.109322699999996', '28.053005', 'Sandton City', '-26.092353899999996', '28.003415500000003', 'Randburg Square', '', '21:45:27', '09:45:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-26.109322699999996,28.053005&markers=color:red|label:D|-26.092353899999996,28.003415500000003&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(53, 51, '', '-25.929133711974416', '28.14596381038427', 'Samrand Ave, Samrand Business Park, Centurion, 0187, South Africa', '', '', 'Set your drop point', '', '07:20:01', '07:20:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-25.929133711974416,28.14596381038427&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(54, 51, '', '-25.8482965233809', '28.236289210617542', '18 Beves Ave, Pierre van Ryneveld, Centurion, 0045, South Africa', '', '', 'Set your drop point', '', '18:54:00', '06:54:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-25.8482965233809,28.236289210617542&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<p class=\"MsoNormal\" style=\"margin-top:15.0pt;margin-right:0cm;margin-bottom:\r\n7.5pt;margin-left:0cm;line-height:normal;mso-outline-level:2\"><b><span style=\"font-size:16.0pt;font-family:\r\n\" inherit\",serif;mso-fareast-font-family:\"times=\"\" new=\"\" roman\";mso-bidi-font-family:=\"\" \"times=\"\" roman\";color:#203864;mso-themecolor:accent1;mso-themeshade:128;=\"\" mso-style-textfill-fill-color:#203864;mso-style-textfill-fill-themecolor:accent1;=\"\" mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:=\"\" lumm=\"50000;mso-fareast-language:EN-ZA\" \"=\"\">Tag Your Ride (Pty) Ltd<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:15.0pt;margin-right:0cm;margin-bottom:\r\n7.5pt;margin-left:0cm;line-height:normal;mso-outline-level:2\"><b><span style=\"font-size:16.0pt;mso-fareast-font-family:\r\n\" times=\"\" new=\"\" roman\";mso-fareast-theme-font:major-fareast;mso-bidi-font-family:=\"\" calibri;mso-bidi-theme-font:minor-latin;color:#1f3864;mso-themecolor:accent1;=\"\" mso-themeshade:128\"=\"\">About Us<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\r\nnormal;mso-outline-level:2\"><span style=\"font-size:13.0pt;mso-fareast-font-family:\r\n\" times=\"\" new=\"\" roman\";mso-fareast-theme-font:major-fareast;mso-bidi-font-family:=\"\" calibri;mso-bidi-theme-font:minor-latin;color:#1f3864;mso-themecolor:accent1;=\"\" mso-themeshade:128\"=\"\">Tag Your Ride (Pty) Ltd is a privately owned, proudly South\r\nAfrican transportation technology company founded in early 2017. Tag Your Ride is a smartphone-enabled \'Ride-Hailing\' service alternative to taxi cabs.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:15.0pt;margin-right:0cm;margin-bottom:\r\n7.5pt;margin-left:0cm;line-height:normal;mso-outline-level:3\"><span style=\"font-size:13.0pt;mso-fareast-font-family:\" times=\"\" new=\"\" roman\";mso-fareast-theme-font:=\"\" major-fareast;mso-bidi-font-family:calibri;mso-bidi-theme-font:minor-latin;=\"\" color:#1f3864;mso-themecolor:accent1;mso-themeshade:128\"=\"\">For more information,\r\nyou can catch us on :<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\r\nnormal;mso-outline-level:3\"><span style=\"font-size:13.0pt;mso-fareast-font-family:\r\n\" times=\"\" new=\"\" roman\";mso-fareast-theme-font:major-fareast;mso-bidi-font-family:=\"\" calibri;mso-bidi-theme-font:minor-latin;color:#1f3864;mso-themecolor:accent1;=\"\" mso-themeshade:128\"=\"\">Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; info@tagyourride.co.za<br>\r\nPhone : &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +27 31 206\r\n0012<br>\r\nWebsite:&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=\"http://www.tagyourride.co.za\"><span style=\"color: rgb(31, 56, 100);\">www.tagyourride.co.za</span></a>\r\n<o:p></o:p></span></p>', '', '', '', ''),
(2, 'Help Center', 'Rogerant Tshibangu', '<p>+27 31 206 0012</p><p><span style=\"color: rgb(242, 242, 242); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 15.6px;=\"\" background-color:=\"\" rgb(50,=\"\" 50,=\"\" 50);\"=\"\"><br></span></p>', '', '', '', ''),
(3, 'Tag', 'Terms and Conditions', '<div class=\"modal-header\" style=\"min-height: 16.4286px; color: rgb(50, 50, 50); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 13px;\"=\"\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px;\"><div class=\"description col-sm-6\" style=\"padding-right: 15px; padding-left: 15px; width: 449px;\"><h3 class=\"modal-title\" style=\"font-family: OstrichSansRoundedMedium; line-height: 1.42857;\">Tag Your Ride</h3><h4 class=\"modal-title\" style=\"font-family: OstrichSansRoundedMedium; line-height: 1.42857;\">Terms &amp; Conditions</h4></div><br></div></div><div class=\"modal-body\" style=\"padding: 20px; color: rgb(50, 50, 50); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 13px;\"=\"\"><p class=\"description\" style=\"margin-bottom: 18px;\">Last updated: October 18th, 2017<br><br><strong>1. Contractual Relationship</strong><br><br>These Terms of Use (â€œTermsâ€) govern the access or use by you, an individual, from within any country in the world of applications, websites, content, products, and services (the â€œServicesâ€) made available by Tag Your Ride, a private limited liability company established in South Africa, having its offices at 476 Bartle Rd, Umbilo, Durban, KwaZulu-Natal with registration number 2017/115075/07.&nbsp;<br><br>PLEASE READ THESE TERMS CAREFULLY BEFORE ACCESSING OR USING THE SERVICES.&nbsp;<br><br>Your access and use of the Services constitutes your agreement to be bound by these Terms, which establishes a contractual relationship between you and Tag Your Ride. If you do not agree to these Terms, you may not access or use the Services. These Terms expressly supersede prior agreements or arrangements with you. Tag Your Ride may immediately terminate these Terms or any Services with respect to you, or generally cease offering or deny access to the Services or any portion thereof, at any time for any reason.&nbsp;<br><br>Supplemental terms may apply to certain Services, such as policies for a particular event, activity or promotion, and such supplemental terms will be disclosed to you in connection with the applicable Services. Supplemental terms are in addition to, and shall be deemed a part of, the Terms for the purposes of the applicable Services. Supplemental terms shall prevail over these Terms in the event of a conflict with respect to the applicable Services.&nbsp;<br><br>Tag Your Ride may amend the Terms related to the Services from time to time. Amendments will be effective upon Tag Your Rideâ€™s posting of such updated Terms at this location or the amended policies or supplemental terms on the applicable Service. Your continued access or use of the Services after such posting constitutes your consent to be bound by the Terms, as amended.&nbsp;<br><br>Our collection and use of personal information in connection with the Services is as provided in Tag Your Rideâ€™s Privacy Policy located at&nbsp;<a href=\"http://www.tagyourride.co.za/#privacy_policy\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(66, 139, 202);\">www.tagyourride.co.za/privacy_policy</a>. Tag Your Ride may provide to a claims processor or an insurer any necessary information (including your contact information) if there is a complaint, dispute or conflict, which may include an accident, involving you and a Third Party Provider (including a transportation network company driver) and such information or data is necessary to resolve the complaint, dispute or conflict.&nbsp;<br><br><strong>2. The Services</strong>&nbsp;<br><br>The Services constitute a technology platform that enables users of Tag Your Rideâ€™s mobile applications or websites provided as part of the Services (each, an â€œApplicationâ€) to arrange and schedule transportation and/or logistics services with independent third party providers of such services, including independent third party transportation providers and independent third party logistics providers under agreement with Tag Your Ride or certain of Tag Your Rideâ€™s affiliates (â€œThird Party Providersâ€). Unless otherwise agreed by Tag Your Ride in a separate written agreement with you, the Services are made available solely for your personal, noncommercial use. YOU ACKNOWLEDGE THAT TAG YOUR RIDE DOES NOT PROVIDE TRANSPORTATION OR LOGISTICS SERVICES OR FUNCTION AS A TRANSPORTATION CARRIER AND THAT ALL SUCH TRANSPORTATION OR LOGISTICS SERVICES ARE PROVIDED BY INDEPENDENT THIRD PARTY CONTRACTORS WHO ARE NOT EMPLOYED BY TAG YOUR RIDE OR ANY OF ITS AFFILIATES.&nbsp;<br><br><strong>License.</strong>&nbsp;<br><br>Subject to your compliance with these Terms, Tag Your Ride grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferrable license to: (i) access and use the Applications on your personal device solely in connection with your use of the Services; and (ii) access and use any content, information and related materials that may be made available through the Services, in each case solely for your personal, noncommercial use. Any rights not expressly granted herein are reserved by Tag Your Ride and Tag Your Rideâ€™s licensors.&nbsp;<br><br><strong>Restrictions.</strong>&nbsp;<br><br>You may not: (i) remove any copyright, trademark or other proprietary notices from any portion of the Services; (ii) reproduce, modify, prepare derivative works based upon, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services except as expressly permitted by Tag Your Ride; (iii) decompile, reverse engineer or disassemble the Services except as may be permitted by applicable law; (iv) link to, mirror or frame any portion of the Services; (v) cause or launch any programs or scripts for the purpose of scraping, indexing, surveying, or otherwise data mining any portion of the Services or unduly burdening or hindering the operation and/or functionality of any aspect of the Services; or (vi) attempt to gain unauthorized access to or impair any aspect of the Services or its related systems or networks.&nbsp;<br><br><strong>Provision of the Services.</strong>&nbsp;<br><br>You acknowledge that portions of the Services may be made available under Tag Your Rideâ€™s various brands or request options associated with transportation or logistics, including the transportation request brand currently referred to as â€œTag Your Rideâ€,You also acknowledge that the Services may be made available under such brands or request options by or in connection with: (i) certain of Tag Your Rideâ€™s subsidiaries and affiliates; or (ii) independent Third Party Providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorizations or licenses.&nbsp;<br><br><strong>Third Party Services and Content.</strong>&nbsp;<br><br>The Services may be made available or accessed in connection with third party services and content (including advertising) that Tag Your Ride does not control. You acknowledge that different terms of use and privacy policies may apply to your use of such third party services and content. Tag Your Ride does not endorse such third party services and content and in no event shall Tag Your Ride be responsible or liable for any products or services of such third party providers. Additionally, Apple Inc., Google, Inc., Microsoft Corporation or BlackBerry Limited and/or their applicable international subsidiaries and affiliates will be third-party beneficiaries to this contract if you access the Services using Applications developed for Apple iOS, Android, Microsoft Windows, or Blackberry-powered mobile devices, respectively. These third party beneficiaries are not parties to this contract and are not responsible for the provision or support of the Services in any manner. Your access to the Services using these devices is subject to terms set forth in the applicable third party beneficiaryâ€™s terms of service.&nbsp;<br><br><strong>Ownership.</strong>&nbsp;<br><br>The Services and all rights therein are and shall remain Tag Your Rideâ€™s property or the property of Tag Your Rideâ€™s licensors. Neither these Terms nor your use of the Services convey or grant to you any rights: (i) in or related to the Services except for the limited license granted above; or (ii) to use or reference in any manner Tag Your Rideâ€™s company names, logos, product and service names, trademarks or services marks or those of Tag Your Rideâ€™s licensors.&nbsp;<br><br><strong>3. Your Use of the Services</strong>&nbsp;<br><br><strong>User Accounts.</strong>&nbsp;<br><br>In order to use most aspects of the Services, you must register for and maintain an active personal user Services account (â€œAccountâ€). You must be at least 18 years of age, or the age of legal majority in your jurisdiction (if different than 18), to obtain an Account. Account registration requires you to submit to Tag Your Ride certain personal information, such as your name, address, mobile phone number and age, as well as at least one valid payment method (either a credit card or accepted payment partner). You agree to maintain accurate, complete, and up-to-date information in your Account. Your failure to maintain accurate, complete, and up-to-date Account information, including having an invalid or expired payment method on file, may result in your inability to access and use the Services or Tag Your Rideâ€™s termination of these Terms with you. You are responsible for all activity that occurs under your Account, and you agree to maintain the security and secrecy of your Account username and password at all times. Unless otherwise permitted by Tag Your Ride in writing, you may only possess one Account.&nbsp;<br><br><strong>User Requirements and Conduct.</strong>&nbsp;<br><br>The Service is not available for use by persons under the age of 18. You may not authorize third parties to use your Account, and you may not allow persons under the age of 18 to receive transportation or logistics services from Third Party Providers unless they are accompanied by you. You may not assign or otherwise transfer your Account to any other person or entity. You agree to comply with all applicable laws when using the Services, and you may only use the Services for lawful purposes (e.g., no transport of unlawful or hazardous materials). You will not, in your use of the Services, cause nuisance, annoyance, inconvenience, or property damage, whether to the Third Party Provider or any other party. In certain instances you may be asked to provide proof of identity to access or use the Services, and you agree that you may be denied access to or use of the Services if you refuse to provide proof of identity.&nbsp;<br><br><strong>Text Messaging.</strong>&nbsp;<br><br>By creating an Account, you agree that the Services may send you text (SMS) messages as part of the normal business operation of your use of the Services. You may opt-out of receiving text (SMS) messages from Tag Your Ride at any time by sending us an email at&nbsp;<a href=\"http://www.tagyourride.co.za/#contact-us\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(66, 139, 202);\">www.tagyourride.co.za/contact-us</a>. You acknowledge that opting out of receiving text (SMS) messages may impact your use of the Services.&nbsp;<br><br><strong>Promotional Codes.</strong>&nbsp;<br><br>Tag Your Ride may, in Tag Your Rideâ€™s sole discretion, create promotional codes that may be redeemed for Account credit, or other features or benefits related to the Services and/or a Third Party Providerâ€™s services, subject to any additional terms that Tag Your Ride establishes on a per promotional code basis (â€œPromo Codesâ€). You agree that Promo Codes: (i) must be used for the intended audience and purpose, and in a lawful manner; (ii) may not be duplicated, sold or transferred in any manner, or made available to the general public (whether posted to a public form or otherwise), unless expressly permitted by Tag Your Ride; (iii) may be disabled by Tag Your Ride at any time for any reason without liability to Tag Your Ride; (iv) may only be used pursuant to the specific terms that Tag Your Ride establishes for such Promo Code; (v) are not valid for cash; and (vi) may expire prior to your use. Tag Your Ride reserves the right to withhold or deduct credits or other features or benefits obtained through the use of Promo Codes by you or any other user in the event that Tag Your Ride determines or believes that the use or redemption of the Promo Code was in error, fraudulent, illegal, or in violation of the applicable Promo Code terms or these Terms.&nbsp;<br><br><strong>User Provided Content.</strong>&nbsp;<br><br>Tag Your Ride may, in Tag Your Rideâ€™s sole discretion, permit you from time to time to submit, upload, publish or otherwise make available to Tag Your Ride through the Services textual, audio, and/or visual content and information, including commentary and feedback related to the Services, initiation of support requests, and submission of entries for competitions and promotions (â€œUser Contentâ€). Any User Content provided by you remains your property. However, by providing User Content to Tag Your Ride, you grant Tag Your Ride a worldwide, perpetual, irrevocable, transferrable, royalty-free license, with the right to sublicense, to use, copy, modify, create derivative works of, distribute, publicly display, publicly perform, and otherwise exploit in any manner such User Content in all formats and distribution channels now known or hereafter devised (including in connection with the Services and Tag Your Rideâ€™s business and on third-party sites and services), without further notice to or consent from you, and without the requirement of payment to you or any other person or entity.&nbsp;<br><br>You represent and warrant that: (i) you either are the sole and exclusive owner of all User Content or you have all rights, licenses, consents and releases necessary to grant Tag Your Ride the license to the User Content as set forth above; and (ii) neither the User Content nor your submission, uploading, publishing or otherwise making available of such User Content nor Tag Your Rideâ€™s use of the User Content as permitted herein will infringe, misappropriate or violate a third partyâ€™s intellectual property or proprietary rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.&nbsp;<br><br>You agree to not provide User Content that is defamatory, libelous, hateful, violent, obscene, pornographic, unlawful, or otherwise offensive, as determined by Tag Your Ride in its sole discretion, whether or not such material may be protected by law. Tag Your Ride may, but shall not be obligated to, review, monitor, or remove User Content, at Tag Your Rideâ€™s sole discretion and at any time and for any reason, without notice to you.&nbsp;<br><br><strong>Network Access and Devices.</strong>&nbsp;<br><br>You are responsible for obtaining the data network access necessary to use the Services. Your mobile networkâ€™s data and messaging rates and fees may apply if you access or use the Services from a wireless-enabled device and you shall be responsible for such rates and fees. You are responsible for acquiring and updating compatible hardware or devices necessary to access and use the Services and Applications and any updates thereto. Tag Your Ride does not guarantee that the Services, or any portion thereof, will function on any particular hardware or devices. In addition, the Services may be subject to malfunctions and delays inherent in the use of the Internet and electronic communications.&nbsp;<br><br><strong>4. Payment</strong>&nbsp;<br><br>You understand that use of the Services may result in charges to you for the services or goods you receive from a Third Party Provider (â€œChargesâ€). After you have received services or goods obtained through your use of the Service, Tag Your Ride will facilitate your payment of the applicable Charges on behalf of the Third Party Provider as such Third Party Providerâ€™s limited payment collection agent. Payment of the Charges in such manner shall be considered the same as payment made directly by you to the Third Party Provider. Charges may include other applicable fees, tolls, and/or surcharges including a booking fee, national, provincial and municipal tolls, airport surcharges and processing fees for split payments, and will be inclusive of applicable taxes where required by law. Charges paid by you are final and non-refundable, unless otherwise determined by Tag Your Ride. You retain the right to request lower Charges from a Third Party Provider for services or goods received by you from such Third Party Provider at the time you receive such services or goods. Tag Your Ride will respond accordingly to any request from a Third Party Provider to modify the Charges for a particular service or good.&nbsp;<br><br>All Charges are due immediately and payment will be facilitated by Tag Your Ride using the preferred payment method designated in your Account, after which Tag Your Ride will send you a receipt by email. If your primary Account payment method is determined to be expired, invalid or otherwise not able to be charged, you agree that Tag Your Ride may, as the Third Party Providerâ€™s limited payment collection agent, use a secondary payment method in your Account, if available.&nbsp;<br><br>As between you and Tag Your Ride, Tag Your Ride reserves the right to establish, remove and/or revise Charges for any or all services or goods obtained through the use of the Services at any time in Tag Your Rideâ€™s sole discretion. Further, you acknowledge and agree that Charges applicable in certain geographical areas may increase substantially during times of high demand. Tag Your Ride will use reasonable efforts to inform you of Charges that may apply, provided that you will be responsible for Charges incurred under your Account regardless of your awareness of such Charges or the amounts thereof. Tag Your Ride may from time to time provide certain users with promotional offers and discounts that may result in different amounts charged for the same or similar services or goods obtained through the use of the Services, and you agree that such promotional offers and discounts, unless also made available to you, shall have no bearing on your use of the Services or the Charges applied to you. You may elect to cancel your request for services or goods from a Third Party Provider at any time prior to such Third Party Providerâ€™s arrival, in which case you may be charged a cancellation fee.&nbsp;<br><br>This payment structure is intended to fully compensate the Third Party Provider for the services or goods provided. Except with respect to taxicab transportation services requested through the Application, Tag Your Ride does not designate any portion of your payment as a tip or gratuity to the Third Party Provider. Any representation by Tag Your Ride (on Tag Your Rideâ€™s website, in the Application, or in Tag Your Rideâ€™s marketing materials) to the effect that tipping is â€œvoluntary,â€ â€œnot required,â€ and/or â€œincludedâ€ in the payments you make for services or goods provided is not intended to suggest that Tag Your Ride provides any additional amounts, beyond those described above, to the Third Party Provider. You understand and agree that, while you are free to provide additional payment as a gratuity to any Third Party Provider who provides you with services or goods obtained through the Service, you are under no obligation to do so. Gratuities are voluntary. After you have received services or goods obtained through the Service, you will have the opportunity to rate your experience and leave additional feedback about your Third Party Provider.&nbsp;<br><br><strong>Repair or Cleaning Fees.</strong>&nbsp;<br><br>You shall be responsible for the cost of repair for damage to, or necessary cleaning of, Third Party Provider vehicles and property resulting from use of the Services under your Account in excess of normal â€œwear and tearâ€ damages and necessary cleaning (â€œRepair or Cleaningâ€). In the event that a Third Party Provider reports the need for Repair or Cleaning, and such Repair or Cleaning request is verified by Tag Your Ride in Tag Your Rideâ€™s reasonable discretion, Tag Your Ride reserves the right to facilitate payment for the reasonable cost of such Repair or Cleaning on behalf of the Third Party Provider using your payment method designated in your Account. Such amounts will be transferred by Tag Your Ride to the applicable Third Party Provider and are non-refundable.&nbsp;<br><br><strong>5. Disclaimers; Limitation of Liability; Indemnity.</strong>&nbsp;<br><br><strong>DISCLAIMER.</strong>&nbsp;<br><br>THE SERVICES ARE PROVIDED â€œAS ISâ€ AND â€œAS AVAILABLE.â€ TAG YOUR RIDE DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN ADDITION, TAG YOUR RIDE MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF THE SERVICES OR ANY SERVICES OR GOODS REQUESTED THROUGH THE USE OF THE SERVICES, OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. TAG YOUR RIDE DOES NOT GUARANTEE THE QUALITY, SUITABILITY, SAFETY OR ABILITY OF THIRD PARTY PROVIDERS. YOU AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE SERVICES, AND ANY SERVICE OR GOOD REQUESTED IN CONNECTION THEREWITH, REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.&nbsp;<br><br><strong>LIMITATION OF LIABILITY.</strong>&nbsp;<br><br>TAG YOUR RIDE SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOST DATA, PERSONAL INJURY OR PROPERTY DAMAGE RELATED TO, IN CONNECTION WITH, OR OTHERWISE RESULTING FROM ANY USE OF THE SERVICES, EVEN IF TAG YOUR RIDE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. TAG YOUR RIDE SHALL NOT BE LIABLE FOR ANY DAMAGES, LIABILITY OR LOSSES ARISING OUT OF: (i) YOUR USE OF OR RELIANCE ON THE SERVICES OR YOUR INABILITY TO ACCESS OR USE THE SERVICES; OR (ii) ANY TRANSACTION OR RELATIONSHIP BETWEEN YOU AND ANY THIRD PARTY PROVIDER, EVEN IF TAG YOUR RIDE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. TAG YOUR RIDE SHALL NOT BE LIABLE FOR DELAY OR FAILURE IN PERFORMANCE RESULTING FROM CAUSES BEYOND TAG YOUR RIDEâ€™S REASONABLE CONTROL. YOU ACKNOWLEDGE THAT THIRD PARTY TRANSPORTATION PROVIDERS PROVIDING TRANSPORTATION SERVICES REQUESTED THROUGH SOME REQUEST BRANDS MAY OFFER RIDESHARING OR PEER-TO-PEER TRANSPORTATION SERVICES AND MAY NOT BE PROFESSIONALLY LICENSED OR PERMITTED. IN NO EVENT SHALL TAG YOUR RIDEâ€™S TOTAL LIABILITY TO YOU IN CONNECTION WITH THE SERVICES FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION EXCEED ONE THOUSAND FIVE HUNDRED RANDS (R1500).&nbsp;<br><br>TAG YOUR RIDEâ€™S SERVICES MAY BE USED BY YOU TO REQUEST AND SCHEDULE TRANSPORTATION, GOODS OR LOGISTICS SERVICES WITH THIRD PARTY PROVIDERS, BUT YOU AGREE THAT TAG YOUR RIDE HAS NO RESPONSIBILITY OR LIABILITY TO YOU RELATED TO ANY TRANSPORTATION, GOODS OR LOGISTICS SERVICES PROVIDED TO YOU BY THIRD PARTY PROVIDERS OTHER THAN AS EXPRESSLY SET FORTH IN THESE TERMS.&nbsp;<br><br>THE LIMITATIONS AND DISCLAIMER IN THIS SECTION 5 DO NOT PURPORT TO LIMIT LIABILITY OR ALTER YOUR RIGHTS AS A CONSUMER THAT CANNOT BE EXCLUDED UNDER APPLICABLE LAW.&nbsp;<br><br><strong>Indemnity.</strong>&nbsp;<br><br>You agree to indemnify and hold Tag Your Ride and its officers, directors, employees and agents harmless from any and all claims, demands, losses, liabilities, and expenses (including attorneysâ€™ fees) arising out of or in connection with: (i) your use of the Services or services or goods obtained through your use of the Services; (ii) your breach or violation of any of these Terms; (iii) Tag Your Rideâ€™s use of your User Content; or (iv) your violation of the rights of any third party, including Third Party Providers.&nbsp;<br><br><strong>6. Lost Property.</strong>&nbsp;<br><br>You understand and agree that it is your responsibility to ensure that you remove your property from the vehicle of a Third Party Provider when disembarking. Should you leave your property in the vehicle of a Third Party Provider, the Third Party Provider may hand over your property to you, Tag Your Ride or to the Tag Your Ride local service entity.&nbsp;<br><br>Whilst you may expect Third Party Providers to hand over your property to you, the offices of Tag Your Ride or the Tag Your Ride local service entity, Tag Your Ride or the Tag Your Ride local service entity shall not be held liable in the event of the Third Party Provider not handing over your property as expected. Moreover, Tag Your Ride or the Tag Your Ride local service entity shall not be liable for the loss or damage to your property whilst it is in transit.&nbsp;<br><br>Whilst Tag Your Ride or the Tag Your Ride local service entity will take reasonable steps to establish the owner of property left in a Third Party Providerâ€™s vehicle if returned to the offices of Tag Your Ride or the Tag Your Ride local service entity, when your property is in Tag Your Rideâ€™s or the Tag Your Ride local service entityâ€™s possession, you understand and agree that: (i) Tag Your Ride or the Tag Your Ride local service entity will only keep your property in its possession for a maximum period of three months from the date on which the Third Party Provider handed your property to Tag Your Ride or the Tag Your Ride local service entity; and (ii) should you fail to collect your property from Tag Your Ride or the Tag Your Ride local service entity before the expiry of the three month period stipulated, Tag Your Ride or the Tag Your Ride local service entity will be entitled to deal with your property as it deems fit and you shall have no claim whatsoever against Tag Your Ride or the Tag Your Ride local service entity in respect of your unclaimed property.&nbsp;<br><br><strong>7. Governing Law; Arbitration.</strong>&nbsp;<br><br>Except as otherwise set forth in these Terms, these Terms shall be exclusively governed by and construed in accordance with the laws of South Africa, excluding its rules on conflicts of laws. The Vienna Convention on the International Sale of Goods of 1980 (CISG) shall not apply. Any dispute, conflict, claim or controversy arising out of or broadly in connection with or relating to the Services or these Terms, including those relating to its validity, its construction or its enforceability (any â€œDisputeâ€) shall be first mandatorily submitted to mediation proceedings under the International Chamber of Commerce Mediation Rules (â€œICC Mediation Rulesâ€). If such Dispute has not been settled within sixty (60) days after a request for mediation has been submitted under such ICC Mediation Rules, such Dispute can be referred to and shall be exclusively and finally resolved by arbitration under the Rules of Arbitration of the International Chamber of Commerce (â€œICC Arbitration Rulesâ€). The ICC Rules\' Emergency Arbitrator provisions are excluded. The Dispute shall be resolved by one (1) arbitrator to be appointed in accordance with the ICC Rules. The place of both mediation and arbitration shall be Amsterdam, The Netherlands, without prejudice to any rights you may have under Article 18 of the Brussels I bis Regulation (OJ EU 2012 L351/1) and/or Articles relevant under the South African Civil Code. The language of the mediation and/or arbitration shall be English, unless you do not speak English, in which case the mediation and/or arbitration shall be conducted in both English and your native language. The existence and content of the mediation and arbitration proceedings, including documents and briefs submitted by the parties, correspondence from and to the International Chamber of Commerce, correspondence from the mediator, and correspondence, orders and awards issued by the sole arbitrator, shall remain strictly confidential and shall not be disclosed to any third party without the express written consent from the other party unless: (i) the disclosure to the third party is reasonably required in the context of conducting the mediation or arbitration proceedings; and (ii) the third party agrees unconditionally in writing to be bound by the confidentiality obligation stipulated herein.&nbsp;<br><br><strong>8. Other Provisions.</strong>&nbsp;<br><br><strong>Claims of Copyright Infringement.</strong>&nbsp;<br><br>Claims of copyright infringement should be sent to Tag Your Rideâ€™s designated agent. Please visit Tag Your Rideâ€™s web page at&nbsp;<a href=\"http://www.tagyourride.co.za/#contact-us\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(66, 139, 202);\">www.tagyourride.co.za/contact-us</a>&nbsp;for the designated address and additional information.&nbsp;<br><br><strong>Notice.</strong>&nbsp;<br><br>Tag Your Ride may give notice by means of a general notice on the Services, electronic mail to your email address in your Account, or by written communication sent to your address as set forth in your Account. You may give notice to Tag Your Ride by written communication to Tag Your Ride\'s address at 476 Bartle Rd, Umbilo, Durban, KwaZulu-Natal.&nbsp;<br><br><strong>General.</strong>&nbsp;<br><br>You may not assign or transfer these Terms in whole or in part without Tag Your Rideâ€™s prior written approval. You give your approval to Tag Your Ride for it to assign or transfer these Terms in whole or in part, including to: (i) a subsidiary or affiliate; (ii) an acquirer of Tag Your Rideâ€™s equity, business or assets; or (iii) a successor by merger. No joint venture, partnership, employment or agency relationship exists between you, Tag Your Ride or any Third Party Provider as a result of the contract between you and Tag Your Ride or use of the Services.&nbsp;<br><br>If any provision of these Terms is held to be illegal, invalid or unenforceable, in whole or in part, under any law, such provision or part thereof shall to that extent be deemed not to form part of these Terms but the legality, validity and enforceability of the other provisions in these Terms shall not be affected. In that event, the parties shall replace the illegal, invalid or unenforceable provision or part thereof with a provision or part thereof that is legal, valid and enforceable and that has, to the greatest extent possible, a similar effect as the illegal, invalid or unenforceable provision or part thereof, given the contents and purpose of these Terms. These Terms constitute the entire agreement and understanding of the parties with respect to its subject matter and replaces and supersedes all prior or contemporaneous agreements or undertakings regarding such subject matter. In these Terms, the words â€œincludingâ€ and â€œincludeâ€ mean â€œincluding, but not limited to.â€</p></div>', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `pay_date` varchar(655) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`, `pay_date`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '100', 'Wednesday, Sep 20', '1', '2017-09-20'),
(2, 2, 2, '1', 'Cash', 'Admin', '100', 'Friday, Sep 22', '1', '2017-09-22'),
(3, 3, 2, '1', 'Cash', 'Admin', '100', 'Friday, Sep 22', '1', ''),
(4, 4, 4, '1', 'Cash', 'Admin', '178', 'Friday, Sep 22', '1', ''),
(5, 5, 6, '1', 'Cash', 'Admin', '60', 'Friday, Sep 22', '1', ''),
(6, 6, 6, '1', 'Cash', 'Admin', '60', 'Friday, Sep 22', '1', ''),
(7, 7, 6, '1', 'Cash', 'Admin', '60', 'Friday, Sep 22', '1', ''),
(8, 8, 6, '1', 'Cash', 'Admin', '60', 'Friday, Sep 22', '1', ''),
(9, 9, 6, '1', 'Cash', 'Admin', '60', 'Friday, Sep 22', '1', ''),
(10, 10, 6, '1', 'Cash', 'Admin', '60', 'Friday, Sep 22', '1', ''),
(11, 11, 6, '1', 'Cash', 'Admin', '60', 'Sunday, Sep 24', '1', ''),
(12, 12, 7, '1', 'Cash', 'Admin', '60', 'Sunday, Sep 24', '1', ''),
(13, 13, 7, '1', 'Cash', 'Admin', '60', 'Sunday, Sep 24', '1', ''),
(14, 14, 6, '1', 'Cash', 'Admin', '60', 'Sunday, Sep 24', '1', ''),
(15, 15, 7, '1', 'Cash', 'Admin', '60', 'Sunday, Sep 24', '1', ''),
(16, 16, 6, '1', 'Cash', 'Admin', '60', 'Monday, Sep 25', '1', ''),
(17, 17, 6, '1', 'Cash', 'Admin', '60', 'Monday, Sep 25', '1', ''),
(18, 18, 6, '1', 'Cash', 'Admin', '60', 'Thursday, Sep 28', '1', ''),
(19, 19, 6, '1', 'Cash', 'Admin', '20', 'Thursday, Sep 28', '1', ''),
(20, 20, 3, '1', 'Cash', 'Admin', '178', 'Friday, Sep 29', '1', ''),
(21, 21, 3, '1', 'Cash', 'Admin', '178', 'Friday, Sep 29', '1', ''),
(22, 22, 6, '1', 'Cash', 'Admin', '5', 'Friday, Sep 29', '1', ''),
(23, 23, 6, '1', 'Cash', 'Admin', '5', 'Friday, Sep 29', '1', ''),
(24, 24, 7, '1', 'Cash', 'Admin', '5', 'Friday, Sep 29', '1', ''),
(25, 25, 7, '1', 'Cash', 'Admin', '5', 'Friday, Sep 29', '1', ''),
(26, 26, 6, '1', 'Cash', 'Ios', '5', '29 September 2017', 'Done', '2017-09-29'),
(27, 27, 7, '1', 'Cash', 'Admin', '5', 'Saturday, Sep 30', '1', ''),
(28, 28, 7, '1', 'Cash', 'Admin', '5', 'Saturday, Sep 30', '1', ''),
(29, 29, 7, '1', 'Cash', 'Admin', '10', 'Saturday, Sep 30', '1', ''),
(30, 30, 7, '1', 'Cash', 'Admin', '5', 'Saturday, Sep 30', '1', ''),
(31, 31, 6, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 1', '1', ''),
(32, 32, 6, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 1', '1', ''),
(33, 33, 6, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(34, 34, 8, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(35, 35, 6, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 3', '1', ''),
(36, 36, 8, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(37, 37, 6, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 3', '1', ''),
(38, 39, 9, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(39, 40, 9, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(40, 41, 7, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(41, 42, 7, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(42, 43, 7, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(43, 44, 9, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(44, 45, 9, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 3', '1', ''),
(45, 46, 9, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 3', '1', ''),
(46, 47, 9, '1', 'Cash', 'Admin', '25', 'Tuesday, Oct 3', '1', ''),
(47, 48, 7, '1', 'Cash', 'Admin', '228.3', 'Wednesday, Oct 4', '1', ''),
(48, 49, 9, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 4', '1', ''),
(49, 50, 12, '1', 'Cash', 'Admin', '5', 'Friday, Oct 6', '1', ''),
(50, 51, 12, '1', 'Cash', 'Admin', '5', 'Friday, Oct 6', '1', ''),
(51, 52, 12, '1', 'Cash', 'Admin', '5', 'Friday, Oct 6', '1', ''),
(52, 53, 11, '1', 'Cash', 'Admin', '5', 'Friday, Oct 6', '1', ''),
(53, 54, 11, '1', 'Cash', 'Admin', '5', 'Friday, Oct 6', '1', ''),
(54, 55, 9, '1', 'Cash', 'Admin', '20', 'Friday, Oct 6', '1', ''),
(55, 56, 13, '1', 'Cash', 'Admin', '5', 'Saturday, Oct 7', '1', ''),
(57, 58, 13, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 8', '1', ''),
(58, 59, 13, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 8', '1', ''),
(62, 63, 8, '1', 'Cash', 'Ios', '5', '9 October 2017', 'Done', '2017-10-09'),
(63, 64, 8, '1', 'Cash', 'Admin', '5', 'Monday, Oct 9', '1', ''),
(64, 65, 8, '1', 'Cash', 'Ios', '5', '9 October 2017', 'Done', '2017-10-09'),
(65, 66, 8, '1', 'Cash', 'Ios', '5', '9 October 2017', 'Done', '2017-10-09'),
(66, 67, 8, '1', 'Cash', 'Ios', '5', '9 October 2017', 'Done', '2017-10-09'),
(70, 68, 8, '1', 'Cash', 'Ios', '5', '10 October 2017', 'Done', '2017-10-10'),
(71, 74, 8, '1', 'Cash', 'Ios', '5', '10 October 2017', 'Done', '2017-10-10'),
(74, 76, 13, '1', 'Cash', 'Admin', '5', 'Thursday, Oct 12', '1', ''),
(75, 77, 14, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(76, 78, 15, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(77, 79, 14, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(78, 80, 17, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(79, 81, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(80, 83, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(81, 84, 16, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(82, 85, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(83, 86, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(84, 88, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(85, 87, 16, '1', 'Cash', 'Admin', '39.4', 'Thursday, Oct 12', '1', ''),
(86, 90, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(87, 91, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(88, 92, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(89, 93, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(90, 94, 9, '1', 'Cash', 'Ios', '20', '12 October 2017', 'Done', '2017-10-12'),
(91, 95, 9, '3', 'Credit Card', 'Stripe', '20', 'Thursday, Oct 12', 'Payment complete.', ''),
(92, 75, 8, '1', 'Cash', 'Ios', '5', '12 October 2017', 'Done', '2017-10-12'),
(93, 96, 9, '3', 'Credit Card', 'Stripe', '20', 'Thursday, Oct 12', 'Payment complete.', ''),
(94, 97, 16, '3', 'Credit Card', 'Stripe', '20', 'Thursday, Oct 12', 'Payment complete.', ''),
(95, 98, 9, '3', 'Credit Card', 'Stripe', '20', 'Thursday, Oct 12', 'Payment complete.', ''),
(96, 99, 9, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(97, 100, 9, '1', 'Cash', 'Admin', '36.4', 'Thursday, Oct 12', '1', ''),
(98, 101, 9, '4', 'Wallet', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(99, 102, 17, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(100, 103, 17, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(101, 105, 16, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(102, 106, 17, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 12', '1', ''),
(103, 107, 20, '1', 'Cash', 'Admin', '5', 'Friday, Oct 13', '1', ''),
(104, 108, 17, '1', 'Cash', 'Admin', '21.1', 'Friday, Oct 13', '1', ''),
(105, 109, 20, '1', 'Cash', 'Admin', '5', 'Friday, Oct 13', '1', ''),
(106, 111, 15, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(107, 112, 9, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(108, 113, 15, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(109, 114, 13, '1', 'Cash', 'Admin', '5', 'Friday, Oct 13', '1', ''),
(110, 115, 15, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(111, 117, 9, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(112, 118, 9, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(113, 120, 9, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(114, 122, 9, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(115, 123, 23, '1', 'Cash', 'Admin', '41', 'Friday, Oct 13', '1', ''),
(116, 124, 15, '1', 'Cash', 'Admin', '97.9', 'Friday, Oct 13', '1', ''),
(117, 125, 15, '1', 'Cash', 'Admin', '20', 'Friday, Oct 13', '1', ''),
(118, 126, 23, '1', 'Cash', 'Admin', '40.65', 'Friday, Oct 13', '1', ''),
(120, 127, 15, '1', 'Cash', 'Admin', '45.83', 'Saturday, Oct 14', '1', ''),
(121, 129, 6, '1', 'Cash', 'Admin', '5', 'Saturday, Oct 14', '1', ''),
(122, 128, 15, '1', 'Cash', 'Admin', '86.29', 'Saturday, Oct 14', '1', ''),
(123, 130, 9, '1', 'Cash', 'Admin', '21.4', 'Saturday, Oct 14', '1', ''),
(124, 131, 24, '1', 'Cash', 'Admin', '20.7', 'Saturday, Oct 14', '1', ''),
(125, 132, 24, '1', 'Cash', 'Admin', '20.7', 'Saturday, Oct 14', '1', ''),
(126, 133, 24, '1', 'Cash', 'Admin', '38.97', 'Sunday, Oct 15', '1', ''),
(127, 134, 25, '1', 'Cash', 'Admin', '5', 'Monday, Oct 16', '1', ''),
(128, 135, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 16', '1', ''),
(129, 136, 27, '1', 'Cash', 'Admin', '5', 'Monday, Oct 16', '1', ''),
(130, 137, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 16', '1', ''),
(131, 138, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 16', '1', ''),
(132, 139, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 16', '1', ''),
(133, 141, 13, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 17', '1', ''),
(134, 142, 27, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 17', '1', ''),
(135, 143, 15, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 18', '1', ''),
(136, 144, 24, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 21', '1', ''),
(137, 145, 24, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 21', '1', ''),
(138, 146, 24, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 21', '1', ''),
(139, 147, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(140, 148, 28, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(141, 149, 28, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(142, 150, 28, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(143, 151, 29, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(144, 152, 29, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(145, 154, 28, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(146, 155, 28, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(147, 156, 30, '1', 'Cash', 'Admin', '5', 'Monday, Oct 23', '1', ''),
(148, 157, 6, '1', 'Cash', 'Admin', '51.71', 'Monday, Oct 23', '1', ''),
(149, 158, 15, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(150, 159, 33, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(152, 161, 33, '1', 'Cash', 'Admin', '22.8', 'Monday, Oct 23', '1', ''),
(153, 162, 15, '1', 'Cash', 'Admin', '20.7', 'Monday, Oct 23', '1', ''),
(154, 163, 34, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(155, 164, 33, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(156, 165, 6, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(157, 166, 33, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(159, 168, 6, '1', 'Cash', 'Ios', '20', '23 October 2017', 'Done', '2017-10-23'),
(162, 172, 6, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(163, 171, 33, '1', 'Cash', 'Admin', '20.7', 'Monday, Oct 23', '1', ''),
(164, 174, 6, '1', 'Cash', 'Ios', '20', '23 October 2017', 'Done', '2017-10-23'),
(165, 175, 6, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(166, 176, 15, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(167, 177, 15, '1', 'Cash', 'Admin', '20', 'Monday, Oct 23', '1', ''),
(169, 179, 9, '1', 'Cash', 'Admin', '42.65', 'Monday, Oct 23', '1', ''),
(170, 180, 6, '3', 'Credit Card or Debit Card', 'Stripe', '20', 'Tuesday, Oct 24', 'Payment complete.', ''),
(171, 181, 6, '3', 'Credit Card or Debit Card', 'Stripe', '20', 'Tuesday, Oct 24', 'Payment complete.', ''),
(172, 182, 6, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 24', '1', ''),
(173, 183, 6, '4', 'Wallet', 'Admin', '20', 'Tuesday, Oct 24', '1', ''),
(174, 178, 33, '1', 'Cash', 'Admin', '661.9', 'Tuesday, Oct 24', '1', ''),
(175, 184, 15, '1', 'Cash', 'Admin', '20.7', 'Tuesday, Oct 24', '1', ''),
(176, 185, 15, '1', 'Cash', 'Admin', '20.7', 'Tuesday, Oct 24', '1', ''),
(177, 186, 12, '4', 'Cash', 'Android', '5', 'Anything', 'Anything', ''),
(178, 187, 12, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything', ''),
(179, 188, 12, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything', ''),
(180, 191, 26, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything', ''),
(181, 192, 27, '3', 'Credit Card or Debit Card', 'Stripe', '5', 'Tuesday, Oct 24', 'Payment complete.', ''),
(182, 190, 12, '4', 'Cash', 'Android', '5', 'Anything', 'Anything', ''),
(183, 194, 12, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 24', '1', ''),
(184, 195, 12, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 24', '1', ''),
(185, 196, 35, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything', ''),
(186, 197, 33, '1', 'Cash', 'Admin', '20.7', 'Tuesday, Oct 24', '1', ''),
(187, 198, 33, '1', 'Cash', 'Admin', '20.7', 'Tuesday, Oct 24', '1', ''),
(188, 199, 7, '1', 'Cash', 'Admin', '24.9', 'Tuesday, Oct 24', '1', ''),
(189, 200, 15, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 24', '1', ''),
(190, 201, 13, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 25', '1', ''),
(191, 203, 40, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 25', '1', ''),
(192, 204, 43, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 25', '1', ''),
(193, 205, 43, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 25', '1', ''),
(194, 206, 44, '1', 'Cash', 'Admin', '10', 'Wednesday, Oct 25', '1', ''),
(195, 207, 44, '1', 'Cash', 'Admin', '25', 'Wednesday, Oct 25', '1', ''),
(196, 208, 15, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 25', '1', ''),
(197, 209, 7, '1', 'Cash', 'Admin', '21.4', 'Wednesday, Oct 25', '1', ''),
(198, 210, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 25', '1', ''),
(199, 211, 7, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 25', '1', ''),
(200, 212, 6, '1', 'Cash', 'Admin', '21.4', 'Wednesday, Oct 25', '1', ''),
(201, 213, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 25', '1', ''),
(202, 214, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Oct 25', '1', ''),
(203, 216, 6, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 26', '1', ''),
(204, 217, 46, '1', 'Cash', 'Admin', '5', 'Thursday, Oct 26', '1', ''),
(205, 218, 33, '1', 'Cash', 'Admin', '20', 'Thursday, Oct 26', '1', ''),
(206, 219, 15, '1', 'Cash', 'Admin', '92.94', 'Friday, Oct 27', '1', ''),
(207, 220, 6, '1', 'Cash', 'Admin', '24.9', 'Friday, Oct 27', '1', ''),
(208, 221, 6, '1', 'Cash', 'Admin', '21.4', 'Friday, Oct 27', '1', ''),
(209, 222, 6, '1', 'Cash', 'Admin', '100.43', 'Friday, Oct 27', '1', ''),
(210, 223, 15, '1', 'Cash', 'Admin', '58.43', 'Friday, Oct 27', '1', ''),
(211, 224, 15, '1', 'Cash', 'Admin', '20.7', 'Saturday, Oct 28', '1', ''),
(212, 225, 15, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(213, 226, 6, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(214, 227, 48, '1', 'Cash', 'Admin', '5', 'Saturday, Oct 28', '1', ''),
(215, 229, 15, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(216, 230, 33, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(217, 231, 7, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(218, 232, 44, '1', 'Cash', 'Admin', '5', 'Saturday, Oct 28', '1', ''),
(219, 233, 35, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(220, 234, 7, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(221, 235, 35, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(222, 236, 33, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(223, 237, 33, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(224, 238, 15, '1', 'Cash', 'Admin', '20', 'Saturday, Oct 28', '1', ''),
(225, 240, 6, '1', 'Cash', 'Admin', '57.52', 'Sunday, Oct 29', '1', ''),
(226, 241, 33, '1', 'Cash', 'Admin', '46.7', 'Sunday, Oct 29', '1', ''),
(227, 242, 33, '1', 'Cash', 'Admin', '20', 'Sunday, Oct 29', '1', ''),
(228, 244, 15, '1', 'Cash', 'Admin', '20', 'Sunday, Oct 29', '1', ''),
(229, 245, 33, '1', 'Cash', 'Admin', '22', 'Sunday, Oct 29', '1', ''),
(230, 246, 6, '1', 'Cash', 'Admin', '20', 'Sunday, Oct 29', '1', ''),
(231, 202, 13, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything', ''),
(232, 247, 25, '1', 'Cash', 'Admin', '185', 'Monday, Oct 30', '1', '2017-10-30'),
(233, 248, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 30', '1', '2017-10-30'),
(234, 249, 13, '1', 'Cash', 'Admin', '5', 'Monday, Oct 30', '1', '2017-10-30'),
(235, 250, 13, '1', 'Cash', 'Admin', '50', 'Monday, Oct 30', '1', '2017-10-30'),
(236, 251, 42, '1', 'Cash', 'Admin', '5', 'Monday, Oct 30', '1', '2017-10-30'),
(237, 252, 7, '1', 'Cash', 'Admin', '0.00', 'Monday, Oct 30', '1', '2017-10-30'),
(238, 253, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(239, 254, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(240, 255, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(241, 256, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(242, 257, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(243, 258, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(244, 259, 42, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(245, 260, 35, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 31', '1', '2017-10-31'),
(246, 261, 35, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 31', '1', '2017-10-31'),
(247, 264, 15, '1', 'Cash', 'Admin', '44.15', 'Tuesday, Oct 31', '1', '2017-10-31'),
(248, 265, 13, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(249, 266, 13, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', '1', '2017-10-31'),
(250, 268, 35, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 31', '1', '2017-10-31'),
(251, 269, 6, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 31', '1', '2017-10-31'),
(252, 270, 6, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 31', '1', '2017-10-31'),
(253, 271, 35, '1', 'Cash', 'Admin', '20', 'Tuesday, Oct 31', '1', '2017-10-31'),
(254, 272, 33, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01'),
(255, 273, 42, '1', 'Cash', 'Admin', '5', 'Wednesday, Nov 1', '1', '2017-11-01 07:19'),
(256, 274, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 07:31'),
(257, 275, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 07:48'),
(258, 278, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 15:01'),
(259, 279, 15, '1', 'Cash', 'Admin', '78.59', 'Wednesday, Nov 1', '1', '2017-11-01 15:56'),
(260, 279, 15, '1', 'Cash', 'Admin', '78.66', 'Wednesday, Nov 1', '1', '2017-11-01 15:56'),
(261, 280, 33, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 16:46'),
(262, 281, 6, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 17:48'),
(263, 282, 15, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 17:57'),
(264, 283, 15, '1', 'Cash', 'Admin', '20', 'Wednesday, Nov 1', '1', '2017-11-01 20:22'),
(265, 284, 6, '1', 'Cash', 'Admin', '112.05', 'Thursday, Nov 2', '1', '2017-11-02 07:05'),
(266, 285, 6, '1', 'Cash', 'Admin', '20', 'Thursday, Nov 2', '1', '2017-11-02 11:27'),
(267, 149, 28, '1', 'Cash', 'Admin', '3805', 'Thursday, Nov 2', '1', '2017-11-02 11:47'),
(268, 149, 28, '1', 'Cash', 'Admin', '3820', 'Thursday, Nov 2', '1', '2017-11-02 11:48'),
(269, 149, 28, '1', 'Cash', 'Admin', '3820', 'Thursday, Nov 2', '1', '2017-11-02 11:48'),
(270, 149, 28, '1', 'Cash', 'Admin', '3820', 'Thursday, Nov 2', '1', '2017-11-02 11:48'),
(271, 149, 28, '1', 'Cash', 'Admin', '3820', 'Thursday, Nov 2', '1', '2017-11-02 11:48'),
(272, 149, 28, '1', 'Cash', 'Admin', '3835', 'Thursday, Nov 2', '1', '2017-11-02 11:48'),
(273, 149, 28, '3', 'Credit Card or Debit Card', 'Stripe', '3865', 'Thursday, Nov 2', '1', '2017-11-02 11:50'),
(274, 287, 6, '1', 'Cash', 'Admin', '20', 'Thursday, Nov 2', '1', '2017-11-02 15:13'),
(275, 288, 6, '1', 'Cash', 'Admin', '77.39', 'Thursday, Nov 2', '1', '2017-11-02 16:01'),
(276, 286, 15, '1', 'Cash', 'Admin', '841.07', 'Thursday, Nov 2', '1', '2017-11-02 16:05'),
(277, 289, 6, '1', 'Cash', 'Admin', '22.1', 'Friday, Nov 3', '1', '2017-11-03 19:27'),
(278, 290, 13, '2', 'Cash', 'Android', '0.00', 'Anything', 'Anything', '2017-11-04 09:54'),
(279, 291, 13, '2', 'Cash', 'Android', '0.00', 'Anything', 'Anything', '2017-11-04 10:01'),
(280, 292, 13, '2', 'Cash', 'Android', '5', 'Anything', 'Anything', '2017-11-04 10:11'),
(281, 293, 13, '2', 'Cash', 'Android', '5', 'Anything', 'Anything', '2017-11-04 10:21'),
(282, 294, 13, '2', 'Cash', 'Android', '5', 'Anything', 'Anything', '2017-11-04 10:46'),
(283, 295, 13, '2', 'Cash', 'Android', '5', 'Anything', 'Anything', '2017-11-04 10:48'),
(284, 296, 13, '1', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-04 10:51', 'Done', '2017-11-04 10:51'),
(285, 299, 15, '1', 'Cash', 'Admin', '20', 'Saturday, Nov 4', '1', '2017-11-04 15:51'),
(286, 300, 6, '1', 'Cash', 'Admin', '43.1', 'Sunday, Nov 5', '1', '2017-11-05 15:40'),
(287, 301, 6, '1', 'Cash', 'Admin', '20', 'Sunday, Nov 5', '1', '2017-11-05 15:44'),
(288, 298, 49, '2', 'Cash', 'Android', '5', 'Anything', 'Anything', '2017-11-06 06:30'),
(289, 302, 15, '1', 'Cash', 'Admin', '20', 'Monday, Nov 6', '1', '2017-11-06 06:31'),
(290, 303, 49, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 06:35', 'Done', '2017-11-06 06:35'),
(291, 297, 49, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 06:55', 'Done', '2017-11-06 06:55'),
(292, 304, 49, '1', 'Cash', 'Admin', '5', 'Monday, Nov 6', '1', '2017-11-06 07:34'),
(293, 305, 49, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 07:39', 'Done', '2017-11-06 07:39'),
(294, 306, 25, '1', 'Cash', 'Admin', '5', 'Monday, Nov 6', '1', '2017-11-06 07:58'),
(295, 307, 25, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 08:01', 'Done', '2017-11-06 08:01'),
(296, 310, 49, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 08:56', 'Done', '2017-11-06 08:56'),
(297, 309, 25, '1', 'Cash', 'Ios', '5', '6 November 2017', 'Done', '2017-11-06 09:02'),
(298, 311, 25, '1', 'Cash', 'Admin', '5', 'Monday, Nov 6', '1', '2017-11-06 09:03'),
(299, 312, 25, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 09:07', 'Done', '2017-11-06 09:07'),
(300, 313, 6, '4', 'Wallet', 'Admin', '20', 'Monday, Nov 6', '1', '2017-11-06 09:16'),
(301, 0, 6, '', '', 'Admin', '59', '', '1', ''),
(302, 0, 49, '', '', 'Admin', '59', '', '1', ''),
(303, 0, 49, '', '', 'Admin', '100', '', '1', ''),
(304, 0, 49, '', '', 'Admin', '100', '', '1', ''),
(305, 0, 49, '', '', 'Admin', '100', '', '1', ''),
(306, 315, 49, '2', 'Cash', 'Android', '5', 'Anything', 'Anything', '2017-11-06 09:42'),
(307, 317, 25, '1', 'Cash', 'Admin', '5', 'Monday, Nov 6', '1', '2017-11-06 10:04'),
(308, 318, 25, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 10:08', 'Done', '2017-11-06 10:08'),
(309, 319, 25, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 10:15', 'Done', '2017-11-06 10:15'),
(310, 316, 49, '2', 'Paypal', 'Android', '5', 'Anything', 'Anything', '2017-11-06 10:18'),
(311, 320, 25, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 10:20', 'Done', '2017-11-06 10:20'),
(312, 321, 49, '2', 'Paypal', 'Android', '5', 'Anything', 'Anything', '2017-11-06 10:20'),
(313, 322, 49, '4', 'Wallet', 'Admin', '5', 'Monday, Nov 6', '1', '2017-11-06 10:28'),
(314, 323, 49, '2', 'Paypal', 'Android', '5', 'Anything', 'Anything', '2017-11-06 10:30'),
(315, 324, 13, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-06 10:36', 'Done', '2017-11-06 10:36'),
(316, 325, 25, '1', 'Wallet', 'Ios', '5', '6 November 2017', 'Done', '2017-11-06 11:08'),
(317, 329, 6, '4', 'Wallet', 'Admin', '20', 'Monday, Nov 6', '1', '2017-11-06 14:21'),
(318, 326, 33, '2', 'Cash', 'Android', '26.72', 'Anything', 'Anything', '2017-11-06 14:46'),
(319, 331, 33, '1', 'Cash', 'Admin', '32', 'Monday, Nov 6', '1', '2017-11-06 19:02'),
(320, 328, 25, '1', 'Cash', 'Ios', '5', '7 November 2017', 'Done', '2017-11-07 08:46'),
(321, 332, 25, '1', 'Cash', 'Ios', '5', '7 November 2017', 'Done', '2017-11-07 08:56'),
(322, 333, 25, '1', 'Cash', 'Ios', '5', '7 November 2017', 'Done', '2017-11-07 09:02'),
(323, 334, 15, '1', 'Cash', 'Admin', '20', 'Tuesday, Nov 7', '1', '2017-11-07 09:47'),
(324, 335, 13, '2', 'Peach Payment', 'Caredit Card', '5.00', '2017-11-07 15:49', 'Done', '2017-11-07 15:49'),
(325, 336, 33, '1', 'Cash', 'Admin', '80.32', 'Wednesday, Nov 8', '1', '2017-11-08 17:10'),
(326, 337, 15, '1', 'Cash', 'Admin', '20', 'Thursday, Nov 9', '1', '2017-11-09 04:32'),
(327, 338, 15, '1', 'Cash', 'Admin', '20', 'Thursday, Nov 9', '1', '2017-11-09 10:06'),
(328, 327, 35, '2', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-09 13:52'),
(329, 340, 6, '1', 'Cash', 'Admin', '20', 'Saturday, Nov 11', '1', '2017-11-11 03:08'),
(330, 339, 15, '2', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-11 10:05'),
(331, 341, 15, '1', 'Cash', 'Admin', '20', 'Saturday, Nov 11', '1', '2017-11-11 10:13'),
(332, 342, 15, '1', 'Cash', 'Admin', '23.5', 'Saturday, Nov 11', '1', '2017-11-11 11:40'),
(333, 345, 33, '1', 'Cash', 'Admin', '21.4', 'Saturday, Nov 11', '1', '2017-11-11 20:44'),
(334, 344, 33, '4', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-11 20:47'),
(335, 343, 33, '2', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-12 15:29'),
(336, 346, 33, '1', 'Cash', 'Admin', '20', 'Monday, Nov 13', '1', '2017-11-13 06:41'),
(337, 347, 6, '1', 'Cash', 'Admin', '20', 'Monday, Nov 13', '1', '2017-11-13 06:56'),
(338, 351, 33, '2', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-14 14:08'),
(339, 350, 33, '2', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-14 14:32'),
(340, 352, 33, '1', 'Cash', 'Admin', '20', 'Tuesday, Nov 14', '1', '2017-11-14 14:34'),
(341, 353, 15, '1', 'Cash', 'Admin', '20', 'Thursday, Nov 16', '1', '2017-11-16 10:20'),
(342, 349, 33, '2', 'Cash', 'Android', '20', 'Anything', 'Anything', '2017-11-17 19:19');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(6, 'Paypal', '', '', 2),
(2, 'Credit Card or Debit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', '', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(46, 121, 'Km', '$', 2, 10, 0, 0, 0, 0, 0, 0, '2', '40', '15', '0', '0', '0', '0'),
(47, 121, 'Km', '$', 3, 10, 0, 0, 0, 0, 0, 0, '2', '30', '10', '0', '0', '0', '0'),
(48, 121, 'Km', '$', 4, 10, 0, 0, 0, 0, 0, 0, '2', '20', '10', '0', '0', '0', '0'),
(49, 56, 'Miles', '&#8360;', 28, 5, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '5', '5'),
(50, 56, 'Miles', '&#8360;', 29, 5, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '5', '5'),
(51, 56, 'Miles', '&#8360;', 30, 5, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '5', '5'),
(52, 121, 'Km', 'R', 28, 20, 0, 0, 0, 0, 0, 0, '1', '20', '7', '5', '2', '5', '0.70'),
(53, 121, 'Km', 'R', 29, 20, 0, 0, 0, 0, 0, 0, '1', '30', '12', '2', '2', '0', '1.10'),
(54, 121, 'Km', 'R', 30, 20, 0, 0, 0, 0, 0, 0, '1', '70', '15', '5', '8', '0', '1.50'),
(55, 121, 'Km', 'R', 32, 20, 0, 0, 0, 0, 0, 0, '1', '50', '12', '5', '5', '5', '1.20'),
(56, 121, 'Km', 'R', 34, 20, 0, 0, 0, 0, 0, 0, '1', '20', '15', '5', '8', '5', '1.50');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'test', 'test notificaqtion', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-10-15', 1),
(2, 'test promo 2', 'test promo\r\nkkknlk khnkjhn', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-10-15', 1),
(3, 'hkvjvjm v', 'hdhtfcjvv', 'uploads/notification/1500380956793.jpg', '', 9, 0, '2017-10-15', 1),
(4, 'tytdc', 'kgjkh', 'uploads/notification/push_image_4.png', '', 0, 0, '2017-10-15', 1),
(5, 'lhlhnl', 'last one', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-10-15', 1),
(6, 'sdfsdf', 'sdfsd', 'uploads/notification/1500380956793.jpg', '', 0, 34, '2017-10-27', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 1, '2017-09-20'),
(2, 2, 1, 0, '2017-09-22'),
(3, 2, 2, 1, '2017-09-22'),
(4, 3, 1, 0, '2017-09-22'),
(5, 3, 2, 1, '2017-09-22'),
(6, 4, 4, 1, '2017-09-22'),
(7, 5, 6, 1, '2017-09-22'),
(8, 6, 6, 1, '2017-09-22'),
(9, 7, 6, 0, '2017-09-22'),
(10, 8, 6, 0, '2017-09-22'),
(11, 9, 6, 1, '2017-09-22'),
(12, 10, 6, 0, '2017-09-22'),
(13, 11, 6, 0, '2017-09-22'),
(14, 12, 6, 0, '2017-09-22'),
(15, 12, 7, 1, '2017-09-22'),
(16, 13, 6, 0, '2017-09-22'),
(17, 13, 7, 1, '2017-09-22'),
(18, 14, 6, 0, '2017-09-22'),
(19, 14, 7, 0, '2017-09-22'),
(20, 15, 6, 0, '2017-09-22'),
(21, 15, 7, 0, '2017-09-22'),
(22, 17, 6, 0, '2017-09-22'),
(23, 17, 7, 0, '2017-09-22'),
(24, 18, 6, 1, '2017-09-22'),
(25, 18, 7, 0, '2017-09-22'),
(26, 19, 6, 0, '2017-09-23'),
(27, 19, 7, 0, '2017-09-23'),
(28, 20, 6, 1, '2017-09-24'),
(29, 20, 7, 0, '2017-09-24'),
(30, 21, 6, 1, '2017-09-24'),
(31, 21, 7, 0, '2017-09-24'),
(32, 22, 6, 0, '2017-09-24'),
(33, 22, 7, 0, '2017-09-24'),
(34, 23, 6, 1, '2017-09-24'),
(35, 23, 7, 0, '2017-09-24'),
(36, 24, 6, 1, '2017-09-24'),
(37, 24, 7, 0, '2017-09-24'),
(38, 25, 6, 0, '2017-09-24'),
(39, 25, 7, 0, '2017-09-24'),
(40, 26, 6, 1, '2017-09-24'),
(41, 26, 7, 0, '2017-09-24'),
(42, 16, 1, 8, '0000-00-00'),
(43, 16, 7, 8, '0000-00-00'),
(44, 27, 6, 0, '2017-09-24'),
(45, 27, 7, 0, '2017-09-24'),
(46, 28, 6, 1, '2017-09-24'),
(47, 28, 7, 0, '2017-09-24'),
(48, 29, 6, 0, '2017-09-24'),
(49, 29, 7, 0, '2017-09-24'),
(50, 30, 6, 0, '2017-09-24'),
(51, 30, 7, 0, '2017-09-24'),
(52, 31, 6, 0, '2017-09-24'),
(53, 31, 7, 0, '2017-09-24'),
(54, 32, 6, 0, '2017-09-24'),
(55, 32, 7, 0, '2017-09-24'),
(56, 33, 6, 0, '2017-09-24'),
(57, 33, 7, 0, '2017-09-24'),
(58, 0, 6, 0, '2017-09-24'),
(59, 0, 7, 0, '2017-09-24'),
(60, 0, 6, 0, '2017-09-24'),
(61, 0, 7, 0, '2017-09-24'),
(62, 0, 6, 0, '2017-09-24'),
(63, 0, 7, 0, '2017-09-24'),
(64, 34, 6, 1, '2017-09-24'),
(65, 34, 7, 0, '2017-09-24'),
(66, 35, 6, 1, '2017-09-25'),
(67, 35, 7, 0, '2017-09-25'),
(68, 36, 6, 1, '2017-09-25'),
(69, 36, 7, 0, '2017-09-25'),
(70, 37, 6, 0, '2017-09-28'),
(71, 37, 7, 0, '2017-09-28'),
(72, 38, 6, 1, '2017-09-28'),
(73, 38, 7, 0, '2017-09-28'),
(74, 39, 6, 0, '2017-09-28'),
(75, 39, 7, 0, '2017-09-28'),
(76, 40, 6, 1, '2017-09-28'),
(77, 40, 7, 0, '2017-09-28'),
(78, 41, 7, 0, '2017-09-28'),
(79, 42, 6, 0, '2017-09-28'),
(80, 42, 7, 0, '2017-09-28'),
(81, 43, 6, 0, '2017-09-28'),
(82, 43, 7, 0, '2017-09-28'),
(83, 44, 6, 0, '2017-09-28'),
(84, 44, 7, 0, '2017-09-28'),
(85, 45, 6, 1, '2017-09-28'),
(86, 45, 7, 0, '2017-09-28'),
(87, 46, 8, 1, '2017-09-29'),
(88, 47, 8, 1, '2017-09-29'),
(89, 48, 11, 1, '2017-09-29'),
(90, 49, 11, 1, '2017-09-29'),
(91, 50, 11, 1, '2017-09-29'),
(92, 51, 11, 1, '2017-09-29'),
(93, 52, 11, 1, '2017-09-29'),
(94, 53, 11, 1, '2017-09-30'),
(95, 54, 11, 1, '2017-09-30'),
(96, 55, 11, 1, '2017-09-30'),
(97, 56, 11, 1, '2017-09-30'),
(98, 57, 11, 1, '2017-10-01'),
(99, 58, 11, 1, '2017-10-01'),
(100, 59, 11, 8, '0000-00-00'),
(101, 60, 11, 1, '0000-00-00'),
(102, 60, 11, 1, '0000-00-00'),
(103, 60, 11, 1, '0000-00-00'),
(104, 61, 11, 0, '2017-10-01'),
(105, 62, 11, 0, '2017-10-02'),
(106, 63, 11, 0, '2017-10-02'),
(107, 64, 11, 1, '2017-10-03'),
(108, 65, 9, 0, '2017-10-03'),
(109, 66, 9, 1, '2017-10-03'),
(110, 67, 11, 1, '2017-10-03'),
(111, 68, 9, 1, '2017-10-03'),
(112, 69, 11, 1, '2017-10-03'),
(113, 70, 11, 0, '2017-10-03'),
(114, 71, 11, 0, '2017-10-03'),
(115, 71, 17, 1, '2017-10-03'),
(116, 72, 11, 0, '2017-10-03'),
(117, 72, 17, 0, '2017-10-03'),
(118, 73, 11, 0, '2017-10-03'),
(119, 73, 17, 0, '2017-10-03'),
(120, 74, 11, 0, '2017-10-03'),
(121, 74, 17, 0, '2017-10-03'),
(122, 75, 11, 0, '2017-10-03'),
(123, 75, 17, 0, '2017-10-03'),
(124, 76, 11, 0, '2017-10-03'),
(125, 76, 17, 1, '2017-10-03'),
(126, 77, 11, 0, '2017-10-03'),
(127, 77, 17, 0, '2017-10-03'),
(128, 77, 11, 0, '0000-00-00'),
(129, 78, 11, 0, '2017-10-03'),
(130, 78, 17, 1, '2017-10-03'),
(131, 79, 11, 0, '2017-10-03'),
(132, 79, 17, 1, '2017-10-03'),
(133, 80, 11, 0, '2017-10-03'),
(134, 80, 17, 1, '2017-10-03'),
(135, 81, 11, 0, '2017-10-03'),
(136, 81, 17, 1, '2017-10-03'),
(137, 82, 11, 0, '2017-10-03'),
(138, 82, 17, 1, '2017-10-03'),
(139, 83, 11, 0, '2017-10-03'),
(140, 83, 17, 1, '2017-10-03'),
(141, 84, 11, 0, '2017-10-03'),
(142, 84, 17, 0, '2017-10-03'),
(143, 85, 11, 0, '2017-10-03'),
(144, 85, 17, 1, '2017-10-03'),
(145, 86, 11, 0, '2017-10-04'),
(146, 86, 17, 1, '2017-10-04'),
(147, 87, 11, 0, '0000-00-00'),
(148, 87, 17, 1, '0000-00-00'),
(149, 88, 22, 1, '2017-10-04'),
(150, 89, 22, 1, '2017-10-04'),
(151, 90, 22, 1, '2017-10-04'),
(152, 91, 22, 1, '2017-10-04'),
(153, 92, 27, 1, '2017-10-04'),
(154, 93, 26, 0, '2017-10-06'),
(155, 93, 28, 1, '2017-10-06'),
(156, 94, 26, 0, '2017-10-06'),
(157, 94, 28, 1, '2017-10-06'),
(158, 95, 26, 0, '2017-10-06'),
(159, 95, 30, 1, '2017-10-06'),
(160, 96, 26, 0, '2017-10-06'),
(161, 96, 30, 1, '2017-10-06'),
(162, 97, 26, 0, '2017-10-06'),
(163, 98, 26, 0, '2017-10-06'),
(164, 98, 30, 1, '2017-10-06'),
(165, 99, 11, 0, '2017-10-06'),
(166, 99, 17, 1, '2017-10-06'),
(167, 100, 26, 0, '2017-10-07'),
(168, 100, 30, 0, '2017-10-07'),
(169, 100, 31, 1, '2017-10-07'),
(170, 101, 26, 0, '2017-10-08'),
(171, 101, 30, 0, '2017-10-08'),
(172, 101, 31, 1, '2017-10-08'),
(173, 102, 26, 0, '2017-10-08'),
(174, 102, 30, 0, '2017-10-08'),
(175, 102, 31, 1, '2017-10-08'),
(176, 103, 26, 0, '2017-10-08'),
(177, 103, 30, 0, '2017-10-08'),
(178, 103, 31, 1, '2017-10-08'),
(179, 104, 26, 0, '2017-10-08'),
(180, 104, 30, 0, '2017-10-08'),
(181, 104, 31, 1, '2017-10-08'),
(182, 105, 26, 0, '2017-10-08'),
(183, 105, 30, 0, '2017-10-08'),
(184, 105, 31, 1, '2017-10-08'),
(185, 106, 26, 0, '2017-10-08'),
(186, 106, 30, 0, '2017-10-08'),
(187, 106, 31, 1, '2017-10-08'),
(188, 107, 9, 1, '2017-10-09'),
(189, 107, 26, 0, '2017-10-09'),
(190, 107, 30, 0, '2017-10-09'),
(191, 107, 31, 0, '2017-10-09'),
(192, 108, 9, 1, '2017-10-09'),
(193, 108, 26, 0, '2017-10-09'),
(194, 108, 30, 0, '2017-10-09'),
(195, 108, 31, 0, '2017-10-09'),
(196, 109, 9, 0, '2017-10-09'),
(197, 109, 26, 0, '2017-10-09'),
(198, 109, 30, 0, '2017-10-09'),
(199, 109, 31, 1, '2017-10-09'),
(200, 110, 9, 1, '2017-10-09'),
(201, 110, 26, 0, '2017-10-09'),
(202, 110, 30, 0, '2017-10-09'),
(203, 111, 9, 1, '2017-10-09'),
(204, 111, 26, 0, '2017-10-09'),
(205, 111, 30, 0, '2017-10-09'),
(206, 111, 26, 0, '0000-00-00'),
(207, 112, 9, 0, '2017-10-09'),
(208, 112, 26, 0, '2017-10-09'),
(209, 112, 30, 0, '2017-10-09'),
(210, 112, 26, 0, '0000-00-00'),
(211, 113, 9, 1, '2017-10-09'),
(212, 113, 26, 0, '2017-10-09'),
(213, 113, 30, 0, '2017-10-09'),
(214, 113, 26, 0, '0000-00-00'),
(215, 114, 9, 1, '2017-10-09'),
(216, 114, 26, 0, '2017-10-09'),
(217, 114, 30, 0, '2017-10-09'),
(218, 114, 26, 0, '0000-00-00'),
(219, 115, 9, 0, '2017-10-10'),
(220, 115, 26, 0, '2017-10-10'),
(221, 115, 30, 0, '2017-10-10'),
(222, 115, 31, 1, '2017-10-10'),
(223, 116, 9, 0, '2017-10-10'),
(224, 116, 26, 0, '2017-10-10'),
(225, 116, 30, 0, '2017-10-10'),
(226, 116, 31, 1, '2017-10-10'),
(227, 117, 9, 0, '2017-10-10'),
(228, 117, 26, 0, '2017-10-10'),
(229, 117, 30, 0, '2017-10-10'),
(230, 117, 31, 1, '2017-10-10'),
(231, 118, 9, 0, '2017-10-10'),
(232, 118, 26, 0, '2017-10-10'),
(233, 118, 30, 0, '2017-10-10'),
(234, 118, 31, 1, '2017-10-10'),
(235, 119, 9, 0, '2017-10-10'),
(236, 119, 26, 0, '2017-10-10'),
(237, 119, 30, 0, '2017-10-10'),
(238, 119, 31, 1, '2017-10-10'),
(239, 120, 9, 1, '2017-10-10'),
(240, 120, 26, 0, '2017-10-10'),
(241, 120, 30, 0, '2017-10-10'),
(242, 120, 31, 0, '2017-10-10'),
(243, 121, 9, 1, '2017-10-10'),
(244, 121, 26, 0, '2017-10-10'),
(245, 121, 30, 0, '2017-10-10'),
(246, 122, 9, 0, '2017-10-12'),
(247, 122, 26, 0, '2017-10-12'),
(248, 122, 30, 0, '2017-10-12'),
(249, 122, 31, 1, '2017-10-12'),
(250, 123, 11, 0, '2017-10-12'),
(251, 123, 17, 1, '2017-10-12'),
(252, 124, 33, 1, '2017-10-12'),
(253, 125, 11, 0, '2017-10-12'),
(254, 125, 17, 1, '2017-10-12'),
(255, 126, 34, 1, '2017-10-12'),
(256, 127, 34, 1, '2017-10-12'),
(257, 128, 34, 0, '2017-10-12'),
(258, 129, 11, 0, '2017-10-12'),
(259, 129, 17, 0, '2017-10-12'),
(260, 129, 11, 0, '0000-00-00'),
(261, 130, 11, 0, '2017-10-12'),
(262, 130, 17, 1, '2017-10-12'),
(263, 131, 11, 0, '2017-10-12'),
(264, 131, 17, 1, '2017-10-12'),
(265, 132, 32, 1, '2017-10-12'),
(266, 133, 11, 0, '2017-10-12'),
(267, 133, 17, 1, '2017-10-12'),
(268, 134, 11, 0, '2017-10-12'),
(269, 134, 17, 1, '2017-10-12'),
(270, 135, 33, 1, '2017-10-12'),
(271, 136, 34, 1, '2017-10-12'),
(272, 137, 11, 0, '2017-10-12'),
(273, 137, 17, 1, '2017-10-12'),
(274, 138, 32, 1, '2017-10-12'),
(275, 139, 11, 0, '2017-10-12'),
(276, 139, 17, 1, '2017-10-12'),
(277, 140, 11, 0, '2017-10-12'),
(278, 140, 17, 1, '2017-10-12'),
(279, 141, 11, 0, '2017-10-12'),
(280, 141, 17, 1, '2017-10-12'),
(281, 142, 11, 0, '2017-10-12'),
(282, 142, 17, 1, '2017-10-12'),
(283, 143, 11, 0, '2017-10-12'),
(284, 143, 17, 1, '2017-10-12'),
(285, 144, 11, 0, '2017-10-12'),
(286, 144, 17, 1, '2017-10-12'),
(287, 145, 11, 0, '2017-10-12'),
(288, 145, 17, 1, '2017-10-12'),
(289, 146, 11, 0, '2017-10-12'),
(290, 146, 17, 1, '2017-10-12'),
(291, 147, 11, 0, '2017-10-12'),
(292, 147, 17, 1, '2017-10-12'),
(293, 148, 32, 1, '2017-10-12'),
(294, 149, 33, 0, '2017-10-12'),
(295, 150, 32, 1, '2017-10-12'),
(296, 151, 11, 0, '2017-10-12'),
(297, 151, 17, 1, '2017-10-12'),
(298, 152, 11, 0, '2017-10-12'),
(299, 152, 17, 1, '2017-10-12'),
(300, 153, 11, 0, '2017-10-12'),
(301, 153, 17, 1, '2017-10-12'),
(302, 154, 34, 1, '2017-10-12'),
(303, 155, 34, 1, '2017-10-12'),
(304, 156, 11, 0, '2017-10-12'),
(305, 156, 17, 1, '2017-10-12'),
(306, 157, 11, 0, '2017-10-12'),
(307, 157, 17, 1, '2017-10-12'),
(308, 158, 11, 0, '2017-10-12'),
(309, 158, 17, 1, '2017-10-12'),
(310, 159, 34, 1, '2017-10-12'),
(311, 160, 34, 1, '2017-10-12'),
(312, 163, 33, 1, '2017-10-12'),
(313, 164, 33, 0, '2017-10-12'),
(314, 165, 9, 0, '2017-10-13'),
(315, 165, 26, 0, '2017-10-13'),
(316, 165, 30, 0, '2017-10-13'),
(317, 165, 31, 0, '2017-10-13'),
(318, 165, 37, 1, '2017-10-13'),
(319, 166, 9, 0, '2017-10-13'),
(320, 166, 26, 0, '2017-10-13'),
(321, 166, 30, 0, '2017-10-13'),
(322, 166, 31, 0, '2017-10-13'),
(323, 166, 31, 0, '0000-00-00'),
(324, 167, 9, 0, '2017-10-13'),
(325, 167, 26, 0, '2017-10-13'),
(326, 167, 31, 0, '2017-10-13'),
(327, 168, 9, 0, '2017-10-13'),
(328, 168, 26, 0, '2017-10-13'),
(329, 168, 31, 0, '2017-10-13'),
(330, 169, 38, 1, '2017-10-13'),
(331, 170, 34, 1, '2017-10-13'),
(332, 171, 38, 1, '2017-10-13'),
(333, 172, 33, 1, '2017-10-13'),
(334, 173, 11, 0, '2017-10-13'),
(335, 173, 17, 1, '2017-10-13'),
(336, 174, 38, 0, '2017-10-13'),
(337, 175, 38, 0, '2017-10-13'),
(338, 176, 33, 1, '2017-10-13'),
(339, 177, 11, 0, '2017-10-13'),
(340, 177, 17, 0, '2017-10-13'),
(341, 178, 11, 0, '2017-10-13'),
(342, 178, 17, 1, '2017-10-13'),
(343, 179, 9, 0, '2017-10-13'),
(344, 179, 26, 0, '2017-10-13'),
(345, 179, 31, 0, '2017-10-13'),
(346, 179, 39, 0, '2017-10-13'),
(347, 179, 40, 1, '2017-10-13'),
(348, 180, 9, 0, '2017-10-13'),
(349, 180, 26, 0, '2017-10-13'),
(350, 180, 31, 0, '2017-10-13'),
(351, 180, 39, 0, '2017-10-13'),
(352, 180, 40, 1, '2017-10-13'),
(353, 181, 33, 1, '2017-10-13'),
(354, 182, 11, 0, '2017-10-13'),
(355, 182, 17, 1, '2017-10-13'),
(356, 183, 11, 0, '2017-10-13'),
(357, 184, 11, 0, '2017-10-13'),
(358, 184, 17, 1, '2017-10-13'),
(359, 185, 11, 0, '2017-10-13'),
(360, 185, 17, 1, '2017-10-13'),
(361, 186, 11, 0, '2017-10-13'),
(362, 186, 17, 1, '2017-10-13'),
(363, 187, 32, 1, '2017-10-13'),
(364, 188, 11, 0, '2017-10-13'),
(365, 188, 17, 1, '2017-10-13'),
(366, 189, 11, 0, '2017-10-13'),
(367, 189, 17, 0, '2017-10-13'),
(368, 190, 11, 0, '2017-10-13'),
(369, 190, 17, 1, '2017-10-13'),
(370, 191, 11, 0, '2017-10-13'),
(371, 191, 17, 1, '2017-10-13'),
(372, 192, 11, 0, '2017-10-13'),
(373, 193, 11, 0, '2017-10-13'),
(374, 193, 17, 1, '2017-10-13'),
(375, 194, 32, 1, '2017-10-13'),
(376, 195, 32, 0, '2017-10-13'),
(377, 195, 33, 1, '2017-10-13'),
(378, 196, 32, 0, '2017-10-13'),
(379, 197, 32, 0, '2017-10-13'),
(380, 197, 33, 1, '2017-10-13'),
(381, 198, 32, 0, '2017-10-13'),
(382, 199, 32, 0, '2017-10-13'),
(383, 199, 33, 1, '2017-10-13'),
(384, 200, 11, 0, '2017-10-13'),
(385, 200, 17, 1, '2017-10-13'),
(386, 201, 32, 0, '2017-10-13'),
(387, 201, 33, 1, '2017-10-13'),
(388, 202, 32, 1, '2017-10-13'),
(389, 202, 33, 0, '2017-10-13'),
(390, 203, 11, 0, '2017-10-13'),
(391, 203, 17, 0, '2017-10-13'),
(392, 203, 11, 0, '0000-00-00'),
(393, 204, 11, 0, '2017-10-13'),
(394, 204, 17, 0, '2017-10-13'),
(395, 204, 11, 0, '0000-00-00'),
(396, 205, 11, 0, '2017-10-13'),
(397, 205, 17, 0, '2017-10-13'),
(398, 205, 11, 0, '0000-00-00'),
(399, 206, 32, 0, '2017-10-14'),
(400, 206, 33, 1, '2017-10-14'),
(401, 207, 33, 0, '2017-10-14'),
(402, 208, 33, 1, '2017-10-14'),
(403, 209, 41, 1, '2017-10-14'),
(404, 210, 11, 0, '2017-10-14'),
(405, 210, 17, 1, '2017-10-14'),
(406, 211, 11, 0, '2017-10-14'),
(407, 211, 17, 1, '2017-10-14'),
(408, 212, 41, 0, '2017-10-14'),
(409, 213, 11, 0, '2017-10-14'),
(410, 213, 17, 1, '2017-10-14'),
(411, 214, 11, 0, '2017-10-14'),
(412, 215, 11, 0, '2017-10-14'),
(413, 216, 33, 0, '2017-10-15'),
(414, 217, 11, 0, '2017-10-15'),
(415, 217, 17, 1, '2017-10-15'),
(416, 218, 11, 0, '2017-10-15'),
(417, 218, 17, 0, '2017-10-15'),
(418, 218, 17, 0, '0000-00-00'),
(419, 219, 11, 1, '2017-10-15'),
(420, 219, 17, 0, '2017-10-15'),
(421, 220, 11, 0, '2017-10-15'),
(422, 220, 17, 0, '2017-10-15'),
(423, 221, 11, 0, '2017-10-15'),
(424, 221, 17, 0, '2017-10-15'),
(425, 222, 33, 1, '2017-10-15'),
(426, 222, 42, 0, '2017-10-15'),
(427, 223, 41, 0, '2017-10-15'),
(428, 224, 41, 0, '2017-10-15'),
(429, 225, 41, 0, '2017-10-15'),
(430, 226, 41, 0, '2017-10-15'),
(431, 227, 41, 0, '2017-10-15'),
(432, 228, 11, 0, '2017-10-15'),
(433, 228, 17, 0, '2017-10-15'),
(434, 229, 41, 0, '2017-10-15'),
(435, 230, 11, 0, '2017-10-15'),
(436, 230, 17, 0, '2017-10-15'),
(437, 231, 38, 1, '2017-10-16'),
(438, 232, 11, 0, '2017-10-16'),
(439, 232, 17, 0, '2017-10-16'),
(440, 234, 44, 1, '2017-10-16'),
(441, 235, 38, 1, '2017-10-16'),
(442, 236, 40, 1, '2017-10-16'),
(443, 237, 40, 1, '2017-10-16'),
(444, 238, 40, 0, '2017-10-16'),
(445, 239, 40, 1, '2017-10-16'),
(446, 240, 40, 1, '2017-10-17'),
(447, 241, 31, 0, '2017-10-17'),
(448, 242, 31, 0, '2017-10-17'),
(449, 243, 31, 0, '2017-10-17'),
(450, 244, 40, 1, '2017-10-17'),
(451, 245, 38, 1, '2017-10-17'),
(452, 246, 33, 1, '2017-10-18'),
(453, 247, 11, 0, '2017-10-21'),
(454, 248, 11, 0, '2017-10-21'),
(455, 249, 17, 1, '2017-10-21'),
(456, 250, 17, 1, '2017-10-21'),
(457, 251, 17, 0, '2017-10-21'),
(458, 251, 11, 0, '0000-00-00'),
(459, 252, 17, 1, '2017-10-21'),
(460, 253, 17, 1, '2017-10-21'),
(461, 254, 29, 1, '2017-10-23'),
(462, 255, 47, 0, '2017-10-23'),
(463, 255, 46, 1, '0000-00-00'),
(464, 256, 46, 0, '2017-10-23'),
(465, 257, 47, 0, '2017-10-23'),
(466, 258, 47, 1, '2017-10-23'),
(467, 259, 47, 1, '2017-10-23'),
(468, 260, 47, 1, '2017-10-23'),
(469, 261, 47, 0, '2017-10-23'),
(470, 262, 48, 1, '2017-10-23'),
(471, 263, 48, 1, '2017-10-23'),
(472, 264, 47, 1, '2017-10-23'),
(473, 265, 47, 1, '2017-10-23'),
(474, 266, 47, 1, '2017-10-23'),
(475, 267, 46, 1, '2017-10-23'),
(476, 268, 17, 1, '2017-10-23'),
(477, 269, 45, 0, '2017-10-23'),
(478, 270, 42, 0, '2017-10-23'),
(479, 271, 33, 1, '2017-10-23'),
(480, 272, 34, 1, '2017-10-23'),
(481, 273, 34, 1, '2017-10-23'),
(482, 274, 34, 1, '2017-10-23'),
(483, 275, 33, 0, '2017-10-23'),
(484, 275, 53, 1, '0000-00-00'),
(485, 276, 33, 0, '2017-10-23'),
(486, 277, 33, 0, '2017-10-23'),
(487, 277, 53, 0, '0000-00-00'),
(488, 278, 34, 1, '2017-10-23'),
(489, 279, 33, 1, '2017-10-23'),
(490, 280, 42, 0, '2017-10-23'),
(491, 281, 42, 0, '2017-10-23'),
(492, 282, 42, 0, '2017-10-23'),
(493, 283, 42, 0, '2017-10-23'),
(494, 284, 34, 1, '2017-10-23'),
(495, 285, 42, 0, '2017-10-23'),
(496, 286, 17, 1, '2017-10-23'),
(497, 287, 34, 1, '2017-10-23'),
(498, 288, 32, 1, '2017-10-23'),
(499, 289, 34, 1, '2017-10-23'),
(500, 290, 42, 0, '2017-10-23'),
(501, 291, 45, 1, '2017-10-23'),
(502, 293, 32, 1, '2017-10-23'),
(503, 296, 11, 0, '2017-10-23'),
(504, 296, 17, 0, '0000-00-00'),
(505, 297, 42, 0, '2017-10-23'),
(506, 298, 32, 1, '2017-10-23'),
(507, 299, 42, 0, '2017-10-23'),
(508, 300, 34, 1, '2017-10-23'),
(509, 301, 17, 1, '2017-10-23'),
(510, 302, 42, 0, '2017-10-23'),
(511, 303, 42, 0, '2017-10-23'),
(512, 304, 17, 0, '2017-10-23'),
(513, 305, 42, 0, '2017-10-23'),
(514, 304, 11, 0, '0000-00-00'),
(515, 306, 11, 1, '2017-10-23'),
(516, 307, 33, 0, '2017-10-23'),
(517, 308, 34, 1, '2017-10-23'),
(518, 309, 42, 0, '2017-10-23'),
(519, 310, 42, 0, '2017-10-23'),
(520, 311, 11, 1, '2017-10-23'),
(521, 312, 33, 0, '2017-10-23'),
(522, 312, 53, 0, '0000-00-00'),
(523, 313, 42, 0, '2017-10-23'),
(524, 314, 42, 0, '2017-10-23'),
(525, 315, 42, 0, '2017-10-23'),
(526, 316, 42, 0, '2017-10-23'),
(527, 317, 42, 0, '2017-10-23'),
(528, 318, 42, 0, '2017-10-23'),
(529, 319, 17, 0, '2017-10-23'),
(530, 319, 11, 1, '0000-00-00'),
(531, 320, 33, 1, '2017-10-23'),
(532, 321, 33, 1, '2017-10-23'),
(533, 322, 33, 0, '2017-10-23'),
(534, 322, 53, 0, '0000-00-00'),
(535, 323, 42, 0, '2017-10-23'),
(536, 324, 42, 0, '2017-10-23'),
(537, 325, 33, 0, '2017-10-23'),
(538, 325, 53, 1, '0000-00-00'),
(539, 326, 34, 0, '2017-10-23'),
(540, 327, 34, 0, '2017-10-23'),
(541, 328, 34, 0, '2017-10-23'),
(542, 329, 34, 1, '2017-10-23'),
(543, 330, 17, 1, '2017-10-23'),
(544, 331, 11, 0, '2017-10-24'),
(545, 331, 17, 0, '0000-00-00'),
(546, 332, 11, 0, '2017-10-24'),
(547, 332, 17, 1, '0000-00-00'),
(548, 333, 11, 0, '2017-10-24'),
(549, 333, 17, 1, '0000-00-00'),
(550, 334, 17, 0, '2017-10-24'),
(551, 334, 11, 1, '0000-00-00'),
(552, 335, 17, 0, '2017-10-24'),
(553, 335, 11, 0, '0000-00-00'),
(554, 336, 17, 0, '2017-10-24'),
(555, 336, 11, 1, '0000-00-00'),
(556, 337, 33, 1, '2017-10-24'),
(557, 338, 11, 1, '2017-10-24'),
(558, 339, 33, 1, '2017-10-24'),
(559, 340, 21, 1, '2017-10-24'),
(560, 341, 21, 1, '2017-10-24'),
(561, 342, 21, 1, '2017-10-24'),
(562, 343, 21, 1, '2017-10-24'),
(563, 344, 21, 1, '0000-00-00'),
(564, 345, 27, 1, '2017-10-24'),
(565, 346, 29, 0, '2017-10-24'),
(566, 347, 27, 1, '2017-10-24'),
(567, 348, 47, 0, '2017-10-24'),
(568, 349, 26, 0, '2017-10-24'),
(569, 350, 49, 1, '2017-10-24'),
(570, 351, 26, 0, '2017-10-24'),
(571, 352, 47, 0, '2017-10-24'),
(572, 353, 47, 0, '2017-10-24'),
(573, 354, 47, 0, '2017-10-24'),
(574, 355, 47, 0, '2017-10-24'),
(575, 356, 47, 0, '2017-10-24'),
(576, 357, 47, 0, '2017-10-24'),
(577, 358, 26, 0, '2017-10-24'),
(578, 359, 27, 1, '2017-10-24'),
(579, 360, 47, 0, '2017-10-24'),
(580, 361, 47, 0, '2017-10-24'),
(581, 362, 60, 1, '2017-10-24'),
(582, 363, 32, 1, '2017-10-24'),
(583, 364, 34, 1, '2017-10-24'),
(584, 365, 34, 1, '2017-10-24'),
(585, 366, 47, 0, '2017-10-24'),
(586, 367, 47, 0, '2017-10-24'),
(587, 368, 47, 0, '2017-10-24'),
(588, 369, 17, 0, '2017-10-24'),
(589, 370, 11, 1, '2017-10-24'),
(590, 372, 53, 1, '2017-10-24'),
(591, 373, 53, 0, '2017-10-24'),
(592, 373, 42, 0, '0000-00-00'),
(593, 374, 33, 0, '2017-10-24'),
(594, 374, 53, 0, '0000-00-00'),
(595, 375, 47, 0, '2017-10-25'),
(596, 376, 66, 1, '2017-10-25'),
(597, 377, 66, 1, '2017-10-25'),
(598, 378, 66, 1, '2017-10-25'),
(599, 379, 48, 0, '2017-10-25'),
(600, 380, 48, 0, '2017-10-25'),
(601, 381, 42, 0, '2017-10-25'),
(602, 382, 48, 0, '2017-10-25'),
(603, 383, 48, 0, '2017-10-25'),
(604, 384, 48, 0, '2017-10-25'),
(605, 385, 48, 0, '2017-10-25'),
(606, 386, 48, 0, '2017-10-25'),
(607, 387, 71, 1, '2017-10-25'),
(608, 388, 67, 1, '2017-10-25'),
(609, 389, 67, 1, '2017-10-25'),
(610, 390, 67, 1, '2017-10-25'),
(611, 391, 72, 1, '2017-10-25'),
(612, 392, 72, 1, '2017-10-25'),
(613, 393, 11, 0, '2017-10-25'),
(614, 395, 33, 1, '2017-10-25'),
(615, 396, 11, 1, '2017-10-25'),
(616, 397, 33, 0, '2017-10-25'),
(617, 398, 11, 1, '2017-10-25'),
(618, 399, 11, 1, '2017-10-25'),
(619, 400, 11, 1, '2017-10-25'),
(620, 401, 11, 1, '2017-10-25'),
(621, 402, 11, 1, '2017-10-25'),
(622, 403, 73, 1, '2017-10-25'),
(623, 404, 33, 1, '2017-10-26'),
(624, 394, 73, 1, '0000-00-00'),
(625, 406, 75, 1, '2017-10-26'),
(626, 407, 34, 1, '2017-10-26'),
(627, 409, 33, 1, '2017-10-27'),
(628, 410, 73, 1, '2017-10-27'),
(629, 411, 73, 0, '2017-10-27'),
(630, 412, 73, 0, '2017-10-27'),
(631, 413, 73, 1, '2017-10-27'),
(632, 414, 73, 0, '2017-10-27'),
(633, 415, 73, 0, '2017-10-27'),
(634, 416, 73, 0, '2017-10-27'),
(635, 417, 73, 0, '2017-10-27'),
(636, 418, 73, 1, '2017-10-27'),
(637, 419, 33, 1, '2017-10-27'),
(638, 420, 33, 1, '2017-10-28'),
(639, 421, 33, 1, '2017-10-28'),
(640, 422, 33, 1, '2017-10-28'),
(641, 423, 33, 0, '2017-10-28'),
(642, 424, 33, 0, '2017-10-28'),
(643, 425, 33, 0, '2017-10-28'),
(644, 426, 76, 0, '2017-10-28'),
(645, 426, 72, 0, '0000-00-00'),
(646, 427, 76, 0, '2017-10-28'),
(647, 427, 72, 0, '0000-00-00'),
(648, 428, 76, 1, '2017-10-28'),
(649, 429, 33, 1, '2017-10-28'),
(650, 430, 34, 1, '2017-10-28'),
(651, 431, 73, 1, '2017-10-28'),
(652, 432, 72, 1, '2017-10-28'),
(653, 433, 32, 1, '2017-10-28'),
(654, 434, 32, 1, '2017-10-28'),
(655, 435, 32, 1, '2017-10-28'),
(656, 436, 73, 1, '2017-10-28'),
(657, 437, 32, 0, '2017-10-28'),
(658, 438, 33, 0, '2017-10-28'),
(659, 438, 32, 0, '0000-00-00'),
(660, 439, 32, 1, '2017-10-28'),
(661, 440, 34, 1, '2017-10-28'),
(662, 441, 34, 1, '2017-10-28'),
(663, 442, 33, 1, '2017-10-28'),
(664, 443, 34, 1, '2017-10-28'),
(665, 444, 17, 1, '2017-10-29'),
(666, 445, 17, 1, '2017-10-29'),
(667, 446, 34, 1, '2017-10-29'),
(668, 447, 17, 0, '2017-10-29'),
(669, 448, 17, 0, '2017-10-29'),
(670, 449, 17, 0, '2017-10-29'),
(671, 450, 17, 0, '2017-10-29'),
(672, 451, 34, 1, '2017-10-29'),
(673, 452, 34, 1, '2017-10-29'),
(674, 453, 34, 1, '2017-10-29'),
(675, 454, 33, 1, '2017-10-29'),
(676, 456, 34, 1, '2017-10-29'),
(677, 457, 17, 1, '2017-10-29'),
(678, 458, 76, 0, '2017-10-30'),
(679, 459, 77, 1, '2017-10-30'),
(680, 460, 77, 0, '2017-10-30'),
(681, 461, 80, 1, '2017-10-30'),
(682, 462, 80, 1, '2017-10-30'),
(683, 463, 80, 1, '2017-10-30'),
(684, 464, 81, 0, '2017-10-30'),
(685, 465, 81, 0, '2017-10-30'),
(686, 466, 81, 1, '2017-10-30'),
(687, 467, 17, 1, '2017-10-30'),
(688, 468, 81, 0, '2017-10-30'),
(689, 469, 81, 1, '2017-10-31'),
(690, 470, 81, 1, '2017-10-31'),
(691, 471, 81, 0, '2017-10-31'),
(692, 472, 81, 1, '2017-10-31'),
(693, 473, 81, 1, '2017-10-31'),
(694, 474, 81, 1, '2017-10-31'),
(695, 475, 81, 1, '2017-10-31'),
(696, 476, 81, 1, '2017-10-31'),
(697, 477, 32, 1, '2017-10-31'),
(698, 478, 17, 0, '2017-10-31'),
(699, 479, 32, 1, '2017-10-31'),
(700, 480, 32, 1, '2017-10-31'),
(701, 481, 32, 1, '2017-10-31'),
(702, 482, 33, 1, '2017-10-31'),
(703, 483, 81, 0, '2017-10-31'),
(704, 484, 80, 1, '2017-10-31'),
(705, 485, 80, 1, '2017-10-31'),
(706, 487, 33, 0, '2017-10-31'),
(707, 487, 32, 1, '0000-00-00'),
(708, 488, 17, 1, '2017-10-31'),
(709, 489, 32, 1, '2017-10-31'),
(710, 490, 32, 1, '2017-10-31'),
(711, 491, 17, 1, '2017-10-31'),
(712, 492, 32, 0, '2017-11-01'),
(713, 493, 33, 0, '2017-11-01'),
(714, 494, 34, 1, '2017-11-01'),
(715, 495, 81, 0, '2017-11-01'),
(716, 496, 81, 1, '2017-11-01'),
(717, 497, 17, 1, '2017-11-01'),
(718, 498, 17, 1, '2017-11-01'),
(719, 499, 33, 1, '2017-11-01'),
(720, 500, 34, 1, '2017-11-01'),
(721, 501, 17, 1, '2017-11-01'),
(722, 502, 32, 0, '2017-11-01'),
(723, 503, 33, 1, '2017-11-01'),
(724, 504, 34, 0, '2017-11-01'),
(725, 505, 34, 0, '2017-11-01'),
(726, 506, 34, 0, '2017-11-01'),
(727, 507, 34, 0, '2017-11-01'),
(728, 508, 34, 0, '2017-11-01'),
(729, 509, 34, 1, '2017-11-01'),
(730, 510, 17, 1, '2017-11-01'),
(731, 511, 33, 1, '2017-11-01'),
(732, 512, 33, 1, '2017-11-01'),
(733, 513, 17, 1, '2017-11-02'),
(734, 514, 17, 1, '2017-11-02'),
(735, 515, 33, 1, '2017-11-02'),
(736, 516, 17, 1, '2017-11-02'),
(737, 517, 17, 1, '2017-11-02'),
(738, 518, 17, 1, '2017-11-02'),
(739, 519, 17, 0, '2017-11-02'),
(740, 520, 17, 1, '2017-11-02'),
(741, 521, 17, 1, '2017-11-03'),
(742, 522, 81, 0, '2017-11-04'),
(743, 523, 83, 1, '2017-11-04'),
(744, 524, 83, 1, '2017-11-04'),
(745, 525, 83, 1, '2017-11-04'),
(746, 526, 83, 1, '2017-11-04'),
(747, 527, 81, 0, '2017-11-04'),
(748, 528, 83, 1, '2017-11-04'),
(749, 529, 83, 1, '2017-11-04'),
(750, 530, 83, 1, '2017-11-04'),
(751, 531, 83, 1, '2017-11-04'),
(752, 532, 83, 1, '2017-11-04'),
(753, 533, 33, 1, '2017-11-04'),
(754, 534, 17, 1, '2017-11-05'),
(755, 535, 17, 1, '2017-11-05'),
(756, 536, 33, 1, '2017-11-06'),
(757, 537, 83, 1, '2017-11-06'),
(758, 538, 83, 1, '2017-11-06'),
(759, 539, 83, 1, '2017-11-06'),
(760, 540, 83, 1, '2017-11-06'),
(761, 541, 83, 0, '2017-11-06'),
(762, 541, 81, 0, '0000-00-00'),
(763, 542, 81, 0, '2017-11-06'),
(764, 543, 77, 0, '2017-11-06'),
(765, 544, 77, 0, '2017-11-06'),
(766, 545, 84, 1, '2017-11-06'),
(767, 546, 84, 1, '2017-11-06'),
(768, 547, 84, 0, '2017-11-06'),
(769, 548, 84, 1, '2017-11-06'),
(770, 549, 84, 0, '2017-11-06'),
(771, 550, 84, 0, '2017-11-06'),
(772, 551, 83, 0, '2017-11-06'),
(773, 552, 84, 1, '2017-11-06'),
(774, 553, 84, 1, '2017-11-06'),
(775, 554, 83, 1, '2017-11-06'),
(776, 555, 84, 1, '2017-11-06'),
(777, 556, 84, 1, '2017-11-06'),
(778, 557, 17, 0, '2017-11-06'),
(779, 558, 17, 0, '2017-11-06'),
(780, 559, 17, 0, '2017-11-06'),
(781, 560, 83, 1, '2017-11-06'),
(782, 561, 83, 1, '2017-11-06'),
(783, 562, 84, 1, '2017-11-06'),
(784, 563, 84, 1, '2017-11-06'),
(785, 564, 84, 0, '2017-11-06'),
(786, 564, 83, 0, '0000-00-00'),
(787, 565, 84, 1, '2017-11-06'),
(788, 566, 84, 1, '2017-11-06'),
(789, 567, 83, 1, '2017-11-06'),
(790, 568, 83, 1, '2017-11-06'),
(791, 569, 83, 1, '2017-11-06'),
(792, 570, 83, 1, '2017-11-06'),
(793, 571, 84, 1, '2017-11-06'),
(794, 572, 34, 1, '2017-11-06'),
(795, 573, 32, 1, '2017-11-06'),
(796, 574, 84, 1, '2017-11-06'),
(797, 575, 73, 1, '2017-11-06'),
(798, 576, 73, 1, '2017-11-06'),
(799, 578, 34, 1, '2017-11-06'),
(800, 580, 84, 1, '2017-11-07'),
(801, 581, 84, 1, '2017-11-07'),
(802, 582, 33, 1, '2017-11-07'),
(803, 583, 83, 0, '2017-11-07'),
(804, 583, 84, 0, '0000-00-00'),
(805, 584, 83, 0, '2017-11-07'),
(806, 584, 84, 0, '0000-00-00'),
(807, 585, 83, 1, '2017-11-07'),
(808, 586, 84, 0, '2017-11-07'),
(809, 587, 84, 0, '2017-11-07'),
(810, 588, 84, 0, '2017-11-07'),
(811, 589, 34, 1, '2017-11-08'),
(812, 590, 33, 1, '2017-11-09'),
(813, 591, 73, 0, '2017-11-09'),
(814, 592, 33, 1, '2017-11-09'),
(815, 593, 32, 0, '2017-11-09'),
(816, 594, 33, 1, '2017-11-10'),
(817, 595, 73, 1, '2017-11-11'),
(818, 596, 86, 1, '2017-11-11'),
(819, 597, 33, 1, '2017-11-11'),
(820, 598, 73, 0, '2017-11-11'),
(821, 599, 73, 0, '2017-11-11'),
(822, 600, 34, 1, '2017-11-11'),
(823, 601, 34, 1, '2017-11-11'),
(824, 602, 34, 1, '2017-11-11'),
(825, 0, 34, 0, '2017-11-12'),
(826, 0, 34, 0, '2017-11-12'),
(827, 603, 34, 1, '2017-11-13'),
(828, 604, 73, 1, '2017-11-13'),
(829, 605, 33, 1, '2017-11-13'),
(830, 606, 34, 0, '2017-11-14'),
(831, 607, 34, 1, '2017-11-14'),
(832, 608, 34, 1, '2017-11-14'),
(833, 609, 34, 1, '2017-11-14'),
(834, 610, 34, 1, '2017-11-14'),
(835, 611, 73, 1, '0000-00-00'),
(836, 611, 17, 0, '0000-00-00'),
(837, 611, 73, 1, '0000-00-00'),
(838, 612, 33, 1, '2017-11-16'),
(839, 613, 34, 0, '2017-11-17'),
(840, 614, 34, 0, '2017-11-17'),
(841, 615, 34, 0, '2017-11-17'),
(842, 616, 34, 0, '2017-11-17'),
(843, 617, 34, 0, '2017-11-17'),
(844, 618, 34, 0, '2017-11-17'),
(845, 619, 34, 0, '2017-11-17'),
(846, 620, 34, 0, '2017-11-17'),
(847, 621, 34, 0, '2017-11-17'),
(848, 622, 34, 0, '2017-11-18'),
(849, 623, 34, 0, '2017-11-18'),
(850, 624, 32, 0, '2017-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 7, 6),
(2, 59, 11),
(3, 60, 11),
(4, 62, 11),
(5, 77, 17),
(6, 111, 30),
(7, 112, 30),
(8, 113, 30),
(9, 114, 30),
(10, 129, 17),
(11, 164, 33),
(12, 166, 30),
(13, 203, 17),
(14, 204, 17),
(15, 205, 17),
(16, 216, 33),
(17, 218, 11),
(18, 218, 11),
(19, 218, 17),
(20, 251, 17),
(21, 255, 47),
(22, 257, 47),
(23, 274, 34),
(24, 274, 34),
(25, 275, 33),
(26, 277, 33),
(27, 296, 11),
(28, 304, 17),
(29, 312, 33),
(30, 319, 17),
(31, 322, 33),
(32, 322, 53),
(33, 325, 33),
(34, 326, 34),
(35, 326, 34),
(36, 327, 34),
(37, 328, 34),
(38, 331, 11),
(39, 332, 11),
(40, 333, 11),
(41, 334, 17),
(42, 335, 17),
(43, 335, 11),
(44, 336, 17),
(45, 373, 53),
(46, 374, 33),
(47, 374, 53),
(48, 397, 33),
(49, 426, 76),
(50, 427, 76),
(51, 438, 33),
(52, 447, 17),
(53, 448, 17),
(54, 449, 17),
(55, 450, 17),
(56, 465, 81),
(57, 478, 17),
(58, 487, 33),
(59, 493, 33),
(60, 508, 34),
(61, 541, 83),
(62, 564, 84),
(63, 583, 83),
(64, 584, 83),
(65, 591, 73),
(66, 606, 34),
(67, 611, 73),
(68, 624, 32);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `checkout_id` varchar(255) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.4121005361906', '77.0432470366359', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.3918271663029', '76.9754270464182', 'Sector 83\nGurugram, Haryana 122004', 'Wednesday, Sep 20', '14:17:59', '02:18:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121005361906,77.0432470366359&markers=color:red|label:D|28.3918271663029,76.9754270464182&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, '10C8E8945850FC6F6C566AB521D4EE17.sbg-vm-tx02', 1, 2, 2, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(2, 2, '', '28.4120922792288', '77.0433020219207', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.3973856088693', '76.9860925152898', 'Sector 76\nGurugram, Haryana 122004', 'Friday, Sep 22', '07:45:02', '07:46:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120922792288,77.0433020219207&markers=color:red|label:D|28.3973856088693,76.9860925152898&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(3, 2, '', '28.4120717891192', '77.0432253640491', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Sep 22', '07:49:35', '07:51:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120717891192,77.0432253640491&markers=color:red|label:D|28.4425708263901,77.0846636965871&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(4, 4, '', '28.4120961128183', '77.0432728528976', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4381350732564', '77.104056738317', 'B-54, Golf Course Road, Suncity, Sector 54\nHaiderpur, Haryana 122011', 'Friday, Sep 22', '10:30:20', '10:31:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120961128183,77.0432728528976&markers=color:red|label:D|28.4381350732564,77.104056738317&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, '', 1, 2, 2, 1, 7, 1, 0, 1, 1, '2017-09-22'),
(5, 6, '', '-26.1232374701914', '28.0348459485658', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1278916897447', '28.0262051895261', '71 Rutland Avenue\nCraighall Park, Randburg, 2196', 'Friday, Sep 22', '12:44:37', '12:50:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232374701914,28.0348459485658&markers=color:red|label:D|-26.1278916897447,28.0262051895261&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(6, 6, '', '-26.1232755278737', '28.0347158387303', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1308551484492', '28.0314063280821', '2 Bompas Road\nDunkeld West, Randburg, 2196', 'Friday, Sep 22', '13:08:29', '01:10:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232755278737,28.0347158387303&markers=color:red|label:D|-26.1308551484492,28.0314063280821&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(7, 6, '', '-26.124400461338', '28.0415336787701', '87 3rd Road\nHyde Park, Sandton, 2196', '-26.127983800411', '28.0446235835552', '76 3rd Avenue\nHyde Park, Sandton, 2196', 'Friday, Sep 22', '14:55:44', '02:55:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.124400461338,28.0415336787701&markers=color:red|label:D|-26.127983800411,28.0446235835552&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(8, 6, '', '-26.124400461338', '28.0415336787701', '87 3rd Road\nHyde Park, Sandton, 2196', '-26.1274061509471', '28.0322203785181', '2 Albury Road\nDunkeld West, Randburg, 2196', 'Friday, Sep 22', '15:21:12', '03:21:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.124400461338,28.0415336787701&markers=color:red|label:D|-26.1274061509471,28.0322203785181&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(9, 6, '', '-26.124400461338', '28.0415336787701', '87 3rd Road\nHyde Park, Sandton, 2196', '-26.1274061509471', '28.0322203785181', '2 Albury Road\nDunkeld West, Randburg, 2196', 'Friday, Sep 22', '15:21:48', '03:23:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.124400461338,28.0415336787701&markers=color:red|label:D|-26.1274061509471,28.0322203785181&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 28, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(10, 6, '', '-26.1304788881795', '28.0314311385155', '1 Bompas Road\nDunkeld West, Randburg, 2196', '-26.132366799491', '28.0363020300865', '24 Bompas Road\nDunkeld West, Randburg, 2196', 'Friday, Sep 22', '16:10:03', '04:10:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1304788881795,28.0314311385155&markers=color:red|label:D|-26.132366799491,28.0363020300865&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(11, 6, '', '-26.0608086695919', '28.0623252518818', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0622157761821', '28.0542999878526', '10 Davies Road\nBryanston, Sandton, 2191', 'Friday, Sep 22', '16:48:38', '04:48:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608086695919,28.0623252518818&markers=color:red|label:D|-26.0622157761821,28.0542999878526&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(12, 6, '', '-26.0608269293751', '28.0623265566383', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0686073374159', '28.0531375855207', '11 North Road\nMorningside, Sandton, 2057', 'Friday, Sep 22', '17:10:28', '05:11:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608269293751,28.0623265566383&markers=color:red|label:D|-26.0686073374159,28.0531375855207&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(13, 6, '', '-26.0608269293751', '28.0623265566383', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0693771202401', '28.0499018356204', '107 Coleraine Drive\nRiverclub, Sandton, 2191', 'Friday, Sep 22', '17:14:35', '05:15:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608269293751,28.0623265566383&markers=color:red|label:D|-26.0693771202401,28.0499018356204&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(14, 6, '', '-26.0599595820201', '28.06455809623', '8 8th Avenue\nEdenburg, Sandton, 2128', '-26.0658386674621', '28.0611677840352', '13 Robert Crescent\nMorningside, Sandton, 2057', 'Friday, Sep 22', '17:35:23', '05:35:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0599595820201,28.06455809623&markers=color:red|label:D|-26.0658386674621,28.0611677840352&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(15, 6, '', '-26.0599595820201', '28.06455809623', '8 8th Avenue\nEdenburg, Sandton, 2128', '-26.0627738695698', '28.06131798774', '2 4th Avenue\nEdenburg, Sandton, 2128', 'Friday, Sep 22', '17:36:51', '05:36:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0599595820201,28.06455809623&markers=color:red|label:D|-26.0627738695698,28.06131798774&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(16, 6, '', '-26.0599595820201', '28.06455809623', '8 8th Avenue\nEdenburg, Sandton, 2128', '-26.0627738695698', '28.06131798774', '2 4th Avenue\nEdenburg, Sandton, 2128', 'Friday, Sep 22', '17:37:55', '04:46:15 PM', '', '2017-09-22', '18:40:38', 0, 2, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(17, 6, '', '-26.0599595820201', '28.06455809623', '8 8th Avenue\nEdenburg, Sandton, 2128', '-26.0627738695698', '28.06131798774', '2 4th Avenue\nEdenburg, Sandton, 2128', 'Friday, Sep 22', '17:39:24', '05:39:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0599595820201,28.06455809623&markers=color:red|label:D|-26.0627738695698,28.06131798774&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-22'),
(18, 6, '', '-26.0599595820201', '28.06455809623', '8 8th Avenue\nEdenburg, Sandton, 2128', '-26.0627738695698', '28.06131798774', '2 4th Avenue\nEdenburg, Sandton, 2128', 'Friday, Sep 22', '19:12:36', '07:14:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0599595820201,28.06455809623&markers=color:red|label:D|-26.0627738695698,28.06131798774&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-22'),
(19, 6, '', '-26.0876832689867', '28.0070666223764', '193 Bram Fischer Drive\nFerndale, Randburg, 2194', '0.0', '0.0', 'No drop off point', 'Saturday, Sep 23', '16:24:50', '04:24:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0876832689867,28.0070666223764&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-23'),
(20, 6, '', '-26.06052401112', '28.0605267360806', '328 Rivonia Boulevard\nEdenburg, Sandton, 2128', '-26.063818066301', '28.0606735870242', '315 Rivonia Road\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '00:15:33', '12:17:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06052401112,28.0605267360806&markers=color:red|label:D|-26.063818066301,28.0606735870242&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-24'),
(21, 7, '', '-26.1093227', '28.053005', 'Sandton City', '-26.0474148', '28.0800782', 'Woodmead', 'Sunday, Sep 24', '08:48:06', '08:49:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1093227,28.053005&markers=color:red|label:D|-26.0474148,28.0800782&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(22, 7, '', '-26.1093227', '28.053005', 'Sandton City', '-26.0474148', '28.0800782', 'Woodmead', 'Sunday, Sep 24', '08:49:18', '08:49:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1093227,28.053005&markers=color:red|label:D|-26.0474148,28.0800782&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-24'),
(23, 7, '', '-26.1093227', '28.053005', 'Sandton City', '-26.0474148', '28.0800782', 'Woodmead', 'Sunday, Sep 24', '08:50:12', '08:52:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1093227,28.053005&markers=color:red|label:D|-26.0474148,28.0800782&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-24'),
(24, 7, '', '-26.0604232', '28.0620258', '4 Wessel Rd', '-26.1093227', '28.053005', 'Sandton City', 'Sunday, Sep 24', '08:52:49', '08:59:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0604232,28.0620258&markers=color:red|label:D|-26.1093227,28.053005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-24'),
(25, 7, '', '-26.0604232', '28.0620258', '4 Wessel Rd', '-26.05551', '28.06203', '20 Wessel Rd', 'Sunday, Sep 24', '16:09:29', '04:09:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0604232,28.0620258&markers=color:red|label:D|-26.05551,28.06203&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(26, 7, '', '-26.0604232', '28.0620258', '4 Wessel Rd', '-26.05551', '28.06203', '20 Wessel Rd', 'Sunday, Sep 24', '16:11:44', '04:19:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0604232,28.0620258&markers=color:red|label:D|-26.05551,28.06203&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-24'),
(27, 6, '', '-26.0623513090895', '28.0557399988174', '150 Coleraine Drive\nMorningside, Sandton, 2057', '-26.0594990612792', '28.0744577944279', '22 Hampton Court Road\nGallo Manor, Sandton, 2052', 'Sunday, Sep 24', '17:16:58', '05:16:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0623513090895,28.0557399988174&markers=color:red|label:D|-26.0594990612792,28.0744577944279&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 1, 0, 0, 2, 0, 1, 1, '2017-09-24'),
(28, 6, '', '-26.0608267054349', '28.062327504158', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0681441401485', '28.0498947948217', '9 Elm Road\nRiverclub, Sandton, 2191', 'Sunday, Sep 24', '17:22:14', '05:23:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608267054349,28.062327504158&markers=color:red|label:D|-26.0681441401485,28.0498947948217&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-24'),
(29, 6, '', '-26.0609607340111', '28.062420040369', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0633057575016', '28.0573949217796', '13 The Crescent Crescent\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '17:32:02', '05:32:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0609607340111,28.062420040369&markers=color:red|label:D|-26.0633057575016,28.0573949217796&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(30, 6, '', '-26.0609607340111', '28.062420040369', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0633057575016', '28.0573949217796', '13 The Crescent Crescent\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '17:32:55', '05:32:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0609607340111,28.062420040369&markers=color:red|label:D|-26.0633057575016,28.0573949217796&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(31, 6, '', '-26.0609607340111', '28.062420040369', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0633057575016', '28.0573949217796', '13 The Crescent Crescent\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '17:33:32', '05:33:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0609607340111,28.062420040369&markers=color:red|label:D|-26.0633057575016,28.0573949217796&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(32, 6, '', '-26.0609607340111', '28.062420040369', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0633057575016', '28.0573949217796', '13 The Crescent Crescent\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '17:34:11', '05:34:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0609607340111,28.062420040369&markers=color:red|label:D|-26.0633057575016,28.0573949217796&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(33, 6, '', '-26.0609607340111', '28.062420040369', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0633057575016', '28.0573949217796', '13 The Crescent Crescent\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '17:36:03', '05:36:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0609607340111,28.062420040369&markers=color:red|label:D|-26.0633057575016,28.0573949217796&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-09-24'),
(34, 7, '', '-26.0608335308824', '28.0623200814969', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0638792058846', '28.0578783899546', '15 The Crescent Crescent\nMorningside, Sandton, 2057', 'Sunday, Sep 24', '17:43:13', '05:45:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608335308824,28.0623200814969&markers=color:red|label:D|-26.0638792058846,28.0578783899546&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-24'),
(35, 6, '', '-26.0486588767041', '28.0612392816954', '37 Wessel Road\nEdenburg, Sandton, 2128', '-26.0504903815076', '28.0555529147387', '27-29 Rietfontein Road\nEdenburg, Sandton, 2128', 'Monday, Sep 25', '08:28:50', '08:36:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0486588767041,28.0612392816954&markers=color:red|label:D|-26.0504903815076,28.0555529147387&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-25'),
(36, 6, '', '-26.0360923454971', '28.051108494401', '23 Witkoppen Road\nSandton, Johannesburg, 2054', '-26.0604232', '28.0620258', '4 Wessel Rd', 'Monday, Sep 25', '09:23:06', '09:31:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0360923454971,28.051108494401&markers=color:red|label:D|-26.0604232,28.0620258&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-25'),
(37, 6, '', '-26.0608397588158', '28.0623375524077', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0652191486469', '28.0555361509323', '2 The Link Link\nMorningside, Sandton, 2057', 'Thursday, Sep 28', '17:05:12', '05:05:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608397588158,28.0623375524077&markers=color:red|label:D|-26.0652191486469,28.0555361509323&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-28'),
(38, 6, '', '-26.0609592280729', '28.0624190345407', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0720704168014', '28.0486106872559', '106 Coleraine Drive\nDuxberry, Sandton, 2191', 'Thursday, Sep 28', '17:09:35', '05:13:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0609592280729,28.0624190345407&markers=color:red|label:D|-26.0720704168014,28.0486106872559&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-28'),
(39, 6, '', '-26.0608049677702', '28.0623011755758', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.068918744692', '28.0524884909391', '9A North Road\nMorningside, Sandton, 2057', 'Thursday, Sep 28', '17:14:17', '05:14:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608049677702,28.0623011755758&markers=color:red|label:D|-26.068918744692,28.0524884909391&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-28'),
(40, 6, '', '-26.0608404677587', '28.0623371531985', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0718346090098', '28.0476001650095', '17 Boekenhout Crescent\nRiverclub, Sandton, 2191', 'Thursday, Sep 28', '18:00:05', '06:00:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608404677587,28.0623371531985&markers=color:red|label:D|-26.0718346090098,28.0476001650095&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-28'),
(41, 6, '', '-26.0608404677587', '28.0623371531985', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0718346090098', '28.0476001650095', '17 Boekenhout Crescent\nRiverclub, Sandton, 2191', 'Thursday, Sep 28', '18:00:36', '06:00:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608404677587,28.0623371531985&markers=color:red|label:D|-26.0718346090098,28.0476001650095&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-28'),
(42, 6, '', '-26.0608914608336', '28.062372431159', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0719062849916', '28.051092736423', '26 Oak Avenue\nDuxberry, Sandton, 2191', 'Thursday, Sep 28', '19:20:02', '07:20:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608914608336,28.062372431159&markers=color:red|label:D|-26.0719062849916,28.051092736423&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-28'),
(43, 6, '', '-26.0608914608336', '28.062372431159', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0719062849916', '28.051092736423', '26 Oak Avenue\nDuxberry, Sandton, 2191', 'Thursday, Sep 28', '19:21:22', '07:21:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608914608336,28.062372431159&markers=color:red|label:D|-26.0719062849916,28.051092736423&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-28'),
(44, 6, '', '-26.0608914608336', '28.062372431159', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0719062849916', '28.051092736423', '26 Oak Avenue\nDuxberry, Sandton, 2191', 'Thursday, Sep 28', '19:22:25', '07:22:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608914608336,28.062372431159&markers=color:red|label:D|-26.0719062849916,28.051092736423&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, '', 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-28'),
(45, 6, '', '-26.0608914608336', '28.0619647353888', '2-4 Wessel Road\nEdenburg, Sandton, 2128', '-26.0622214986858', '28.0650546401739', '30 Stiglingh Road\nEdenburg, Sandton, 2128', 'Thursday, Sep 28', '19:27:39', '07:42:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608914608336,28.0619647353888&markers=color:red|label:D|-26.0622214986858,28.0650546401739&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 4, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-28'),
(46, 3, '', '28.4120698674721', '77.0432792231441', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4353770059017', '77.0829045027494', '1221D, Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122003', 'Friday, Sep 29', '10:48:43', '10:49:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120698674721,77.0432792231441&markers=color:red|label:D|28.4353770059017,77.0829045027494&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-29'),
(47, 3, '', '28.4120675083395', '77.043268494308', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.3967981018153', '77.0150597020984', 'Darbaripur, Sector 75\nGurugram, Haryana', 'Friday, Sep 29', '11:04:15', '11:05:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120675083395,77.043268494308&markers=color:red|label:D|28.3967981018153,77.0150597020984&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-29'),
(48, 6, '', '-26.1308187265081', '28.0325258150697', '5 Bompas Road\nDunkeld West, Randburg, 2196', '-26.1303040019935', '28.0345217138529', '12 North Road\nDunkeld West, Randburg, 2196', 'Friday, Sep 29', '15:22:58', '03:24:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1308187265081,28.0325258150697&markers=color:red|label:D|-26.1303040019935,28.0345217138529&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-29'),
(49, 6, '', '-26.1303952569385', '28.0344280042015', '12 North Road\nDunkeld West, Randburg, 2196', '-26.0923539', '28.0034155', 'Randburg Square', 'Friday, Sep 29', '15:30:44', '03:32:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1303952569385,28.0344280042015&markers=color:red|label:D|-26.0923539,28.0034155&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-29'),
(50, 7, '', '-26.060875851245', '28.0623482254703', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.063587361969', '28.0378295481205', '25 Westbourne Road\nBryanston, Sandton, 2191', 'Friday, Sep 29', '16:25:42', '04:26:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.060875851245,28.0623482254703&markers=color:red|label:D|-26.063587361969,28.0378295481205&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-29'),
(51, 7, '', '-26.0608821972897', '28.0623555406484', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0518777675209', '28.0910589918494', 'Waterval Crescent\nWoodmead, Sandton, 2191', 'Friday, Sep 29', '16:38:01', '04:39:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608821972897,28.0623555406484&markers=color:red|label:D|-26.0518777675209,28.0910589918494&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-29'),
(52, 6, '', '-26.06096038', '28.06241993', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0063121', '28.2108827', 'Tembisa', 'Friday, Sep 29', '20:11:47', '08:15:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06096038,28.06241993&markers=color:red|label:D|-26.0063121,28.2108827&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-09-29'),
(53, 7, '', '-26.0449313351457', '28.0610055103898', '55 Wessel Road\nEdenburg, Sandton, 2128', '-26.0906799659904', '28.0370916053653', '125 Olympia Avenue\nParkmore, Sandton, 2196', 'Saturday, Sep 30', '13:03:22', '01:17:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0449313351457,28.0610055103898&markers=color:red|label:D|-26.0906799659904,28.0370916053653&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-30'),
(54, 7, '', '-26.0519286721665', '28.061465844512', '32 Wessel Road\nEdenburg, Sandton, 2128', '-26.06218385063', '28.0588050931692', '1A De La Rey Road\nEdenburg, Sandton, 2128', 'Saturday, Sep 30', '13:18:30', '01:26:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0519286721665,28.061465844512&markers=color:red|label:D|-26.06218385063,28.0588050931692&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-30'),
(55, 7, '', '-26.0516488469479', '28.0604881793261', '24 11th Avenue\nEdenburg, Sandton, 2128', '-26.06071', '28.06227', '4A Wessel Rd', 'Saturday, Sep 30', '13:31:01', '01:34:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0516488469479,28.0604881793261&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-30'),
(56, 7, '', '-26.0614301112781', '28.0593505931257', '4 De La Rey Road\nEdenburg, Sandton, 2128', '-26.0616998461468', '28.0623546615243', '0C 5th Avenue\nEdenburg, Sandton, 2128', 'Saturday, Sep 30', '13:35:02', '01:40:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0614301112781,28.0593505931257&markers=color:red|label:D|-26.0616998461468,28.0623546615243&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-30'),
(57, 6, '', '-26.0598897058699', '28.0609817057848', 'Mutual Road\nEdenburg, Sandton, 2128', '-26.0618173085262', '28.0622262507677', '8 5th Avenue\nEdenburg, Sandton, 2128', 'Sunday, Oct 1', '11:27:54', '11:31:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0598897058699,28.0609817057848&markers=color:red|label:D|-26.0618173085262,28.0622262507677&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-01'),
(58, 6, '', '-26.0607721903971', '28.062348626554', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0321398586635', '28.1119584292173', 'Pretoria Main Road\nWaterval 5-Ir, Midrand, 2090', 'Sunday, Oct 1', '11:59:35', '12:01:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607721903971,28.062348626554&markers=color:red|label:D|-26.0321398586635,28.1119584292173&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-01'),
(59, 6, '', '-26.0528919405232', '28.0352616682649', '104 Eccleston Crescent\nBryanston, Sandton, 2191', '-25.9918202033573', '28.0104498937726', '0B Broadacres Drive\nZevenfontein 407-Jr, Midrand', 'Sunday, Oct 1', '12:04:07', '12:10:56 PM', '', '2017-10-01', '13:10:41', 11, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-01'),
(60, 6, '', '-26.060780954375', '28.0622758076147', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.064732446103', '28.0535855144262', '136 Coleraine Drive\nMorningside, Sandton, 2057', 'Sunday, Oct 1', '12:08:39', '12:13:25 PM', '', '2017-10-01', '14:08:00', 11, 28, '', 2, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-01'),
(61, 6, '', '-26.060780954375', '28.0622758076147', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0607995985132', '28.0626520514488', '7 5th Avenue\nEdenburg, Sandton, 2128', 'Sunday, Oct 1', '17:29:56', '05:29:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.060780954375,28.0622758076147&markers=color:red|label:D|-26.0607995985132,28.0626520514488&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-01'),
(62, 6, '', '-26.0146711041028', '28.1005446240306', 'Jukskei View Drive\nWaterval 5-Ir, Midrand, 2090', '-26.0481472100909', '28.093597702682', 'Ben Schoeman Freeway & N3 Eastern Bypass & N3 & N1\nWoodmead, Sandton, 2191', 'Monday, Oct 2', '12:28:14', '12:28:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0146711041028,28.1005446240306&markers=color:red|label:D|-26.0481472100909,28.093597702682&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-02'),
(63, 6, 'hdhxhshdjd', '-26.0146046049218', '28.1007788765872', 'Jukskei View Drive\nWaterval 5-Ir, Midrand, 2090', '-26.1240632', '28.0347471', '55 6th Rd', 'Monday, Oct 2', '12:35:40', '12:35:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0146046049218,28.1007788765872&markers=color:red|label:D|-26.1240632,28.0347471&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-02'),
(64, 6, '', '-26.0606384627937', '28.0624421685934', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0928873913499', '28.0296692624688', '4 Holt Street\nGlenadrienne, Sandton, 2196', 'Tuesday, Oct 3', '06:08:37', '06:12:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0606384627937,28.0624421685934&markers=color:red|label:D|-26.0928873913499,28.0296692624688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(65, 8, '', '28.4117213050661', '77.0422630012035', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.4422244280655', '77.0878112688661', 'Plot No:1, Wazirabad, Sector 52A\nGurugram, Haryana 122003', 'Tuesday, Oct 3', '06:55:08', '06:55:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4117213050661,77.0422630012035&markers=color:red|label:D|28.4422244280655,77.0878112688661&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(66, 8, '', '28.4117213050661', '77.0422630012035', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.4422244280655', '77.0878112688661', 'Plot No:1, Wazirabad, Sector 52A\nGurugram, Haryana 122003', 'Tuesday, Oct 3', '06:56:09', '06:58:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4117213050661,77.0422630012035&markers=color:red|label:D|28.4422244280655,77.0878112688661&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(67, 6, 'PROMO123', '-26.1232704409231', '28.034724843041', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1484293662491', '27.9807363823056', '3 Lawley Avenue\nNorthcliff, Randburg, 2115', 'Tuesday, Oct 3', '14:37:18', '02:38:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232704409231,28.034724843041&markers=color:red|label:D|-26.1484293662491,27.9807363823056&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(68, 8, '', '28.4121730794703', '77.0432627946138', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4373025012268', '77.0839136838913', 'Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Tuesday, Oct 3', '14:55:56', '02:57:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121730794703,77.0432627946138&markers=color:red|label:D|28.4373025012268,77.0839136838913&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(69, 6, 'PROMO123', '-26.1232785381463', '28.0347111448646', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1685090550385', '28.0169448629022', '14 Emmarentia Avenue\nParkview, Randburg, 2122', 'Tuesday, Oct 3', '15:08:04', '03:09:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232785381463,28.0347111448646&markers=color:red|label:D|-26.1685090550385,28.0169448629022&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(70, 9, '', '-26.0607893581189', '28.0623057112098', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0001415050412', '28.103587590158', '50-52 Albertyn Street\nVorna Valley, Midrand, 1686', 'Tuesday, Oct 3', '16:50:53', '04:50:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607893581189,28.0623057112098&markers=color:red|label:D|-26.0001415050412,28.103587590158&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(71, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0110806804428', '28.1020925939083', 'Waterfall Drive\nWaterval 5-Ir, Midrand, 2090', 'Tuesday, Oct 3', '17:05:18', '05:07:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.0110806804428,28.1020925939083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(72, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0818182290436', '28.0422659218311', '46 Coleraine Drive\nRiverclub, Sandton, 2191', 'Tuesday, Oct 3', '17:23:40', '05:23:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.0818182290436,28.0422659218311&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(73, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0818182290436', '28.0422659218311', '46 Coleraine Drive\nRiverclub, Sandton, 2191', 'Tuesday, Oct 3', '17:24:56', '05:24:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.0818182290436,28.0422659218311&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(74, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0818182290436', '28.0422659218311', '46 Coleraine Drive\nRiverclub, Sandton, 2191', 'Tuesday, Oct 3', '17:25:45', '05:25:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.0818182290436,28.0422659218311&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(75, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0818182290436', '28.0422659218311', '46 Coleraine Drive\nRiverclub, Sandton, 2191', 'Tuesday, Oct 3', '17:27:49', '05:27:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.0818182290436,28.0422659218311&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(76, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0818182290436', '28.0422659218311', '46 Coleraine Drive\nRiverclub, Sandton, 2191', 'Tuesday, Oct 3', '17:31:09', '05:32:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.0818182290436,28.0422659218311&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(77, 9, '', '-26.0607723299957', '28.0623486299701', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1042150345562', '28.0237197875977', '5-7 Gleneagles Road\nGlenadrienne, Sandton, 2024', 'Tuesday, Oct 3', '17:35:52', '05:35:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607723299957,28.0623486299701&markers=color:red|label:D|-26.1042150345562,28.0237197875977&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(78, 7, '', '-26.0599559677375', '28.0630433186889', '4 8th Avenue\nEdenburg, Sandton, 2128', '-26.0347788814166', '28.0057147890329', '18A Penguin Drive\nNorscot, Sandton, 2055', 'Tuesday, Oct 3', '17:48:28', '05:53:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0599559677375,28.0630433186889&markers=color:red|label:D|-26.0347788814166,28.0057147890329&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(79, 7, '', '-26.0599145572875', '28.0610017385595', 'Mutual Road\nEdenburg, Sandton, 2128', '-25.9891699682007', '28.0107841640711', '0A Valley Boulevard\nDainfern Valley, Midrand, 2191', 'Tuesday, Oct 3', '17:59:56', '06:04:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0599145572875,28.0610017385595&markers=color:red|label:D|-25.9891699682007,28.0107841640711&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(80, 7, '', '-26.0546047054474', '28.06057233366', '26 10th Avenue\nEdenburg, Sandton, 2128', '-26.06071', '28.06227', '4A Wessel Rd', 'Tuesday, Oct 3', '18:08:58', '06:13:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0546047054474,28.06057233366&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(81, 9, '', '-26.0607791547809', '28.062276934697', '4A Wessel Road\nEdenburg, Sandton, 2128', '-25.9767313878716', '28.1538132950664', '72 Allan Road\nGlen Austin AH, Midrand, 1685', 'Tuesday, Oct 3', '18:58:32', '07:03:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607791547809,28.062276934697&markers=color:red|label:D|-25.9767313878716,28.1538132950664&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(82, 9, '', '-26.0607791547809', '28.062276934697', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1462573002211', '28.054449185729', '49 Glenhove Road\nMelrose Estate, Johannesburg, 2196', 'Tuesday, Oct 3', '19:28:02', '07:29:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607791547809,28.062276934697&markers=color:red|label:D|-26.1462573002211,28.054449185729&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(83, 9, '', '-26.0607791547809', '28.062276934697', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1141203281227', '27.9934228956699', '10 Tuin Avenue\nRobindale, Randburg, 2194', 'Tuesday, Oct 3', '19:51:18', '07:54:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607791547809,28.062276934697&markers=color:red|label:D|-26.1141203281227,27.9934228956699&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(84, 9, '', '-26.0610547045169', '28.0623767897487', '2 Wessel Road\nEdenburg, Sandton, 2128', '-25.9575735033209', '28.1118323653936', '1186 Canyon Way\nCrescent Wood Country Estate, Midrand, 1685', 'Tuesday, Oct 3', '19:57:15', '07:57:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0610547045169,28.0623767897487&markers=color:red|label:D|-25.9575735033209,28.1118323653936&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-03'),
(85, 9, '', '-26.0610547045169', '28.0623767897487', '2 Wessel Road\nEdenburg, Sandton, 2128', '-25.9575735033209', '28.1118323653936', '1186 Canyon Way\nCrescent Wood Country Estate, Midrand, 1685', 'Tuesday, Oct 3', '19:57:44', '08:02:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0610547045169,28.0623767897487&markers=color:red|label:D|-25.9575735033209,28.1118323653936&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-03'),
(86, 7, '', '-26.0613663448264', '28.0592050776147', '4 De La Rey Road\nEdenburg, Sandton, 2128', '-26.13032', '28.03148', '1 Bompas Rd', 'Wednesday, Oct 4', '07:21:22', '07:48:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0613663448264,28.0592050776147&markers=color:red|label:D|-26.13032,28.03148&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(87, 9, '', '-26.1240632', '28.034747100000004', '55 6th Road, Hyde Park, Sandton, South Africa', '-26.13032', '28.031479999999988', '1 Bompas Road, Randburg, South Africa', 'Wednesday, Oct 4', '14:40:24', '02:41:35 PM', '', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 2, 1, '2017-10-04'),
(88, 10, '', '28.411467697180143', '77.04328056424856', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '14:58:14', '02:59:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.411467697180143,77.04328056424856&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 28, '', 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-04'),
(89, 10, '', '28.411467697180143', '77.04328056424856', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '14:59:46', '03:11:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.411467697180143,77.04328056424856&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 28, '', 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-04'),
(90, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '15:11:58', '03:13:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 28, '', 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-04'),
(91, 10, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 4', '15:17:36', '03:17:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 28, '', 1, 1, 3, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(92, 11, '', '28.410764079563467', '77.04352229833603', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '', '', 'Set your drop point', 'Wednesday, Oct 4', '15:41:47', '02:29:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410764079563467,77.04352229833603&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 28, '', 1, 1, 2, 0, 4, 1, 0, 1, 1, '2017-10-04'),
(93, 12, '', '28.407149782296468', '77.04684253782034', '191A, Sai Dham Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.412164527622824', '77.04328659921885', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Friday, Oct 6', '12:33:10', '12:34:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.407149782296468,77.04684253782034&markers=color:red|label:D|28.412164527622824,77.04328659921885&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(94, 12, '', '28.41213149979167', '77.04333957284689', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '12:36:00', '12:36:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41213149979167,77.04333957284689&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 28, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(95, 12, '', '28.411821568481468', '77.05086953938007', '24, Block C Uppal Southland St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '14:00:21', '02:00:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.411821568481468,77.05086953938007&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 30, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(96, 11, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.459269417552733', '77.07241907715797', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', 'Friday, Oct 6', '14:31:15', '02:31:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 30, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(97, 11, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.462932631979015', '77.08659756928682', '6101, DLF Phase IV, Sector 27, Gurugram, Haryana 122022, India', 'Friday, Oct 6', '14:32:51', '02:32:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.462932631979015,77.08659756928682&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-06'),
(98, 11, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.462932631979015', '77.08659756928682', '6101, DLF Phase IV, Sector 27, Gurugram, Haryana 122022, India', 'Friday, Oct 6', '14:36:15', '02:37:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.462932631979015,77.08659756928682&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 30, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(99, 9, '', '-26.1306188571414', '28.0314851179719', '1 Bompas Road\nDunkeld West, Randburg, 2196', '-26.0604232', '28.0620258', '4 Wessel Rd', 'Friday, Oct 6', '17:28:08', '05:29:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1306188571414,28.0314851179719&markers=color:red|label:D|-26.0604232,28.0620258&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(100, 13, '', '28.412218492717617', '77.0432299375534', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 7', '06:21:34', '06:22:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412218492717617,77.0432299375534&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-07'),
(101, 13, '', '28.431174463393354', '77.03370306640863', 'N Block, JMD Gardens, SH13, Sector 33, Gurugram, Haryana 122022, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Sunday, Oct 8', '08:33:39', '08:34:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.431174463393354,77.03370306640863&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-08'),
(102, 13, '', '28.4346785592648', '77.03559301793575', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Sunday, Oct 8', '09:04:36', '09:04:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4346785592648,77.03559301793575&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-08'),
(103, 13, '', '28.4346785592648', '77.03559301793575', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Sunday, Oct 8', '09:06:12', '09:06:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4346785592648,77.03559301793575&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-08'),
(104, 13, '', '28.4346785592648', '77.03559301793575', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Sunday, Oct 8', '09:06:49', '09:07:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4346785592648,77.03559301793575&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-08'),
(105, 13, '', '28.4346779696076', '77.03559402376415', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.431802762069513', '77.0386980101466', '706, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', 'Sunday, Oct 8', '09:11:17', '09:11:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4346779696076,77.03559402376415&markers=color:red|label:D|28.431802762069513,77.0386980101466&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-08'),
(106, 13, '', '28.4346779696076', '77.03559402376415', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.431802762069513', '77.0386980101466', '706, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', 'Sunday, Oct 8', '09:12:09', '09:12:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4346779696076,77.03559402376415&markers=color:red|label:D|28.431802762069513,77.0386980101466&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-08'),
(107, 8, '', '28.4121381788309', '77.0434251110729', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4363971011904', '77.073788009584', '15, Road Number 12, Block D, Indira Colony 2, Sector 52\nGurugram, Haryana 122003', 'Monday, Oct 9', '10:40:26', '10:41:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121381788309,77.0434251110729&markers=color:red|label:D|28.4363971011904,77.073788009584&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-09'),
(108, 8, '', '28.4121640468426', '77.0433340898694', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.435458672852', '77.0793002843857', 'Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Monday, Oct 9', '13:43:13', '01:43:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121640468426,77.0433340898694&markers=color:red|label:D|28.435458672852,77.0793002843857&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-09'),
(109, 8, '', '28.4121981955891', '77.0433773256492', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4210869728006', '77.067963257432', 'Tigra, Sector 57\nGurugram, Haryana 122003', 'Monday, Oct 9', '13:44:12', '11:03:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121981955891,77.0433773256492&markers=color:red|label:D|28.4210869728006,77.067963257432&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 9, 0, 8, 2, 0, 1, 1, '2017-10-09'),
(110, 8, '', '28.4121981955891', '77.0433773256492', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4210869728006', '77.067963257432', 'Tigra, Sector 57\nGurugram, Haryana 122003', 'Monday, Oct 9', '13:44:53', '01:45:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121981955891,77.0433773256492&markers=color:red|label:D|28.4210869728006,77.067963257432&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-09'),
(111, 8, '', '28.4121367946888', '77.0434042381484', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.428317146293', '77.0667864382267', '416, Orchid Island, Sector 51\nGurugram, Haryana 122022', 'Monday, Oct 9', '14:04:41', '02:05:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121367946888,77.0434042381484&markers=color:red|label:D|28.428317146293,77.0667864382267&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-09'),
(112, 8, '', '28.4121614962419', '77.0434412697964', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4338223735604', '77.0848554745317', 'Wazirabad Road, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Monday, Oct 9', '14:14:14', '02:14:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121614962419,77.0434412697964&markers=color:red|label:D|28.4338223735604,77.0848554745317&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-10-09'),
(113, 8, '', '28.4121614962419', '77.0434412697964', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4338223735604', '77.0848554745317', 'Wazirabad Road, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Monday, Oct 9', '14:14:36', '02:15:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121614962419,77.0434412697964&markers=color:red|label:D|28.4338223735604,77.0848554745317&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-09'),
(114, 8, '', '28.4592693', '77.0724192', 'Huda Metro Station', '28.454976739784', '77.1046314015985', 'DLF Golf Course, Sector 42\nGurugram, Haryana', 'Monday, Oct 9', '14:19:59', '02:20:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4592693,77.0724192&markers=color:red|label:D|28.454976739784,77.1046314015985&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-09'),
(115, 13, '', '28.404876923037943', '77.05288756638765', '3/1, Emilia 1, Golf Course Ext Rd, Vatika City, Block W, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '11:04:11', '11:04:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.404876923037943,77.05288756638765&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(116, 13, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '11:22:59', '11:23:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(117, 13, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '11:35:18', '11:35:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(118, 13, '', '28.412216723370673', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '11:36:27', '11:36:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04321216791868&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(119, 13, '', '28.412048340385475', '77.04332482069731', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '12:15:29', '12:16:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412048340385475,77.04332482069731&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(120, 8, '', '28.4121730794703', '77.0433013513684', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4215080412643', '77.0598975196481', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Tuesday, Oct 10', '12:21:10', '12:21:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121730794703,77.0433013513684&markers=color:red|label:D|28.4215080412643,77.0598975196481&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(121, 8, '', '28.4122046328325', '77.0433231443167', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4327073169161', '77.0789663493633', '62, Manohara Marg, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Tuesday, Oct 10', '12:42:52', '12:43:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122046328325,77.0433231443167&markers=color:red|label:D|28.4327073169161,77.0789663493633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-10'),
(122, 13, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '05:35:06', '05:35:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 31, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(123, 14, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.074752508788237', '28.042486868798733', '4 Umgeni Rd, Riverclub, Sandton, 2191, South Africa', 'Thursday, Oct 12', '05:57:46', '05:59:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.074752508788237,28.042486868798733&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(124, 15, '', '-29.62555269229388', '30.403768569231033', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.584209', '30.378982999999998', 'Pick n Pay - Liberty Midlands Mall', 'Thursday, Oct 12', '06:09:24', '06:11:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62555269229388,30.403768569231033&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(125, 14, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.1240632', '28.034747099999997', '55 6th Rd', 'Thursday, Oct 12', '06:56:47', '06:58:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.1240632,28.034747099999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(126, 17, '', '-33.92112768093593', '18.585733361542225', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Thursday, Oct 12', '07:01:56', '07:03:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92112768093593,18.585733361542225&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(127, 17, '', '-33.92112378593522', '18.58571793884039', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.924868499999995', '18.4240553', 'Cape Town', 'Thursday, Oct 12', '07:19:37', '07:20:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92112378593522,18.58571793884039&markers=color:red|label:D|-33.924868499999995,18.4240553&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-12'),
(128, 17, '', '-33.92112378593522', '18.58571793884039', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.7342304', '18.9621091', 'Paarl', 'Thursday, Oct 12', '07:34:22', '07:34:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92112378593522,18.58571793884039&markers=color:red|label:D|-33.7342304,18.9621091&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(129, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.132164223734435', '27.992936074733734', '75 4th St, Linden, Randburg, 2104, South Africa', 'Thursday, Oct 12', '09:49:26', '09:49:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.132164223734435,27.992936074733734&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 2, 0, 1, 1, '2017-10-12'),
(130, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.132164223734435', '27.992936074733734', '75 4th St, Linden, Randburg, 2104, South Africa', 'Thursday, Oct 12', '09:50:45', '09:53:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.132164223734435,27.992936074733734&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(131, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.132164223734435', '27.992936074733734', '75 4th St, Linden, Randburg, 2104, South Africa', 'Thursday, Oct 12', '09:59:43', '10:00:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.132164223734435,27.992936074733734&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(132, 16, '', '-29.736321699141012', '31.06166180223227', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.818767', '31.011423999999998', '253 Peter Mokaba Rd', 'Thursday, Oct 12', '10:53:16', '10:55:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.736321699141012,31.06166180223227&markers=color:red|label:D|-29.818767,31.011423999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(133, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.153241656188957', '27.965070605278015', '5 Friedman Dr, Northcliff, Randburg, 2115, South Africa', 'Thursday, Oct 12', '11:01:13', '11:06:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|-26.153241656188957,27.965070605278015&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(134, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.118604996550097', '28.069375678896904', '61 Aspen Rd, Atholhurst, Sandton, 2196, South Africa', 'Thursday, Oct 12', '11:07:46', '11:10:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.118604996550097,28.069375678896904&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(135, 15, '', '-29.62555269229388', '30.403768569231033', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.581220000000002', '30.376410000000003', 'Musica - Liberty Mall', 'Thursday, Oct 12', '11:08:21', '11:09:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62555269229388,30.403768569231033&markers=color:red|label:D|-29.581220000000002,30.376410000000003&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-12'),
(136, 17, '', '-33.9145354155857', '18.420993760228157', 'Helen Suzman Blvd, De Waterkant, Cape Town, 8001, South Africa', '-33.913109999999996', '18.54276', '3 Voortrekker Rd, Townsend Estate', 'Thursday, Oct 12', '11:11:32', '11:13:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.9145354155857,18.420993760228157&markers=color:red|label:D|-33.913109999999996,18.54276&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 7, 1, 0, 1, 1, '2017-10-12'),
(137, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.118604996550097', '28.069375678896904', '61 Aspen Rd, Atholhurst, Sandton, 2196, South Africa', 'Thursday, Oct 12', '11:11:43', '11:14:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.118604996550097,28.069375678896904&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-12'),
(138, 16, '', '-29.73953305913419', '31.059599518775943', 'The Executive Rd, Umhlanga, 4051, South Africa', '-29.7259994', '31.065828500000002', 'Gateway', 'Thursday, Oct 12', '11:19:25', '11:29:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.73953305913419,31.059599518775943&markers=color:red|label:D|-29.7259994,31.065828500000002&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(139, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.118604996550097', '28.069375678896904', '61 Aspen Rd, Atholhurst, Sandton, 2196, South Africa', 'Thursday, Oct 12', '11:22:57', '11:25:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.118604996550097,28.069375678896904&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(140, 9, '', '-26.1232967139394', '28.0345991245892', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1669957656913', '27.9687539488077', '149 Waterval Road\nNewlands, Randburg, 2092', 'Thursday, Oct 12', '11:26:20', '11:33:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232967139394,28.0345991245892&markers=color:red|label:D|-26.1669957656913,27.9687539488077&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-12'),
(141, 9, '', '-26.123283354582288', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.118604996550097', '28.069375678896904', '61 Aspen Rd, Atholhurst, Sandton, 2196, South Africa', 'Thursday, Oct 12', '11:37:56', '11:40:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123283354582288,28.034492544829845&markers=color:red|label:D|-26.118604996550097,28.069375678896904&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(142, 9, '', '-26.1233007646468', '28.0347153598065', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1115791302981', '28.1059784442186', '4934 17th Avenue\nAlexandra, 2014', 'Thursday, Oct 12', '14:11:44', '02:13:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233007646468,28.0347153598065&markers=color:red|label:D|-26.1115791302981,28.1059784442186&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(143, 9, '', '-26.123319463436', '28.0346563357107', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1106046034575', '28.1225752830505', 'Lombardy East, Johannesburg\n2090', 'Thursday, Oct 12', '14:15:31', '02:16:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123319463436,28.0346563357107&markers=color:red|label:D|-26.1106046034575,28.1225752830505&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(144, 9, '', '-26.1233369374191', '28.0348640307784', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1008606947713', '28.1485303491354', 'Lakeview Drive\nLakeside, Lethabong, 1609', 'Thursday, Oct 12', '14:37:55', '02:38:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233369374191,28.0348640307784&markers=color:red|label:D|-26.1008606947713,28.1485303491354&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(145, 9, '', '-26.1233355462298', '28.0347944109704', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1539711567404', '28.0625648796558', '50 4th Street\nHoughton Estate, Johannesburg, 2198', 'Thursday, Oct 12', '14:43:51', '02:44:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233355462298,28.0347944109704&markers=color:red|label:D|-26.1539711567404,28.0625648796558&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-10-12'),
(146, 9, '', '-26.1233722082821', '28.0346207612506', '55 6th Avenue\nHyde Park, Sandton, 2196', '-26.145322790761', '28.0679950118065', '29-30 Pretoria Street\nOaklands, Johannesburg, 2192', 'Thursday, Oct 12', '14:59:01', '02:59:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233722082821,28.0346207612506&markers=color:red|label:D|-26.145322790761,28.0679950118065&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 3, 2, 1, 1, '2017-10-12'),
(147, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.16874256144879', '28.0722476541996', '47 1st Ave, Houghton Estate, Johannesburg, 2198, South Africa', 'Thursday, Oct 12', '15:13:03', '03:14:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|-26.16874256144879,28.0722476541996&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 3, 2, 1, 1, '2017-10-12'),
(148, 16, '', '-29.736265802924756', '31.06169298291206', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.8420982', '30.9946288', '253 Peter Mokaba Ridge', 'Thursday, Oct 12', '15:28:28', '03:31:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.736265802924756,31.06169298291206&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-10-12'),
(149, 9, '', '-29.6102663032845', '30.390163064003', '54 Payn Street\nPietermaritzburg, 3201', '-29.8794861926611', '30.9841865301132', '22 Brisker Road\nUmbilo, Berea, 4075', 'Thursday, Oct 12', '15:30:37', '03:30:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.6102663032845,30.390163064003&markers=color:red|label:D|-29.8794861926611,30.9841865301132&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(150, 9, '', '-29.7447861228282', '31.0442391782999', '4 Fernlea Road\nSunningdale, Umhlanga, 4051', '-29.8794861926611', '30.9841865301132', '22 Brisker Road\nUmbilo, Berea, 4075', 'Thursday, Oct 12', '15:33:00', '03:33:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.7447861228282,31.0442391782999&markers=color:red|label:D|-29.8794861926611,30.9841865301132&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 2, 1, 1, '2017-10-12'),
(151, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.1383533', '28.0405646', 'Dunkeld', 'Thursday, Oct 12', '16:23:40', '04:24:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(152, 9, '', '-26.0747311269819', '28.0464826896787', '91 Coleraine Drive\nRiverclub, Sandton, 2191', '-26.06071', '28.06227', '4A Wessel Rd', 'Thursday, Oct 12', '17:01:38', '05:07:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0747311269819,28.0464826896787&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(153, 9, '', '-26.0607817729906', '28.0622792766961', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1036526225716', '28.0333341658115', '38 Sutherland Avenue\nHurlingham, Sandton, 2070', 'Thursday, Oct 12', '19:15:22', '07:17:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607817729906,28.0622792766961&markers=color:red|label:D|-26.1036526225716,28.0333341658115&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 4, 2, 1, 1, '2017-10-12'),
(154, 17, '', '-33.8942695', '18.6294384', 'Bellville', '-33.9019884', '18.420827799999994', 'Waterfront', 'Thursday, Oct 12', '20:16:22', '08:18:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.8942695,18.6294384&markers=color:red|label:D|-33.9019884,18.420827799999994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(155, 17, '', '-33.8942695', '18.6294384', 'Bellville', '-33.8275375', '18.494798700000004', 'Table View', 'Thursday, Oct 12', '20:25:17', '08:29:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.8942695,18.6294384&markers=color:red|label:D|-33.8275375,18.494798700000004&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(156, 9, '', '-26.0607817600001', '28.0622793099996', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0542500812377', '28.1131583824754', '16C Stirling Avenue\nBuccleuch, Sandton, 2090', 'Thursday, Oct 12', '20:38:32', '08:39:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607817600001,28.0622793099996&markers=color:red|label:D|-26.0542500812377,28.1131583824754&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-12'),
(157, 16, '', '-26.106067999999997', '28.05318799999999', 'Sandton Convention Centre', '-25.748946399999998', '28.1854042', 'Pretoria Central', 'Thursday, Oct 12', '20:44:52', '08:46:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.106067999999997,28.05318799999999&markers=color:red|label:D|-25.748946399999998,28.1854042&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(158, 16, '', '-26.106067999999997', '28.05318799999999', 'Sandton Convention Centre', '-25.748946399999998', '28.1854042', 'Pretoria Central', 'Thursday, Oct 12', '20:48:32', '08:54:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.106067999999997,28.05318799999999&markers=color:red|label:D|-25.748946399999998,28.1854042&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-12'),
(159, 17, '', '-33.89692822342947', '18.628637976944447', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-26.204102799999998', '28.0473051', 'Johannesburg', 'Thursday, Oct 12', '20:50:57', '08:51:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89692822342947,18.628637976944447&markers=color:red|label:D|-26.204102799999998,28.0473051&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-12'),
(160, 17, '', '-33.89692822342947', '18.628637976944447', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-26.204102799999998', '28.0473051', 'Johannesburg', 'Thursday, Oct 12', '20:51:39', '08:52:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89692822342947,18.628637976944447&markers=color:red|label:D|-26.204102799999998,28.0473051&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(161, 16, '', '-26.106067999999997', '28.05318799999999', 'Sandton Convention Centre', '-25.748946399999998', '28.1854042', 'Pretoria Central', 'Thursday, Oct 12', '20:58:04', '08:58:04 PM', '', '14/10/2017', '12:57', 0, 28, '', 2, 1, 1, 0, 0, 3, 0, 1, 1, '2017-10-12'),
(162, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.6196074', '30.3959516', 'University of KwaZulu-Natal - Pietermaritzburg Campus', 'Thursday, Oct 12', '21:20:15', '09:20:15 PM', '', '12/10/2017', '22:19', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(163, 16, '', '-29.887132', '30.984187', 'Umbilo Road', '-29.730992399999998', '31.072205999999998', 'Umhlanga Ridge', 'Thursday, Oct 12', '21:21:14', '09:21:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.887132,30.984187&markers=color:red|label:D|-29.730992399999998,31.072205999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 2, 0, 7, 1, 0, 1, 1, '2017-10-12'),
(164, 15, '', '-29.855878651713923', '31.003583930432793', '249 King Dinizulu Rd, Bulwer, Durban, 4001, South Africa', '', '', 'Set your drop point', 'Thursday, Oct 12', '21:23:13', '09:23:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.855878651713923,31.003583930432793&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(165, 20, '', '28.4136802474741', '77.0452901402732', '1002, Sispal Vihar Internal Road, Block K, South City II, Sector 49\nGurugram, Haryana 122018', '28.4317396670167', '77.0796660706401', '62, Manohara Marg, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Friday, Oct 13', '07:05:31', '07:05:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4136802474741,77.0452901402732&markers=color:red|label:D|28.4317396670167,77.0796660706401&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 37, 28, '', 1, 1, 7, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(166, 20, '', '28.4121316604723', '77.0433445297692', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317396670167', '77.0796660706401', '62, Manohara Marg, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Friday, Oct 13', '07:41:25', '07:41:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121316604723,77.0433445297692&markers=color:red|label:D|28.4317396670167,77.0796660706401&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(167, 20, '', '28.4121316604723', '77.0433445297692', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317396670167', '77.0796660706401', '62, Manohara Marg, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Friday, Oct 13', '07:43:42', '07:43:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121316604723,77.0433445297692&markers=color:red|label:D|28.4317396670167,77.0796660706401&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(168, 20, '', '28.4121095943497', '77.0432807256456', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317396670167', '77.0796660706401', '62, Manohara Marg, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Friday, Oct 13', '07:46:55', '07:46:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121095943497,77.0432807256456&markers=color:red|label:D|28.4317396670167,77.0796660706401&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(169, 20, '', '28.4120846120494', '77.043260447681', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4650120716056', '77.1235563978553', 'Block E, Aya Nagar Extension, Aya Nagar\nNew Delhi, Delhi', 'Friday, Oct 13', '07:53:03', '07:53:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120846120494,77.043260447681&markers=color:red|label:D|28.4650120716056,77.1235563978553&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 30, '', 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(170, 17, '', '-33.91981366452245', '18.58677338808775', '22 Glenhurst St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Friday, Oct 13', '08:26:52', '08:29:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.91981366452245,18.58677338808775&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(171, 20, '', '28.4121008310821', '77.043289616704', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4407497903343', '77.0869251340628', 'Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Friday, Oct 13', '09:28:52', '09:30:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121008310821,77.043289616704&markers=color:red|label:D|28.4407497903343,77.0869251340628&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(172, 15, '', '-29.62555269229388', '30.403768569231033', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.626340000000003', '30.413919999999994', '19 Medwood Pl', 'Friday, Oct 13', '09:29:13', '09:30:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62555269229388,30.403768569231033&markers=color:red|label:D|-29.626340000000003,30.413919999999994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(173, 9, '', '-26.1233158655199', '28.0348368734121', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1620760102825', '28.0993758514524', '12 Greene Street\nLinksfield, Johannesburg, 2192', 'Friday, Oct 13', '13:15:34', '01:17:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233158655199,28.0348368734121&markers=color:red|label:D|-26.1620760102825,28.0993758514524&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(174, 21, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 13', '13:20:57', '01:20:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(175, 21, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 13', '13:22:10', '01:22:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(176, 15, '', '-29.62555269229388', '30.403768569231033', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.626340000000003', '30.413919999999994', '19 Medwood Pl', 'Friday, Oct 13', '13:40:29', '01:42:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62555269229388,30.403768569231033&markers=color:red|label:D|-29.626340000000003,30.413919999999994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(177, 9, '', '-26.1231992327963', '28.0341994848092', '60 Hyde Close\nHyde Park, Sandton, 2196', '-26.1063237543356', '28.107174038887', '1478 20th Avenue\nAlexandra, 2014', 'Friday, Oct 13', '13:49:38', '01:49:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1231992327963,28.0341994848092&markers=color:red|label:D|-26.1063237543356,28.107174038887&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(178, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.115650258514826', '28.090326078236107', '29 Arkwright Ave, Wynberg, Sandton, 2090, South Africa', 'Friday, Oct 13', '13:53:09', '01:53:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|-26.115650258514826,28.090326078236107&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(179, 13, '', '28.412141820989998', '77.04322054982185', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 13', '14:53:11', '02:53:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412141820989998,77.04322054982185&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(180, 13, '', '28.412141820989998', '77.04322054982185', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 13', '14:55:28', '02:55:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412141820989998,77.04322054982185&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(181, 15, '', '-29.62141143668117', '30.405040271580223', '26 Fairfield Ave, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.596799092900394', '30.404670462012295', '20 Ohrtmann Rd, Willowton, Pietermaritzburg, 3201, South Africa', 'Friday, Oct 13', '15:17:56', '03:18:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62141143668117,30.405040271580223&markers=color:red|label:D|-29.596799092900394,30.404670462012295&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(182, 9, '', '-26.1233242200602', '28.0347810584458', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.0818356948143', '28.1050453707576', '6 Hammonia Way\nKelvin, Sandton, 2090', 'Friday, Oct 13', '15:19:48', '03:22:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233242200602,28.0347810584458&markers=color:red|label:D|-26.0818356948143,28.1050453707576&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 3, 2, 1, 1, '2017-10-13');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(183, 9, '', '-26.1233349576262', '28.0346772170378', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.0944374722753', '28.1331736966968', '53 Oak Avenue\nLinbro Park AH, Sandton, 2090', 'Friday, Oct 13', '15:21:18', '03:21:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233349576262,28.0346772170378&markers=color:red|label:D|-26.0944374722753,28.1331736966968&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 2, 1, 1, '2017-10-13'),
(184, 9, '', '-26.1233349576262', '28.0346772170378', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.0944374722753', '28.1331736966968', '53 Oak Avenue\nLinbro Park AH, Sandton, 2090', 'Friday, Oct 13', '15:23:09', '03:23:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233349576262,28.0346772170378&markers=color:red|label:D|-26.0944374722753,28.1331736966968&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 3, 2, 1, 1, '2017-10-13'),
(185, 9, '', '-26.1233412960239', '28.034673718327', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1720437985569', '27.9808916151524', '8-12 Milner Road\nSophiatown, Johannesburg, 2092', 'Friday, Oct 13', '15:24:46', '03:25:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233412960239,28.034673718327&markers=color:red|label:D|-26.1720437985569,27.9808916151524&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(186, 9, '', '-26.1233370966486', '28.0346838229744', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1966281679284', '27.9542076587677', '9 Helderberg Drive\nBosmont, Randburg, 2093', 'Friday, Oct 13', '15:27:12', '03:27:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233370966486,28.0346838229744&markers=color:red|label:D|-26.1966281679284,27.9542076587677&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(187, 16, '', '-29.736282105991055', '31.06161218136549', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.818767', '31.011423999999998', '253 Peter Mokaba Rd', 'Friday, Oct 13', '15:29:03', '03:54:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.736282105991055,31.06161218136549&markers=color:red|label:D|-29.818767,31.011423999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-13'),
(188, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.084782855718156', '28.09833616018295', '18 Eastway St, Kelvin, Sandton, 2090, South Africa', 'Friday, Oct 13', '15:32:48', '03:33:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|-26.084782855718156,28.09833616018295&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(189, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.084782855718156', '28.09833616018295', '18 Eastway St, Kelvin, Sandton, 2090, South Africa', 'Friday, Oct 13', '15:33:29', '03:33:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|-26.084782855718156,28.09833616018295&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(190, 9, '', '-26.123304727514547', '28.034492544829845', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.084782855718156', '28.09833616018295', '18 Eastway St, Kelvin, Sandton, 2090, South Africa', 'Friday, Oct 13', '15:34:12', '03:34:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123304727514547,28.034492544829845&markers=color:red|label:D|-26.084782855718156,28.09833616018295&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(191, 9, '', '-26.123309543949453', '28.03446974605322', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.165410838096', '27.972720935940742', '19 Tram St, Albertville, Randburg, 2195, South Africa', 'Friday, Oct 13', '15:35:11', '03:36:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123309543949453,28.03446974605322&markers=color:red|label:D|-26.165410838096,27.972720935940742&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(192, 9, '', '-26.123309543949453', '28.03446974605322', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.165410838096', '27.972720935940742', '19 Tram St, Albertville, Randburg, 2195, South Africa', 'Friday, Oct 13', '15:47:30', '03:47:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123309543949453,28.03446974605322&markers=color:red|label:D|-26.165410838096,27.972720935940742&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(193, 9, '', '-26.123309543949453', '28.03446974605322', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.165410838096', '27.972720935940742', '19 Tram St, Albertville, Randburg, 2195, South Africa', 'Friday, Oct 13', '15:47:51', '03:48:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123309543949453,28.03446974605322&markers=color:red|label:D|-26.165410838096,27.972720935940742&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(194, 15, '', '-29.86626064524452', '30.999396666884422', '264 Umbilo Rd, Bulwer, Berea, 4083, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Friday, Oct 13', '16:34:28', '04:36:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86626064524452,30.999396666884422&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 2, 0, 4, 1, 0, 1, 1, '2017-10-13'),
(195, 23, '', '-29.862913', '30.983584', '5 Princess Anne Pl', '-29.821880499999995', '31.0102536', 'Peter Mokaba Road', 'Friday, Oct 13', '16:36:33', '04:37:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.862913,30.983584&markers=color:red|label:D|-29.821880499999995,31.0102536&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(196, 15, '', '-29.86626064524452', '30.999396666884422', '264 Umbilo Rd, Bulwer, Berea, 4083, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Friday, Oct 13', '16:37:41', '04:37:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86626064524452,30.999396666884422&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(197, 15, '', '-29.86626064524452', '30.999396666884422', '264 Umbilo Rd, Bulwer, Berea, 4083, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Friday, Oct 13', '16:38:29', '04:58:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86626064524452,30.999396666884422&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(198, 23, '', '-29.862913', '30.983584', '5 Princess Anne Pl', '-29.8385914', '30.997902199999995', 'Peter Mokaba Ridge', 'Friday, Oct 13', '16:43:38', '04:43:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.862913,30.983584&markers=color:red|label:D|-29.8385914,30.997902199999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(199, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870533097846415', '30.978108309209347', '359 King George V Ave, Glenwood, Durban, 4001, South Africa', 'Friday, Oct 13', '18:13:54', '06:15:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.870533097846415,30.978108309209347&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(200, 9, '', '-26.06076997', '28.0623503800001', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1055192900543', '28.0256603658199', '13 Gleneagles Road\nHurlingham, Sandton, 2070', 'Friday, Oct 13', '18:17:04', '06:17:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06076997,28.0623503800001&markers=color:red|label:D|-26.1055192900543,28.0256603658199&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(201, 23, '', '-29.862913', '30.983584', '5 Princess Anne Pl', '-29.847008300000002', '31.0019708', 'Musgrave', 'Friday, Oct 13', '18:33:09', '06:33:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.862913,30.983584&markers=color:red|label:D|-29.847008300000002,31.0019708&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 2, 0, 7, 1, 0, 1, 1, '2017-10-13'),
(202, 23, '', '-29.862913', '30.983584', '5 Princess Anne Pl', '-29.847008300000002', '31.0019708', 'Musgrave', 'Friday, Oct 13', '18:37:55', '06:46:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.862913,30.983584&markers=color:red|label:D|-29.847008300000002,31.0019708&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(203, 9, '', '-26.0607818284167', '28.0622785538435', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0867744885923', '28.0751397460699', '27 Herold Avenue\nWendywood, Sandton, 2148', 'Friday, Oct 13', '20:04:29', '08:04:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607818284167,28.0622785538435&markers=color:red|label:D|-26.0867744885923,28.0751397460699&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(204, 9, '', '-26.0607818284167', '28.0622785538435', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0867744885923', '28.0751397460699', '27 Herold Avenue\nWendywood, Sandton, 2148', 'Friday, Oct 13', '20:05:15', '08:05:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607818284167,28.0622785538435&markers=color:red|label:D|-26.0867744885923,28.0751397460699&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(205, 9, '', '-26.0607818284167', '28.0622785538435', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0867744885923', '28.0751397460699', '27 Herold Avenue\nWendywood, Sandton, 2148', 'Friday, Oct 13', '20:05:54', '08:05:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607818284167,28.0622785538435&markers=color:red|label:D|-26.0867744885923,28.0751397460699&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(206, 15, '', '-29.871805058426983', '30.97932569682598', 'Convent Cl, University, Berea, 4041, South Africa', '-29.855856499999998', '31.002168500000003', 'Berea Centre Road', 'Saturday, Oct 14', '11:22:48', '11:29:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.871805058426983,30.97932569682598&markers=color:red|label:D|-29.855856499999998,31.002168500000003&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(207, 16, '', '-29.884247700000003', '30.979028099999997', 'Umbilo', '-29.818912713420644', '31.011828370392326', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', 'Saturday, Oct 14', '11:29:16', '11:29:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.884247700000003,30.979028099999997&markers=color:red|label:D|-29.818912713420644,31.011828370392326&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 0, 1, 1, '2017-10-14'),
(208, 15, '', '-29.856884739906082', '31.02627005428076', '161 Monty Naicker Rd, Durban Central, Durban, 4001, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Saturday, Oct 14', '11:58:28', '12:16:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.856884739906082,31.02627005428076&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(209, 6, '', '-26.0607700296639', '28.0623500126901', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0118722342679', '28.1506637111306', '28 Burger Road\nPresident Park AH, Midrand, 1685', 'Saturday, Oct 14', '11:59:15', '12:01:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607700296639,28.0623500126901&markers=color:red|label:D|-26.0118722342679,28.1506637111306&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 29, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(210, 9, '', '-26.060776708218892', '28.062391877174374', '4A Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.070143886364242', '28.019639812409878', '7 Clonmore Rd, Bryanston, Sandton, 2191, South Africa', 'Saturday, Oct 14', '12:18:02', '12:20:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.060776708218892,28.062391877174374&markers=color:red|label:D|-26.070143886364242,28.019639812409878&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(211, 24, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.097250630007043', '28.037856705486774', '77 7th St, Parkmore, Sandton, 2196, South Africa', 'Saturday, Oct 14', '15:26:36', '03:29:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.097250630007043,28.037856705486774&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(212, 24, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.088859433531915', '28.013796620070934', '15 Arohnson Rd, Lyme Park, Sandton, 2060, South Africa', 'Saturday, Oct 14', '15:31:29', '03:31:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.088859433531915,28.013796620070934&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-14'),
(213, 24, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.088859433531915', '28.013796620070934', '15 Arohnson Rd, Lyme Park, Sandton, 2060, South Africa', 'Saturday, Oct 14', '15:33:55', '03:35:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.088859433531915,28.013796620070934&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(214, 24, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.078462042510726', '28.028498142957687', '30A Arklow Rd, Bryanston, Sandton, 2191, South Africa', 'Saturday, Oct 14', '21:18:51', '09:18:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.078462042510726,28.028498142957687&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-14'),
(215, 24, '', '-26.06078176', '28.06227931', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0711009817326', '28.0338685959578', '30 East Hertford Road\nBryanston, Sandton, 2191', 'Saturday, Oct 14', '21:21:53', '09:21:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06078176,28.06227931&markers=color:red|label:D|-26.0711009817326,28.0338685959578&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-14'),
(216, 15, '', '-29.890898217199304', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.86077843825714', '31.010788679122925', '31 Berea Rd, Greyville, Berea, 4001, South Africa', 'Sunday, Oct 15', '05:07:57', '05:07:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890898217199304,30.979106426239017&markers=color:red|label:D|-29.86077843825714,31.010788679122925&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 2, 0, 0, 3, 0, 1, 1, '2017-10-15'),
(217, 24, '', '-26.074406183275', '28.044501543045', '99 Ballyclare Drive\nRiverclub, Sandton, 2191', '-26.06071', '28.06227', '4A Wessel Rd', 'Sunday, Oct 15', '13:27:28', '01:36:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.074406183275,28.044501543045&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(218, 24, '', '-26.0607818284167', '28.0622792243958', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1634343878591', '28.0766950920224', '6 Woodlands Road\nVictoria, Johannesburg, 2192', 'Sunday, Oct 15', '15:04:25', '03:04:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607818284167,28.0622792243958&markers=color:red|label:D|-26.1634343878591,28.0766950920224&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(219, 24, '', '-26.0607818284167', '28.0622792243958', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1634343878591', '28.0766950920224', '6 Woodlands Road\nVictoria, Johannesburg, 2192', 'Sunday, Oct 15', '15:05:34', '03:06:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607818284167,28.0622792243958&markers=color:red|label:D|-26.1634343878591,28.0766950920224&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-15'),
(220, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.03455263875636', '28.099365457892418', '878 Victoria Falls Ave, Waterval 5-Ir, Midrand, 2090, South Africa', 'Sunday, Oct 15', '15:09:29', '03:09:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.03455263875636,28.099365457892418&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(221, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.03455263875636', '28.099365457892418', '878 Victoria Falls Ave, Waterval 5-Ir, Midrand, 2090, South Africa', 'Sunday, Oct 15', '15:26:43', '03:26:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.03455263875636,28.099365457892418&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(222, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.83104337888784', '30.98247729241848', '78 Keal Rd, Sparks, Berea, 4091, South Africa', 'Sunday, Oct 15', '15:33:01', '03:34:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.83104337888784,30.98247729241848&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-15'),
(223, 9, '', '-26.061181805529856', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.078175956659788', '28.039667531847954', '27 Jukskei Ave, Riverclub, Sandton, 2191, South Africa', 'Sunday, Oct 15', '17:09:27', '05:09:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.061181805529856,28.062570914626125&markers=color:red|label:D|-26.078175956659788,28.039667531847954&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(224, 24, '', '-26.0607817600331', '28.0622793100326', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0194298431775', '28.0999149754643', 'Unnamed Road\nWaterval 5-Ir, Midrand, 2066', 'Sunday, Oct 15', '17:12:27', '05:12:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607817600331,28.0622793100326&markers=color:red|label:D|-26.0194298431775,28.0999149754643&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(225, 24, '', '-26.0607817600331', '28.0622793100326', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0194298431775', '28.0999149754643', 'Unnamed Road\nWaterval 5-Ir, Midrand, 2066', 'Sunday, Oct 15', '17:12:44', '05:12:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607817600331,28.0622793100326&markers=color:red|label:D|-26.0194298431775,28.0999149754643&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(226, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.094895446799978', '28.035806156694886', '60 9th St, Parkmore, Sandton, 2196, South Africa', 'Sunday, Oct 15', '17:19:14', '05:19:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.094895446799978,28.035806156694886&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(227, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.094895446799978', '28.035806156694886', '60 9th St, Parkmore, Sandton, 2196, South Africa', 'Sunday, Oct 15', '17:20:18', '05:20:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.094895446799978,28.035806156694886&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(228, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.094895446799978', '28.035806156694886', '60 9th St, Parkmore, Sandton, 2196, South Africa', 'Sunday, Oct 15', '18:54:34', '06:54:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.094895446799978,28.035806156694886&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(229, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.094895446799978', '28.035806156694886', '60 9th St, Parkmore, Sandton, 2196, South Africa', 'Sunday, Oct 15', '18:55:15', '06:55:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.094895446799978,28.035806156694886&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 29, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(230, 6, '', '-26.06116734854956', '28.062570914626125', '7 5th Ave, Edenburg, Sandton, 2128, South Africa', '-26.094895446799978', '28.035806156694886', '60 9th St, Parkmore, Sandton, 2196, South Africa', 'Sunday, Oct 15', '18:56:03', '06:56:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06116734854956,28.062570914626125&markers=color:red|label:D|-26.094895446799978,28.035806156694886&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(231, 25, '', '28.4120770596907', '77.0432814197413', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4201395626342', '77.0532282069325', '75, Vikas Marg, Malibu Town, Sector 47\nGurugram, Haryana 122018', 'Monday, Oct 16', '11:13:24', '11:15:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120770596907,77.0432814197413&markers=color:red|label:D|28.4201395626342,77.0532282069325&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(232, 6, '', '-26.12328124739157', '28.034492209553715', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.096004394122676', '28.132532984018326', '53 Clulee Rd, Linbro Park AH, Sandton, 2090, South Africa', 'Monday, Oct 16', '11:28:13', '11:28:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.12328124739157,28.034492209553715&markers=color:red|label:D|-26.096004394122676,28.132532984018326&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-16'),
(233, 13, '', '28.412169540774844', '77.04319003969431', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 16', '15:01:11', '03:01:11 PM', '', '16/10/2017', '19:30', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-16'),
(234, 13, '', '28.412169540774844', '77.04319003969431', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 16', '15:26:02', '03:27:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169540774844,77.04319003969431&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(235, 27, '', '28.4121194092437', '77.0433120802045', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4423137547207', '77.0948691666126', 'Khatu Shyam Road, Parsvnath Exotica, DLF Phase 5, Sector 53\nGurugram, Haryana 122003', 'Monday, Oct 16', '15:26:50', '03:27:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121194092437,77.0433120802045&markers=color:red|label:D|28.4423137547207,77.0948691666126&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(236, 13, '', '28.43458332958568', '77.03541565686464', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 16', '17:08:21', '05:09:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43458332958568,77.03541565686464&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(237, 13, '', '28.43458332958568', '77.03541565686464', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.45758485044269', '77.07547478377819', 'Mahalaxmi Apartments, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '17:10:40', '05:11:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43458332958568,77.03541565686464&markers=color:red|label:D|28.45758485044269,77.07547478377819&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(238, 13, '', '28.43458332958568', '77.03541565686464', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.45758485044269', '77.07547478377819', 'Mahalaxmi Apartments, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '17:13:56', '05:13:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43458332958568,77.03541565686464&markers=color:red|label:D|28.45758485044269,77.07547478377819&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-16'),
(239, 13, '', '28.43458332958568', '77.03541565686464', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.45758485044269', '77.07547478377819', 'Mahalaxmi Apartments, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '17:13:57', '05:15:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43458332958568,77.03541565686464&markers=color:red|label:D|28.45758485044269,77.07547478377819&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(240, 13, '', '28.412155680883338', '77.0432061329484', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 17', '06:27:53', '06:49:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412155680883338,77.0432061329484&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-17'),
(241, 13, '', '28.408760837508158', '77.04242091625929', 'P-103, Sohna Rd, Parsvnath Greenville, Tatvam Villas, Uppal Southend, Sector 48, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 17', '06:49:04', '06:49:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.408760837508158,77.04242091625929&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(242, 13, '', '28.408760837508158', '77.04242091625929', 'P-103, Sohna Rd, Parsvnath Greenville, Tatvam Villas, Uppal Southend, Sector 48, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 17', '06:50:23', '06:50:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.408760837508158,77.04242091625929&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(243, 13, '', '28.408760837508158', '77.04242091625929', 'P-103, Sohna Rd, Parsvnath Greenville, Tatvam Villas, Uppal Southend, Sector 48, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 17', '06:53:17', '06:53:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.408760837508158,77.04242091625929&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(244, 13, '', '28.41214771881717', '77.04322457313538', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 17', '06:54:21', '06:54:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41214771881717,77.04322457313538&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 40, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(245, 27, '', '28.4120791648749', '77.0432892304655', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4233783552437', '77.0559456199408', 'B/27, Vikas Marg, Pocket C, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Tuesday, Oct 17', '09:04:49', '09:05:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120791648749,77.0432892304655&markers=color:red|label:D|28.4233783552437,77.0559456199408&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(246, 15, '', '-29.572085680284125', '30.356660597026348', '23 Mc Carthy Dr, Chase Valley Downs, Pietermaritzburg, 3201, South Africa', '-29.555928496176108', '30.388436391949657', '494 Bombay Rd, Northdale, Pietermaritzburg, 3201, South Africa', 'Wednesday, Oct 18', '12:02:17', '12:03:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.572085680284125,30.356660597026348&markers=color:red|label:D|-29.555928496176108,30.388436391949657&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-18'),
(247, 24, '', '-26.061215538477', '28.0625109001994', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0962621332926', '28.0113729089499', '15 Jean Avenue\nBordeaux, Randburg, 2194', 'Saturday, Oct 21', '09:16:35', '09:16:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.061215538477,28.0625109001994&markers=color:red|label:D|-26.0962621332926,28.0113729089499&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-21'),
(248, 24, '', '-26.061215538477', '28.0625109001994', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.0962621332926', '28.0113729089499', '15 Jean Avenue\nBordeaux, Randburg, 2194', 'Saturday, Oct 21', '09:18:43', '09:18:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.061215538477,28.0625109001994&markers=color:red|label:D|-26.0962621332926,28.0113729089499&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-21'),
(249, 24, '', '-26.0607931600596', '28.0622814805618', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0436369476923', '28.0877012014389', '54 Lincoln Street\nWoodmead, Sandton, 2191', 'Saturday, Oct 21', '09:19:39', '09:20:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607931600596,28.0622814805618&markers=color:red|label:D|-26.0436369476923,28.0877012014389&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 2, 0, 4, 1, 0, 1, 1, '2017-10-21'),
(250, 24, '', '-26.0491815969743', '28.0566465854645', '90 12th Avenue\nEdenburg, Sandton, 2128', '-26.0779052284581', '28.0511222407222', '11 Acacia Road\nDuxberry, Sandton, 2191', 'Saturday, Oct 21', '09:22:46', '09:24:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0491815969743,28.0566465854645&markers=color:red|label:D|-26.0779052284581,28.0511222407222&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-21'),
(251, 24, '', '-26.0607990506477', '28.0623363236268', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0746010291463', '28.0343470349908', '54 Ballyclare Drive\nBryanston, Sandton, 2191', 'Saturday, Oct 21', '09:28:13', '09:28:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607990506477,28.0623363236268&markers=color:red|label:D|-26.0746010291463,28.0343470349908&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-21'),
(252, 24, '', '-26.0607571309899', '28.0623583495617', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0907633737622', '28.0329140648246', '30 12th Street\nParkmore, Sandton, 2196', 'Saturday, Oct 21', '14:18:41', '02:19:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607571309899,28.0623583495617&markers=color:red|label:D|-26.0907633737622,28.0329140648246&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-21'),
(253, 24, '', '-26.0608226610783', '28.0623266339185', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.06166099318', '28.0558167770505', '154 Coleraine Drive\nMorningside, Sandton, 2057', 'Saturday, Oct 21', '14:23:01', '02:23:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608226610783,28.0623266339185&markers=color:red|label:D|-26.06166099318,28.0558167770505&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-21'),
(254, 13, '', '28.41208461204939', '77.04330068081617', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 23', '05:48:45', '05:49:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41208461204939,77.04330068081617&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 29, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(255, 28, '', '28.41216039914448', '77.0432735234499', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.427868684092815', '77.05816011875866', 'Satpaul Mittal Marg, Huda Colony, Sector 51, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '06:48:40', '06:53:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41216039914448,77.0432735234499&markers=color:red|label:D|28.427868684092815,77.05816011875866&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 46, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(256, 28, '', '28.41216039914448', '77.0432735234499', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.427868684092815', '77.05816011875866', 'Satpaul Mittal Marg, Huda Colony, Sector 51, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '06:55:43', '06:55:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41216039914448,77.0432735234499&markers=color:red|label:D|28.427868684092815,77.05816011875866&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(257, 27, '', '28.4120887405307', '77.0432862639427', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4319935211828', '77.081671692431', 'H62, Manohara Marg, Block B1, Sector 52\nGurugram, Haryana 122011', 'Monday, Oct 23', '06:58:00', '06:58:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120887405307,77.0432862639427&markers=color:red|label:D|28.4319935211828,77.081671692431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 47, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(258, 28, '', '28.412187234250737', '77.04321954399347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.423412853866918', '77.06370525062083', 'J - Block, Block J, Mayfield Garden, Sector 51, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '07:38:27', '11:50:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412187234250737,77.04321954399347&markers=color:red|label:D|28.423412853866918,77.06370525062083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 30, '', 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-10-23'),
(259, 28, '', '28.412187234250737', '77.04321954399347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.423412853866918', '77.06370525062083', 'J - Block, Block J, Mayfield Garden, Sector 51, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '07:43:29', '07:43:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412187234250737,77.04321954399347&markers=color:red|label:D|28.423412853866918,77.06370525062083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 47, 30, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-23'),
(260, 28, '', '28.412187234250737', '77.04321954399347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.42763398516879', '77.05834284424782', '1412, Vikas Marg, Huda Colony, Sector 46, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '07:54:26', '07:57:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412187234250737,77.04321954399347&markers=color:red|label:D|28.42763398516879,77.05834284424782&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 47, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(261, 29, '', '28.412194311640278', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Set your drop point', 'Monday, Oct 23', '09:16:25', '09:16:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412194311640278,77.04321216791868&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(262, 29, '', '28.412194311640278', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 23', '10:37:39', '10:38:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412194311640278,77.04321216791868&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 48, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(263, 29, '', '28.412194311640278', '77.04321216791868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 23', '10:39:26', '10:39:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412194311640278,77.04321216791868&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 48, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(264, 28, '', '28.412185759794543', '77.04322155565023', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.424358170739826', '77.0601449534297', 'B - 77, Nirvana Central Rd, Pocket C, Mayfield Garden, Sector 50, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '10:41:02', '10:42:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412185759794543,77.04322155565023&markers=color:red|label:D|28.424358170739826,77.0601449534297&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 47, 30, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-23'),
(265, 28, '', '28.412185759794543', '77.04322155565023', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.424358170739826', '77.0601449534297', 'B - 77, Nirvana Central Rd, Pocket C, Mayfield Garden, Sector 50, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '10:43:27', '10:43:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412185759794543,77.04322155565023&markers=color:red|label:D|28.424358170739826,77.0601449534297&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 47, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(266, 28, '', '28.412185759794543', '77.04322155565023', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.424358170739826', '77.0601449534297', 'B - 77, Nirvana Central Rd, Pocket C, Mayfield Garden, Sector 50, Gurugram, Haryana 122018, India', 'Monday, Oct 23', '10:44:29', '10:44:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412185759794543,77.04322155565023&markers=color:red|label:D|28.424358170739826,77.0601449534297&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 47, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(267, 30, '', '28.41217455392666', '77.04324468970299', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.436886212755127', '77.06709623336792', '3115, Vikas Marg, Sector 46, Gurugram, Haryana 122022, India', 'Monday, Oct 23', '11:23:28', '11:25:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41217455392666,77.04324468970299&markers=color:red|label:D|28.436886212755127,77.06709623336792&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 46, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(268, 6, '', '-26.087799500827586', '28.038552068173885', '16 Coleraine Dr, Riverclub, Sandton, 2191, South Africa', '-26.06071', '28.062269999999998', '4A Wessel Rd', 'Monday, Oct 23', '16:24:52', '04:39:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.087799500827586,28.038552068173885&markers=color:red|label:D|-26.06071,28.062269999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(269, 24, '', '-26.1232502415809', '28.0346638709307', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1013553763663', '28.1278863921762', '129 Hilton Road\nLinbro Park AH, Sandton, 2090', 'Monday, Oct 23', '17:31:19', '05:31:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232502415809,28.0346638709307&markers=color:red|label:D|-26.1013553763663,28.1278863921762&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 32, '', 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-10-23');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(270, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.872284474332123', '30.98807204514742', '188 Z K Mathews Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '18:01:12', '06:01:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.872284474332123,30.98807204514742&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(271, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.872284474332123', '30.98807204514742', '188 Z K Mathews Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '18:02:05', '06:03:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.872284474332123,30.98807204514742&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(272, 33, '', '-33.897121915366675', '18.628475703299046', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.97146300000001', '18.602085100000004', 'Cape Town International Airport', 'Monday, Oct 23', '18:55:30', '06:57:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897121915366675,18.628475703299046&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(273, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.97146300000001', '18.602085100000004', 'Cape Town International Airport', 'Monday, Oct 23', '19:06:03', '07:10:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.97146300000001,18.602085100000004&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-23'),
(274, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.45328394385306', '18.726691156625748', '115 Arcadia St, Malmesbury, 7299, South Africa', 'Monday, Oct 23', '19:16:36', '07:24:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.45328394385306,18.726691156625748&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(275, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870918612640367', '30.978154242038727', '359 King George V Ave, Glenwood, Durban, 4001, South Africa', 'Monday, Oct 23', '19:23:23', '07:25:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.870918612640367,30.978154242038727&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 53, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(276, 35, '', '-29.81899474324569', '31.01191285997629', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '19:25:00', '07:25:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81899474324569,31.01191285997629&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 0, 1, 1, '2017-10-23'),
(277, 35, '', '-29.81899474324569', '31.01191285997629', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.820027382449176', '31.01786367595196', '75 Trematon Dr, Morningside, Berea, 4001, South Africa', 'Monday, Oct 23', '19:26:17', '07:26:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81899474324569,31.01191285997629&markers=color:red|label:D|-29.820027382449176,31.01786367595196&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 0, 1, 1, '2017-10-23'),
(278, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.45328394385306', '18.726691156625748', '115 Arcadia St, Malmesbury, 7299, South Africa', 'Monday, Oct 23', '19:26:59', '07:31:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.45328394385306,18.726691156625748&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(279, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.875983080404136', '30.98582100123167', '269 Z K Mathews Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '19:28:21', '07:29:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.875983080404136,30.98582100123167&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(280, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.875983080404136', '30.98582100123167', '269 Z K Mathews Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '19:30:21', '07:30:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.875983080404136,30.98582100123167&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(281, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.875983080404136', '30.98582100123167', '269 Z K Mathews Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '19:31:08', '07:31:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.875983080404136,30.98582100123167&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(282, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870918612640367', '30.978154242038727', '359 King George V Ave, Glenwood, Durban, 4001, South Africa', 'Monday, Oct 23', '19:31:24', '07:31:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.870918612640367,30.978154242038727&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(283, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870918612640367', '30.978154242038727', '359 King George V Ave, Glenwood, Durban, 4001, South Africa', 'Monday, Oct 23', '19:32:00', '07:32:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.870918612640367,30.978154242038727&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(284, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.45328394385306', '18.726691156625748', '115 Arcadia St, Malmesbury, 7299, South Africa', 'Monday, Oct 23', '19:34:04', '07:34:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.45328394385306,18.726691156625748&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-23'),
(285, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.868772677647545', '30.979320667684078', 'Anniversary Rd, University, Berea, 4041, South Africa', 'Monday, Oct 23', '19:35:22', '07:35:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.868772677647545,30.979320667684078&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(286, 6, '', '-26.06093392830813', '28.062397241592407', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.081277391131874', '28.052085489034653', '4 Majuba Ln, Morningside, Sandton, 2057, South Africa', 'Monday, Oct 23', '19:36:05', '07:37:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06093392830813,28.062397241592407&markers=color:red|label:D|-26.081277391131874,28.052085489034653&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(287, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.45328394385306', '18.726691156625748', '115 Arcadia St, Malmesbury, 7299, South Africa', 'Monday, Oct 23', '19:36:30', '07:37:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.45328394385306,18.726691156625748&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 2, 1, 0, 1, 1, '2017-10-23'),
(288, 35, '', '-29.81891038633243', '31.01160541176796', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.883039182146494', '30.981110371649265', '15 Dirk Uys St, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '19:37:02', '07:38:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81891038633243,31.01160541176796&markers=color:red|label:D|-29.883039182146494,30.981110371649265&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-23'),
(289, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.45328394385306', '18.726691156625748', '115 Arcadia St, Malmesbury, 7299, South Africa', 'Monday, Oct 23', '19:38:11', '07:38:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897177573887944,18.628518618643284&markers=color:red|label:D|-33.45328394385306,18.726691156625748&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-23'),
(290, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.875983080404136', '30.98582100123167', '269 Z K Mathews Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '19:38:32', '07:38:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.875983080404136,30.98582100123167&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(291, 6, '', '-26.0608767026296', '28.0620036274195', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1013553763663', '28.1278863921762', '129 Hilton Road\nLinbro Park AH, Sandton, 2090', 'Monday, Oct 23', '19:39:01', '07:39:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608767026296,28.0620036274195&markers=color:red|label:D|-26.1013553763663,28.1278863921762&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 45, 32, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-10-23'),
(292, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.372907764525735', '18.65019891411066', 'R45, South Africa', 'Monday, Oct 23', '19:40:42', '07:40:42 PM', '', '23/10/2017', '20:40', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(293, 35, '', '-29.81891038633243', '31.01160541176796', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.818779196646556', '31.02113328874111', '11 Trematon Dr, Morningside, Berea, 4001, South Africa', 'Monday, Oct 23', '19:41:14', '07:42:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81891038633243,31.01160541176796&markers=color:red|label:D|-29.818779196646556,31.02113328874111&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-23'),
(294, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.372907764525735', '18.65019891411066', 'R45, South Africa', 'Monday, Oct 23', '19:41:35', '07:41:35 PM', '', '23/10/2017', '08:43', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(295, 33, '', '-33.897177573887944', '18.628518618643284', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.31314064176782', '18.695570826530457', 'N7, South Africa', 'Monday, Oct 23', '19:46:15', '07:46:15 PM', '', '23/10/2017', '09:48', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(296, 6, '', '-26.0608128155179', '28.0623229114862', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0904965891279', '28.0330646038055', '29 12th Avenue\nParkmore, Sandton, 2196', 'Monday, Oct 23', '19:48:25', '07:48:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608128155179,28.0623229114862&markers=color:red|label:D|-26.0904965891279,28.0330646038055&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(297, 15, '', '-29.89087786985409', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870861337912277', '30.978143513202667', '359 King George V Ave, Glenwood, Durban, 4001, South Africa', 'Monday, Oct 23', '19:49:28', '07:49:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087786985409,30.979106426239017&markers=color:red|label:D|-29.870861337912277,30.978143513202667&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(298, 35, '', '-29.81891038633243', '31.01160541176796', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.864450721587772', '31.04167565703392', '10 Prince St, South Beach, Durban, 4001, South Africa', 'Monday, Oct 23', '19:49:54', '07:51:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81891038633243,31.01160541176796&markers=color:red|label:D|-29.864450721587772,31.04167565703392&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-23'),
(299, 15, '', '-29.89087786985409', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.87610169401848', '30.977230556309227', '90 Levenhall Rd, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '19:54:58', '07:54:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087786985409,30.979106426239017&markers=color:red|label:D|-29.87610169401848,30.977230556309227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(300, 33, '', '-33.897084345844284', '18.627562075853348', '2 Bloem St, Bosbell, Cape Town, 7530, South Africa', '-26.132626799999997', '28.1766294', 'Edenvale', 'Monday, Oct 23', '19:56:18', '07:58:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897084345844284,18.627562075853348&markers=color:red|label:D|-33.91694940000001,18.3875487&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(301, 6, '', '-26.06093392830813', '28.062397241592407', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.081277391131874', '28.052085489034653', '4 Majuba Ln, Morningside, Sandton, 2057, South Africa', 'Monday, Oct 23', '19:57:14', '07:58:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06093392830813,28.062397241592407&markers=color:red|label:D|-26.081277391131874,28.052085489034653&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(302, 35, '', '-29.818925803290853', '31.011615805327896', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '19:58:04', '07:58:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.818925803290853,31.011615805327896&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(303, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '20:00:34', '08:00:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(304, 6, '', '-26.06093392830813', '28.062397241592407', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '20:00:44', '08:00:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06093392830813,28.062397241592407&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(305, 35, '', '-29.818925803290853', '31.011615805327896', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '20:00:47', '08:00:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.818925803290853,31.011615805327896&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(306, 6, '', '-26.0608080317784', '28.0623080581427', '4A Wessel Road\nEdenburg, Sandton, 2128', '0.0', '0.0', 'No drop off point', 'Monday, Oct 23', '20:02:29', '08:10:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608080317784,28.0623080581427&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-23'),
(307, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.871491067379875', '30.97771670669317', '290 King George V Ave, University, Berea, 4041, South Africa', 'Monday, Oct 23', '20:03:32', '08:03:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.871491067379875,30.97771670669317&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(308, 33, '', '-33.897084345844284', '18.627562075853348', '2 Bloem St, Bosbell, Cape Town, 7530, South Africa', '-33.886549755435', '18.65039337426424', '12 Peperdruif St, Blommendal, Cape Town, 7530, South Africa', 'Monday, Oct 23', '20:05:01', '08:06:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897084345844284,18.627562075853348&markers=color:red|label:D|-33.886549755435,18.65039337426424&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-23'),
(309, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.876426427137975', '30.977261401712894', '117 Levenhall Rd, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '20:05:47', '08:05:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.876426427137975,30.977261401712894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(310, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.876426427137975', '30.977261401712894', '117 Levenhall Rd, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '20:10:09', '08:10:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.876426427137975,30.977261401712894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(311, 6, '', '-26.0608496140384', '28.062320443198', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0909277802429', '28.035333417356', '43 13th Avenue\nParkmore, Sandton, 2196', 'Monday, Oct 23', '20:10:47', '08:11:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0608496140384,28.062320443198&markers=color:red|label:D|-26.0909277802429,28.035333417356&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-10-23'),
(312, 15, '', '-29.890880776617948', '30.97910676151514', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.879344326273333', '30.98166223615408', '354 Z K Mathews Rd, Umbilo, Durban, 4001, South Africa', 'Monday, Oct 23', '20:13:56', '08:13:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890880776617948,30.97910676151514&markers=color:red|label:D|-29.879344326273333,30.98166223615408&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(313, 34, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '20:14:36', '08:14:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(314, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.88035482917544', '30.98461501300335', '88 Bartle Rd, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '20:20:16', '08:20:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.88035482917544,30.98461501300335&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(315, 15, '', '-29.89088339270534', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.871589335054562', '30.977756939828396', '290 King George V Ave, University, Berea, 4041, South Africa', 'Monday, Oct 23', '20:24:06', '08:24:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088339270534,30.979107096791267&markers=color:red|label:D|-29.871589335054562,30.977756939828396&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(316, 15, '', '-29.89087786985409', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.879283277150904', '30.981649830937386', '354 Z K Mathews Rd, Umbilo, Durban, 4001, South Africa', 'Monday, Oct 23', '20:33:08', '08:33:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087786985409,30.979106426239017&markers=color:red|label:D|-29.879283277150904,30.981649830937386&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(317, 15, '', '-29.89087786985409', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.879283277150904', '30.981649830937386', '354 Z K Mathews Rd, Umbilo, Durban, 4001, South Africa', 'Monday, Oct 23', '20:33:46', '08:33:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087786985409,30.979106426239017&markers=color:red|label:D|-29.879283277150904,30.981649830937386&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(318, 15, '', '-29.890880776617948', '30.97910676151514', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.87212079287218', '30.977857857942585', '290 King George V Ave, University, Berea, 4041, South Africa', 'Monday, Oct 23', '20:44:08', '08:44:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890880776617948,30.97910676151514&markers=color:red|label:D|-29.87212079287218,30.977857857942585&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(319, 6, '', '-26.06093392830813', '28.062397241592407', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.070829634847126', '28.052868023514744', '5 Steenbok St, Morningside, Sandton, 2057, South Africa', 'Monday, Oct 23', '20:49:10', '08:50:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06093392830813,28.062397241592407&markers=color:red|label:D|-26.070829634847126,28.052868023514744&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(320, 15, '', '-29.890880776617948', '30.97910676151514', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.87212079287218', '30.977857857942585', '290 King George V Ave, University, Berea, 4041, South Africa', 'Monday, Oct 23', '20:53:40', '08:54:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890880776617948,30.97910676151514&markers=color:red|label:D|-29.87212079287218,30.977857857942585&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(321, 15, '', '-29.890880776617948', '30.97910676151514', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.876447068151002', '30.977183952927593', '117 Levenhall Rd, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '21:07:42', '09:07:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890880776617948,30.97910676151514&markers=color:red|label:D|-29.876447068151002,30.977183952927593&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-23'),
(322, 15, '', '-29.89087786985409', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.87522313591028', '30.985113233327866', '106 Francois Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '21:14:47', '09:14:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087786985409,30.979106426239017&markers=color:red|label:D|-29.87522313591028,30.985113233327866&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(323, 34, '', '-29.890883102028965', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.872963618692882', '30.98290074616671', '555 Lena Ahrens Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '21:15:48', '09:15:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890883102028965,30.979107096791267&markers=color:red|label:D|-29.872963618692882,30.98290074616671&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(324, 34, '', '-29.89088281135259', '30.979107096791267', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.87849719368336', '30.979307256639004', '68 Deane Rd, Umbilo, Berea, 4075, South Africa', 'Monday, Oct 23', '21:17:17', '09:17:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89088281135259,30.979107096791267&markers=color:red|label:D|-29.87849719368336,30.979307256639004&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(325, 15, '', '-29.89087786985409', '30.979106426239017', '476 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.87522313591028', '30.985113233327866', '106 Francois Rd, Glenwood, Berea, 4001, South Africa', 'Monday, Oct 23', '21:17:41', '09:18:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087786985409,30.979106426239017&markers=color:red|label:D|-29.87522313591028,30.985113233327866&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 53, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(326, 33, '', '-33.89706236070869', '18.628457933664322', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '21:40:10', '09:40:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89706236070869,18.628457933664322&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(327, 33, '', '-33.89706236070869', '18.628457933664322', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '21:41:02', '09:41:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89706236070869,18.628457933664322&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(328, 33, '', '-33.89706236070869', '18.628457933664322', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Monday, Oct 23', '21:42:40', '09:42:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89706236070869,18.628457933664322&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(329, 33, '', '-33.89706236070869', '18.628457933664322', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-26.182783399999995', '28.061054199999997', 'Yeoville', 'Monday, Oct 23', '21:43:14', '07:35:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89706236070869,18.628457933664322&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(330, 9, '', '-26.060948385317246', '28.062397241592407', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.087701035000926', '28.06966669857502', '3 East Rd, Morningside, Sandton, 2057, South Africa', 'Monday, Oct 23', '21:48:03', '09:53:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.060948385317246,28.062397241592407&markers=color:red|label:D|-26.087701035000926,28.06966669857502&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(331, 6, '', '-26.0607381561342', '28.0624002590775', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0696662392425', '28.0489074066281', '51 Oak Avenue\nRiverclub, Sandton, 2191', 'Tuesday, Oct 24', '06:41:32', '06:41:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607381561342,28.0624002590775&markers=color:red|label:D|-26.0696662392425,28.0489074066281&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 8, 1, 1, '2017-10-24'),
(332, 6, '', '-26.0607381561342', '28.0624002590775', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0696662392425', '28.0489074066281', '51 Oak Avenue\nRiverclub, Sandton, 2191', 'Tuesday, Oct 24', '06:48:19', '06:49:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607381561342,28.0624002590775&markers=color:red|label:D|-26.0696662392425,28.0489074066281&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 3, 8, 1, 1, '2017-10-24'),
(333, 6, '', '-26.0810244371306', '28.0262618511915', '7 Arklow Road\nBryanston, Sandton, 2191', '-26.0944979936664', '28.0066475272179', 'Jan Smuts Avenue\nBlairgowrie, Randburg, 2194', 'Tuesday, Oct 24', '06:52:31', '06:53:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0810244371306,28.0262618511915&markers=color:red|label:D|-26.0944979936664,28.0066475272179&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 3, 8, 1, 1, '2017-10-24'),
(334, 6, '', '-26.05913220958336', '28.074366599321362', '19-21 Hampton Ct Rd, Gallo Manor, Sandton, 2052, South Africa', '-26.077992560204436', '28.061710260808468', '427 Summit Rd, Morningside, Sandton, 2057, South Africa', 'Tuesday, Oct 24', '06:56:25', '06:57:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.05913220958336,28.074366599321362&markers=color:red|label:D|-26.077992560204436,28.061710260808468&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(335, 6, '', '-26.05913220958336', '28.074366599321362', '19-21 Hampton Ct Rd, Gallo Manor, Sandton, 2052, South Africa', '-26.077992560204436', '28.061710260808468', '427 Summit Rd, Morningside, Sandton, 2057, South Africa', 'Tuesday, Oct 24', '06:59:14', '06:59:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.05913220958336,28.074366599321362&markers=color:red|label:D|-26.077992560204436,28.061710260808468&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 3, 0, 1, 1, '2017-10-24'),
(336, 6, '', '-26.05913220958336', '28.074366599321362', '19-21 Hampton Ct Rd, Gallo Manor, Sandton, 2052, South Africa', '-26.077992560204436', '28.061710260808468', '427 Summit Rd, Morningside, Sandton, 2057, South Africa', 'Tuesday, Oct 24', '07:00:08', '07:01:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.05913220958336,28.074366599321362&markers=color:red|label:D|-26.077992560204436,28.061710260808468&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-10-24'),
(337, 15, '', '-29.62113513318496', '30.39504200220108', '236 King Edward Ave, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.602564228258604', '30.387501977384087', '261 Boshoff St, Pietermaritzburg, 3201, South Africa', 'Tuesday, Oct 24', '09:06:47', '09:08:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62113513318496,30.39504200220108&markers=color:red|label:D|-29.602564228258604,30.387501977384087&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(338, 6, '', '-26.1232935895081', '28.0348147451878', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1469687864646', '28.0030446499586', '48 John Mckenzie Drive\nEmmarentia, Randburg, 2195', 'Tuesday, Oct 24', '09:32:17', '09:33:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232935895081,28.0348147451878&markers=color:red|label:D|-26.1469687864646,28.0030446499586&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 9, 0, 8, 3, 8, 1, 1, '2017-10-24'),
(339, 15, '', '-29.625428535792757', '30.40361098945141', '17 Shores Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.602564228258604', '30.387501977384087', '261 Boshoff St, Pietermaritzburg, 3201, South Africa', 'Tuesday, Oct 24', '09:39:36', '09:41:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.625428535792757,30.40361098945141&markers=color:red|label:D|-29.602564228258604,30.387501977384087&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(340, 12, '', '28.41227835560483', '77.0434371381998', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '11:38:11', '11:38:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41227835560483,77.0434371381998&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-10-24'),
(341, 12, '', '28.41227835560483', '77.0434371381998', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '11:40:33', '11:41:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41227835560483,77.0434371381998&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 28, '', 1, 1, 2, 1, 0, 3, 0, 1, 1, '2017-10-24'),
(342, 12, '', '28.41228572787924', '77.04344116151331', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '11:59:04', '12:02:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41228572787924,77.04344116151331&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 28, '', 1, 1, 9, 0, 8, 3, 0, 1, 1, '2017-10-24'),
(343, 12, '', '28.41227009865687', '77.0434421673417', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '12:04:46', '12:05:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41227009865687,77.0434421673417&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-24'),
(344, 12, '', '28.412314627189968', '77.04341299831867', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '12:49:09', '12:57:24 PM', '', '24/10/2017', '17:21', 21, 28, '', 2, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-24'),
(345, 12, '', '28.412307844699374', '77.04341366887093', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.45872293100687', '77.07370452582836', '713, Sector 30 M Wide Main Rd, Sushant Lok Phase I, Sector 27, Gurugram, Haryana 122022, India', 'Tuesday, Oct 24', '13:01:53', '01:02:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412307844699374,77.04341366887093&markers=color:red|label:D|28.45872293100687,77.07370452582836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-10-24'),
(346, 20, '', '28.4121165206874', '77.0432620143417', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4361529867968', '77.0771934092045', '18, Ardee City Road, Block E, Ardee City, Sector 52\nGurugram, Haryana 122022', 'Tuesday, Oct 24', '13:14:49', '01:14:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121165206874,77.0432620143417&markers=color:red|label:D|28.4361529867968,77.0771934092045&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-10-24'),
(347, 26, '', '28.412326127933895', '77.04341333359478', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.412336154222437', '77.04339757561684', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'Tuesday, Oct 24', '13:17:40', '01:18:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412326127933895,77.04341333359478&markers=color:red|label:D|28.412336154222437,77.04339757561684&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-24'),
(348, 27, '', '28.4121114471749', '77.043286934495', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4266087929382', '77.0846864953637', 'Sector Road, Sushant Lok Phase 3, Block B1, Sector 57\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:22:07', '01:22:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121114471749,77.043286934495&markers=color:red|label:D|28.4266087929382,77.0846864953637&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-10-24'),
(349, 26, '', '28.412283073860504', '77.0434371381998', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:34:54', '01:34:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412283073860504,77.0434371381998&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 0, 1, 1, '2017-10-24'),
(350, 27, '', '28.4121114471749', '77.043286934495', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4266087929382', '77.0846864953637', 'Sector Road, Sushant Lok Phase 3, Block B1, Sector 57\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:34:55', '01:36:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121114471749,77.043286934495&markers=color:red|label:D|28.4266087929382,77.0846864953637&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 49, 30, '', 1, 1, 7, 1, 0, 3, 9, 1, 1, '2017-10-24'),
(351, 26, '', '28.412283073860504', '77.0434371381998', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:35:24', '01:35:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412283073860504,77.0434371381998&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 3, 0, 1, 1, '2017-10-24'),
(352, 27, '', '28.4120864710234', '77.0432206687755', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:37:37', '01:37:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120864710234,77.0432206687755&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24'),
(353, 27, '', '28.4120864710234', '77.0432206687755', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:38:18', '01:38:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120864710234,77.0432206687755&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24'),
(354, 27, '', '28.4121320895744', '77.0432433485985', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:40:13', '01:40:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121320895744,77.0432433485985&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24'),
(355, 27, '', '28.4121320895744', '77.0432433485985', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:41:47', '01:41:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121320895744,77.0432433485985&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24'),
(356, 27, '', '28.4121320895744', '77.0432433485985', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:44:43', '01:44:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121320895744,77.0432433485985&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24'),
(357, 27, '', '28.4121320895744', '77.0432433485985', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:51:52', '01:51:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121320895744,77.0432433485985&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24'),
(358, 27, '', '28.4121320895744', '77.0432433485985', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4307333842903', '77.0979379490018', 'Jalvayu Towers Road, Sector 56\nGurugram, Haryana 122011', 'Tuesday, Oct 24', '13:52:05', '01:52:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121320895744,77.0432433485985&markers=color:red|label:D|28.4307333842903,77.0979379490018&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 3, 9, 1, 1, '2017-10-24');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(359, 12, '', '28.41230342133572', '77.04343378543854', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:56:16', '01:56:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41230342133572,77.04343378543854&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(360, 12, '', '28.41230342133572', '77.04343378543854', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:57:19', '01:57:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41230342133572,77.04343378543854&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(361, 12, '', '28.41230342133572', '77.04343378543854', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:58:07', '01:58:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41230342133572,77.04343378543854&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(362, 12, '', '28.412311678281064', '77.04342171549797', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '14:06:31', '02:07:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412311678281064,77.04342171549797&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 60, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(363, 35, '', '-29.736349647237457', '31.06161955744028', '305 Umhlanga Rocks Dr, La Lucia, Durban, 4022, South Africa', '-29.849106300000003', '30.9900032', '25 Peter Mokaba Ridge', 'Tuesday, Oct 24', '14:12:58', '02:14:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.736349647237457,31.06161955744028&markers=color:red|label:D|-29.849106300000003,30.9900032&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-24'),
(364, 33, '', '-33.92109763378299', '18.585752807557583', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Tuesday, Oct 24', '14:38:33', '02:40:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92109763378299,18.585752807557583&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(365, 33, '', '-33.92109763378299', '18.585752807557583', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Tuesday, Oct 24', '14:50:26', '02:52:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92109763378299,18.585752807557583&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(366, 27, '', '28.4121480137085', '77.0432591065764', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4316756874153', '77.0126675069332', '28, Block B, Sector 34\nGurugram, Haryana 122001', 'Tuesday, Oct 24', '14:53:52', '02:53:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121480137085,77.0432591065764&markers=color:red|label:D|28.4316756874153,77.0126675069332&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(367, 27, '', '28.4121480137085', '77.0432591065764', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4316756874153', '77.0126675069332', '28, Block B, Sector 34\nGurugram, Haryana 122001', 'Tuesday, Oct 24', '14:56:06', '02:56:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121480137085,77.0432591065764&markers=color:red|label:D|28.4316756874153,77.0126675069332&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(368, 27, '', '28.4121480137085', '77.0432591065764', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4316756874153', '77.0126675069332', '28, Block B, Sector 34\nGurugram, Haryana 122001', 'Tuesday, Oct 24', '14:57:15', '02:57:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121480137085,77.0432591065764&markers=color:red|label:D|28.4316756874153,77.0126675069332&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(369, 7, '', '-26.063477732075004', '28.06087106466293', '315 Rivonia Rd, Morningside, Sandton, 2057, South Africa', '-26.06071', '28.062269999999998', '4A Wessel Rd', 'Tuesday, Oct 24', '16:37:34', '04:37:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.063477732075004,28.06087106466293&markers=color:red|label:D|-26.06071,28.062269999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(370, 7, '', '-26.063477732075004', '28.06087106466293', '315 Rivonia Rd, Morningside, Sandton, 2057, South Africa', '-26.06071', '28.062269999999998', '4A Wessel Rd', 'Tuesday, Oct 24', '16:38:23', '04:46:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.063477732075004,28.06087106466293&markers=color:red|label:D|-26.06071,28.062269999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(371, 7, '', '-26.063477732075004', '28.06087106466293', '315 Rivonia Rd, Morningside, Sandton, 2057, South Africa', '-26.06071', '28.062269999999998', '4A Wessel Rd', 'Tuesday, Oct 24', '17:19:10', '05:19:10 PM', '', '24/10/2017', '18:35', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(372, 15, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.872092010491784', '30.985875986516476', '500 Lena Ahrens Rd, Glenwood, Berea, 4001, South Africa', 'Tuesday, Oct 24', '21:03:56', '09:04:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.872092010491784,30.985875986516476&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 53, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(373, 36, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.86481125518369', '30.98961263895035', '344 Sir Duncan Rd, Glenwood, Berea, 4001, South Africa', 'Tuesday, Oct 24', '21:06:20', '09:06:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.86481125518369,30.98961263895035&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(374, 36, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.86481125518369', '30.98961263895035', '344 Sir Duncan Rd, Glenwood, Berea, 4001, South Africa', 'Tuesday, Oct 24', '21:08:08', '09:08:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.86481125518369,30.98961263895035&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(375, 27, '', '28.41219667077', '77.0432510599494', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.436157409164', '77.0999844744802', 'IILM Institute, Sector 53\nGurugram, Haryana', 'Wednesday, Oct 25', '06:11:42', '06:11:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41219667077,77.0432510599494&markers=color:red|label:D|28.436157409164,77.0999844744802&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(376, 13, '', '28.41232583304304', '77.0434146746993', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '06:15:52', '06:16:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232583304304,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 66, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(377, 13, '', '28.41232583304304', '77.0434146746993', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '06:17:54', '06:18:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232583304304,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 66, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-25'),
(378, 13, '', '28.41232583304304', '77.0434146746993', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '06:19:29', '06:20:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232583304304,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 66, 28, '', 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-25'),
(379, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:05:18', '08:05:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(380, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:06:44', '08:06:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(381, 15, '', '-29.89087932323602', '30.979453772306442', '471 Bartle Rd, Umbilo, Berea, 4075, South Africa', '', '', 'Set your drop point', 'Wednesday, Oct 25', '08:07:01', '08:07:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89087932323602,30.979453772306442&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(382, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:07:10', '08:07:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(383, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:07:56', '08:07:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(384, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:11:59', '08:11:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(385, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:16:13', '08:16:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(386, 37, '', '28.410442348408626', '77.04313572496176', '71, Shanti St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '08:21:10', '08:21:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410442348408626,77.04313572496176&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(387, 40, '', '28.412285432988263', '77.0434596017003', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '10:08:20', '10:08:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412285432988263,77.0434596017003&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 71, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(388, 43, '', '28.4121390246119', '77.0432468690696', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4381038224416', '77.1010365709662', 'Parsvnath Exotica, Sector 53\nGurugram, Haryana', 'Wednesday, Oct 25', '11:57:38', '11:58:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121390246119,77.0432468690696&markers=color:red|label:D|28.4381038224416,77.1010365709662&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 67, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(389, 43, '', '28.4120704572552', '77.0432510599494', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4478836975381', '77.091085575521', '1251, DLF Phase 5, Sector 53\nGurugram, Haryana 122022', 'Wednesday, Oct 25', '12:32:37', '01:28:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120704572552,77.0432510599494&markers=color:red|label:D|28.4478836975381,77.091085575521&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 67, 30, '', 1, 1, 9, 0, 15, 1, 0, 1, 1, '2017-10-25'),
(390, 43, '', '28.41206573899', '77.0432584360242', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4490637425048', '77.1119099110365', 'Sector 54\nGurugram, Haryana', 'Wednesday, Oct 25', '13:29:07', '01:29:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41206573899,77.0432584360242&markers=color:red|label:D|28.4490637425048,77.1119099110365&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 67, 30, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(391, 44, '', '28.412285432988263', '77.0434596017003', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '14:49:36', '02:52:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412285432988263,77.0434596017003&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 72, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(392, 44, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '14:53:21', '02:59:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 72, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(393, 6, '', '-26.1232837969846', '28.0349001632408', '57 6th Road\nHyde Park, Sandton, 2196', '-26.161440149326', '27.9931154474616', 'Waterval 211-Iq, Randburg\nSouth Africa', 'Wednesday, Oct 25', '15:09:11', '03:09:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232837969846,28.0349001632408&markers=color:red|label:D|-26.161440149326,27.9931154474616&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(394, 6, '', '-26.1232837969846', '28.0349001632408', '57 6th Road\nHyde Park, Sandton, 2196', '-26.161440149326', '27.9931154474616', 'Waterval 211-Iq, Randburg\nSouth Africa', 'Wednesday, Oct 25', '15:10:14', '08:39:28 AM', '', '2017-10-25', '16:30:33', 73, 28, '', 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(395, 15, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.86929426533469', '30.9801984205842', '303 King George V Ave, University, Berea, 4041, South Africa', 'Wednesday, Oct 25', '15:27:49', '03:28:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.86929426533469,30.9801984205842&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(396, 7, '', '-26.123316166547095', '28.034538477659222', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.13741362173651', '28.020378425717354', '102 15th St, Parkhurst, Randburg, 2193, South Africa', 'Wednesday, Oct 25', '15:52:43', '03:55:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123316166547095,28.034538477659222&markers=color:red|label:D|-26.13741362173651,28.020378425717354&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(397, 35, '', '-29.86687848315297', '30.984192229807377', '159 Lamont Rd, University, Berea, 4041, South Africa', '-29.818767', '31.011423999999998', '253 Peter Mokaba Rd', 'Wednesday, Oct 25', '16:18:29', '04:18:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86687848315297,30.984192229807377&markers=color:red|label:D|-29.818767,31.011423999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(398, 6, '', '-26.06093181999416', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.07786758544384', '28.051768653094772', '17 Acacia Rd, Duxberry, Sandton, 2191, South Africa', 'Wednesday, Oct 25', '17:10:56', '05:12:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06093181999416,28.062429428100586&markers=color:red|label:D|-26.07786758544384,28.051768653094772&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(399, 7, '', '-26.06094627700352', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.07911431563666', '28.040701188147068', '17 Jukskei Ave, Riverclub, Sandton, 2191, South Africa', 'Wednesday, Oct 25', '17:26:41', '05:27:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06094627700352,28.062429428100586&markers=color:red|label:D|-26.07911431563666,28.040701188147068&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(400, 6, '', '-26.05768286307919', '28.07175178080797', '3 Hampton Ct Rd, Gallo Manor, Sandton, 2052, South Africa', '-26.071542784916772', '28.057126365602016', '15 Redhill Rd, Morningside, Sandton, 2057, South Africa', 'Wednesday, Oct 25', '17:29:00', '05:31:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.05768286307919,28.07175178080797&markers=color:red|label:D|-26.071542784916772,28.057126365602016&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(401, 6, '', '-26.1232839566368', '28.0348234623671', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1683910980142', '28.0095171555877', '28 Congo Road\nEmmarentia, Randburg, 2195', 'Wednesday, Oct 25', '19:17:41', '07:19:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232839566368,28.0348234623671&markers=color:red|label:D|-26.1683910980142,28.0095171555877&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(402, 6, '', '-26.060785088641', '28.0625130926997', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0959246032598', '28.0596013739705', '7 Marion Street\nSandown, Sandton, 2031', 'Wednesday, Oct 25', '19:20:36', '07:21:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.060785088641,28.0625130926997&markers=color:red|label:D|-26.0959246032598,28.0596013739705&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(403, 6, '', '-26.06093121761873', '28.062429763376716', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '', '', 'Set your drop point', 'Wednesday, Oct 25', '20:35:46', '08:36:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06093121761873,28.062429763376716&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-10-25'),
(404, 15, '', '-29.852295034392906', '30.99667724221945', '459 King Dinuzulu Rd N, Bulwer, Berea, 4083, South Africa', '', '', 'Set your drop point', 'Thursday, Oct 26', '07:03:50', '07:10:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.852295034392906,30.99667724221945&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-10-26'),
(405, 24, '', '-26.1240632', '28.034747100000004', '55 6th Road, Sandton, South Africa', '-29.59658', '30.37406999999996', '23 Chief Albert Luthuli Street, Pietermaritzburg, South Africa', 'Thursday, Oct 26', '08:35:14', '08:38:04 AM', '', '', '', 0, 28, '', 1, 1, 17, 0, 12, 1, 0, 2, 1, '2017-10-26'),
(406, 46, '', '28.412332615532467', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 26', '11:32:51', '11:33:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412332615532467,77.0434146746993&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 75, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(407, 33, '', '-33.897141674145885', '18.62888004630804', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.9067916', '18.5808115', 'Parow', 'Thursday, Oct 26', '20:18:57', '08:20:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897141674145885,18.62888004630804&markers=color:red|label:D|-33.9067916,18.5808115&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(408, 33, '', '-33.89418810328757', '18.628881722688675', '27 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.97146300000001', '18.602085100000004', 'Cape Town International Airport', 'Thursday, Oct 26', '20:24:34', '08:24:34 PM', '', '26/10/2017', '21:25', 0, 30, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-26'),
(409, 15, '', '-29.86626238973342', '31.010035648941994', '1 Maydon Rd, Maydon Wharf, Durban, 4001, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Friday, Oct 27', '14:19:24', '02:41:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86626238973342,31.010035648941994&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(410, 6, '', '-26.124560305166', '28.0344677343965', '44 Hyde Close\nHyde Park, Sandton, 2196', '-26.124476018429', '28.0345901101828', '50 6th Road\nHyde Park, Sandton, 2196', 'Friday, Oct 27', '15:13:45', '03:21:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.124560305166,28.0344677343965&markers=color:red|label:D|-26.124476018429,28.0345901101828&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(411, 6, '', '-26.124560305166', '28.0344677343965', '44 Hyde Close\nHyde Park, Sandton, 2196', '-26.124476018429', '28.0345901101828', '50 6th Road\nHyde Park, Sandton, 2196', 'Friday, Oct 27', '15:22:06', '03:22:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.124560305166,28.0344677343965&markers=color:red|label:D|-26.124476018429,28.0345901101828&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(412, 6, '', '-26.124560305166', '28.0344677343965', '44 Hyde Close\nHyde Park, Sandton, 2196', '-26.124476018429', '28.0345901101828', '50 6th Road\nHyde Park, Sandton, 2196', 'Friday, Oct 27', '15:22:51', '03:22:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.124560305166,28.0344677343965&markers=color:red|label:D|-26.124476018429,28.0345901101828&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(413, 6, '', '-26.1232760874798', '28.0348708201461', '57 6th Road\nHyde Park, Sandton, 2196', '-26.1235651157414', '28.0352569743991', '58-64 6th Road\nHyde Park, Sandton, 2196', 'Friday, Oct 27', '15:24:25', '03:27:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232760874798,28.0348708201461&markers=color:red|label:D|-26.1235651157414,28.0352569743991&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(414, 6, '', '-26.1232779360918', '28.0348563194275', '57 6th Road\nHyde Park, Sandton, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Friday, Oct 27', '16:39:49', '04:39:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232779360918,28.0348563194275&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(415, 6, '', '-26.1232779360918', '28.0348563194275', '57 6th Road\nHyde Park, Sandton, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Friday, Oct 27', '16:40:14', '04:40:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232779360918,28.0348563194275&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(416, 6, '', '-26.1232779360918', '28.0348563194275', '57 6th Road\nHyde Park, Sandton, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Friday, Oct 27', '16:41:28', '04:41:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232779360918,28.0348563194275&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(417, 6, '', '-26.1235329058997', '28.0344912037253', '55 6th Avenue\nHyde Park, Sandton, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Friday, Oct 27', '16:42:59', '04:42:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1235329058997,28.0344912037253&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(418, 6, '', '-26.1235329058997', '28.0344912037253', '55 6th Avenue\nHyde Park, Sandton, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Friday, Oct 27', '16:43:30', '05:12:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1235329058997,28.0344912037253&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(419, 15, '', '-29.855946984614427', '31.003281511366364', '37 Noble Rd, Bulwer, Berea, 4083, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Friday, Oct 27', '17:16:28', '05:46:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.855946984614427,31.003281511366364&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(420, 15, '', '-29.890714800266004', '30.979106426239017', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870740392185493', '30.978056006133556', '359 King George V Ave, Glenwood, Durban, 4001, South Africa', 'Saturday, Oct 28', '07:52:55', '08:05:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890714800266004,30.979106426239017&markers=color:red|label:D|-29.870740392185493,30.978056006133556&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(421, 15, '', '-29.870938964058684', '30.97685471177101', 'University, Berea, 4041, South Africa', '-29.851297914486633', '30.996978990733624', '36 Lanercost Ave, Musgrave, Berea, 4001, South Africa', 'Saturday, Oct 28', '08:45:56', '08:47:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.870938964058684,30.97685471177101&markers=color:red|label:D|-29.851297914486633,30.996978990733624&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(422, 6, '', '-29.8743314874558', '30.9811797738075', '15 Baines Road\nUmbilo, Berea, 4075', '-29.8594179436428', '30.9994918853045', '17-19 Cohen Avenue\nBulwer, Berea, 4083', 'Saturday, Oct 28', '12:59:29', '01:00:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.8743314874558,30.9811797738075&markers=color:red|label:D|-29.8594179436428,30.9994918853045&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(423, 47, '', '-29.84746055290757', '30.98452180624008', '15 Felix Dlamini Rd, Westridge, Berea, 4091, South Africa', '-30.035042699999995', '30.890083899999997', 'Amanzimtoti', 'Saturday, Oct 28', '14:10:34', '02:10:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.84746055290757,30.98452180624008&markers=color:red|label:D|-30.035042699999995,30.890083899999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(424, 47, '', '-29.84746055290757', '30.98452180624008', '15 Felix Dlamini Rd, Westridge, Berea, 4091, South Africa', '-30.035042699999995', '30.890083899999997', 'Amanzimtoti', 'Saturday, Oct 28', '14:11:34', '02:11:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.84746055290757,30.98452180624008&markers=color:red|label:D|-30.035042699999995,30.890083899999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(425, 35, '', '-29.8588712981389', '30.95096603035927', 'N2, Chesterville, Durban, 4138, South Africa', '-30.035042699999995', '30.890083899999997', 'Amanzimtoti', 'Saturday, Oct 28', '14:13:31', '02:13:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.8588712981389,30.95096603035927&markers=color:red|label:D|-30.035042699999995,30.890083899999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(426, 48, '', '28.4121129216321', '77.0432654768229', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4329446580904', '77.0819550007582', 'Wazirabad, Sector 52\nGurugram, Haryana 122003', 'Saturday, Oct 28', '14:32:30', '02:32:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121129216321,77.0432654768229&markers=color:red|label:D|28.4329446580904,77.0819550007582&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(427, 48, '', '28.4121129216321', '77.0432654768229', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4329446580904', '77.0819550007582', 'Wazirabad, Sector 52\nGurugram, Haryana 122003', 'Saturday, Oct 28', '14:33:52', '02:33:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121129216321,77.0432654768229&markers=color:red|label:D|28.4329446580904,77.0819550007582&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(428, 48, '', '28.4121129216321', '77.0432654768229', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4329446580904', '77.0819550007582', 'Wazirabad, Sector 52\nGurugram, Haryana 122003', 'Saturday, Oct 28', '14:35:22', '02:35:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121129216321,77.0432654768229&markers=color:red|label:D|28.4329446580904,77.0819550007582&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 76, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(429, 15, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.871533514375493', '30.980191715061665', 'Princess Alice Ave, University, Berea, 4041, South Africa', 'Saturday, Oct 28', '18:54:07', '06:55:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.871533514375493,30.980191715061665&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(430, 33, '', '-33.89112171373907', '18.631466701626778', '167 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Saturday, Oct 28', '18:55:03', '07:00:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89112171373907,18.631466701626778&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(431, 7, '', '-26.06094627700352', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.103708020874347', '28.050084225833416', '3 Sandown Valley Cres, Sandown, Sandton, 2031, South Africa', 'Saturday, Oct 28', '19:05:59', '07:06:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06094627700352,28.062429428100586&markers=color:red|label:D|-26.103708020874347,28.050084225833416&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(432, 44, '', '28.434482792896453', '77.0354837179184', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 28', '19:25:48', '07:26:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434482792896453,77.0354837179184&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 72, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(433, 35, '', '-29.819092771707208', '31.011539697647095', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.818486273600243', '31.021663025021553', '1 Trematon Dr, Morningside, Berea, 4001, South Africa', 'Saturday, Oct 28', '19:33:39', '07:44:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.819092771707208,31.011539697647095&markers=color:red|label:D|-29.818486273600243,31.021663025021553&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 9, 0, 15, 1, 0, 1, 1, '2017-10-28'),
(434, 35, '', '-29.818812939464696', '31.01181361824274', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.78823495014597', '31.02285861968994', '31 Grove Cres, Park Hill, Durban North, 4051, South Africa', 'Saturday, Oct 28', '19:47:13', '07:48:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.818812939464696,31.01181361824274&markers=color:red|label:D|-29.78823495014597,31.02285861968994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(435, 35, '', '-29.818812939464696', '31.01181361824274', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.78823495014597', '31.02285861968994', '31 Grove Cres, Park Hill, Durban North, 4051, South Africa', 'Saturday, Oct 28', '19:48:48', '07:48:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.818812939464696,31.01181361824274&markers=color:red|label:D|-29.78823495014597,31.02285861968994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(436, 7, '', '-26.06094627700352', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.092776885573432', '28.007782436907288', '10 June Ave, Bordeaux, Randburg, 2194, South Africa', 'Saturday, Oct 28', '19:50:41', '07:53:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06094627700352,28.062429428100586&markers=color:red|label:D|-26.092776885573432,28.007782436907288&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(437, 35, '', '-29.819011905505544', '31.011888720095158', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.854249116261943', '30.9815264493227', '41 Essex Grove, Westridge, Berea, 4091, South Africa', 'Saturday, Oct 28', '19:51:18', '07:51:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.819011905505544,31.011888720095158&markers=color:red|label:D|-29.854249116261943,30.9815264493227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(438, 15, '', '-29.890714800266004', '30.979106426239017', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '', '', 'Set your drop point', 'Saturday, Oct 28', '19:51:24', '07:51:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890714800266004,30.979106426239017&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(439, 35, '', '-29.818903695953495', '31.011783108115196', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.823206389802706', '31.036471165716648', 'Ruth First Hwy, Stamford Hill, Durban, 4025, South Africa', 'Saturday, Oct 28', '19:55:25', '07:57:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.818903695953495,31.011783108115196&markers=color:red|label:D|-29.823206389802706,31.036471165716648&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(440, 33, '', '-33.88328936837612', '18.590637780725956', '7 John Vorster Ave, Plattekloof 1, Cape Town, 7500, South Africa', '-33.9067916', '18.5808115', 'Parow', 'Saturday, Oct 28', '20:28:22', '08:30:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.88328936837612,18.590637780725956&markers=color:red|label:D|-33.9067916,18.5808115&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(441, 33, '', '-33.88328936837612', '18.590637780725956', '7 John Vorster Ave, Plattekloof 1, Cape Town, 7500, South Africa', '-33.9067916', '18.5808115', 'Parow', 'Saturday, Oct 28', '20:30:36', '08:30:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.88328936837612,18.590637780725956&markers=color:red|label:D|-33.9067916,18.5808115&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(442, 15, '', '-29.890714800266004', '30.979106426239017', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.870119670823637', '30.981170386075974', '303 King George V Ave, University, Berea, 4041, South Africa', 'Saturday, Oct 28', '21:00:43', '09:01:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890714800266004,30.979106426239017&markers=color:red|label:D|-29.870119670823637,30.981170386075974&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(443, 33, '', '-33.88328936837612', '18.590637780725956', '7 John Vorster Ave, Plattekloof 1, Cape Town, 7500, South Africa', '-28.752551766628525', '17.633829452097416', 'B1, Vioolsdrif, Namibia', 'Saturday, Oct 28', '21:28:48', '09:29:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.88328936837612,18.590637780725956&markers=color:red|label:D|-28.752551766628525,17.633829452097416&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 9, 0, 15, 1, 0, 1, 1, '2017-10-28'),
(444, 6, '', '-26.0612607165159', '28.0622627958655', '0D 5th Avenue\nEdenburg, Sandton, 2128', '-26.0544446594345', '28.0238948017359', '372 Main Road\nBryanston, Sandton, 2191', 'Sunday, Oct 29', '08:20:15', '08:31:47 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0612607165159,28.0622627958655&markers=color:red|label:D|-26.0544446594345,28.0238948017359&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(445, 7, '', '-26.061088738687932', '28.062042854726315', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '', '', 'Set your drop point', 'Sunday, Oct 29', '12:39:24', '12:40:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.061088738687932,28.062042854726315&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-10-29'),
(446, 33, '', '-33.897129150976504', '18.62842943519354', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.823732975088014', '18.490969240665436', '6 Raats Dr, Table View, Cape Town, 7441, South Africa', 'Sunday, Oct 29', '13:09:00', '01:42:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897129150976504,18.62842943519354&markers=color:red|label:D|-33.823732975088014,18.490969240665436&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(447, 6, '', '-26.09568', '28.05701', 'First Car Rental Sandton', '-26.117283', '28.083781', 'Jozi Boyz', 'Sunday, Oct 29', '14:00:35', '02:00:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.09568,28.05701&markers=color:red|label:D|-26.117283,28.083781&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(448, 7, '', '-26.06094627700352', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.071350343243914', '28.047729246318344', '7 Jacaranda Ave, Riverclub, Sandton, 2191, South Africa', 'Sunday, Oct 29', '14:01:45', '02:01:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06094627700352,28.062429428100586&markers=color:red|label:D|-26.071350343243914,28.047729246318344&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(449, 7, '', '-26.06094627700352', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.071350343243914', '28.047729246318344', '7 Jacaranda Ave, Riverclub, Sandton, 2191, South Africa', 'Sunday, Oct 29', '14:02:23', '02:02:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06094627700352,28.062429428100586&markers=color:red|label:D|-26.071350343243914,28.047729246318344&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(450, 7, '', '-26.06094627700352', '28.062429428100586', '2 Wessel Rd, Edenburg, Sandton, 2128, South Africa', '-26.071350343243914', '28.047729246318344', '7 Jacaranda Ave, Riverclub, Sandton, 2191, South Africa', 'Sunday, Oct 29', '14:03:05', '02:03:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.06094627700352,28.062429428100586&markers=color:red|label:D|-26.071350343243914,28.047729246318344&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-29');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(451, 33, '', '-33.89745002182541', '18.628593385219574', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.9657883', '18.481020000000004', 'Rondebosch', 'Sunday, Oct 29', '14:18:45', '02:21:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89745002182541,18.628593385219574&markers=color:red|label:D|-33.9657883,18.481020000000004&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(452, 33, '', '-33.897129150976504', '18.62842943519354', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Sunday, Oct 29', '14:49:04', '02:49:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897129150976504,18.62842943519354&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-10-29'),
(453, 33, '', '-33.89738629301915', '18.628574274480343', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.95129800000001', '18.383098300000004', 'Camps Bay', 'Sunday, Oct 29', '14:49:49', '02:50:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89738629301915,18.628574274480343&markers=color:red|label:D|-33.95129800000001,18.383098300000004&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 9, 0, 15, 1, 0, 1, 1, '2017-10-29'),
(454, 15, '', '-29.89071741635777', '30.97911413758993', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.876145011210117', '30.977387130260468', '88 Levenhall Rd, Umbilo, Berea, 4075, South Africa', 'Sunday, Oct 29', '15:04:47', '03:05:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.89071741635777,30.97911413758993&markers=color:red|label:D|-29.876145011210117,30.977387130260468&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(455, 33, '', '-33.89749204385065', '18.62865138798952', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.95129800000001', '18.383098300000004', 'Camps Bay', 'Sunday, Oct 29', '15:14:01', '03:14:01 PM', '', '29/10/2017', '08:30', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(456, 33, '', '-33.89749204385065', '18.62865138798952', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.95129800000001', '18.383098300000004', 'Camps Bay', 'Sunday, Oct 29', '15:15:23', '03:21:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89749204385065,18.62865138798952&markers=color:red|label:D|-33.95129800000001,18.383098300000004&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(457, 6, '', '-26.09568', '28.05701', 'First Car Rental Sandton', '-26.117283', '28.083781', 'Jozi Boyz', 'Sunday, Oct 29', '18:00:33', '06:01:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.09568,28.05701&markers=color:red|label:D|-26.117283,28.083781&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(458, 25, '', '28.4126053892041', '77.043030783534', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4707417', '77.0463719', 'Sector 14', 'Monday, Oct 30', '07:26:51', '07:26:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4126053892041,77.043030783534&markers=color:red|label:D|28.4707417,77.0463719&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-30'),
(459, 25, '', '28.4126053892041', '77.043030783534', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4707417', '77.0463719', 'Sector 14', 'Monday, Oct 30', '07:28:18', '08:09:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4126053892041,77.043030783534&markers=color:red|label:D|28.4707417,77.0463719&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 77, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(460, 13, '', '28.412346180510045', '77.0433922111988', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.459080000000004', '77.07051799999999', 'Huda City Metro Station', 'Monday, Oct 30', '09:13:14', '09:13:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412346180510045,77.0433922111988&markers=color:red|label:D|28.459080000000004,77.07051799999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-30'),
(461, 13, '', '28.412331435969133', '77.04339925199747', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 30', '09:27:44', '10:00:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412331435969133,77.04339925199747&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 80, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(462, 13, '', '28.412331435969133', '77.04339925199747', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.455569524653686', '77.0731972530484', '31, Sector 44, Gurugram, Haryana 122022, India', 'Monday, Oct 30', '10:02:10', '10:04:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412331435969133,77.04339925199747&markers=color:red|label:D|28.455569524653686,77.0731972530484&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 80, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(463, 13, '', '28.41230489579029', '77.04341500997543', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 30', '10:06:26', '10:18:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41230489579029,77.04341500997543&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 80, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(464, 42, '', '28.4120954387155', '77.0432622917714', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4333317736711', '77.0753832533956', '794, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Monday, Oct 30', '10:40:25', '10:40:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120954387155,77.0432622917714&markers=color:red|label:D|28.4333317736711,77.0753832533956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-30'),
(465, 42, '', '28.4120954387155', '77.0432622917714', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4333317736711', '77.0753832533956', '794, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Monday, Oct 30', '11:09:28', '11:09:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120954387155,77.0432622917714&markers=color:red|label:D|28.4333317736711,77.0753832533956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-30'),
(466, 42, '', '28.4120954387155', '77.0432622917714', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4333317736711', '77.0753832533956', '794, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Monday, Oct 30', '11:46:09', '11:47:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120954387155,77.0432622917714&markers=color:red|label:D|28.4333317736711,77.0753832533956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(467, 7, 'TAG4432y7R', '-26.123316166547095', '28.034538477659222', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.171158843882758', '27.966990061104298', '53 Italian Rd, Newlands, Randburg, 2092, South Africa', 'Monday, Oct 30', '13:33:32', '02:00:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123316166547095,28.034538477659222&markers=color:red|label:D|-26.171158843882758,27.966990061104298&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(468, 13, '', '28.434482498067332', '77.0354837179184', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 30', '19:40:24', '07:40:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434482498067332,77.0354837179184&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-30'),
(469, 42, '', '28.4120730171245', '77.0432773791971', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4139801578161', '77.076452113688', 'Golf Course Extension Road, Sushant Lok III Extension, Sector 62\nGurugram, Haryana 122005', 'Tuesday, Oct 31', '12:15:27', '12:15:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120730171245,77.0432773791971&markers=color:red|label:D|28.4139801578161,77.076452113688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(470, 42, '', '28.4120730171245', '77.0432773791971', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4139801578161', '77.076452113688', 'Golf Course Extension Road, Sushant Lok III Extension, Sector 62\nGurugram, Haryana 122005', 'Tuesday, Oct 31', '12:16:59', '12:18:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120730171245,77.0432773791971&markers=color:red|label:D|28.4139801578161,77.076452113688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(471, 42, '', '28.4120910996628', '77.0433164387941', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '12:21:15', '12:21:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120910996628,77.0433164387941&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(472, 42, '', '28.4121443196536', '77.0432535083141', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4270425179841', '77.0693251490593', '601, Orchid Island, Sector 51\nGurugram, Haryana 122003', 'Tuesday, Oct 31', '12:46:19', '12:47:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121443196536,77.0432535083141&markers=color:red|label:D|28.4270425179841,77.0693251490593&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(473, 42, '', '28.4120908121918', '77.0432569591932', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '13:13:39', '01:14:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120908121918,77.0432569591932&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(474, 42, '', '28.4121214734837', '77.0432678237557', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '13:23:12', '01:24:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121214734837,77.0432678237557&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(475, 42, '', '28.4121056227278', '77.0432185382368', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4207107225325', '77.0516896247864', '75, Vikas Marg, Malibu Town, Sector 47\nGurugram, Haryana 122018', 'Tuesday, Oct 31', '13:40:44', '01:41:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121056227278,77.0432185382368&markers=color:red|label:D|28.4207107225325,77.0516896247864&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(476, 42, '', '28.4453599548796', '77.0332983881235', 'Gurgaon - Delhi Expressway, Shanti Nagar, Sector 11\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '13:55:00', '01:55:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4453599548796,77.0332983881235&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(477, 35, '', '-29.81911458811731', '31.011811271309853', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.84121710986794', '31.019039489328858', 'Daly Rd, Windermere, Berea, 4001, South Africa', 'Tuesday, Oct 31', '14:43:54', '02:45:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81911458811731,31.011811271309853&markers=color:red|label:D|-29.84121710986794,31.019039489328858&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(478, 6, '', '-26.123301115188244', '28.0345156788826', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.152065236656934', '28.009728714823723', '28 Louw Geldenhuys Dr, Emmarentia, Randburg, 2195, South Africa', 'Tuesday, Oct 31', '14:56:34', '02:56:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123301115188244,28.0345156788826&markers=color:red|label:D|-26.152065236656934,28.009728714823723&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(479, 35, '', '-29.81916956544966', '31.01200740784407', '253 Peter Mokaba Rd, Morningside, Berea, 4001, South Africa', '-29.88968347341243', '30.977603718638417', '147 Fenniscowles Rd, Umbilo, Berea, 4075, South Africa', 'Tuesday, Oct 31', '15:15:06', '03:16:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.81916956544966,31.01200740784407&markers=color:red|label:D|-29.88968347341243,30.977603718638417&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(480, 15, '', '-29.86077116915374', '31.006473675370216', '45-47 Umbilo Rd, Greyville, Berea, 4013, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Tuesday, Oct 31', '15:37:52', '03:38:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86077116915374,31.006473675370216&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 2, 0, 4, 1, 0, 1, 1, '2017-10-31'),
(481, 15, '', '-29.858606406607656', '31.00260928273201', '14 Brand Rd, Bulwer, Berea, 4083, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Tuesday, Oct 31', '15:38:38', '03:39:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.858606406607656,31.00260928273201&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-10-31'),
(482, 15, '', '-29.8623366310386', '31.000976487994194', '59 Esther Roberts Rd, Bulwer, Berea, 4083, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Tuesday, Oct 31', '15:41:27', '03:49:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.8623366310386,31.000976487994194&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(483, 13, '', '28.434647012600408', '77.03503612428904', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '16:19:03', '04:19:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434647012600408,77.03503612428904&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(484, 13, '', '28.434646717771713', '77.03503612428904', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '16:21:31', '04:21:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434646717771713,77.03503612428904&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 80, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(485, 13, '', '28.434647012600408', '77.03503578901291', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '16:27:25', '04:27:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434647012600408,77.03503578901291&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 80, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(486, 13, '', '28.434647012600408', '77.03503578901291', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '16:34:42', '04:34:42 PM', '', '31/10/2017', '22:04', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(487, 6, '', '-29.8633249179746', '30.9634996578097', '150 Booth Road\nWiggins, Durban, 4091', '-29.931993988891', '30.9086357429624', '68 Riversdale Road\nSilverglen, Chatsworth, 4092', 'Tuesday, Oct 31', '17:47:28', '05:48:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.8633249179746,30.9634996578097&markers=color:red|label:D|-29.931993988891,30.9086357429624&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-10-31'),
(488, 35, '', '-26.097114836405687', '28.0570874735713', '159 Rivonia Rd, Morningside, Sandton, 2057, South Africa', '-26.24360958247708', '27.9342107847333', '5847 Taukobong St, Orlando East, Soweto, 1804, South Africa', 'Tuesday, Oct 31', '17:50:45', '05:51:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.097114836405687,28.0570874735713&markers=color:red|label:D|-26.24360958247708,27.9342107847333&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(489, 6, '', '-29.7878080885464', '31.0229317098856', '31 Grove Crescent\nPark Hill, Durban North, 4051', '-29.852175810898', '31.0267270356417', 'Samora Machel Street\nDurban Central, Durban, 4025', 'Tuesday, Oct 31', '17:53:49', '05:54:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.7878080885464,31.0229317098856&markers=color:red|label:D|-29.852175810898,31.0267270356417&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(490, 6, '', '-29.803933288287', '31.0135647654533', '19 Stanhope Place\nUmgeni Park, Durban North, 4051', '-29.8333771378048', '30.927219428122', '3 Chapel Place\nBerea West, Westville, 3629', 'Tuesday, Oct 31', '17:58:19', '05:58:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.803933288287,31.0135647654533&markers=color:red|label:D|-29.8333771378048,30.927219428122&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(491, 35, '', '-26.106067999999997', '28.05318799999999', 'Sandton Convention Centre', '-26.2402112', '27.9556537', 'Diepkloof Zone 4', 'Tuesday, Oct 31', '18:01:03', '06:01:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.106067999999997,28.05318799999999&markers=color:red|label:D|-26.2402112,27.9556537&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(492, 15, '', '-29.86203831125836', '31.006888076663017', '56 Magwaza Maphalala St, Congela, Durban, 4013, South Africa', '', '', 'Set your drop point', 'Wednesday, Nov 1', '06:19:57', '06:19:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.86203831125836,31.006888076663017&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(493, 15, '', '-29.625585625800188', '30.40388122200966', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.604302508160725', '30.40388356894255', '12-14 Ernest Tooth Rd, Wensleydale, Pietermaritzburg, 3201, South Africa', 'Wednesday, Nov 1', '06:20:30', '06:20:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.625585625800188,30.40388122200966&markers=color:red|label:D|-29.604302508160725,30.40388356894255&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(494, 33, '', '-33.92165239154649', '18.584349676966667', 'Francie Van Zijl Dr, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Wednesday, Nov 1', '06:31:30', '06:32:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92165239154649,18.584349676966667&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(495, 42, '', '28.412083536413', '77.0432539936873', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Nov 1', '07:18:15', '07:18:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412083536413,77.0432539936873&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(496, 42, '', '28.412083536413', '77.0432539936873', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Nov 1', '07:19:23', '07:19:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412083536413,77.0432539936873&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(497, 6, '', '-26.1233224590201', '28.0348546019523', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1587236328378', '28.0640883743763', '18 9th Avenue\nHoughton Estate, Johannesburg, 2198', 'Wednesday, Nov 1', '07:28:00', '07:31:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233224590201,28.0348546019523&markers=color:red|label:D|-26.1587236328378,28.0640883743763&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(498, 6, '', '-26.1233659716776', '28.0346425961753', '55 6th Avenue\nHyde Park, Sandton, 2196', '-26.1567196836516', '28.011888563633', '208 Barry Hertzog Avenue\nGreenside, Randburg, 2034', 'Wednesday, Nov 1', '07:45:50', '07:48:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233659716776,28.0346425961753&markers=color:red|label:D|-26.1567196836516,28.011888563633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(499, 15, '', '-29.62121382729585', '30.396519899368286', '91 Ridge Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.63024925132666', '30.395236797630783', '47 Oribi Rd, Scottsville, Pietermaritzburg, 3201, South Africa', 'Wednesday, Nov 1', '12:13:26', '12:14:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62121382729585,30.396519899368286&markers=color:red|label:D|-29.63024925132666,30.395236797630783&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-01'),
(500, 33, '', '-34.41337037393672', '19.20483112335205', 'R43, Sand Bay, Sandbaai, 7200, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Wednesday, Nov 1', '14:36:57', '04:40:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.41337037393672,19.20483112335205&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 7, 1, 0, 1, 1, '2017-11-01'),
(501, 6, '', '-26.1232595734277', '28.0347902700305', '57 6th Road\nHyde Park, Sandton, 2196', '-26.0207979', '27.940088', 'Missouri Crescent', 'Wednesday, Nov 1', '14:59:29', '03:01:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1232595734277,28.0347902700305&markers=color:red|label:D|-26.0207979,27.940088&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(502, 15, '', '-29.8568542084772', '31.026657298207283', '157-161 Monty Naicker Rd, Durban Central, Durban, 4001, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Wednesday, Nov 1', '15:34:15', '03:34:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.8568542084772,31.026657298207283&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(503, 15, '', '-29.856677707557647', '31.02537956088781', '37 Dr A B Xuma St, Durban Central, Durban, 4001, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Wednesday, Nov 1', '15:35:24', '03:56:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.856677707557647,31.02537956088781&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(504, 33, '', '-33.897348445321285', '18.628592379391193', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.922111599999994', '18.645025799999996', 'Bellville South', 'Wednesday, Nov 1', '16:41:52', '04:41:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897348445321285,18.628592379391193&markers=color:red|label:D|-33.922111599999994,18.645025799999996&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(505, 33, '', '-33.897348445321285', '18.628592379391193', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.922111599999994', '18.645025799999996', 'Bellville South', 'Wednesday, Nov 1', '16:42:30', '04:42:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897348445321285,18.628592379391193&markers=color:red|label:D|-33.922111599999994,18.645025799999996&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(506, 33, '', '-33.897348445321285', '18.628592379391193', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.922111599999994', '18.645025799999996', 'Bellville South', 'Wednesday, Nov 1', '16:43:05', '04:43:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897348445321285,18.628592379391193&markers=color:red|label:D|-33.922111599999994,18.645025799999996&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(507, 33, '', '-33.897348445321285', '18.628592379391193', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.91694940000001', '18.3875487', 'Sea Point', 'Wednesday, Nov 1', '16:44:17', '04:44:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897348445321285,18.628592379391193&markers=color:red|label:D|-33.91694940000001,18.3875487&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(508, 33, '', '-33.89689761115776', '18.62841635942459', '0A Durban Rd, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Wednesday, Nov 1', '16:44:50', '04:44:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89689761115776,18.62841635942459&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(509, 33, '', '-33.89689761115776', '18.62841635942459', '0A Durban Rd, Oakdale, Cape Town, 7530, South Africa', '51.46583929999999', '0.009033799999999998', 'Blackheath', 'Wednesday, Nov 1', '16:45:33', '04:46:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89689761115776,18.62841635942459&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(510, 6, '', '-26.0607589381189', '28.0623496323824', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.0999592428322', '28.0286379531026', '43-47 Cawdor Avenue\nHurlingham, Sandton, 2070', 'Wednesday, Nov 1', '17:47:48', '05:48:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607589381189,28.0623496323824&markers=color:red|label:D|-26.0999592428322,28.0286379531026&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(511, 15, '', '-29.869999596409453', '30.98067484796047', '303 King George V Ave, University, Berea, 4041, South Africa', '-29.888166694296487', '30.979305244982243', '3 Blanche Grove, Umbilo, Berea, 4075, South Africa', 'Wednesday, Nov 1', '17:57:23', '05:57:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.869999596409453,30.98067484796047&markers=color:red|label:D|-29.888166694296487,30.979305244982243&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(512, 15, '', '-29.890825548090184', '30.979308262467388', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.875993837036734', '30.98400481045246', '14 Camborne Rd, Umbilo, Berea, 4075, South Africa', 'Wednesday, Nov 1', '20:21:48', '08:22:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890825548090184,30.979308262467388&markers=color:red|label:D|-29.875993837036734,30.98400481045246&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(513, 6, '', '-26.0611351719369', '28.06252783167', '2 Wessel Road\nEdenburg, Sandton, 2128', '-26.1240632', '28.0347471', '55 6th Rd', 'Thursday, Nov 2', '06:14:43', '07:05:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0611351719369,28.06252783167&markers=color:red|label:D|-26.1240632,28.0347471&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(514, 6, '', '-26.1233216659239', '28.0348446494522', '0C 6th Road\nHyde Park, Sandton, 2196', '-26.1393781723326', '28.0179889127612', '77 11th Street\nParkhurst, Randburg, 2193', 'Thursday, Nov 2', '11:26:32', '11:27:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233216659239,28.0348446494522&markers=color:red|label:D|-26.1393781723326,28.0179889127612&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(515, 15, '', '-29.62286638942869', '30.405142530798912', '3 Blackburrow Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Thursday, Nov 2', '14:07:06', '04:05:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.62286638942869,30.405142530798912&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(516, 6, '', '-26.1306841759329', '28.03148008883', '2 Bompas Road\nDunkeld West, Randburg, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Thursday, Nov 2', '15:12:42', '03:13:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1306841759329,28.03148008883&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(517, 6, '', '-26.1306841759329', '28.03148008883', '2 Bompas Road\nDunkeld West, Randburg, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Thursday, Nov 2', '15:14:08', '03:15:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1306841759329,28.03148008883&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-11-02'),
(518, 6, '', '-26.1306841759329', '28.03148008883', '2 Bompas Road\nDunkeld West, Randburg, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Thursday, Nov 2', '15:15:31', '03:15:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1306841759329,28.03148008883&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-02'),
(519, 6, '', '-26.1306841759329', '28.03148008883', '2 Bompas Road\nDunkeld West, Randburg, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Thursday, Nov 2', '15:16:00', '03:16:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1306841759329,28.03148008883&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 9, 0, 13, 1, 0, 1, 1, '2017-11-02'),
(520, 6, '', '-26.0924474476148', '28.0364830792211', '55 12th Avenue\nParkmore, Sandton, 2196', '-26.06071', '28.06227', '4A Wessel Rd', 'Thursday, Nov 2', '15:34:52', '04:01:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0924474476148,28.0364830792211&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(521, 6, '', '-26.0607952208242', '28.0622868225047', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1362912215505', '28.071759827435', '20 Stirling Street\nWaverley, Johannesburg, 2090', 'Friday, Nov 3', '19:18:29', '07:27:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607952208242,28.0622868225047&markers=color:red|label:D|-26.1362912215505,28.071759827435&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '4BD1EF4C5DED3159677193B7540FA7FC.sbg-vm-tx01', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-03'),
(522, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '09:29:41', '09:29:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-04'),
(523, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '09:38:33', '09:38:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(524, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '09:57:07', '09:57:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(525, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '10:02:25', '10:03:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(526, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '10:14:17', '10:14:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(527, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '10:28:03', '10:28:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-04'),
(528, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '10:28:48', '10:29:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '32FEE91AE0BB679AD952DF2E1B980F47.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(529, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '10:47:12', '10:47:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, 'D436FF2B0F7BE6535C950F222D5D5AA8.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(530, 13, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '10:48:47', '10:49:02 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, 'E86D04EA7FF1F6C6A4C6814D6C61E623.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(531, 49, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '11:13:33', '11:13:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, 'DE09DF01E5F551436CE792FCD1118796.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(532, 49, '', '28.41229752351723', '77.04347100108862', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Nov 4', '11:25:08', '11:25:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41229752351723,77.04347100108862&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-04'),
(533, 15, '', '-29.890708696051643', '30.979108437895775', '474 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.868044078464873', '30.983386896550655', '128 Lamont Rd, University, Berea, 4041, South Africa', 'Saturday, Nov 4', '15:50:41', '03:51:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890708696051643,30.979108437895775&markers=color:red|label:D|-29.868044078464873,30.983386896550655&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-04'),
(534, 6, '', '-26.2444311372053', '27.9081893339753', '11870 Senokoanyana Street\nOrlando West, Soweto, 1804', '-26.06071', '28.06227', '4A Wessel Rd', 'Sunday, Nov 5', '15:02:25', '03:40:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.2444311372053,27.9081893339753&markers=color:red|label:D|-26.06071,28.06227&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(535, 6, '', '-26.0607635747496', '28.0627661017178', '7 5th Avenue\nEdenburg, Sandton, 2128', '-26.114640542829', '28.0370600894094', '29 Cleveland Road\nSandhurst, Sandton, 2196', 'Sunday, Nov 5', '15:43:10', '03:44:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0607635747496,28.0627661017178&markers=color:red|label:D|-26.114640542829,28.0370600894094&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 17, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(536, 15, '', '-29.61551738074098', '30.40095828473568', '85 Alan Paton Ave, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.614792477246766', '30.41320960968733', '2 Comrie Pl, Hayfields, Pietermaritzburg, 3201, South Africa', 'Monday, Nov 6', '06:29:00', '06:31:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.61551738074098,30.40095828473568&markers=color:red|label:D|-29.614792477246766,30.41320960968733&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-06'),
(537, 49, '', '28.41229015124363', '77.0434421673417', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '06:31:26', '06:31:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41229015124363,77.0434421673417&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-06'),
(538, 49, '', '28.41229015124363', '77.0434421673417', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '06:32:01', '06:32:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41229015124363,77.0434421673417&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '3F9525215005D7BCB3E1D97A7475CF34.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(539, 49, '', '28.412306370244856', '77.04342842102051', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '07:28:19', '07:34:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412306370244856,77.04342842102051&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-06'),
(540, 49, '', '28.41230312644482', '77.04345021396875', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '07:35:09', '07:35:33 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41230312644482,77.04345021396875&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, 'FDA5CAC42C698E3CFC78BAABC18C15E0.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(541, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Monday, Nov 6', '07:39:46', '07:39:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-06');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `checkout_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(542, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Monday, Nov 6', '07:40:43', '07:40:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-06'),
(543, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Monday, Nov 6', '07:50:44', '07:50:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-06'),
(544, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Monday, Nov 6', '07:51:07', '07:51:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-06'),
(545, 25, '', '28.4120994282768', '77.0432356768181', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4260184994357', '77.0738255605102', '37, G Block, Sector 15, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Monday, Nov 6', '07:57:49', '07:58:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120994282768,77.0432356768181&markers=color:red|label:D|28.4260184994357,77.0738255605102&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-06'),
(546, 25, '', '28.4120859671649', '77.0432660636278', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4342251119137', '77.0901749655604', '1, Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Monday, Nov 6', '07:59:08', '07:59:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120859671649,77.0432660636278&markers=color:red|label:D|28.4342251119137,77.0901749655604&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '24A11DB5EC4D7BADEA1E34DA4EA09150.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(547, 25, '', '28.4120761377392', '77.0432773079133', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4438909621165', '77.0842090621591', 'Sector 52A\nGurugram, Haryana 122022', 'Monday, Nov 6', '08:49:10', '08:49:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120761377392,77.0432773079133&markers=color:red|label:D|28.4438909621165,77.0842090621591&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-06'),
(548, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '08:49:19', '08:49:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 9, 0, 13, 2, 0, 1, 1, '2017-11-06'),
(549, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '08:50:03', '08:50:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-06'),
(550, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '08:51:03', '08:51:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-06'),
(551, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '08:53:23', '08:53:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-06'),
(552, 25, '', '28.4120761377392', '77.0432773079133', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4438909621165', '77.0842090621591', 'Sector 52A\nGurugram, Haryana 122022', 'Monday, Nov 6', '08:54:27', '08:54:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120761377392,77.0432773079133&markers=color:red|label:D|28.4438909621165,77.0842090621591&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 9, 0, 15, 2, 0, 1, 1, '2017-11-06'),
(553, 25, '', '28.4120761377392', '77.0432773079133', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4438909621165', '77.0842090621591', 'Sector 52A\nGurugram, Haryana 122022', 'Monday, Nov 6', '08:54:58', '08:55:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120761377392,77.0432773079133&markers=color:red|label:D|28.4438909621165,77.0842090621591&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '918FC4EEF7AFFEB1772E6DF441741A07.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(554, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '08:55:08', '08:55:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '34CB874529D209BD1FF6412E0F15C674.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(555, 25, '', '28.4121049965735', '77.043263102834', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4455338860454', '77.1090382710099', 'Park Drive, DLF Phase 5, Sector 54\nGurugram, Haryana 122011', 'Monday, Nov 6', '09:03:06', '09:03:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121049965735,77.043263102834&markers=color:red|label:D|28.4455338860454,77.1090382710099&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-06'),
(556, 25, '', '28.4121288070248', '77.0432556311054', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.452888848953', '77.1098224818707', 'DLF Golf Course, Sector 42\nGurugram, Haryana', 'Monday, Nov 6', '09:06:19', '09:06:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121288070248,77.0432556311054&markers=color:red|label:D|28.452888848953,77.1098224818707&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '6B653688344CEDFD12F68F414ADC1A17.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(557, 6, '', '-26.0146129519257', '28.100635483861', 'Jukskei View Drive\nWaterval 5-Ir, Midrand, 2090', '-26.1744220997639', '28.0323930457234', '30 Jan Smuts Avenue\nForest Town, Johannesburg, 2193', 'Monday, Nov 6', '09:12:06', '09:12:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0146129519257,28.100635483861&markers=color:red|label:D|-26.1744220997639,28.0323930457234&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-11-06'),
(558, 6, '', '-26.0146129519257', '28.100635483861', 'Jukskei View Drive\nWaterval 5-Ir, Midrand, 2090', '-26.1744220997639', '28.0323930457234', '30 Jan Smuts Avenue\nForest Town, Johannesburg, 2193', 'Monday, Nov 6', '09:13:01', '09:13:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0146129519257,28.100635483861&markers=color:red|label:D|-26.1744220997639,28.0323930457234&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-11-06'),
(559, 6, '', '-26.0146129519257', '28.100635483861', 'Jukskei View Drive\nWaterval 5-Ir, Midrand, 2090', '-26.1744220997639', '28.0323930457234', '30 Jan Smuts Avenue\nForest Town, Johannesburg, 2193', 'Monday, Nov 6', '09:15:15', '09:16:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.0146129519257,28.100635483861&markers=color:red|label:D|-26.1744220997639,28.0323930457234&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-11-06'),
(560, 49, '', '28.412320230116645', '77.04342037439346', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '09:36:21', '09:38:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320230116645,77.04342037439346&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(561, 49, '', '28.412329371733243', '77.04344786703587', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '09:55:21', '09:55:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412329371733243,77.04344786703587&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(562, 25, '', '28.4120921241006', '77.0432788117852', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4366488804217', '77.1233223751187', 'Gurgaon - Faridabad Road, Mandi\nGurugram, Haryana 122011', 'Monday, Nov 6', '10:04:00', '10:04:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120921241006,77.0432788117852&markers=color:red|label:D|28.4366488804217,77.1233223751187&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-06'),
(563, 25, '', '28.412090296354', '77.0432869128219', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4340488035114', '77.1268957480788', 'Baliawas\nHaryana', 'Monday, Nov 6', '10:06:54', '10:07:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412090296354,77.0432869128219&markers=color:red|label:D|28.4340488035114,77.1268957480788&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, 'A8167002601D149EF6726D58367FCBED.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(564, 25, '', '28.4121381445121', '77.043290287328', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4306325491035', '77.1061572432518', '86, Golf Course Road, Sushant Lok 2, Sector 55\nGurugram, Haryana 122011', 'Monday, Nov 6', '10:12:44', '10:12:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121381445121,77.043290287328&markers=color:red|label:D|28.4306325491035,77.1061572432518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-06'),
(565, 25, '', '28.4121381445121', '77.043290287328', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4306325491035', '77.1061572432518', '86, Golf Course Road, Sushant Lok 2, Sector 55\nGurugram, Haryana 122011', 'Monday, Nov 6', '10:14:28', '10:15:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121381445121,77.043290287328&markers=color:red|label:D|28.4306325491035,77.1061572432518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, 'ABC2F71FB9EA3BD863329602BE762B65.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(566, 25, '', '28.4121158905592', '77.0431958232792', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4409697196169', '77.0909843221307', 'Khatu Shyam Road, Sector 52A\nGurugram, Haryana 122003', 'Monday, Nov 6', '10:18:17', '10:19:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121158905592,77.0431958232792&markers=color:red|label:D|28.4409697196169,77.0909843221307&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '577C9F0A3609AE3243C815DF64E0315A.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(567, 49, '', '28.412332910423295', '77.04343177378178', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '10:19:44', '10:20:02 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412332910423295,77.04343177378178&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(568, 49, '', '28.412332910423295', '77.04343177378178', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '10:27:58', '10:28:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412332910423295,77.04343177378178&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-11-06'),
(569, 49, '', '28.412320525007523', '77.04343881458044', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '10:29:27', '10:29:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412320525007523,77.04343881458044&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(570, 13, '', '28.412327307497304', '77.04343613237143', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Nov 6', '10:31:58', '10:32:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412327307497304,77.04343613237143&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, 'E4C0DE200EA06050249F772BD4E03BCE.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(571, 25, '', '28.412107386357', '77.0432248105522', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4579359146742', '77.1342895925045', 'Aya Nagar Extension, Aya Nagar\nNew Delhi, Delhi', 'Monday, Nov 6', '11:07:59', '11:08:33 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412107386357,77.0432248105522&markers=color:red|label:D|28.4579359146742,77.1342895925045&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-11-06'),
(572, 33, '', '-34.06460718354391', '18.449613265693188', '53 Tokai Rd, Kirstenhof, Cape Town, 7945, South Africa', '-34.00844560000001', '18.466181600000002', 'Wynberg', 'Monday, Nov 6', '14:15:20', '02:19:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.06460718354391,18.449613265693188&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '89FC6B89C354FCBF0EBC04A1E02C53DD.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(573, 35, '', '-29.72178151967816', '31.071062274277207', '19 Park Ln, Umhlanga, South Africa', '-29.74071091048201', '31.057281419634815', 'Umhlanga Rocks Dr, Umhlanga, 4051, South Africa', 'Monday, Nov 6', '14:16:45', '02:17:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.72178151967816,31.071062274277207&markers=color:red|label:D|-29.74071091048201,31.057281419634815&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, 'ED231E3DC3CE25373F5DA91E213CD3B2.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(574, 25, '', '28.4121365129453', '77.0432423427701', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4505117337245', '77.1051044762135', 'Golf Drive, Sector 42\nGurugram, Haryana 122011', 'Monday, Nov 6', '14:20:52', '02:21:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432423427701&markers=color:red|label:D|28.4505117337245,77.1051044762135&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-06'),
(575, 6, '', '-26.123288472045292', '28.03453378379345', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.161463320846234', '27.973164841532707', '43-45 3rd St, Albertskroon, Randburg, 2195, South Africa', 'Monday, Nov 6', '14:20:53', '02:21:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123288472045292,28.03453378379345&markers=color:red|label:D|-26.161463320846234,27.973164841532707&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-11-06'),
(576, 6, '', '-26.123288472045292', '28.03453378379345', '57 6th Rd, Hyde Park, Sandton, 2196, South Africa', '-26.171556638008376', '28.0275047197938', '24 Woolston Rd, Westcliff, Randburg, 2193, South Africa', 'Monday, Nov 6', '14:22:44', '02:23:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.123288472045292,28.03453378379345&markers=color:red|label:D|-26.171556638008376,28.0275047197938&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '4E22F8FBA9771A984E938A8425672A48.sbg-vm-tx02', 1, 1, 7, 0, 0, 2, 0, 1, 1, '2017-11-06'),
(577, 33, '', '-33.89687980037643', '18.628881722688675', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.92038261970244', '18.424362279474735', '1 St Georges Mall, Cape Town City Centre, Cape Town, 8000, South Africa', 'Monday, Nov 6', '18:54:41', '06:54:41 PM', '', '6/11/2017', '21:15', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-06'),
(578, 33, '', '-33.89687980037643', '18.628881722688675', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.92038261970244', '18.424362279474735', '1 St Georges Mall, Cape Town City Centre, Cape Town, 8000, South Africa', 'Monday, Nov 6', '18:55:18', '07:02:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89687980037643,18.628881722688675&markers=color:red|label:D|-33.92038261970244,18.424362279474735&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-06'),
(579, 33, '', '-33.89687980037643', '18.628881722688675', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.92038261970244', '18.424362279474735', '1 St Georges Mall, Cape Town City Centre, Cape Town, 8000, South Africa', 'Monday, Nov 6', '19:04:04', '07:04:04 PM', '', '6/11/2017', '21:15', 0, 28, '', 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-06'),
(580, 25, '', '28.4121102676091', '77.0433083921671', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4517333135029', '77.1160468831658', 'DLF Golf Course, Sector 42\nGurugram, Haryana', 'Tuesday, Nov 7', '08:48:08', '08:48:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121102676091,77.0433083921671&markers=color:red|label:D|28.4517333135029,77.1160468831658&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-07'),
(581, 25, '', '28.4121169802066', '77.0433214680078', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.43435601629', '77.0768708735704', 'Koyal Vihar, Sector 52\nGurugram, Haryana 122003', 'Tuesday, Nov 7', '08:59:50', '09:00:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121169802066,77.0433214680078&markers=color:red|label:D|28.43435601629,77.0768708735704&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 84, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-07'),
(582, 15, '', '-29.625822280413907', '30.403006821870804', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.615095322565363', '30.410727895796295', '7 Hesketh Dr, Hayfields, Pietermaritzburg, 3201, South Africa', 'Tuesday, Nov 7', '09:45:06', '09:47:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.625822280413907,30.403006821870804&markers=color:red|label:D|-29.615095322565363,30.410727895796295&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-07'),
(583, 13, '', '28.412329371733243', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Nov 7', '10:12:36', '10:12:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412329371733243,77.04343680292368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(584, 13, '', '28.43464494879959', '77.03504148870707', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.427282525505394', '77.03819945454597', 'Karhana Street, Rail Vihar, Pragati Hills Society, Sector 38, Gurugram, Haryana 122018, India', 'Tuesday, Nov 7', '15:47:58', '03:47:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43464494879959,77.03504148870707&markers=color:red|label:D|28.427282525505394,77.03819945454597&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(585, 13, '', '28.43464494879959', '77.03504148870707', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.427282525505394', '77.03819945454597', 'Karhana Street, Rail Vihar, Pragati Hills Society, Sector 38, Gurugram, Haryana 122018, India', 'Tuesday, Nov 7', '15:48:14', '03:48:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43464494879959,77.03504148870707&markers=color:red|label:D|28.427282525505394,77.03819945454597&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 83, 28, 'EDA8941041E79DBFD347D101F540A6A1.sbg-vm-tx02', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-07'),
(586, 31, '', '28.39522697633868', '76.96788132190704', 'Lifestyle Home Apartments, Vatika City, Sector 83, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 7', '16:28:38', '04:28:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.39522697633868,76.96788132190704&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(587, 31, '', '28.39522697633868', '76.96788132190704', 'Lifestyle Home Apartments, Vatika City, Sector 83, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 7', '16:29:09', '04:29:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.39522697633868,76.96788132190704&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(588, 31, '', '28.39522697633868', '76.96788132190704', 'Lifestyle Home Apartments, Vatika City, Sector 83, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 7', '16:29:16', '04:29:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.39522697633868,76.96788132190704&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(589, 33, '', '-33.88041770864327', '18.57471987605095', '36 Hennie Winterbach St, Panorama, Cape Town, 7500, South Africa', '-33.897239899999995', '18.6291699', '100 Durban Rd, Oakdale', 'Wednesday, Nov 8', '16:54:37', '05:10:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.88041770864327,18.57471987605095&markers=color:red|label:D|-33.897239899999995,18.6291699&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(590, 15, '', '-29.83615582203001', '30.881149135529995', '23 Entabeni Rd, Paradise Valley, Pinetown, 3610, South Africa', '-29.818456312207502', '30.8841897547245', '4 Travislee Pl, Cowies Hill Park, Pinetown, 3610, South Africa', 'Thursday, Nov 9', '04:30:42', '04:32:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.83615582203001,30.881149135529995&markers=color:red|label:D|-29.818456312207502,30.8841897547245&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-09'),
(591, 6, '', '-26.1233176716829', '28.034669905901', '57 6th Road\nHyde Park, Sandton, 2196', '-26.0919777480508', '28.0365692451596', '54-58 13th Street\nParkmore, Sandton, 2196', 'Thursday, Nov 9', '05:42:12', '05:42:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-26.1233176716829,28.034669905901&markers=color:red|label:D|-26.0919777480508,28.0365692451596&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-09'),
(592, 15, '', '-29.625709490661187', '30.40324755012989', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.598206590614282', '30.409043133258823', '50 Ohrtmann Rd, Willowton, Pietermaritzburg, 3201, South Africa', 'Thursday, Nov 9', '10:05:12', '10:06:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.625709490661187,30.40324755012989&markers=color:red|label:D|-29.598206590614282,30.409043133258823&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-09'),
(593, 35, '', '-29.72162952974259', '31.071184650063515', '1d Umhlanga Ridge Blvd, Umhlanga, 4319, South Africa', '-29.70449570252407', '31.050242967903614', '170 Sugar Cane Rd, Blackburn Estate, Blackburn, South Africa', 'Thursday, Nov 9', '13:54:16', '01:54:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.72162952974259,31.071184650063515&markers=color:red|label:D|-29.70449570252407,31.050242967903614&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-09'),
(594, 15, '', '-29.890707533344116', '30.979107096791267', '470 Bartle Rd, Umbilo, Berea, 4075, South Africa', '-29.881431893060412', '30.981144905090332', '3 Selborne Rd, Umbilo, Berea, 4075, South Africa', 'Friday, Nov 10', '19:30:10', '07:30:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.890707533344116,30.979107096791267&markers=color:red|label:D|-29.881431893060412,30.981144905090332&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, 'A4B4C819736085652E4127E692838CFC.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-10'),
(595, 6, '', '-18.9792751541667', '32.680663280189', 'Carrington Road\nMutare, Zimbabwe', '-18.9747127527262', '32.6501930505037', 'Simon Mazorodze Road\nMutare, Zimbabwe', 'Saturday, Nov 11', '03:06:41', '03:08:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-18.9792751541667,32.680663280189&markers=color:red|label:D|-18.9747127527262,32.6501930505037&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-11'),
(596, 15, '', '-29.625810331117915', '30.40298704057932', '62 Carbis Rd, Scottsville, Pietermaritzburg, 3201, South Africa', '-29.616330304111504', '30.409022010862827', '3 Ronson Pl, Hayfields, Pietermaritzburg, 3201, South Africa', 'Saturday, Nov 11', '10:12:57', '10:13:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.625810331117915,30.40298704057932&markers=color:red|label:D|-29.616330304111504,30.409022010862827&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 86, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-11'),
(597, 15, '', '-29.792557294126123', '30.740168541669846', '450 Kassier Rd, Summerveld, Outer West Durban, South Africa', '-29.890926', '30.979083', '476 Bartle Rd', 'Saturday, Nov 11', '11:28:50', '11:40:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.792557294126123,30.740168541669846&markers=color:red|label:D|-29.890926,30.979083&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-11'),
(598, 6, '', '-18.9789587386963', '32.6805717498064', 'Carrington Road\nMutare, Zimbabwe', '-18.9577432645554', '32.6758661493659', 'Merewe Street\nMutare, Zimbabwe', 'Saturday, Nov 11', '19:52:23', '07:52:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-18.9789587386963,32.6805717498064&markers=color:red|label:D|-18.9577432645554,32.6758661493659&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(599, 6, '', '-18.9789587386963', '32.6805717498064', 'Carrington Road\nMutare, Zimbabwe', '-18.9577432645554', '32.6758661493659', 'Merewe Street\nMutare, Zimbabwe', 'Saturday, Nov 11', '19:52:47', '07:52:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-18.9789587386963,32.6805717498064&markers=color:red|label:D|-18.9577432645554,32.6758661493659&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(600, 33, '', '-33.89704761118416', '18.62864200025797', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-34.0947539', '18.394966099999998', 'Noordhoek', 'Saturday, Nov 11', '20:02:12', '08:02:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89704761118416,18.62864200025797&markers=color:red|label:D|-34.0947539,18.394966099999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-11'),
(601, 33, '', '-33.89704761118416', '18.62864200025797', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-34.0947539', '18.394966099999998', 'Noordhoek', 'Saturday, Nov 11', '20:02:59', '08:03:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89704761118416,18.62864200025797&markers=color:red|label:D|-34.0947539,18.394966099999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-11-11'),
(602, 33, '', '-33.89704761118416', '18.62864200025797', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-26.132626799999997', '28.1766294', 'Edenvale', 'Saturday, Nov 11', '20:36:52', '08:44:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89704761118416,18.62864200025797&markers=color:red|label:D|-26.132626799999997,28.1766294&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-11'),
(603, 33, '', '-33.92108567056174', '18.585708551108837', '34 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-34.037444', '18.676945099999994', 'Khayelitsha', 'Monday, Nov 13', '06:40:38', '06:41:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92108567056174,18.585708551108837&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(604, 6, '', '-25.7562776978525', '28.340310305357', '877 Libertas Avenue\nEquestria, Pretoria, 0184', '-25.7728118', '28.2335863', '337 Veale St', 'Monday, Nov 13', '06:53:32', '06:56:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-25.7562776978525,28.340310305357&markers=color:red|label:D|-25.7728118,28.2335863&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 73, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(605, 15, '', '-29.597260584062173', '30.370036438107494', '157 Victoria Rd, Pietermaritzburg, 3201, South Africa', '-29.582215118042583', '30.382233113050464', '115 Melsetter Rd, Woodlands, Pietermaritzburg, 3201, South Africa', 'Monday, Nov 13', '10:23:09', '10:24:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.597260584062173,30.370036438107494&markers=color:red|label:D|-29.582215118042583,30.382233113050464&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13'),
(606, 33, '', '-33.92112183843481', '18.58570922166109', '34 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Tuesday, Nov 14', '13:41:10', '01:41:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92112183843481,18.58570922166109&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(607, 33, '', '-33.921119890934335', '18.585726656019688', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Tuesday, Nov 14', '13:56:59', '01:57:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.921119890934335,18.585726656019688&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-14'),
(608, 33, '', '-33.921119890934335', '18.585726656019688', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.89268470116723', '18.630182929337025', '155 Durban Rd, Oakdale, Cape Town, 7530, South Africa', 'Tuesday, Nov 14', '14:05:35', '02:05:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.921119890934335,18.585726656019688&markers=color:red|label:D|-33.89268470116723,18.630182929337025&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-14'),
(609, 33, '', '-33.921119890934335', '18.585726656019688', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.89268470116723', '18.630182929337025', '155 Durban Rd, Oakdale, Cape Town, 7530, South Africa', 'Tuesday, Nov 14', '14:07:02', '02:07:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.921119890934335,18.585726656019688&markers=color:red|label:D|-33.89268470116723,18.630182929337025&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, 'F302BEC479AF37FE35B98FAFB98F0FAE.sbg-vm-tx01', 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-14'),
(610, 33, '', '-33.92111182271765', '18.585717603564262', '38 Selsdon St, Beaconvale, Cape Town, 7500, South Africa', '-33.8942695', '18.6294384', 'Bellville', 'Tuesday, Nov 14', '14:33:19', '02:34:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.92111182271765,18.585717603564262&markers=color:red|label:D|-33.8942695,18.6294384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 34, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(611, 6, '', '-26.0607034190696', '28.0624373852636', '4A Wessel Road\nEdenburg, Sandton, 2128', '-26.1062761851196', '28.0358996987343', '92 Argyle Avenue\nHurlingham, Sandton, 2070', 'Wednesday, Nov 15', '14:35:59', '02:42:02 PM', '', '2017-11-15', '16:55:52', 73, 28, '', 2, 1, 3, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(612, 15, '', '-29.601459969149346', '30.38119979202747', 'Henrietta St, Pietermaritzburg, 3201, South Africa', '-29.581410958481445', '30.398427285254', 'Ottos Bluff Rd, Woodlands, Pietermaritzburg, 3201, South Africa', 'Thursday, Nov 16', '10:18:57', '10:20:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.601459969149346,30.38119979202747&markers=color:red|label:D|-29.581410958481445,30.398427285254&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 33, 28, '', 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-16'),
(613, 33, '', '-33.89716393755359', '18.628418035805225', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.924157699999995', '18.454311299999997', 'Woodstock', 'Friday, Nov 17', '19:20:09', '07:20:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89716393755359,18.628418035805225&markers=color:red|label:D|-33.924157699999995,18.454311299999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(614, 33, '', '-33.89716393755359', '18.628418035805225', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.924157699999995', '18.454311299999997', 'Woodstock', 'Friday, Nov 17', '19:21:06', '07:21:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89716393755359,18.628418035805225&markers=color:red|label:D|-33.924157699999995,18.454311299999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(615, 33, '', '-33.896947703960286', '18.628612831234932', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.924868499999995', '18.4240553', 'Cape Town', 'Friday, Nov 17', '19:23:30', '07:23:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.896947703960286,18.628612831234932&markers=color:red|label:D|-33.924868499999995,18.4240553&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(616, 33, '', '-33.897071822666476', '18.62888742238283', '100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.924868499999995', '18.4240553', 'Cape Town', 'Friday, Nov 17', '19:25:12', '07:25:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897071822666476,18.62888742238283&markers=color:red|label:D|-33.924868499999995,18.4240553&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(617, 33, '', '-33.89708879852928', '18.628616854548454', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Friday, Nov 17', '19:31:53', '07:31:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89708879852928,18.628616854548454&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(618, 33, '', '-33.89708879852928', '18.628616854548454', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '', '', 'Set your drop point', 'Friday, Nov 17', '21:41:17', '09:41:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89708879852928,18.628616854548454&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(619, 33, '', '-33.89708879852928', '18.628616854548454', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.9321045', '18.860152', 'Stellenbosch', 'Friday, Nov 17', '21:42:17', '09:42:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89708879852928,18.628616854548454&markers=color:red|label:D|-33.9321045,18.860152&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(620, 33, '', '-33.897154197313434', '18.628456592559814', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.9067916', '18.5808115', 'Parow', 'Friday, Nov 17', '21:43:51', '09:43:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897154197313434,18.628456592559814&markers=color:red|label:D|-33.9067916,18.5808115&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(621, 33, '', '-33.897139726097436', '18.62847402691841', '4 Alexandra St, Oakdale, Cape Town, 7530, South Africa', '-33.87298', '18.634609999999995', 'Tyger Valley Centre', 'Friday, Nov 17', '21:46:09', '09:46:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.897139726097436,18.62847402691841&markers=color:red|label:D|-33.87298,18.634609999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(622, 33, '', '-33.89726412281635', '18.628528341650963', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.85634279999999', '18.695091299999998', 'Okavango Road', 'Saturday, Nov 18', '06:45:06', '06:45:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89726412281635,18.628528341650963&markers=color:red|label:D|-33.85634279999999,18.695091299999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(623, 33, '', '-33.89722571847954', '18.62854577600956', '96-100 Durban Rd, Oakdale, Cape Town, 7530, South Africa', '-33.85634279999999', '18.695091299999998', 'Okavango Road', 'Saturday, Nov 18', '06:47:54', '06:47:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-33.89722571847954,18.62854577600956&markers=color:red|label:D|-33.85634279999999,18.695091299999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(624, 35, '', '-29.721595754170103', '31.07119034975767', '1d Umhlanga Ridge Blvd, Umhlanga, 4319, South Africa', '-29.818767', '31.011423999999998', '253 Peter Mokaba Rd', 'Tuesday, Nov 28', '10:54:38', '10:54:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-29.721595754170103,31.07119034975767&markers=color:red|label:D|-29.818767,31.011423999999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 32, 28, '', 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '10111', 1),
(3, 'Ambulance', '084124', 1),
(4, 'ambulance', '101', 2),
(6, 'Breakdown', '199', 2),
(7, 'Breakdown', '199', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Insurance'),
(3, 'Inspection Certificate'),
(4, 'Operators Card (Double disc)'),
(5, 'Operating License/Permit'),
(6, 'Registration Certificate'),
(7, 'Comprehensive vehicle insurance (including R5Million passenger liability)'),
(8, 'Police Clearance Certificate');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(58, 56, 6, 1),
(57, 56, 5, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(56, 56, 4, 1),
(55, 56, 3, 1),
(54, 56, 2, 1),
(59, 121, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-10-07 00.00.01 AM', '2017-10-09 01:29:12 PM', 31, '1.75', '0000-00-00', 0),
(2, '2017-10-06 00.00.01 AM', '2017-10-09 01:29:12 PM', 30, '0.75', '0000-00-00', 0),
(3, '2017-10-06 00.00.01 AM', '2017-10-09 01:29:12 PM', 29, '0', '0000-00-00', 0),
(4, '2017-10-06 00.00.01 AM', '2017-10-09 01:29:12 PM', 28, '0.5', '0000-00-00', 0),
(5, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 27, '0', '0000-00-00', 0),
(6, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 26, '0', '0000-00-00', 0),
(7, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 25, '0', '0000-00-00', 0),
(8, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 24, '0', '0000-00-00', 0),
(9, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 23, '0', '0000-00-00', 0),
(10, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 22, '0', '0000-00-00', 0),
(11, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 21, '0', '0000-00-00', 0),
(12, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 20, '0', '0000-00-00', 0),
(13, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 19, '0', '0000-00-00', 0),
(14, '2017-10-04 00.00.01 AM', '2017-10-09 01:29:12 PM', 18, '0', '0000-00-00', 0),
(15, '2017-10-03 00.00.01 AM', '2017-10-09 01:29:12 PM', 17, '17.42', '2017-10-24', 1),
(16, '2017-10-03 00.00.01 AM', '2017-10-09 01:29:12 PM', 16, '0', '0000-00-00', 0),
(17, '2017-10-03 00.00.01 AM', '2017-10-09 01:29:12 PM', 15, '0', '0000-00-00', 0),
(18, '2017-10-03 00.00.01 AM', '2017-10-09 01:29:12 PM', 14, '0', '0000-00-00', 0),
(19, '2017-10-03 00.00.01 AM', '2017-10-09 01:29:12 PM', 13, '0', '0000-00-00', 0),
(20, '2017-10-03 00.00.01 AM', '2017-10-09 01:29:12 PM', 12, '0', '0000-00-00', 0),
(21, '2017-09-29 00.00.01 AM', '2017-10-09 01:29:12 PM', 11, '3.55', '0000-00-00', 0),
(22, '2017-09-29 00.00.01 AM', '2017-10-09 01:29:12 PM', 10, '0', '0000-00-00', 0),
(23, '2017-09-29 00.00.01 AM', '2017-10-09 01:29:12 PM', 9, '0.5', '0000-00-00', 0),
(24, '2017-09-29 00.00.01 AM', '2017-10-09 01:29:12 PM', 8, '21.36', '0000-00-00', 0),
(25, '2017-09-22 00.00.01 AM', '2017-10-09 01:29:12 PM', 7, '12', '0000-00-00', 0),
(26, '2017-09-22 00.00.01 AM', '2017-10-09 01:29:12 PM', 6, '74', '0000-00-00', 0),
(27, '2017-09-22 00.00.01 AM', '2017-10-09 01:29:12 PM', 5, '0', '0000-00-00', 0),
(28, '2017-09-22 00.00.01 AM', '2017-10-09 01:29:12 PM', 4, '10.68', '0000-00-00', 0),
(29, '2017-09-22 00.00.01 AM', '2017-10-09 01:29:12 PM', 3, '0', '0000-00-00', 0),
(30, '2017-09-22 00.00.01 AM', '2017-10-09 01:29:12 PM', 2, '10', '0000-00-00', 0),
(31, '2017-09-20 00.00.01 AM', '2017-10-09 01:29:12 PM', 1, '5', '2017-10-09', 1),
(32, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 65, '0', '0000-00-00', 0),
(33, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 64, '0', '0000-00-00', 0),
(34, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 63, '0', '0000-00-00', 0),
(35, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 62, '0', '0000-00-00', 0),
(36, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 61, '0', '0000-00-00', 0),
(37, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 60, '0.25', '0000-00-00', 0),
(38, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 59, '0', '0000-00-00', 0),
(39, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 58, '0', '0000-00-00', 0),
(40, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 57, '0', '0000-00-00', 0),
(41, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 56, '0', '0000-00-00', 0),
(42, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 53, '8.14', '0000-00-00', 0),
(43, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 52, '0', '0000-00-00', 0),
(44, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 51, '0', '0000-00-00', 0),
(45, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 50, '0', '0000-00-00', 0),
(46, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 49, '-4.75', '0000-00-00', 0),
(47, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 48, '0.5', '0000-00-00', 0),
(48, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 47, '1', '0000-00-00', 0),
(49, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 46, '0.5', '0000-00-00', 0),
(50, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 45, '4', '0000-00-00', 0),
(51, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 44, '0.25', '0000-00-00', 0),
(52, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 43, '0', '0000-00-00', 0),
(53, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 42, '0', '0000-00-00', 0),
(54, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 41, '0.25', '0000-00-00', 0),
(55, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 40, '1.25', '0000-00-00', 0),
(56, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 39, '0', '0000-00-00', 0),
(57, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 38, '1.25', '0000-00-00', 0),
(58, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 37, '0', '0000-00-00', 0),
(59, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 36, '0', '0000-00-00', 0),
(60, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 35, '0', '0000-00-00', 0),
(61, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 34, '130.82', '2017-10-27', 1),
(62, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 33, '55.49', '0000-00-00', 0),
(63, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 32, '-121.36', '0000-00-00', 0),
(64, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 31, '1.5', '0000-00-00', 0),
(65, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 30, '0', '0000-00-00', 0),
(66, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 29, '0.25', '0000-00-00', 0),
(67, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 28, '0', '0000-00-00', 0),
(68, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 27, '-4.25', '0000-00-00', 0),
(69, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 26, '0', '0000-00-00', 0),
(70, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 25, '0', '0000-00-00', 0),
(71, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 24, '0', '0000-00-00', 0),
(72, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 23, '0', '0000-00-00', 0),
(73, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 22, '0', '0000-00-00', 0),
(74, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 21, '-9.25', '0000-00-00', 0),
(75, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 20, '0', '0000-00-00', 0),
(76, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 19, '0', '0000-00-00', 0),
(77, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 18, '0', '0000-00-00', 0),
(78, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 17, '12.04', '0000-00-00', 0),
(79, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 16, '0', '0000-00-00', 0),
(80, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 15, '0', '0000-00-00', 0),
(81, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 14, '0', '0000-00-00', 0),
(82, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 13, '0', '0000-00-00', 0),
(83, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 12, '0', '0000-00-00', 0),
(84, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 11, '-14.75', '0000-00-00', 0),
(85, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 10, '0', '0000-00-00', 0),
(86, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 9, '2', '0000-00-00', 0),
(87, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 8, '0', '0000-00-00', 0),
(88, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 7, '0', '0000-00-00', 0),
(89, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 6, '0', '0000-00-00', 0),
(90, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 5, '0', '0000-00-00', 0),
(91, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 4, '0', '0000-00-00', 0),
(92, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 3, '0', '0000-00-00', 0),
(93, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 2, '0', '0000-00-00', 0),
(94, '2017-10-09 01:30:12 PM', '2017-10-24 07:31:37 PM', 1, '0', '0000-00-00', 0),
(95, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 75, '0.25', '0000-00-00', 0),
(96, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 74, '0', '0000-00-00', 0),
(97, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 73, '33.35', '0000-00-00', 0),
(98, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 72, '1.75', '0000-00-00', 0),
(99, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 71, '0.25', '0000-00-00', 0),
(100, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 70, '0', '0000-00-00', 0),
(101, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 69, '0', '0000-00-00', 0),
(102, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 68, '0', '0000-00-00', 0),
(103, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 67, '0.5', '0000-00-00', 0),
(104, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 66, '-4.5', '0000-00-00', 0),
(105, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 65, '0', '0000-00-00', 0),
(106, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 64, '0', '0000-00-00', 0),
(107, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 63, '0', '0000-00-00', 0),
(108, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 62, '0', '0000-00-00', 0),
(109, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 61, '0', '0000-00-00', 0),
(110, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 60, '0', '0000-00-00', 0),
(111, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 59, '0', '0000-00-00', 0),
(112, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 58, '0', '0000-00-00', 0),
(113, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 57, '0', '0000-00-00', 0),
(114, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 56, '0', '0000-00-00', 0),
(115, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 53, '4', '0000-00-00', 0),
(116, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 52, '0', '0000-00-00', 0),
(117, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 51, '0', '0000-00-00', 0),
(118, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 50, '0', '0000-00-00', 0),
(119, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 49, '0', '0000-00-00', 0),
(120, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 48, '0', '0000-00-00', 0),
(121, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 47, '0', '0000-00-00', 0),
(122, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 46, '0', '0000-00-00', 0),
(123, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 45, '0', '0000-00-00', 0),
(124, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 44, '0', '0000-00-00', 0),
(125, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 43, '0', '0000-00-00', 0),
(126, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 42, '0', '0000-00-00', 0),
(127, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 41, '0', '0000-00-00', 0),
(128, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 40, '0', '0000-00-00', 0),
(129, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 39, '0', '0000-00-00', 0),
(130, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 38, '0', '0000-00-00', 0),
(131, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 37, '0', '0000-00-00', 0),
(132, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 36, '0', '0000-00-00', 0),
(133, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 35, '0', '0000-00-00', 0),
(134, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 34, '4', '2017-10-27', 1),
(135, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 33, '22.59', '0000-00-00', 0),
(136, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 32, '0', '0000-00-00', 0),
(137, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 31, '0', '0000-00-00', 0),
(138, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 30, '0', '0000-00-00', 0),
(139, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 29, '0', '0000-00-00', 0),
(140, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 28, '0', '0000-00-00', 0),
(141, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 27, '0', '0000-00-00', 0),
(142, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 26, '0', '0000-00-00', 0),
(143, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 25, '0', '0000-00-00', 0),
(144, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 24, '0', '0000-00-00', 0),
(145, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 23, '0', '0000-00-00', 0),
(146, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 22, '0', '0000-00-00', 0),
(147, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 21, '0', '0000-00-00', 0),
(148, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 20, '0', '0000-00-00', 0),
(149, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 19, '0', '0000-00-00', 0),
(150, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 18, '0', '0000-00-00', 0),
(151, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 17, '0', '0000-00-00', 0),
(152, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 16, '0', '0000-00-00', 0),
(153, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 15, '0', '0000-00-00', 0),
(154, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 14, '0', '0000-00-00', 0),
(155, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 13, '0', '0000-00-00', 0),
(156, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 12, '0', '0000-00-00', 0),
(157, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 11, '6.14', '0000-00-00', 0),
(158, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 10, '0', '0000-00-00', 0),
(159, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 9, '0', '0000-00-00', 0),
(160, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 8, '0', '0000-00-00', 0),
(161, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 7, '0', '0000-00-00', 0),
(162, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 6, '0', '0000-00-00', 0),
(163, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 5, '0', '0000-00-00', 0),
(164, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 4, '0', '0000-00-00', 0),
(165, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 3, '0', '0000-00-00', 0),
(166, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 2, '0', '0000-00-00', 0),
(167, '2017-10-24 07:32:37 PM', '2017-10-27 05:31:06 PM', 1, '0', '0000-00-00', 0),
(168, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 81, '3.25', '0000-00-00', 0),
(169, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 80, '3.5', '0000-00-00', 0),
(170, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 79, '0', '0000-00-00', 0),
(171, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 78, '0', '0000-00-00', 0),
(172, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 77, '9.25', '0000-00-00', 0),
(173, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 76, '0.25', '0000-00-00', 0),
(174, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 75, '0', '0000-00-00', 0),
(175, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 74, '0', '0000-00-00', 0),
(176, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 73, '8', '0000-00-00', 0),
(177, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 72, '0.25', '0000-00-00', 0),
(178, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 71, '0', '0000-00-00', 0),
(179, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 70, '0', '0000-00-00', 0),
(180, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 69, '0', '0000-00-00', 0),
(181, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 68, '0', '0000-00-00', 0),
(182, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 67, '0', '0000-00-00', 0),
(183, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 66, '0', '0000-00-00', 0),
(184, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 65, '0', '0000-00-00', 0),
(185, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 64, '0', '0000-00-00', 0),
(186, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 63, '0', '0000-00-00', 0),
(187, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 62, '0', '0000-00-00', 0),
(188, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 61, '0', '0000-00-00', 0),
(189, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 60, '0', '0000-00-00', 0),
(190, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 59, '0', '0000-00-00', 0),
(191, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 58, '0', '0000-00-00', 0),
(192, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 57, '0', '0000-00-00', 0),
(193, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 56, '0', '0000-00-00', 0),
(194, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 53, '0', '0000-00-00', 0),
(195, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 52, '0', '0000-00-00', 0),
(196, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 51, '0', '0000-00-00', 0),
(197, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 50, '0', '0000-00-00', 0),
(198, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 49, '0', '0000-00-00', 0),
(199, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 48, '0', '0000-00-00', 0),
(200, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 47, '0', '0000-00-00', 0),
(201, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 46, '0', '0000-00-00', 0),
(202, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 45, '0', '0000-00-00', 0),
(203, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 44, '0', '0000-00-00', 0),
(204, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 43, '0', '0000-00-00', 0),
(205, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 42, '0', '0000-00-00', 0),
(206, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 41, '0', '0000-00-00', 0),
(207, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 40, '0', '0000-00-00', 0),
(208, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 39, '0', '0000-00-00', 0),
(209, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 38, '0', '0000-00-00', 0),
(210, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 37, '0', '0000-00-00', 0),
(211, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 36, '0', '0000-00-00', 0),
(212, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 35, '0', '0000-00-00', 0),
(213, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 34, '37.74', '0000-00-00', 0),
(214, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 33, '252.32', '0000-00-00', 0),
(215, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 32, '24', '0000-00-00', 0),
(216, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 31, '0', '0000-00-00', 0),
(217, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 30, '0', '0000-00-00', 0),
(218, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 29, '0', '0000-00-00', 0),
(219, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 28, '0', '0000-00-00', 0),
(220, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 27, '0', '0000-00-00', 0),
(221, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 26, '0', '0000-00-00', 0),
(222, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 25, '0', '0000-00-00', 0),
(223, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 24, '0', '0000-00-00', 0),
(224, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 23, '0', '0000-00-00', 0),
(225, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 22, '0', '0000-00-00', 0),
(226, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 21, '0', '0000-00-00', 0),
(227, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 20, '0', '0000-00-00', 0),
(228, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 19, '0', '0000-00-00', 0),
(229, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 18, '0', '0000-00-00', 0),
(230, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 17, '85.39', '0000-00-00', 0),
(231, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 16, '0', '0000-00-00', 0),
(232, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 15, '0', '0000-00-00', 0),
(233, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 14, '0', '0000-00-00', 0),
(234, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 13, '0', '0000-00-00', 0),
(235, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 12, '0', '0000-00-00', 0),
(236, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 11, '0', '0000-00-00', 0),
(237, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 10, '0', '0000-00-00', 0),
(238, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 9, '0', '0000-00-00', 0),
(239, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 8, '0', '0000-00-00', 0),
(240, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 7, '0', '0000-00-00', 0),
(241, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 6, '0', '0000-00-00', 0),
(242, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 5, '0', '0000-00-00', 0),
(243, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 4, '0', '0000-00-00', 0),
(244, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 3, '0', '0000-00-00', 0),
(245, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 2, '0', '0000-00-00', 0),
(246, '2017-10-27 05:32:06 PM', '2017-11-03 01:33:13 PM', 1, '-2525.75', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1505913397document_image_13.jpg', '2017-09-28', 1),
(2, 1, 2, 'uploads/driver/1505913417document_image_12.jpg', '2017-09-28', 1),
(3, 1, 4, 'uploads/driver/1505913427document_image_14.jpg', '2017-09-20', 1),
(4, 2, 3, 'uploads/driver/1506060662document_image_23.jpg', '2017-09-28', 1),
(5, 2, 2, 'uploads/driver/1506060671document_image_22.jpg', '2017-09-27', 1),
(6, 2, 4, 'uploads/driver/1506060682document_image_24.jpg', '2017-09-27', 1),
(7, 3, 3, 'uploads/driver/1506071468document_image_33.jpg', '2017-09-29', 1),
(8, 3, 2, 'uploads/driver/1506071476document_image_32.jpg', '2017-09-27', 1),
(9, 3, 4, 'uploads/driver/1506071483document_image_34.jpg', '2017-09-27', 1),
(10, 4, 3, 'uploads/driver/1506072566document_image_43.jpg', '2017-09-28', 1),
(11, 4, 2, 'uploads/driver/1506072574document_image_42.jpg', '2017-09-26', 1),
(12, 4, 4, 'uploads/driver/1506072583document_image_44.jpg', '2017-09-29', 1),
(13, 5, 3, 'uploads/driver/1506072832document_image_53.jpg', '2017-09-28', 1),
(14, 5, 2, 'uploads/driver/1506072839document_image_52.jpg', '2017-09-30', 1),
(15, 5, 4, 'uploads/driver/1506072845document_image_54.jpg', '2017-09-28', 1),
(16, 6, 1, 'uploads/driver/1506074504document_image_61.jpg', '2018-01-16', 1),
(17, 6, 2, 'uploads/driver/1506074525document_image_62.jpg', '2018-03-28', 1),
(18, 6, 3, 'uploads/driver/1506074543document_image_63.jpg', '2018-02-28', 1),
(19, 7, 1, 'uploads/driver/1506096259document_image_71.jpg', '2017-11-23', 1),
(20, 7, 2, 'uploads/driver/1506096278document_image_72.jpg', '2018-03-15', 1),
(21, 7, 3, 'uploads/driver/1506096296document_image_73.jpg', '2018-05-31', 1),
(22, 8, 3, 'uploads/driver/1506678402document_image_83.jpg', '2017-10-14', 1),
(23, 8, 2, 'uploads/driver/1506678420document_image_82.jpg', '2017-10-14', 1),
(24, 8, 4, 'uploads/driver/1506678429document_image_84.jpg', '2017-10-14', 1),
(25, 11, 3, 'uploads/driver/1506694857document_image_113.jpg', '2017-12-27', 1),
(26, 11, 2, 'uploads/driver/1506694875document_image_112.jpg', '2017-12-28', 1),
(27, 11, 1, 'uploads/driver/1506694893document_image_111.jpg', '2017-11-22', 1),
(28, 11, 4, 'uploads/driver/1506694910document_image_114.jpg', '2017-12-23', 1),
(29, 11, 6, 'uploads/driver/1506694924document_image_116.jpg', '2017-10-26', 1),
(30, 9, 6, 'uploads/driver/1507007051document_image_96.jpg', '2017-10-05', 1),
(31, 9, 5, 'uploads/driver/1507007060document_image_95.jpg', '2017-10-27', 1),
(32, 9, 4, 'uploads/driver/1507007072document_image_94.jpg', '2017-10-07', 1),
(33, 9, 3, 'uploads/driver/1507007081document_image_93.jpg', '2017-10-20', 1),
(34, 9, 2, 'uploads/driver/1507007110document_image_92.jpg', '2017-10-27', 1),
(35, 9, 1, 'uploads/driver/1507007131document_image_91.jpg', '2017-10-26', 1),
(36, 12, 6, 'uploads/driver/1507025277document_image_126.jpg', '2017-10-28', 1),
(37, 12, 5, 'uploads/driver/1507025290document_image_125.jpg', '2017-10-28', 1),
(38, 12, 4, 'uploads/driver/1507025303document_image_124.jpg', '2017-10-28', 1),
(39, 12, 3, 'uploads/driver/1507025318document_image_123.jpg', '2017-10-28', 1),
(40, 12, 2, 'uploads/driver/1507025325document_image_122.jpg', '2017-10-28', 1),
(41, 12, 1, 'uploads/driver/1507025332document_image_121.jpg', '2017-10-28', 1),
(42, 13, 6, 'uploads/driver/1507034325document_image_136.jpg', '2017-10-27', 1),
(43, 13, 5, 'uploads/driver/1507034335document_image_135.jpg', '2017-10-28', 1),
(44, 13, 4, 'uploads/driver/1507034342document_image_134.jpg', '2017-10-28', 1),
(45, 13, 3, 'uploads/driver/1507034350document_image_133.jpg', '2017-10-28', 1),
(46, 13, 2, 'uploads/driver/1507034360document_image_132.jpg', '2017-10-28', 1),
(47, 13, 1, 'uploads/driver/1507034370document_image_131.jpg', '2017-10-28', 1),
(48, 17, 6, 'uploads/driver/1507046577document_image_176.jpg', '2017-11-03', 1),
(49, 17, 4, 'uploads/driver/1507046595document_image_174.jpg', '2017-11-03', 1),
(50, 17, 3, 'uploads/driver/1507046618document_image_173.jpg', '2017-10-03', 1),
(51, 17, 2, 'uploads/driver/1507046664document_image_172.jpg', '2017-11-04', 1),
(52, 20, 6, 'uploads/driver/1507112998document_image_206.jpg', '4-10-2017', 1),
(53, 20, 5, 'uploads/driver/1507113006document_image_205.jpg', '4-10-2017', 1),
(54, 20, 4, 'uploads/driver/1507113083document_image_204.jpg', '4-10-2017', 1),
(55, 20, 2, 'uploads/driver/1507113205document_image_202.jpg', '4-10-2017', 1),
(56, 20, 3, 'uploads/driver/1507113221document_image_203.jpg', '4-10-2017', 1),
(57, 21, 6, 'uploads/driver/1507113787document_image_216.jpg', '4-10-2017', 1),
(58, 21, 5, 'uploads/driver/1507113795document_image_215.jpg', '15-10-2017', 1),
(59, 21, 4, 'uploads/driver/1507113806document_image_214.jpg', '23-10-2017', 1),
(60, 21, 3, 'uploads/driver/1507113817document_image_213.jpg', '4-10-2017', 1),
(61, 21, 2, 'uploads/driver/1507113826document_image_212.jpg', '4-10-2017', 1),
(62, 22, 6, 'uploads/driver/1507116799document_image_226.jpg', '4-10-2017', 1),
(63, 22, 5, 'uploads/driver/1507116806document_image_225.jpg', '4-10-2017', 1),
(64, 22, 4, 'uploads/driver/1507116814document_image_224.jpg', '4-10-2017', 1),
(65, 22, 3, 'uploads/driver/1507116821document_image_223.jpg', '4-10-2017', 1),
(66, 22, 2, 'uploads/driver/1507116829document_image_222.jpg', '4-10-2017', 1),
(67, 24, 6, 'uploads/driver/1507118249document_image_246.jpg', '4-10-2017', 1),
(68, 24, 5, 'uploads/driver/1507118278document_image_245.jpg', '4-10-2017', 1),
(69, 24, 4, 'uploads/driver/1507118313document_image_244.jpg', '4-10-2017', 1),
(70, 24, 3, 'uploads/driver/1507118379document_image_243.jpg', '4-10-2017', 1),
(71, 24, 2, 'uploads/driver/1507118388document_image_242.jpg', '9-10-2017', 1),
(72, 25, 6, 'uploads/driver/1507118983document_image_256.jpg', '4-10-2017', 1),
(73, 25, 5, 'uploads/driver/1507118996document_image_255.jpg', '4-10-2017', 1),
(74, 25, 4, 'uploads/driver/1507119007document_image_254.jpg', '4-10-2017', 1),
(75, 25, 3, 'uploads/driver/1507119016document_image_253.jpg', '21-10-2017', 1),
(76, 25, 2, 'uploads/driver/1507119025document_image_252.jpg', '21-10-2017', 1),
(77, 26, 6, 'uploads/driver/1507123608document_image_266.jpg', '4-10-2017', 1),
(78, 26, 5, 'uploads/driver/1507123624document_image_265.jpg', '4-10-2017', 1),
(79, 26, 4, 'uploads/driver/1507123641document_image_264.jpg', '4-10-2017', 1),
(80, 26, 3, 'uploads/driver/1507123650document_image_263.jpg', '4-10-2017', 1),
(81, 26, 2, 'uploads/driver/1507123658document_image_262.jpg', '4-10-2017', 1),
(82, 27, 6, 'uploads/driver/1507128050document_image_276.jpg', '4-10-2017', 1),
(83, 27, 5, 'uploads/driver/1507128058document_image_275.jpg', '4-10-2017', 1),
(84, 27, 4, 'uploads/driver/1507128065document_image_274.jpg', '4-10-2017', 1),
(85, 27, 2, 'uploads/driver/1507128073document_image_272.jpg', '4-10-2017', 1),
(86, 27, 3, 'uploads/driver/1507128087document_image_273.jpg', '4-10-2017', 1),
(87, 28, 6, 'uploads/driver/1507288028document_image_286.jpg', '6-10-2017', 1),
(88, 28, 5, 'uploads/driver/1507288039document_image_285.jpg', '21-10-2017', 1),
(89, 28, 4, 'uploads/driver/1507288049document_image_284.jpg', '6-10-2017', 1),
(90, 28, 3, 'uploads/driver/1507288074document_image_283.jpg', '6-10-2017', 1),
(91, 28, 2, 'uploads/driver/1507288089document_image_282.jpg', '6-10-2017', 1),
(92, 29, 6, 'uploads/driver/1507292171document_image_296.jpg', '6-10-2017', 1),
(93, 29, 5, 'uploads/driver/1507292182document_image_295.jpg', '6-10-2017', 1),
(94, 29, 4, 'uploads/driver/1507292204document_image_294.jpg', '6-10-2017', 1),
(95, 29, 3, 'uploads/driver/1507292238document_image_293.jpg', '6-10-2017', 1),
(96, 29, 2, 'uploads/driver/1507292270document_image_292.jpg', '6-10-2017', 1),
(97, 30, 6, 'uploads/driver/1507294673document_image_306.jpg', '15-10-2017', 1),
(98, 30, 5, 'uploads/driver/1507294682document_image_305.jpg', '6-10-2017', 1),
(99, 30, 4, 'uploads/driver/1507294692document_image_304.jpg', '6-10-2017', 1),
(100, 30, 3, 'uploads/driver/1507294702document_image_303.jpg', '28-10-2017', 1),
(101, 30, 2, 'uploads/driver/1507294714document_image_302.jpg', '6-10-2017', 1),
(102, 31, 6, 'uploads/driver/1507353567document_image_316.jpg', '7-10-2017', 1),
(103, 31, 5, 'uploads/driver/1507353577document_image_315.jpg', '7-10-2017', 1),
(104, 31, 4, 'uploads/driver/1507353585document_image_314.jpg', '7-10-2017', 1),
(105, 31, 3, 'uploads/driver/1507353592document_image_313.jpg', '7-10-2017', 1),
(106, 31, 2, 'uploads/driver/1507353600document_image_312.jpg', '7-10-2017', 1),
(107, 33, 1, 'uploads/driver/1507737692document_image_331.jpg', '29-10-2019', 1),
(108, 34, 1, 'uploads/driver/1507737816document_image_341.jpg', '15-3-2019', 1),
(109, 35, 6, 'uploads/driver/1507783199document_image_356.jpg', '12-10-2017', 1),
(110, 35, 5, 'uploads/driver/1507783206document_image_355.jpg', '12-10-2017', 1),
(111, 35, 4, 'uploads/driver/1507783215document_image_354.jpg', '12-10-2017', 1),
(112, 35, 3, 'uploads/driver/1507783222document_image_353.jpg', '12-10-2017', 1),
(113, 35, 2, 'uploads/driver/1507783228document_image_352.jpg', '12-10-2017', 1),
(114, 32, 1, 'uploads/driver/1507801924document_image_321.jpg', '26-10-2018', 1),
(115, 36, 6, 'uploads/driver/1507839196document_image_366.jpg', '12-10-2017', 1),
(116, 36, 5, 'uploads/driver/1507839227document_image_365.jpg', '26-10-2017', 1),
(117, 36, 4, 'uploads/driver/1507839247document_image_364.jpg', '26-10-2017', 1),
(118, 36, 3, 'uploads/driver/1507839269document_image_363.jpg', '31-10-2017', 1),
(119, 36, 2, 'uploads/driver/1507839290document_image_362.jpg', '31-10-2017', 1),
(120, 37, 6, 'uploads/driver/1507874468document_image_376.jpg', '2017-10-28', 1),
(121, 37, 5, 'uploads/driver/1507874481document_image_375.jpg', '2017-10-28', 1),
(122, 37, 4, 'uploads/driver/1507874491document_image_374.jpg', '2017-10-28', 1),
(123, 37, 3, 'uploads/driver/1507874505document_image_373.jpg', '2017-10-28', 1),
(124, 37, 2, 'uploads/driver/1507874523document_image_372.jpg', '2017-10-28', 1),
(125, 38, 6, 'uploads/driver/1507877357document_image_386.jpg', '2017-10-28', 1),
(126, 38, 5, 'uploads/driver/1507877367document_image_385.jpg', '2017-10-28', 1),
(127, 38, 4, 'uploads/driver/1507877374document_image_384.jpg', '2017-10-28', 1),
(128, 38, 3, 'uploads/driver/1507877383document_image_383.jpg', '2017-10-28', 1),
(129, 38, 2, 'uploads/driver/1507877393document_image_382.jpg', '2017-10-28', 1),
(130, 39, 6, 'uploads/driver/1507895425document_image_396.jpg', '13-10-2017', 1),
(131, 39, 5, 'uploads/driver/1507895446document_image_395.jpg', '13-10-2017', 1),
(132, 39, 4, 'uploads/driver/1507895456document_image_394.jpg', '13-10-2017', 1),
(133, 39, 3, 'uploads/driver/1507895465document_image_393.jpg', '13-10-2017', 1),
(134, 39, 2, 'uploads/driver/1507895477document_image_392.jpg', '13-10-2017', 1),
(135, 40, 6, 'uploads/driver/1507902568document_image_406.jpg', '13-10-2017', 1),
(136, 40, 5, 'uploads/driver/1507902575document_image_405.jpg', '13-10-2017', 1),
(137, 40, 4, 'uploads/driver/1507902582document_image_404.jpg', '13-10-2017', 1),
(138, 40, 3, 'uploads/driver/1507902589document_image_403.jpg', '13-10-2017', 1),
(139, 40, 2, 'uploads/driver/1507902599document_image_402.jpg', '22-10-2017', 1),
(140, 41, 1, 'uploads/driver/1507978584document_image_411.jpg', '28-10-2017', 1),
(141, 42, 1, 'uploads/driver/1508077896document_image_421.jpg', '15-10-2019', 1),
(142, 43, 6, 'uploads/driver/1508160065document_image_436.jpg', '16-10-2017', 1),
(143, 43, 3, 'uploads/driver/1508160077document_image_433.jpg', '4-11-2017', 1),
(144, 43, 5, 'uploads/driver/1508160088document_image_435.jpg', '16-10-2017', 1),
(145, 43, 4, 'uploads/driver/1508160095document_image_434.jpg', '16-10-2017', 1),
(146, 43, 2, 'uploads/driver/1508160102document_image_432.jpg', '28-10-2017', 1),
(147, 44, 6, 'uploads/driver/1508161432document_image_446.jpg', '28-10-2017', 1),
(148, 44, 5, 'uploads/driver/1508161442document_image_445.jpg', '16-10-2017', 1),
(149, 44, 4, 'uploads/driver/1508161452document_image_444.jpg', '16-10-2017', 1),
(150, 44, 3, 'uploads/driver/1508161462document_image_443.jpg', '16-10-2017', 1),
(151, 44, 2, 'uploads/driver/1508161472document_image_442.jpg', '28-10-2017', 1),
(152, 45, 1, '', '2018-03-15', 2),
(153, 46, 6, 'uploads/driver/1508737037document_image_466.jpg', '2017-10-23', 2),
(154, 46, 5, 'uploads/driver/1508737119document_image_465.jpg', '2017-10-23', 2),
(155, 46, 4, 'uploads/driver/1508737127document_image_464.jpg', '2017-10-23', 2),
(156, 46, 3, 'uploads/driver/1508737135document_image_463.jpg', '2017-10-27', 2),
(157, 46, 2, 'uploads/driver/1508737142document_image_462.jpg', '2017-10-23', 2),
(158, 47, 6, 'uploads/driver/1508737433document_image_476.jpg', '27-10-2017', 2),
(159, 47, 4, 'uploads/driver/1508737492document_image_474.jpg', '23-10-2017', 2),
(160, 47, 5, 'uploads/driver/1508737508document_image_475.jpg', '23-10-2017', 2),
(161, 47, 3, 'uploads/driver/1508737525document_image_473.jpg', '23-10-2017', 2),
(162, 47, 2, 'uploads/driver/1508737537document_image_472.jpg', '23-10-2017', 2),
(163, 48, 6, 'uploads/driver/1508751361document_image_486.jpg', '23-10-2017', 1),
(164, 48, 5, 'uploads/driver/1508751373document_image_485.jpg', '23-10-2017', 1),
(165, 48, 4, 'uploads/driver/1508751385document_image_484.jpg', '23-10-2017', 1),
(166, 48, 3, 'uploads/driver/1508751399document_image_483.jpg', '23-10-2017', 1),
(167, 48, 2, 'uploads/driver/1508751416document_image_482.jpg', '23-10-2017', 1),
(168, 49, 6, 'uploads/driver/1508751633document_image_496.jpg', '2017-10-28', 1),
(169, 49, 5, 'uploads/driver/1508751643document_image_495.jpg', '2017-10-28', 1),
(170, 49, 4, 'uploads/driver/1508751651document_image_494.jpg', '2017-10-23', 1),
(171, 49, 3, 'uploads/driver/1508751663document_image_493.jpg', '2017-10-28', 1),
(172, 49, 2, 'uploads/driver/1508751671document_image_492.jpg', '2017-10-28', 1),
(173, 53, 1, 'uploads/driver/1508782900document_image_531.jpg', '23-10-2019', 1),
(174, 54, 6, 'uploads/driver/150882880801503122273821.jpgdocument_image_54.jpg', '', 2),
(175, 54, 5, 'uploads/driver/150882880811503669504055.jpgdocument_image_54.jpg', '', 2),
(176, 54, 4, 'uploads/driver/150882880821500439809889.jpgdocument_image_54.jpg', '', 2),
(177, 54, 3, 'uploads/driver/150882880831504349969867.jpgdocument_image_54.jpg', '', 2),
(178, 54, 2, 'uploads/driver/150882880841503669504055.jpgdocument_image_54.jpg', '', 2),
(179, 55, 6, 'uploads/driver/150882895401504349924908.jpgdocument_image_55.jpg', '', 1),
(180, 55, 5, 'uploads/driver/150882895411503669504055.jpgdocument_image_55.jpg', '', 1),
(181, 55, 4, 'uploads/driver/150882895421500381908415.jpgdocument_image_55.jpg', '', 1),
(182, 55, 3, 'uploads/driver/150882895431504349969867.jpgdocument_image_55.jpg', '', 1),
(183, 55, 2, 'uploads/driver/150882895441503122292297.jpgdocument_image_55.jpg', '', 1),
(184, 56, 6, 'uploads/driver/1508841035document_image_566.jpg', '24-10-2017', 1),
(185, 56, 5, 'uploads/driver/1508841043document_image_565.jpg', '24-10-2017', 1),
(186, 56, 4, 'uploads/driver/1508841054document_image_564.jpg', '24-10-2017', 1),
(187, 56, 3, 'uploads/driver/1508841062document_image_563.jpg', '24-10-2017', 1),
(188, 56, 2, 'uploads/driver/1508841068document_image_562.jpg', '24-10-2017', 1),
(189, 57, 6, 'uploads/driver/150884668401503122292297.jpgdocument_image_57.jpg', '', 2),
(190, 57, 5, 'uploads/driver/150884668411503669504055.jpgdocument_image_57.jpg', '', 2),
(191, 57, 4, 'uploads/driver/150884668421500439838331.jpgdocument_image_57.jpg', '', 2),
(192, 57, 3, 'uploads/driver/150884668431504349924908.jpgdocument_image_57.jpg', '', 2),
(193, 57, 2, 'uploads/driver/150884668441500381272231.jpgdocument_image_57.jpg', '', 2),
(194, 58, 6, 'uploads/driver/1508848677document_image_586.jpg', '24-10-2017', 2),
(195, 59, 6, 'uploads/driver/1508848767document_image_596.jpg', '24-10-2017', 1),
(196, 59, 5, 'uploads/driver/1508848832document_image_595.jpg', '24-10-2017', 1),
(197, 59, 4, 'uploads/driver/1508848881document_image_594.jpg', '24-10-2017', 1),
(198, 59, 3, 'uploads/driver/1508848892document_image_593.jpg', '24-10-2017', 1),
(199, 59, 2, 'uploads/driver/1508848902document_image_592.jpg', '24-10-2017', 1),
(200, 60, 6, 'uploads/driver/1508850023document_image_606.jpg', '24-10-2017', 1),
(201, 60, 5, 'uploads/driver/1508850033document_image_605.jpg', '24-10-2017', 1),
(202, 60, 4, 'uploads/driver/1508850042document_image_604.jpg', '24-10-2017', 1),
(203, 60, 3, 'uploads/driver/1508850057document_image_603.jpg', '24-10-2017', 1),
(204, 60, 2, 'uploads/driver/1508850068document_image_602.jpg', '24-10-2017', 1),
(205, 61, 6, 'uploads/driver/1508850315document_image_616.jpg', '2017-10-28', 1),
(206, 61, 5, 'uploads/driver/1508850324document_image_615.jpg', '2017-10-28', 1),
(207, 61, 4, 'uploads/driver/1508850335document_image_614.jpg', '2017-10-28', 1),
(208, 61, 3, 'uploads/driver/1508850343document_image_613.jpg', '2017-10-24', 1),
(209, 61, 2, 'uploads/driver/1508850351document_image_612.jpg', '2017-10-28', 1),
(210, 62, 6, 'uploads/driver/1508851326document_image_626.jpg', '2017-10-28', 1),
(211, 62, 5, 'uploads/driver/1508851334document_image_625.jpg', '2017-10-28', 1),
(212, 62, 4, 'uploads/driver/1508851342document_image_624.jpg', '2017-10-28', 1),
(213, 62, 3, 'uploads/driver/1508851349document_image_623.jpg', '2017-10-28', 1),
(214, 62, 2, 'uploads/driver/1508851359document_image_622.jpg', '2017-10-28', 1),
(215, 64, 6, 'uploads/driver/1508852390document_image_646.jpg', '24-10-2017', 2),
(216, 64, 5, 'uploads/driver/1508852401document_image_645.jpg', '24-10-2017', 2),
(217, 64, 4, 'uploads/driver/1508852410document_image_644.jpg', '24-10-2017', 2),
(218, 64, 3, 'uploads/driver/1508852421document_image_643.jpg', '24-10-2017', 2),
(219, 64, 2, 'uploads/driver/1508852432document_image_642.jpg', '24-10-2017', 2),
(220, 65, 5, 'uploads/driver/1508867108document_image_655.jpg', '24-10-2017', 2),
(221, 65, 6, 'uploads/driver/1508867118document_image_656.jpg', '24-10-2017', 2),
(222, 65, 4, 'uploads/driver/1508867128document_image_654.jpg', '24-10-2017', 2),
(223, 65, 3, 'uploads/driver/1508867136document_image_653.jpg', '24-10-2017', 2),
(224, 65, 2, 'uploads/driver/1508867146document_image_652.jpg', '24-10-2017', 2),
(225, 66, 6, 'uploads/driver/1508908249document_image_666.jpg', '25-10-2017', 2),
(226, 66, 5, 'uploads/driver/1508908258document_image_665.jpg', '25-10-2017', 2),
(227, 66, 4, 'uploads/driver/1508908267document_image_664.jpg', '25-10-2017', 2),
(228, 66, 3, 'uploads/driver/1508908276document_image_663.jpg', '25-10-2017', 2),
(229, 66, 2, 'uploads/driver/1508908285document_image_662.jpg', '25-10-2017', 2),
(230, 67, 6, 'uploads/driver/1508918036document_image_676.jpg', '2017-10-28', 2),
(231, 67, 5, 'uploads/driver/1508918091document_image_675.jpg', '2017-10-28', 2),
(232, 67, 4, 'uploads/driver/1508918196document_image_674.jpg', '2017-10-28', 2),
(233, 67, 3, 'uploads/driver/1508918215document_image_673.jpg', '2017-10-28', 2),
(234, 67, 2, 'uploads/driver/1508918227document_image_672.jpg', '2017-10-28', 2),
(235, 68, 6, 'uploads/driver/1508918833document_image_686.jpg', '2017-10-28', 1),
(236, 68, 5, 'uploads/driver/1508918875document_image_685.jpg', '2017-10-28', 1),
(237, 68, 4, 'uploads/driver/1508919839document_image_684.jpg', '2017-10-28', 1),
(238, 68, 3, 'uploads/driver/1508919870document_image_683.jpg', '2017-10-28', 1),
(239, 68, 2, 'uploads/driver/1508920640document_image_682.jpg', '2017-10-28', 1),
(240, 69, 6, 'uploads/driver/1508921490document_image_696.jpg', '28-10-2017', 1),
(241, 69, 5, 'uploads/driver/1508921560document_image_695.jpg', '25-10-2017', 1),
(242, 71, 6, 'uploads/driver/1508921889document_image_716.jpg', '25-10-2017', 2),
(243, 71, 5, 'uploads/driver/1508921906document_image_715.jpg', '25-10-2017', 2),
(244, 71, 4, 'uploads/driver/1508921918document_image_714.jpg', '25-10-2017', 2),
(245, 71, 3, 'uploads/driver/1508922016document_image_713.jpg', '25-10-2017', 2),
(246, 71, 2, 'uploads/driver/1508922070document_image_712.jpg', '25-10-2017', 2),
(247, 72, 5, 'uploads/driver/1508939163document_image_725.jpg', '25-10-2017', 2),
(248, 72, 6, 'uploads/driver/1508939173document_image_726.jpg', '25-10-2017', 2),
(249, 72, 4, 'uploads/driver/1508939189document_image_724.jpg', '25-10-2017', 2),
(250, 72, 3, 'uploads/driver/1508939197document_image_723.jpg', '25-10-2017', 2),
(251, 72, 2, 'uploads/driver/1508939206document_image_722.jpg', '25-10-2017', 2),
(252, 73, 1, 'uploads/driver/1508955965document_image_731.jpg', '2017-12-08', 2),
(253, 75, 6, 'uploads/driver/1509013660document_image_756.jpg', '28-10-2017', 2),
(254, 75, 5, 'uploads/driver/1509013669document_image_755.jpg', '28-10-2017', 2),
(255, 75, 4, 'uploads/driver/1509013679document_image_754.jpg', '28-10-2017', 2),
(256, 75, 3, 'uploads/driver/1509013690document_image_753.jpg', '28-10-2017', 2),
(257, 75, 2, 'uploads/driver/1509013702document_image_752.jpg', '28-10-2017', 2),
(258, 76, 6, 'uploads/driver/1509197328document_image_766.jpg', '2017-11-25', 2),
(259, 76, 5, 'uploads/driver/1509197336document_image_765.jpg', '2017-11-18', 2),
(260, 76, 4, 'uploads/driver/1509197345document_image_764.jpg', '2017-11-25', 2),
(261, 76, 3, 'uploads/driver/1509197353document_image_763.jpg', '2017-11-25', 2),
(262, 76, 2, 'uploads/driver/1509197362document_image_762.jpg', '2017-11-25', 2),
(263, 77, 6, 'uploads/driver/1509348279document_image_776.jpg', '2018-01-30', 2),
(264, 77, 5, 'uploads/driver/1509348288document_image_775.jpg', '2018-01-30', 2),
(265, 77, 4, 'uploads/driver/1509348296document_image_774.jpg', '2017-12-26', 2),
(266, 77, 3, 'uploads/driver/1509348304document_image_773.jpg', '2017-12-26', 2),
(267, 77, 2, 'uploads/driver/1509348311document_image_772.jpg', '2017-12-26', 2),
(268, 78, 6, 'uploads/driver/1509349808document_image_786.jpg', '30-10-2017', 1),
(269, 78, 5, 'uploads/driver/1509349831document_image_785.jpg', '30-10-2017', 1),
(270, 78, 4, 'uploads/driver/1509349859document_image_784.jpg', '30-10-2017', 1),
(271, 79, 6, 'uploads/driver/1509354992document_image_796.jpg', '30-10-2017', 1),
(272, 79, 5, 'uploads/driver/1509355002document_image_795.jpg', '30-10-2017', 1),
(273, 79, 4, 'uploads/driver/1509355012document_image_794.jpg', '30-10-2017', 1),
(274, 79, 3, 'uploads/driver/1509355022document_image_793.jpg', '30-10-2017', 1),
(275, 79, 2, 'uploads/driver/1509355031document_image_792.jpg', '30-10-2017', 1),
(276, 80, 6, 'uploads/driver/1509355179document_image_806.jpg', '30-10-2017', 2),
(277, 80, 5, 'uploads/driver/1509355188document_image_805.jpg', '30-10-2017', 2),
(278, 80, 4, 'uploads/driver/1509355198document_image_804.jpg', '30-10-2017', 2),
(279, 80, 3, 'uploads/driver/1509355209document_image_803.jpg', '30-10-2017', 2),
(280, 80, 2, 'uploads/driver/1509355219document_image_802.jpg', '30-10-2017', 2),
(281, 81, 6, 'uploads/driver/1509359777document_image_816.jpg', '2017-11-11', 2),
(282, 81, 5, 'uploads/driver/1509359785document_image_815.jpg', '2017-11-24', 2),
(283, 81, 4, 'uploads/driver/1509359799document_image_814.jpg', '2017-11-24', 2),
(284, 81, 3, 'uploads/driver/1509359812document_image_813.jpg', '2017-11-23', 2),
(285, 81, 2, 'uploads/driver/1509359823document_image_812.jpg', '2017-11-24', 2),
(286, 82, 6, 'uploads/driver/1509787844document_image_826.jpg', '4-11-2017', 1),
(287, 82, 5, 'uploads/driver/1509787854document_image_825.jpg', '4-11-2017', 1),
(288, 82, 4, 'uploads/driver/1509787865document_image_824.jpg', '4-11-2017', 1),
(289, 82, 3, 'uploads/driver/1509787874document_image_823.jpg', '4-11-2017', 1),
(290, 82, 2, 'uploads/driver/1509787884document_image_822.jpg', '4-11-2017', 1),
(291, 83, 6, 'uploads/driver/1509788177document_image_836.jpg', '4-11-2017', 2),
(292, 83, 5, 'uploads/driver/1509788186document_image_835.jpg', '4-11-2017', 2),
(293, 83, 4, 'uploads/driver/1509788205document_image_834.jpg', '4-11-2017', 2),
(294, 83, 3, 'uploads/driver/1509788214document_image_833.jpg', '4-11-2017', 2),
(295, 83, 2, 'uploads/driver/1509788224document_image_832.jpg', '4-11-2017', 2),
(296, 84, 6, 'uploads/driver/1509954796document_image_846.jpg', '2017-11-30', 2),
(297, 84, 5, 'uploads/driver/1509954805document_image_845.jpg', '2017-11-30', 2),
(298, 84, 4, 'uploads/driver/1509954814document_image_844.jpg', '2017-11-30', 2),
(299, 84, 3, 'uploads/driver/1509954823document_image_843.jpg', '2017-11-30', 2),
(300, 84, 2, 'uploads/driver/1509954831document_image_842.jpg', '2017-11-30', 2),
(301, 85, 1, 'uploads/driver/1510394511document_image_851.jpg', '26-1-2018', 1),
(302, 86, 1, 'uploads/driver/1510394534document_image_861.jpg', '11-11-2020', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-09-20 14:17:28', '', '', 0, 0, '2017-09-20'),
(2, 2, '2017-09-22 07:42:33', '2017-09-22 07:42:29', '0 Hours 29 Minutes', 0, 29, '2017-09-22'),
(3, 3, '2017-09-22 10:12:42', '2017-09-22 10:12:25', '0 Hours 0 Minutes', 0, 0, '2017-09-22'),
(4, 4, '2017-09-22 10:29:55', '2017-09-22 10:32:48', '0 Hours 2 Minutes', 0, 2, '2017-09-22'),
(5, 5, '2017-09-22 10:34:14', '', '', 0, 0, '2017-09-22'),
(6, 6, '2017-09-22 15:21:34', '2017-09-23 07:32:21', '20 Hours 28 Minutes', 20, 28, '2017-09-22'),
(7, 7, '2017-09-22 17:09:57', '2017-10-01 09:01:47', '15 Hours 51 Minutes', 15, 51, '2017-09-22'),
(8, 6, '2017-09-23 08:33:42', '2017-09-24 17:36:17', '9 Hours 2 Minutes', 9, 2, '2017-09-23'),
(9, 6, '2017-09-24 17:36:20', '2017-09-28 19:21:52', '1 Hours 45 Minutes', 1, 45, '2017-09-24'),
(10, 6, '2017-09-28 19:22:42', '2017-09-28 19:22:38', '0 Hours 0 Minutes', 0, 0, '2017-09-28'),
(11, 8, '2017-09-29 10:47:16', '2017-09-29 12:56:55', '2 Hours 9 Minutes', 2, 9, '2017-09-29'),
(12, 11, '2017-09-29 20:10:11', '2017-10-01 19:34:45', '27 Hours 69 Minutes', 27, 69, '2017-09-29'),
(13, 11, '2017-10-02 12:27:03', '2017-10-25 15:07:13', '2 Hours 40 Minutes', 2, 40, '2017-10-02'),
(14, 9, '2017-10-03 14:54:34', '2017-10-04 07:51:09', '18 Hours 94 Minutes', 18, 94, '2017-10-03'),
(15, 12, '2017-10-03 11:08:59', '2017-10-03 11:12:09', '0 Hours 3 Minutes', 0, 3, '2017-10-03'),
(16, 13, '2017-10-03 13:39:39', '2017-10-03 13:54:22', '0 Hours 14 Minutes', 0, 14, '2017-10-03'),
(17, 17, '2017-10-03 17:25:31', '2017-10-12 14:36:52', '21 Hours 31 Minutes', 21, 31, '2017-10-03'),
(18, 22, '2017-10-04 15:11:25', '2017-10-04 15:11:22', '0 Hours 15 Minutes', 0, 15, '2017-10-04'),
(19, 27, '2017-10-04 15:41:37', '2017-10-06 14:31:30', '22 Hours 49 Minutes', 22, 49, '2017-10-04'),
(20, 26, '2017-10-05 06:24:34', '', '', 0, 0, '2017-10-05'),
(21, 28, '2017-10-06 12:32:05', '', '', 0, 0, '2017-10-06'),
(22, 30, '2017-10-06 14:36:01', '2017-10-13 07:42:32', '17 Hours 38 Minutes', 17, 38, '2017-10-06'),
(23, 27, '2017-10-06 14:32:31', '2017-10-06 14:33:43', '0 Hours 1 Minutes', 0, 1, '2017-10-06'),
(24, 31, '2017-10-07 06:20:12', '2017-10-10 12:21:43', '6 Hours 1 Minutes', 6, 1, '2017-10-07'),
(25, 9, '2017-10-09 10:35:55', '', '', 0, 0, '2017-10-09'),
(26, 32, '2017-10-11 16:58:31', '2017-10-11 17:05:03', '0 Hours 6 Minutes', 0, 6, '2017-10-11'),
(27, 33, '2017-10-11 17:01:45', '2017-10-12 16:15:32', '23 Hours 13 Minutes', 23, 13, '2017-10-11'),
(28, 34, '2017-10-11 17:26:11', '2017-10-11 17:29:14', '0 Hours 19 Minutes', 0, 19, '2017-10-11'),
(29, 31, '2017-10-12 05:28:45', '', '', 0, 0, '2017-10-11'),
(30, 35, '2017-10-12 05:40:36', '', '', 0, 0, '2017-10-11'),
(31, 34, '2017-10-12 20:14:15', '2017-10-20 20:49:01', '13 Hours 48 Minutes', 13, 48, '2017-10-12'),
(32, 32, '2017-10-12 11:17:37', '2017-10-12 20:42:59', '9 Hours 50 Minutes', 9, 50, '2017-10-12'),
(33, 17, '2017-10-12 17:52:22', '2017-10-13 11:38:57', '20 Hours 135 Minutes', 20, 135, '2017-10-12'),
(34, 33, '2017-10-12 16:15:35', '2017-10-13 09:28:39', '17 Hours 13 Minutes', 17, 13, '2017-10-12'),
(35, 37, '2017-10-13 07:46:38', '2017-10-13 07:45:04', '0 Hours 6 Minutes', 0, 6, '2017-10-13'),
(36, 30, '2017-10-13 07:44:47', '', '', 0, 0, '2017-10-13'),
(37, 38, '2017-10-13 07:52:38', '2017-10-16 10:24:53', '4 Hours 62 Minutes', 4, 62, '2017-10-13'),
(38, 33, '2017-10-13 18:38:51', '2017-10-18 12:02:06', '35 Hours 43 Minutes', 35, 43, '2017-10-13'),
(39, 39, '2017-10-13 12:51:33', '2017-10-17 08:48:20', '19 Hours 56 Minutes', 19, 56, '2017-10-13'),
(40, 17, '2017-10-13 15:47:35', '2017-10-13 15:43:48', '1 Hours 71 Minutes', 1, 71, '2017-10-13'),
(41, 40, '2017-10-13 14:50:08', '2017-10-17 06:50:29', '16 Hours 0 Minutes', 16, 0, '2017-10-13'),
(42, 32, '2017-10-13 18:37:45', '2017-10-14 11:24:22', '19 Hours 55 Minutes', 19, 55, '2017-10-13'),
(43, 41, '2017-10-14 11:57:34', '', '', 0, 0, '2017-10-14'),
(44, 17, '2017-10-14 21:54:48', '2017-10-20 10:56:36', '21 Hours 35 Minutes', 21, 35, '2017-10-14'),
(45, 42, '2017-10-15 15:31:56', '', '', 0, 0, '2017-10-15'),
(46, 38, '2017-10-16 14:49:20', '2017-10-17 07:17:45', '38 Hours 162 Minutes', 38, 162, '2017-10-16'),
(47, 43, '2017-10-16 14:21:52', '', '', 0, 0, '2017-10-16'),
(48, 44, '2017-10-16 15:01:41', '2017-10-16 16:53:48', '1 Hours 68 Minutes', 1, 68, '2017-10-16'),
(49, 40, '2017-10-17 06:50:33', '2017-10-17 07:00:44', '0 Hours 10 Minutes', 0, 10, '2017-10-17'),
(50, 38, '2017-10-17 09:01:37', '2017-10-23 06:24:48', '21 Hours 107 Minutes', 21, 107, '2017-10-17'),
(51, 33, '2017-10-18 12:02:09', '2017-10-23 18:01:52', '5 Hours 59 Minutes', 5, 59, '2017-10-18'),
(52, 45, '2017-10-20 10:58:16', '2017-10-21 09:17:12', '22 Hours 18 Minutes', 22, 18, '2017-10-20'),
(53, 34, '2017-10-20 20:53:16', '2017-10-23 20:00:40', '23 Hours 7 Minutes', 23, 7, '2017-10-20'),
(54, 17, '2017-10-21 09:18:32', '', '', 0, 0, '2017-10-21'),
(55, 29, '2017-10-23 05:47:41', '', '', 0, 0, '2017-10-22'),
(56, 46, '2017-10-23 11:23:07', '2017-10-23 14:39:11', '9 Hours 99 Minutes', 9, 99, '2017-10-23'),
(57, 47, '2017-10-23 06:56:54', '2017-10-23 06:55:32', '0 Hours 9 Minutes', 0, 9, '2017-10-23'),
(58, 48, '2017-10-23 10:37:06', '', '', 0, 0, '2017-10-23'),
(59, 49, '2017-10-23 10:41:40', '2017-10-23 10:47:22', '0 Hours 5 Minutes', 0, 5, '2017-10-23'),
(60, 45, '2017-10-23 15:56:34', '', '', 0, 0, '2017-10-23'),
(61, 33, '2017-10-23 20:50:25', '2017-10-25 15:27:15', '20 Hours 83 Minutes', 20, 83, '2017-10-23'),
(62, 53, '2017-10-23 19:22:05', '', '', 0, 0, '2017-10-23'),
(63, 32, '2017-10-23 19:32:39', '', '', 0, 0, '2017-10-23'),
(64, 34, '2017-10-23 20:01:33', '2017-10-24 07:37:47', '11 Hours 36 Minutes', 11, 36, '2017-10-23'),
(65, 37, '2017-10-24 11:07:42', '2017-10-24 13:18:15', '2 Hours 10 Minutes', 2, 10, '2017-10-24'),
(66, 21, '2017-10-24 11:35:49', '', '', 0, 0, '2017-10-24'),
(67, 27, '2017-10-24 12:59:50', '2017-10-24 13:58:55', '0 Hours 59 Minutes', 0, 59, '2017-10-24'),
(68, 49, '2017-10-24 13:20:22', '', '', 0, 0, '2017-10-24'),
(69, 49, '2017-10-24 13:20:22', '2017-10-24 14:02:01', '0 Hours 41 Minutes', 0, 41, '2017-10-24'),
(70, 60, '2017-10-24 14:05:35', '2017-10-24 14:37:22', '0 Hours 31 Minutes', 0, 31, '2017-10-24'),
(71, 34, '2017-10-24 14:37:51', '', '', 0, 0, '2017-10-24'),
(72, 62, '2017-10-24 14:53:22', '2017-10-25 08:50:50', '17 Hours 57 Minutes', 17, 57, '2017-10-24'),
(73, 52, '2017-10-24 18:43:57', '', '', 0, 0, '2017-10-24'),
(74, 65, '2017-10-24 18:48:31', '', '', 0, 0, '2017-10-24'),
(75, 66, '2017-10-25 06:13:36', '2017-10-25 08:09:35', '1 Hours 55 Minutes', 1, 55, '2017-10-25'),
(76, 64, '2017-10-25 08:07:01', '2017-10-25 08:20:35', '0 Hours 13 Minutes', 0, 13, '2017-10-25'),
(77, 67, '2017-10-25 11:56:55', '2017-10-28 14:26:54', '2 Hours 88 Minutes', 2, 88, '2017-10-25'),
(78, 71, '2017-10-25 10:02:24', '', '', 0, 0, '2017-10-25'),
(79, 72, '2017-10-25 14:49:21', '2017-10-28 19:30:14', '4 Hours 40 Minutes', 4, 40, '2017-10-25'),
(80, 11, '2017-10-25 15:07:17', '2017-10-25 19:22:56', '4 Hours 15 Minutes', 4, 15, '2017-10-25'),
(81, 33, '2017-10-25 15:27:20', '2017-10-28 08:43:15', '17 Hours 15 Minutes', 17, 15, '2017-10-25'),
(82, 32, '2017-10-25 16:06:10', '2017-10-28 14:58:52', '22 Hours 52 Minutes', 22, 52, '2017-10-25'),
(83, 73, '2017-10-25 19:51:14', '2017-10-27 15:23:06', '19 Hours 45 Minutes', 19, 45, '2017-10-25'),
(84, 75, '2017-10-26 11:32:11', '', '', 0, 0, '2017-10-26'),
(85, 34, '2017-10-26 20:18:31', '2017-10-29 16:05:43', '19 Hours 47 Minutes', 19, 47, '2017-10-26'),
(86, 73, '2017-10-27 16:41:18', '2017-11-06 14:12:50', '26 Hours 61 Minutes', 26, 61, '2017-10-27'),
(87, 33, '2017-10-28 08:43:23', '2017-10-30 15:42:06', '6 Hours 58 Minutes', 6, 58, '2017-10-28'),
(88, 76, '2017-10-28 14:31:56', '', '', 0, 0, '2017-10-28'),
(89, 32, '2017-10-28 14:58:58', '', '', 0, 0, '2017-10-28'),
(90, 72, '2017-10-28 19:30:17', '2017-10-28 19:30:20', '0 Hours 0 Minutes', 0, 0, '2017-10-28'),
(91, 17, '2017-10-28 20:58:25', '2017-10-30 10:44:04', '13 Hours 45 Minutes', 13, 45, '2017-10-28'),
(92, 34, '2017-10-29 16:06:11', '', '', 0, 0, '2017-10-29'),
(93, 77, '2017-10-30 07:28:14', '2017-11-06 07:42:48', '0 Hours 15 Minutes', 0, 15, '2017-10-30'),
(94, 78, '2017-10-30 08:04:51', '', '', 0, 0, '2017-10-30'),
(95, 80, '2017-10-30 19:39:51', '2017-10-31 16:19:16', '30 Hours 51 Minutes', 30, 51, '2017-10-30'),
(96, 81, '2017-10-30 10:39:45', '2017-11-06 07:42:08', '21 Hours 2 Minutes', 21, 2, '2017-10-30'),
(97, 17, '2017-10-30 13:30:18', '2017-10-30 13:29:06', '2 Hours 44 Minutes', 2, 44, '2017-10-30'),
(98, 33, '2017-10-30 15:42:09', '2017-10-31 15:38:54', '23 Hours 56 Minutes', 23, 56, '2017-10-30'),
(99, 32, '2017-10-31 14:40:04', '2017-11-11 10:06:09', '19 Hours 26 Minutes', 19, 26, '2017-10-31'),
(100, 33, '2017-10-31 15:41:09', '2017-11-06 06:28:36', '14 Hours 49 Minutes', 14, 49, '2017-10-31'),
(101, 80, '2017-10-31 16:28:54', '2017-10-31 16:28:51', '0 Hours 8 Minutes', 0, 8, '2017-10-31'),
(102, 34, '2017-11-01 16:42:58', '2017-11-11 18:54:27', '11 Hours 78 Minutes', 11, 78, '2017-11-01'),
(103, 17, '2017-11-01 07:39:47', '2017-11-06 09:09:14', '1 Hours 29 Minutes', 1, 29, '2017-11-01'),
(104, 83, '2017-11-04 10:28:42', '2017-11-06 07:40:06', '21 Hours 61 Minutes', 21, 61, '2017-11-04'),
(105, 33, '2017-11-06 06:28:40', '2017-11-07 09:13:11', '2 Hours 44 Minutes', 2, 44, '2017-11-06'),
(106, 77, '2017-11-06 07:49:51', '2017-11-06 07:51:35', '0 Hours 1 Minutes', 0, 1, '2017-11-06'),
(107, 84, '2017-11-06 08:54:15', '2017-11-06 07:56:52', '0 Hours 0 Minutes', 0, 0, '2017-11-06'),
(108, 83, '2017-11-06 08:53:15', '2017-11-06 08:53:12', '0 Hours 4 Minutes', 0, 4, '2017-11-06'),
(109, 17, '2017-11-06 09:11:06', '2017-11-06 09:10:21', '0 Hours 1 Minutes', 0, 1, '2017-11-06'),
(110, 73, '2017-11-06 14:12:57', '2017-11-13 06:49:38', '16 Hours 36 Minutes', 16, 36, '2017-11-06'),
(111, 33, '2017-11-07 09:30:25', '2017-11-09 04:27:41', '18 Hours 73 Minutes', 18, 73, '2017-11-07'),
(112, 33, '2017-11-09 04:27:50', '2017-11-11 11:28:14', '7 Hours 0 Minutes', 7, 0, '2017-11-08'),
(113, 86, '2017-11-11 10:11:15', '2017-11-11 10:11:09', '0 Hours 7 Minutes', 0, 7, '2017-11-11'),
(114, 87, '2017-11-11 10:04:51', '', '', 0, 0, '2017-11-11'),
(115, 85, '2017-11-11 10:04:57', '2017-11-11 10:05:33', '0 Hours 0 Minutes', 0, 0, '2017-11-11'),
(116, 33, '2017-11-11 11:28:17', '', '', 0, 0, '2017-11-11'),
(117, 34, '2017-11-11 18:54:35', '2017-11-13 06:39:31', '11 Hours 44 Minutes', 11, 44, '2017-11-11'),
(118, 34, '2017-11-13 06:40:14', '', '', 0, 0, '2017-11-13'),
(119, 73, '2017-11-13 06:49:55', '2017-11-13 06:49:46', '0 Hours 0 Minutes', 0, 0, '2017-11-13'),
(120, 32, '2017-11-28 10:54:11', '', '', 0, 0, '2017-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(39, 'French', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 2),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login successfully'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Required fields missing'),
(7, 4, 'en', 'Email or Password incorrect'),
(9, 5, 'en', 'Logout successfully'),
(11, 6, 'en', 'No record found'),
(13, 7, 'en', 'Signup succesfully'),
(15, 8, 'en', 'Phone number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'RC copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password changed successfully'),
(27, 14, 'en', 'Old password is not matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon applied successfully'),
(33, 17, 'en', 'User does not exist'),
(35, 18, 'en', 'Updated successfully'),
(37, 19, 'en', 'Phone number already exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'OTP sent to phone for verification'),
(45, 23, 'en', 'Rating was successful'),
(47, 24, 'en', 'Email sent successfully'),
(49, 25, 'en', 'Booking accepted'),
(51, 26, 'en', 'Driver has arrived'),
(53, 27, 'en', 'Ride cancelled successfully'),
(55, 28, 'en', 'Ride ended'),
(57, 29, 'en', 'Ride booked successfully'),
(59, 30, 'en', 'Ride rejected'),
(61, 31, 'en', 'Ride has started'),
(63, 32, 'en', 'New ride allocated'),
(65, 33, 'en', 'Ride cancelled by customer'),
(67, 34, 'en', 'Booking accepted'),
(69, 35, 'en', 'Booking rejected'),
(71, 36, 'en', 'Booking cancelled by driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(5, 1313, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(6, 1312, 323, 4.5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(7, 1320, 7, 3, '', 232, 4, '', '0000-00-00'),
(8, 1321, 7, 4.5, '', 232, 4, '', '0000-00-00'),
(9, 1322, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(10, 1319, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(11, 1327, 7, 4, '', 232, 0, '', '0000-00-00'),
(12, 1328, 7, 4.5, '', 232, 0, '', '0000-00-00'),
(13, 1332, 7, 4.5, '', 212, 5, 'Static comment', '0000-00-00'),
(14, 1334, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(15, 1338, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(16, 1343, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(17, 1341, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(18, 1346, 323, 4.5, '', 212, 5, 'Static comment', '0000-00-00'),
(19, 1349, 7, 5, '', 232, 4.5, '', '0000-00-00'),
(20, 1350, 7, 4.5, '', 232, 4, '', '0000-00-00'),
(21, 1351, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(22, 1348, 323, 4.5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(23, 1353, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(24, 1356, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(25, 1357, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(26, 1358, 419, 0, '', 282, 5, 'Static comment', '0000-00-00'),
(27, 1361, 323, 0, '', 212, 5, 'Static comment', '0000-00-00'),
(28, 1362, 61, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(29, 1363, 61, 5, '', 258, 4.5, 'Static comment', '0000-00-00'),
(30, 1366, 61, 4.5, '', 258, 4.5, 'Static comment', '0000-00-00'),
(31, 1368, 420, 4.5, '', 282, 5, 'Static comment', '0000-00-00'),
(32, 1367, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(33, 1380, 421, 4.5, '', 282, 5, 'Static comment', '0000-00-00'),
(34, 1386, 292, 5, '', 238, 0, '', '0000-00-00'),
(35, 1387, 61, 5, '', 258, 5, 'Static comment', '0000-00-00'),
(36, 1391, 424, 5, '', 254, 4, 'Static comment', '0000-00-00'),
(37, 1392, 424, 0, '', 254, 4, 'Static comment', '0000-00-00'),
(38, 1393, 424, 5, '', 254, 4.5, 'Static comment', '0000-00-00'),
(39, 1395, 424, 5, '', 254, 4.5, 'Static comment', '0000-00-00'),
(40, 1396, 424, 5, '', 254, 5, 'Static comment', '0000-00-00'),
(41, 1397, 424, 4.5, '', 254, 4.5, 'Static comment', '0000-00-00'),
(42, 1390, 7, 5, '', 232, 5, '', '0000-00-00'),
(43, 1403, 7, 4.5, '', 232, 4.5, '', '0000-00-00'),
(44, 1405, 7, 4.5, '', 232, 5, '', '0000-00-00'),
(45, 1404, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(46, 1410, 7, 4.5, '', 232, 4, '', '0000-00-00'),
(47, 1411, 7, 5, '', 232, 5, '', '0000-00-00'),
(48, 1415, 7, 3.5, '', 232, 4.5, '', '2017-08-26'),
(49, 1416, 7, 4.5, '', 232, 5, '', '2017-08-26'),
(50, 1417, 421, 5, '', 282, 5, 'Static comment', '2017-08-26'),
(51, 1419, 7, 5, '', 232, 4.5, '', '2017-08-26'),
(52, 1421, 7, 0, '', 232, 4.5, '', '2017-08-26'),
(53, 1422, 7, 0, '', 232, 4.5, '', '2017-08-26'),
(54, 1423, 323, 5, 'hshdhisjdidj', 212, 4.5, 'Static comment', '2017-08-26'),
(55, 1425, 323, 4.5, 'hshshhdbbx', 212, 5, 'Static comment', '2017-08-26'),
(56, 1427, 323, 4, 'yyujjj', 212, 5, 'Static commentffgvvc', '2017-08-26'),
(57, 1428, 7, 0, '', 232, 4.5, '', '2017-08-26'),
(58, 1429, 7, 0, '', 232, 4.5, 'dhxhgsfahxcnxgfuvjbk k ', '2017-08-26'),
(59, 1430, 7, 5, 'good work', 232, 4, '', '2017-08-26'),
(60, 1431, 7, 4, 'geerr', 232, 5, 'dhhchccj', '2017-08-26'),
(61, 1433, 425, 5, '', 287, 5, 'Static comment', '2017-08-26'),
(62, 1437, 61, 5, '', 212, 5, 'dhdhd', '2017-08-26'),
(63, 1440, 61, 1.5, '', 212, 5, '', '2017-08-26'),
(64, 1441, 426, 2, '', 286, 4, 'Static comment', '2017-08-26'),
(65, 1451, 427, 5, '', 292, 5, 'Static comment', '2017-08-27'),
(66, 1454, 427, 5, 'hjiiiioi', 292, 5, 'Static comment', '2017-08-27'),
(67, 1463, 429, 4, '', 294, 5, '', '2017-08-27'),
(68, 1464, 429, 5, 'nice ride', 294, 5, 'good customer', '2017-08-27'),
(69, 1465, 429, 5, '', 294, 5, '', '2017-08-27'),
(70, 1466, 429, 3.5, '', 294, 5, '', '2017-08-27'),
(71, 1468, 429, 4.5, '', 294, 5, '', '2017-08-27'),
(72, 1469, 7, 5, '', 294, 5, '', '2017-08-27'),
(73, 1473, 430, 3.5, '', 286, 4.5, 'Static comment', '2017-08-27'),
(74, 1474, 432, 5, 'hdjr', 286, 4, 'Static comment', '2017-08-27'),
(75, 1487, 191, 5, '', 296, 4.5, 'Static comment', '2017-08-28'),
(76, 1488, 429, 5, '', 294, 5, '', '2017-08-28'),
(77, 1489, 429, 5, '', 294, 5, '', '2017-08-28'),
(78, 1490, 7, 5, '', 232, 5, 'Stdydyuf', '2017-08-28'),
(79, 1491, 7, 4, '', 232, 4, '', '2017-08-28'),
(80, 1492, 7, 5, '', 302, 4, '', '2017-08-28'),
(81, 1493, 435, 4.5, '', 303, 4, 'Hi', '2017-08-28'),
(82, 1494, 435, 4.5, '', 303, 4, 'H', '2017-08-28'),
(83, 1501, 435, 4, '', 303, 3.5, '', '2017-08-28'),
(84, 1495, 323, 5, '', 212, 5, '', '2017-08-28'),
(85, 1502, 323, 5, '', 212, 5, '', '2017-08-28'),
(86, 1506, 435, 3.5, '', 303, 4, '', '2017-08-28'),
(87, 1508, 435, 4, '', 303, 4, '', '2017-08-28'),
(88, 1509, 435, 4, '', 303, 4.5, '', '2017-08-28'),
(89, 1510, 435, 5, '', 303, 4, '', '2017-08-28'),
(90, 1507, 323, 4.5, '', 212, 5, 'xhchxhxh', '2017-08-28'),
(91, 1512, 323, 5, '', 212, 5, '', '2017-08-28'),
(92, 1513, 323, 0, '', 212, 5, '', '2017-08-28'),
(93, 1514, 323, 0, '', 212, 4.5, '', '2017-08-28'),
(94, 1515, 323, 5, '', 212, 0, '', '2017-08-28'),
(95, 1516, 323, 5, '', 212, 0, '', '2017-08-28'),
(96, 1517, 323, 5, '', 212, 5, '', '2017-08-28'),
(97, 1518, 323, 5, '', 212, 5, '', '2017-08-28'),
(98, 1519, 323, 4.5, '', 212, 5, '', '2017-08-28'),
(99, 1520, 323, 5, '', 212, 5, '', '2017-08-28'),
(100, 1521, 323, 5, '', 212, 5, '', '2017-08-28'),
(101, 1522, 323, 4.5, '', 212, 4.5, '', '2017-08-28'),
(102, 1523, 323, 4.5, '', 212, 4.5, '', '2017-08-28'),
(103, 1524, 435, 4, '', 303, 4, '', '2017-08-28'),
(104, 1525, 323, 5, '', 212, 4.5, '', '2017-08-28'),
(105, 1526, 323, 4.5, '', 212, 5, '', '2017-08-28'),
(106, 1527, 435, 4, '', 232, 5, '', '2017-08-28'),
(107, 1529, 435, 4, '', 232, 4, '', '2017-08-28'),
(108, 1528, 323, 5, '', 212, 4.5, '', '2017-08-28'),
(109, 1530, 409, 5, '', 154, 5, 'Static comment', '2017-08-28'),
(110, 1531, 61, 2.5, '', 306, 3.5, '', '2017-08-28'),
(111, 1532, 61, 2.5, '', 306, 2.5, '', '2017-08-28'),
(112, 1536, 61, 5, '', 306, 4.5, '', '2017-08-28'),
(113, 1470, 359, 3.5, '', 241, 0, '', '2017-08-28'),
(114, 1538, 441, 4, '', 286, 0, '', '2017-08-28'),
(115, 1539, 442, 3.5, '', 247, 3, 'Static comment', '2017-08-29'),
(116, 1540, 442, 0, '', 247, 2.5, 'Static comment', '2017-08-29'),
(117, 1541, 442, 0, '', 247, 2.5, 'Static comment', '2017-08-29'),
(118, 1535, 244, 5, 'great!', 169, 5, 'ok', '2017-08-29'),
(119, 1547, 244, 5, '', 169, 1.5, '', '2017-08-29'),
(120, 1548, 244, 3.5, '', 169, 5, '', '2017-08-29'),
(121, 1549, 443, 5, '', 306, 5, '', '2017-08-29'),
(122, 1551, 61, 5, '', 306, 4.5, '', '2017-08-29'),
(123, 1553, 61, 5, '', 306, 5, '', '2017-08-29'),
(124, 1554, 61, 4.5, '', 306, 5, '', '2017-08-29'),
(125, 1197, 360, 0, '', 140, 4, '', '2017-08-29'),
(126, 1555, 360, 5, '', 140, 5, '', '2017-08-29'),
(127, 1559, 447, 1, '', 311, 0, '', '2017-08-29'),
(128, 1560, 447, 0, '', 311, 3, '', '2017-08-29'),
(129, 1569, 450, 5, '', 282, 5, 'nice', '2017-08-29'),
(130, 1572, 411, 0, '', 315, 4, '', '2017-08-29'),
(131, 1574, 411, 0, '', 315, 4, '', '2017-08-29'),
(132, 1579, 454, 0, '', 316, 5, '', '2017-08-29'),
(133, 1584, 150, 5, '', 318, 0, '', '2017-08-30'),
(134, 1596, 61, 1.5, '', 95, 5, '', '2017-08-30'),
(135, 1597, 244, 5, '', 169, 5, '', '2017-08-30'),
(136, 1599, 150, 0, '', 318, 5, '', '2017-08-30'),
(137, 1600, 150, 4.5, '', 318, 5, '', '2017-08-30'),
(138, 1601, 409, 5, '', 154, 5, '', '2017-08-30'),
(139, 1602, 411, 5, '', 321, 4, '', '2017-08-30'),
(140, 1615, 411, 4, '', 321, 0, '', '2017-08-30'),
(141, 1635, 435, 4.5, '', 232, 4.5, '', '2017-08-30'),
(142, 1639, 435, 4, '', 232, 4.5, '', '2017-08-30'),
(143, 1640, 435, 4, '', 232, 5, '', '2017-08-30'),
(144, 1645, 435, 4, '', 232, 5, '', '2017-08-30'),
(145, 1647, 435, 4, '', 232, 4.5, '', '2017-08-30'),
(146, 1649, 462, 3.5, '', 325, 0, '', '2017-08-30'),
(147, 1651, 435, 5, '', 232, 5, '', '2017-08-30'),
(148, 1656, 411, 0, '', 325, 4, '', '2017-08-30'),
(149, 1660, 411, 0, '', 325, 4, '', '2017-08-30'),
(150, 1671, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(151, 1672, 435, 5, '', 232, 5, '', '2017-08-31'),
(152, 1675, 323, 5, '', 212, 5, '', '2017-08-31'),
(153, 1677, 435, 4, '', 232, 3.5, '', '2017-08-31'),
(154, 1678, 435, 5, '', 232, 5, '', '2017-08-31'),
(155, 1676, 465, 4.5, '', 212, 4.5, '', '2017-08-31'),
(156, 1681, 465, 5, '', 212, 5, '', '2017-08-31'),
(157, 1683, 465, 4, '', 212, 5, '', '2017-08-31'),
(158, 1682, 435, 5, '', 232, 4, '', '2017-08-31'),
(159, 1684, 465, 4, '', 212, 5, '', '2017-08-31'),
(160, 1685, 465, 5, '', 212, 5, '', '2017-08-31'),
(161, 1686, 435, 5, '', 232, 4.5, '', '2017-08-31'),
(162, 1687, 435, 4, '', 232, 5, '', '2017-08-31'),
(163, 1689, 465, 4.5, '', 212, 5, '', '2017-08-31'),
(164, 1691, 465, 5, '', 212, 5, '', '2017-08-31'),
(165, 1688, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(166, 1693, 435, 5, '', 232, 5, '', '2017-08-31'),
(167, 1694, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(168, 1695, 435, 3, '', 232, 5, '', '2017-08-31'),
(169, 1697, 465, 5, '', 212, 4.5, '', '2017-08-31'),
(170, 1696, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(171, 1700, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(172, 1699, 323, 5, '', 212, 5, '', '2017-08-31'),
(173, 1701, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(174, 1702, 323, 5, 'n7b7hh', 212, 5, '', '2017-08-31'),
(175, 1703, 435, 5, '', 232, 4.5, '', '2017-08-31'),
(176, 1704, 323, 5, '', 212, 4.5, '', '2017-08-31'),
(177, 1706, 323, 5, '', 212, 4.5, '', '2017-08-31'),
(178, 1705, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(179, 1707, 435, 4.5, '', 232, 4, '', '2017-08-31'),
(180, 1708, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(181, 1709, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(182, 1710, 435, 5, '', 232, 5, '', '2017-08-31'),
(183, 1711, 435, 4, '', 232, 3.5, '', '2017-08-31'),
(184, 1712, 435, 4, '', 232, 4, '', '2017-08-31'),
(185, 1713, 435, 0, '', 232, 5, '', '2017-08-31'),
(186, 1714, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(187, 1715, 435, 4, '', 232, 4, '', '2017-08-31'),
(188, 1716, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(189, 1718, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(190, 1719, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(191, 1723, 435, 4.5, '', 232, 4, '', '2017-08-31'),
(192, 1724, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(193, 1722, 411, 0, '', 325, 4, 'buhvug', '2017-08-31'),
(194, 1725, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(195, 1726, 435, 3.5, '', 232, 4.5, '', '2017-08-31'),
(196, 1727, 323, 5, '', 212, 5, '', '2017-08-31'),
(197, 1728, 435, 4, '', 232, 5, '', '2017-08-31'),
(198, 1729, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(199, 1730, 411, 0, '', 329, 4, '', '2017-08-31'),
(200, 1731, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(201, 1738, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(202, 1740, 413, 0, '', 250, 5, '', '2017-08-31'),
(203, 1741, 468, 5, '', 212, 3.5, '', '2017-08-31'),
(204, 1742, 468, 5, '', 212, 4, '', '2017-08-31'),
(205, 1743, 413, 5, '', 250, 5, '', '2017-09-01'),
(206, 1747, 471, 4.5, '', 331, 2, '', '2017-09-01'),
(207, 1748, 471, 0, '', 331, 4.5, '', '2017-09-01'),
(208, 1750, 464, 5, 'great', 169, 0, '', '2017-09-01'),
(209, 1751, 435, 4, '', 232, 5, '', '2017-09-01'),
(210, 1754, 323, 5, '', 212, 5, '', '2017-09-01'),
(211, 1756, 323, 5, '', 212, 5, '', '2017-09-01'),
(212, 1758, 468, 5, '', 212, 5, '', '2017-09-01'),
(213, 1759, 468, 5, '', 212, 4.5, '', '2017-09-01'),
(214, 1760, 468, 5, '', 212, 5, '', '2017-09-01'),
(215, 1761, 61, 5, '', 294, 5, '', '2017-09-01'),
(216, 1762, 468, 4.5, '', 212, 4.5, '', '2017-09-01'),
(217, 1763, 468, 5, '', 212, 5, '', '2017-09-01'),
(218, 1765, 468, 1, '', 212, 5, '', '2017-09-01'),
(219, 1766, 468, 0, '', 212, 5, '', '2017-09-01'),
(220, 1767, 468, 3, '', 212, 5, '', '2017-09-01'),
(221, 1768, 468, 1.5, '', 212, 5, '', '2017-09-01'),
(222, 1769, 468, 0, '', 212, 5, '', '2017-09-01'),
(223, 1772, 435, 4.5, '', 232, 5, '', '2017-09-01'),
(224, 1773, 468, 5, 'hsdhdjdjdjdudj', 212, 5, 'nbh', '2017-09-01'),
(225, 1774, 254, 0, '', 316, 5, '', '2017-09-01'),
(226, 1775, 468, 5, '', 212, 5, '', '2017-09-01'),
(227, 1779, 254, 5, '', 316, 5, '', '2017-09-01'),
(228, 1778, 468, 5, '', 212, 5, '', '2017-09-01'),
(229, 1783, 474, 5, '', 333, 5, '', '2017-09-01'),
(230, 1784, 476, 0, '', 333, 4.5, '', '2017-09-01'),
(231, 1786, 361, 5, '', 235, 0, '', '2017-09-01'),
(232, 1787, 474, 4.5, '', 333, 0, '', '2017-09-01'),
(233, 1789, 360, 4, '', 140, 4, '', '2017-09-02'),
(234, 1794, 359, 3, '', 336, 2.5, '', '2017-09-02'),
(235, 1795, 474, 5, '', 333, 5, '', '2017-09-02'),
(236, 1796, 468, 5, '', 212, 5, '', '2017-09-02'),
(237, 1800, 480, 5, '', 337, 5, '', '2017-09-02'),
(238, 1801, 479, 5, '', 338, 5, '', '2017-09-02'),
(239, 1803, 479, 4, '', 338, 5, '', '2017-09-02'),
(240, 1804, 479, 5, '', 338, 5, '', '2017-09-02'),
(241, 1805, 479, 4, '', 338, 5, '', '2017-09-02'),
(242, 1806, 479, 4.5, '', 338, 4.5, '', '2017-09-02'),
(243, 1807, 479, 4.5, '', 338, 5, '', '2017-09-02'),
(244, 1808, 480, 5, '', 337, 5, '', '2017-09-02'),
(245, 1809, 479, 5, 'Chufgigijvjv', 338, 5, 'jcgjkg', '2017-09-02'),
(246, 1812, 479, 5, '', 338, 5, '', '2017-09-02'),
(247, 1813, 479, 4.5, '', 338, 4.5, '', '2017-09-02'),
(248, 1814, 479, 4.5, 'NBC', 338, 5, '', '2017-09-02'),
(249, 1815, 479, 4.5, '', 338, 5, '', '2017-09-02'),
(250, 1816, 479, 4.5, '', 338, 4.5, '', '2017-09-02'),
(251, 1821, 468, 5, '', 212, 5, '', '2017-09-02'),
(252, 1822, 468, 4.5, '', 212, 5, '', '2017-09-02'),
(253, 1820, 413, 5, '', 250, 5, '', '2017-09-02'),
(254, 1824, 360, 5, '', 140, 5, '', '2017-09-02'),
(255, 1835, 7, 4.5, '', 338, 4.5, '', '2017-09-02'),
(256, 1836, 468, 4.5, 'dffcf', 212, 4.5, '', '2017-09-02'),
(257, 1837, 7, 5, '', 338, 4.5, '', '2017-09-02'),
(258, 1839, 468, 0, '', 212, 4, '', '2017-09-02'),
(259, 1840, 7, 4.5, '', 338, 4.5, '', '2017-09-02'),
(260, 1842, 491, 5, '', 342, 5, '', '2017-09-02'),
(261, 1843, 493, 5, '', 212, 5, '', '2017-09-02'),
(262, 1845, 495, 4.5, '', 344, 4.5, '', '2017-09-02'),
(263, 1849, 495, 4.5, '', 344, 4, '', '2017-09-02'),
(264, 1862, 496, 4.5, '', 345, 5, '', '2017-09-02'),
(265, 1863, 360, 4, '', 140, 5, '', '2017-09-03'),
(266, 1869, 487, 0, '', 146, 5, '', '2017-09-03'),
(267, 1870, 487, 0, '', 146, 5, '', '2017-09-03'),
(268, 1872, 333, 5, '', 220, 3.5, '', '2017-09-03'),
(269, 1871, 487, 5, '', 146, 5, 'in night', '2017-09-03'),
(270, 1877, 487, 0, '', 212, 5, '', '2017-09-03'),
(271, 1878, 487, 5, '', 212, 5, '', '2017-09-03'),
(272, 1879, 468, 4.5, '', 212, 4.5, '', '2017-09-03'),
(273, 1880, 468, 3.5, '', 212, 5, '', '2017-09-03'),
(274, 1881, 468, 4.5, '', 212, 4.5, '', '2017-09-03'),
(275, 1882, 468, 5, '', 212, 5, '', '2017-09-03'),
(276, 1883, 468, 5, '', 212, 4.5, '', '2017-09-03'),
(277, 1887, 468, 5, '', 212, 5, '', '2017-09-03'),
(278, 1888, 360, 4, '', 140, 5, '', '2017-09-03'),
(279, 1895, 464, 2, 'fddvggcff', 169, 2.5, 'f', '2017-09-04'),
(280, 1899, 504, 5, '', 348, 5, '', '2017-09-04'),
(281, 1901, 504, 5, '', 350, 5, '', '2017-09-04'),
(282, 1906, 323, 5, '', 212, 5, '', '2017-09-04'),
(283, 1907, 323, 4.5, '', 212, 4.5, '', '2017-09-04'),
(284, 141, 46, 5, '', 31, 4, '', '2017-09-04'),
(285, 1909, 46, 5, '', 358, 0, '', '2017-09-04'),
(286, 1911, 436, 5, '', 359, 0, '', '2017-09-04'),
(287, 1916, 46, 4.5, '', 358, 5, '', '2017-09-04'),
(288, 1918, 46, 5, '', 360, 3.5, '', '2017-09-04'),
(289, 1919, 46, 5, '', 362, 0, '', '2017-09-04'),
(290, 1912, 46, 0, '', 359, 5, '', '2017-09-04'),
(291, 1921, 413, 5, '', 235, 5, '', '2017-09-04'),
(292, 1923, 7, 4.5, '', 359, 5, '', '2017-09-04'),
(293, 1924, 323, 5, '', 212, 5, '', '2017-09-04'),
(294, 1926, 510, 0, '', 365, 5, '', '2017-09-04'),
(295, 1934, 510, 3.5, '', 367, 3, '', '2017-09-04'),
(296, 1935, 510, 4, '', 367, 4, '', '2017-09-04'),
(297, 1937, 510, 0, '', 367, 5, '', '2017-09-04'),
(298, 1936, 510, 0, '', 365, 3, '', '2017-09-05'),
(299, 1941, 240, 5, '', 96, 5, '', '2017-09-05'),
(300, 1942, 240, 5, '', 96, 5, '', '2017-09-05'),
(301, 1943, 514, 5, '', 368, 0, '', '2017-09-05'),
(302, 1944, 514, 5, '', 368, 5, '', '2017-09-05'),
(303, 1945, 515, 0, '', 294, 4, '', '2017-09-05'),
(304, 1947, 7, 4.5, '', 338, 4.5, '', '2017-09-05'),
(305, 1948, 7, 0, '', 338, 5, '', '2017-09-05'),
(306, 1949, 7, 4.5, '', 338, 4.5, '', '2017-09-05'),
(307, 1950, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(308, 1951, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(309, 1952, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(310, 1953, 7, 4.5, '', 338, 4.5, '', '2017-09-05'),
(311, 1955, 7, 4, '', 338, 5, '', '2017-09-05'),
(312, 1956, 7, 5, '', 338, 5, '', '2017-09-05'),
(313, 1961, 7, 5, '', 338, 5, '', '2017-09-05'),
(314, 1962, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(315, 1963, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(316, 1964, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(317, 1966, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(318, 1967, 7, 5, '', 338, 5, '', '2017-09-05'),
(319, 1968, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(320, 1969, 7, 4.5, '', 338, 4, '', '2017-09-05'),
(321, 1965, 520, 0, '', 212, 5, '', '2017-09-05'),
(322, 1973, 7, 4.5, '', 338, 4, '', '2017-09-05'),
(323, 1974, 7, 3.5, '', 338, 5, '', '2017-09-05'),
(324, 1976, 520, 4.5, '', 212, 5, '', '2017-09-05'),
(325, 1975, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(326, 1978, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(327, 1977, 520, 4.5, '', 212, 5, '', '2017-09-05'),
(328, 1979, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(329, 1980, 7, 4, '', 338, 5, '', '2017-09-05'),
(330, 1981, 7, 5, '', 338, 5, '', '2017-09-05'),
(331, 1982, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(332, 1984, 7, 5, '', 338, 5, '', '2017-09-05'),
(333, 1986, 520, 5, '', 212, 4.5, '', '2017-09-05'),
(334, 1987, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(335, 1989, 524, 5, '', 372, 5, 'Ho', '2017-09-05'),
(336, 1990, 524, 5, '', 372, 4, '', '2017-09-05'),
(337, 1988, 520, 4.5, '', 212, 5, '', '2017-09-05'),
(338, 1992, 524, 5, 'NBC', 372, 4.5, 'NBC', '2017-09-05'),
(339, 1994, 524, 4, '', 372, 4.5, '', '2017-09-05'),
(340, 1995, 524, 4.5, '', 372, 5, '', '2017-09-05'),
(341, 1996, 524, 5, '', 372, 4.5, '', '2017-09-05'),
(342, 1998, 524, 4.5, '', 372, 5, '', '2017-09-05'),
(343, 1999, 524, 5, '', 372, 4.5, '', '2017-09-05'),
(344, 2000, 524, 4.5, '', 372, 5, '', '2017-09-05'),
(345, 2005, 525, 4.5, '', 310, 4.5, '', '2017-09-05'),
(346, 2010, 525, 5, '', 310, 4.5, '', '2017-09-05'),
(347, 2011, 525, 5, '', 310, 5, '', '2017-09-05'),
(348, 2024, 525, 4.5, '', 310, 0, '', '2017-09-05'),
(349, 2025, 525, 0, '', 310, 4.5, '', '2017-09-05'),
(350, 2027, 525, 4, '', 310, 4.5, '', '2017-09-05'),
(351, 2028, 525, 4.5, '', 310, 5, '', '2017-09-05'),
(352, 2029, 520, 4.5, '', 212, 4.5, '', '2017-09-05'),
(353, 2032, 528, 5, '', 377, 5, '', '2017-09-05'),
(354, 2034, 528, 5, '', 377, 5, '', '2017-09-05'),
(355, 2039, 530, 5, '', 379, 5, '', '2017-09-06'),
(356, 2040, 532, 5, '', 379, 5, '', '2017-09-06'),
(357, 2041, 532, 4.5, '', 381, 5, '', '2017-09-06'),
(358, 2042, 532, 5, '', 381, 5, '', '2017-09-06'),
(359, 2043, 532, 0.5, '', 384, 5, '', '2017-09-06'),
(360, 2050, 520, 5, '', 212, 4.5, '', '2017-09-06'),
(361, 2051, 520, 5, '', 212, 5, '', '2017-09-06'),
(362, 2061, 525, 4.5, '', 310, 4, '', '2017-09-06'),
(363, 2062, 525, 4.5, '', 310, 4, '', '2017-09-06'),
(364, 2070, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(365, 2069, 545, 4.5, '', 212, 5, '', '2017-09-06'),
(366, 2071, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(367, 2072, 546, 5, '', 338, 4.5, '', '2017-09-06'),
(368, 2074, 546, 5, '', 338, 5, '', '2017-09-06'),
(369, 2076, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(370, 2077, 546, 4.5, '', 338, 4.5, '', '2017-09-06'),
(371, 2073, 436, 5, '', 212, 5, '', '2017-09-06'),
(372, 2079, 546, 5, '', 338, 4, '', '2017-09-06'),
(373, 2081, 546, 4, '', 390, 5, '', '2017-09-06'),
(374, 2083, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(375, 2090, 548, 4.5, '', 390, 5, '', '2017-09-06'),
(376, 2103, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(377, 2108, 549, 4, '', 338, 4.5, '', '2017-09-06'),
(378, 2110, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(379, 2112, 549, 5, '', 338, 5, '', '2017-09-06'),
(380, 2116, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(381, 2118, 549, 4, '', 338, 4, '', '2017-09-06'),
(382, 2119, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(383, 2121, 549, 5, '', 338, 5, '', '2017-09-06'),
(384, 2124, 549, 4, '', 338, 5, '', '2017-09-06'),
(385, 2125, 545, 5, '', 390, 5, '', '2017-09-06'),
(386, 2137, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(387, 2138, 549, 5, '', 338, 5, '', '2017-09-06'),
(388, 2143, 549, 5, '', 338, 5, '', '2017-09-06'),
(389, 2145, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(390, 2147, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(391, 2150, 549, 5, '', 338, 5, '', '2017-09-06'),
(392, 2153, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(393, 2155, 549, 5, '', 338, 5, '', '2017-09-06'),
(394, 2157, 549, 4, '', 338, 5, '', '2017-09-06'),
(395, 2160, 551, 5, '', 390, 4.5, '', '2017-09-06'),
(396, 2162, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(397, 2163, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(398, 2161, 551, 4.5, '', 390, 5, '', '2017-09-06'),
(399, 2165, 551, 5, '', 390, 5, '', '2017-09-06'),
(400, 2166, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(401, 2168, 551, 4.5, '', 390, 5, '', '2017-09-06'),
(402, 2169, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(403, 2171, 549, 5, '', 338, 5, '', '2017-09-06'),
(404, 2172, 552, 5, '', 397, 4.5, '', '2017-09-06'),
(405, 2174, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(406, 2175, 551, 5, '', 390, 5, '', '2017-09-06'),
(407, 2177, 551, 5, '', 390, 5, '', '2017-09-06'),
(408, 2176, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(409, 2178, 551, 4.5, '', 390, 5, '', '2017-09-06'),
(410, 2179, 551, 4, '', 390, 5, '', '2017-09-06'),
(411, 2180, 549, 5, '', 338, 5, '', '2017-09-06'),
(412, 2173, 552, 4, '', 397, 0, '', '2017-09-06'),
(413, 2181, 551, 3.5, '', 390, 5, '', '2017-09-06'),
(414, 2182, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(415, 2183, 551, 4.5, '', 390, 4.5, '', '2017-09-06'),
(416, 2184, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(417, 2186, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(418, 2188, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(419, 2190, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(420, 2193, 549, 5, '', 338, 4, '', '2017-09-06'),
(421, 2196, 549, 5, '', 338, 5, '', '2017-09-06'),
(422, 2197, 552, 5, '', 397, 5, '', '2017-09-06'),
(423, 2199, 552, 4.5, '', 397, 3, '', '2017-09-06'),
(424, 2200, 552, 2, '', 397, 2, '', '2017-09-06'),
(425, 2201, 552, 5, '', 397, 5, '', '2017-09-06'),
(426, 2202, 552, 0, '', 397, 5, '', '2017-09-06'),
(427, 2203, 552, 4.5, '', 397, 5, '', '2017-09-06'),
(428, 2204, 549, 5, '', 338, 5, '', '2017-09-06'),
(429, 2209, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(430, 2211, 549, 0, '', 338, 4.5, '', '2017-09-06'),
(431, 2212, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(432, 2213, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(433, 2214, 549, 4, '', 338, 5, '', '2017-09-06'),
(434, 2215, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(435, 2216, 549, 4, '', 338, 5, '', '2017-09-06'),
(436, 2217, 554, 5, '', 399, 5, '', '2017-09-06'),
(437, 2218, 554, 4, '', 399, 4.5, '', '2017-09-06'),
(438, 2219, 549, 5, '', 338, 5, '', '2017-09-06'),
(439, 2220, 549, 4, '', 338, 5, '', '2017-09-06'),
(440, 2221, 549, 4, '', 338, 5, '', '2017-09-06'),
(441, 2222, 536, 5, '', 384, 5, 'hhh', '2017-09-06'),
(442, 2223, 556, 5, '', 403, 4.5, '', '2017-09-06'),
(443, 2224, 556, 5, '', 401, 5, '', '2017-09-06'),
(444, 2225, 555, 5, '', 401, 5, '', '2017-09-06'),
(445, 2226, 555, 5, '', 401, 1.5, '', '2017-09-06'),
(446, 2227, 555, 5, '', 400, 5, '', '2017-09-06'),
(447, 2229, 556, 3, '', 400, 5, '', '2017-09-06'),
(448, 2232, 558, 5, '', 400, 0, '', '2017-09-06'),
(449, 2235, 555, 5, '', 401, 5, '', '2017-09-07'),
(450, 2238, 556, 5, '', 401, 5, '', '2017-09-07'),
(451, 2104, 436, 0, '', 391, 4.5, '', '2017-09-07'),
(452, 2254, 514, 4, '', 368, 4, '', '2017-09-07'),
(453, 2258, 549, 5, '', 338, 4, '', '2017-09-07'),
(454, 2261, 549, 5, '', 338, 5, '', '2017-09-07'),
(455, 2263, 549, 5, '', 338, 5, '', '2017-09-07'),
(456, 2266, 549, 5, '', 338, 5, '', '2017-09-07'),
(457, 2279, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(458, 2283, 549, 5, '', 338, 5, '', '2017-09-07'),
(459, 2285, 436, 5, '', 391, 5, '', '2017-09-07'),
(460, 2292, 566, 0, '', 408, 5, 'very bad', '2017-09-07'),
(461, 2297, 436, 5, '', 391, 5, '', '2017-09-07'),
(462, 2316, 360, 4, '', 140, 4.5, '', '2017-09-07'),
(463, 2318, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(464, 2325, 549, 0, '', 338, 5, '', '2017-09-07'),
(465, 2326, 549, 4, '', 338, 4.5, '', '2017-09-07'),
(466, 2327, 549, 4, '', 338, 5, '', '2017-09-07'),
(467, 2333, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(468, 2338, 436, 5, '', 409, 5, 'shxuffjcj', '2017-09-07'),
(469, 2343, 545, 5, '', 409, 5, '', '2017-09-07'),
(470, 2346, 549, 5, '', 338, 5, '', '2017-09-07'),
(471, 2349, 549, 4, '', 338, 4, '', '2017-09-07'),
(472, 2350, 549, 5, '', 338, 4, '', '2017-09-07'),
(473, 2351, 549, 5, '', 338, 5, '', '2017-09-07'),
(474, 2355, 549, 5, '', 338, 4.5, '', '2017-09-07'),
(475, 2356, 558, 5, '', 95, 5, '', '2017-09-07'),
(476, 2357, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(477, 2360, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(478, 2366, 549, 4, '', 338, 4.5, '', '2017-09-07'),
(479, 2368, 549, 4, '', 338, 4, '', '2017-09-07'),
(480, 2369, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(481, 2373, 549, 5, '', 338, 4.5, '', '2017-09-07'),
(482, 2367, 545, 5, '', 390, 5, '', '2017-09-07'),
(483, 2380, 549, 5, '', 338, 5, '', '2017-09-07'),
(484, 2381, 549, 4, '', 338, 5, '', '2017-09-07'),
(485, 2383, 549, 4, '', 338, 4, '', '2017-09-07'),
(486, 2384, 549, 5, '', 338, 5, '', '2017-09-07'),
(487, 2385, 549, 5, '', 338, 5, '', '2017-09-07'),
(488, 2386, 549, 4, '', 338, 5, '', '2017-09-07'),
(489, 2387, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(490, 2388, 549, 4.5, '', 338, 4.5, '', '2017-09-07'),
(491, 2389, 549, 4, '', 338, 5, '', '2017-09-07'),
(492, 2390, 549, 4, '', 338, 5, '', '2017-09-07'),
(493, 2393, 549, 4, '', 338, 4.5, '', '2017-09-07'),
(494, 2396, 545, 5, 'hghhuh', 390, 5, '', '2017-09-07'),
(495, 2403, 528, 5, '', 376, 5, '', '2017-09-07'),
(496, 2406, 528, 3, '', 376, 3, '', '2017-09-07'),
(497, 2410, 569, 4.5, '', 415, 5, '', '2017-09-07'),
(498, 2411, 528, 3, '', 376, 2, '', '2017-09-07'),
(499, 2413, 569, 5, '', 415, 5, '', '2017-09-07'),
(500, 2414, 528, 3, '', 376, 4, '', '2017-09-07'),
(501, 2415, 528, 0, '', 376, 3.5, '', '2017-09-07'),
(502, 2417, 569, 4.5, '', 390, 5, '', '2017-09-07'),
(503, 2422, 545, 4.5, '', 390, 4.5, '', '2017-09-07'),
(504, 2423, 549, 4.5, '', 338, 4, '', '2017-09-07'),
(505, 2424, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(506, 2425, 549, 5, '', 338, 5, '', '2017-09-07'),
(507, 2426, 545, 5, '', 390, 5, '', '2017-09-07'),
(508, 2430, 545, 5, '', 390, 5, '', '2017-09-07'),
(509, 2431, 582, 5, '', 254, 4.5, '', '2017-09-07'),
(510, 2432, 582, 5, '', 265, 5, '', '2017-09-07'),
(511, 2433, 582, 5, '', 265, 4.5, '', '2017-09-07'),
(512, 2434, 582, 5, '', 265, 5, '', '2017-09-07'),
(513, 2435, 582, 5, '', 420, 5, '', '2017-09-07'),
(514, 2436, 582, 5, '', 420, 4.5, '', '2017-09-07'),
(515, 2437, 582, 1.5, '', 420, 5, '', '2017-09-07'),
(516, 2441, 583, 5, '', 420, 5, '', '2017-09-07'),
(517, 2449, 586, 4.5, '', 422, 2, '', '2017-09-07'),
(518, 2453, 545, 5, '', 390, 5, '', '2017-09-08'),
(519, 2454, 583, 5, '', 420, 4.5, '', '2017-09-08'),
(520, 2456, 545, 3.5, '', 390, 4, '', '2017-09-08'),
(521, 2458, 545, 5, '', 390, 4, '', '2017-09-08'),
(522, 2461, 569, 5, '', 409, 5, '', '2017-09-08'),
(523, 2462, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(524, 2463, 586, 4, '', 422, 5, '', '2017-09-08'),
(525, 2464, 586, 5, '', 422, 5, '', '2017-09-08'),
(526, 2466, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(527, 2471, 586, 3, '', 422, 4.5, '', '2017-09-08'),
(528, 2472, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(529, 2473, 528, 1.5, '', 376, 5, '', '2017-09-08'),
(530, 2475, 545, 5, '', 390, 5, '', '2017-09-08'),
(531, 2474, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(532, 2480, 590, 4, '', 410, 5, '', '2017-09-08'),
(533, 2481, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(534, 2482, 586, 4, '', 422, 4, '', '2017-09-08'),
(535, 2483, 586, 5, '', 422, 5, '', '2017-09-08'),
(536, 2484, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(537, 2485, 586, 5, '', 422, 5, '', '2017-09-08'),
(538, 2486, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(539, 2487, 586, 5, '', 422, 4.5, '', '2017-09-08'),
(540, 2488, 586, 5, '', 422, 5, '', '2017-09-08'),
(541, 2489, 586, 5, '', 422, 4.5, '', '2017-09-08'),
(542, 2470, 569, 0, '', 409, 5, '', '2017-09-08'),
(543, 2492, 569, 0, '', 409, 5, '', '2017-09-08'),
(544, 2491, 586, 4, '', 422, 5, '', '2017-09-08'),
(545, 2494, 569, 0, '', 409, 5, '', '2017-09-08'),
(546, 2499, 586, 4, '', 422, 5, '', '2017-09-08'),
(547, 2498, 569, 0, '', 409, 5, '', '2017-09-08'),
(548, 2504, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(549, 2505, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(550, 2506, 586, 5, '', 422, 5, '', '2017-09-08'),
(551, 2508, 586, 4, '', 422, 5, '', '2017-09-08'),
(552, 2511, 586, 5, '', 422, 5, '', '2017-09-08'),
(553, 2512, 586, 4, '', 422, 4.5, '', '2017-09-08'),
(554, 2510, 569, 0, '', 414, 5, '', '2017-09-08'),
(555, 2513, 586, 4, '', 422, 4, '', '2017-09-08'),
(556, 2514, 586, 4, '', 422, 4.5, '', '2017-09-08'),
(557, 2515, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(558, 2516, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(559, 2518, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(560, 2519, 551, 5, '', 390, 4.5, '', '2017-09-08'),
(561, 2520, 569, 0, '', 414, 5, '', '2017-09-08'),
(562, 2521, 569, 0, '', 414, 5, '', '2017-09-08'),
(563, 2525, 586, 4, '', 422, 5, '', '2017-09-08'),
(564, 2526, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(565, 2527, 586, 5, '', 422, 5, '', '2017-09-08'),
(566, 2528, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(567, 2529, 586, 5, '', 422, 5, '', '2017-09-08'),
(568, 2530, 586, 4, '', 422, 5, '', '2017-09-08'),
(569, 2531, 569, 0, '', 397, 5, '', '2017-09-08'),
(570, 2534, 586, 5, '', 422, 5, '', '2017-09-08'),
(571, 2538, 579, 0, '', 428, 5, ' ggf', '2017-09-08'),
(572, 2542, 551, 5, '', 390, 5, '', '2017-09-08'),
(573, 2543, 551, 4.5, '', 390, 4.5, '', '2017-09-08'),
(574, 2544, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(575, 2546, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(576, 2547, 441, 0, '', 286, 4, '', '2017-09-08'),
(577, 2548, 594, 5, '', 433, 5, '', '2017-09-08'),
(578, 2549, 464, 3, 'bdbdncnxnxnxnfjfhbccbxbxnnsnnxnxndndndndjdjddkkdksushshdbbnnnnnnÃ±nnnnn', 169, 2.5, '', '2017-09-08'),
(579, 2554, 596, 4, '', 286, 4, '', '2017-09-09'),
(580, 2555, 464, 5, '', 169, 5, '', '2017-09-09'),
(581, 2559, 209, 0, '', 250, 5, '', '2017-09-09'),
(582, 2561, 61, 5, '', 441, 3, '', '2017-09-09'),
(583, 2562, 61, 5, '', 441, 2.5, '', '2017-09-09'),
(584, 2564, 61, 5, '', 441, 5, '', '2017-09-09'),
(585, 2565, 598, 4, '', 441, 4.5, '', '2017-09-09'),
(586, 2556, 464, 5, '', 169, 4.5, '', '2017-09-09'),
(587, 2568, 388, 5, '', 444, 5, '', '2017-09-09'),
(588, 2567, 464, 5, '', 169, 0, '', '2017-09-09'),
(589, 2569, 598, 5, '', 441, 0, '', '2017-09-09'),
(590, 2570, 551, 5, '', 444, 5, '', '2017-09-09'),
(591, 2574, 551, 5, '', 390, 5, '', '2017-09-10'),
(592, 2575, 551, 5, '', 390, 5, '', '2017-09-10'),
(593, 2576, 514, 5, '', 368, 5, '', '2017-09-10'),
(594, 2577, 551, 0, '', 390, 4.5, '', '2017-09-10'),
(595, 2578, 576, 5, '', 268, 5, '', '2017-09-10'),
(596, 2581, 576, 0.5, '', 268, 3, '', '2017-09-10'),
(597, 2420, 360, 5, '', 140, 5, '', '2017-09-10'),
(598, 2582, 360, 5, '', 140, 4, '', '2017-09-10'),
(599, 2584, 393, 5, '', 277, 4, '', '2017-09-10'),
(600, 2585, 583, 4.5, '', 453, 5, '', '2017-09-10'),
(601, 2588, 588, 2.5, '', 423, 3, '', '2017-09-10'),
(602, 2589, 322, 5, '', 456, 5, '', '2017-09-11'),
(603, 2591, 598, 5, '', 453, 4.5, '', '2017-09-11'),
(604, 2593, 545, 5, '', 391, 0, '', '2017-09-11'),
(605, 2594, 545, 5, '', 391, 5, '', '2017-09-11'),
(606, 2595, 545, 4.5, '', 391, 5, '', '2017-09-11'),
(607, 2597, 621, 5, '', 391, 5, '', '2017-09-11'),
(608, 2598, 436, 0, '', 391, 5, '', '2017-09-11'),
(609, 2602, 621, 0, '', 391, 4.5, '', '2017-09-11'),
(610, 2603, 621, 5, '', 391, 5, '', '2017-09-11'),
(611, 2617, 624, 0, '', 466, 5, '', '2017-09-11'),
(612, 2618, 545, 4.5, '', 390, 5, '', '2017-09-11'),
(613, 2619, 545, 0, '', 390, 5, '', '2017-09-11'),
(614, 0, 624, 0, '', 466, 5, '', '2017-09-11'),
(615, 2627, 545, 0, '', 390, 5, '', '2017-09-11'),
(616, 2628, 545, 0, '', 390, 5, '', '2017-09-11'),
(617, 2625, 586, 4.5, '', 422, 0, '', '2017-09-11'),
(618, 2630, 545, 0, '', 390, 4, '', '2017-09-11'),
(619, 2631, 545, 0, '', 390, 4, '', '2017-09-11'),
(620, 2632, 586, 4, '', 422, 4, '', '2017-09-11'),
(621, 2633, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(622, 1352, 545, 4, '', 390, 0, '', '2017-09-11'),
(623, 2634, 586, 4, '', 422, 4.5, '', '2017-09-11'),
(624, 2635, 545, 0, '', 390, 5, '', '2017-09-11'),
(625, 2636, 545, 0, '', 390, 5, '', '2017-09-11'),
(626, 1355, 545, 5, '', 390, 0, '', '2017-09-11'),
(627, 2637, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(628, 2638, 586, 4.5, '', 422, 4.5, '', '2017-09-11'),
(629, 2639, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(630, 2640, 586, 0, '', 422, 4.5, '', '2017-09-11'),
(631, 2641, 586, 4, '', 422, 4.5, '', '2017-09-11'),
(632, 2642, 545, 0, '', 390, 5, '', '2017-09-11'),
(633, 2643, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(634, 2644, 545, 0, '', 390, 5, '', '2017-09-11'),
(635, 2645, 545, 0, '', 390, 5, '', '2017-09-11'),
(636, 1364, 545, 5, '', 390, 0, '', '2017-09-11'),
(637, 2648, 586, 0, '', 422, 4.5, '', '2017-09-11'),
(638, 2649, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(639, 2650, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(640, 2651, 545, 0, '', 390, 5, '', '2017-09-11'),
(641, 2653, 545, 0, '', 390, 5, '', '2017-09-11'),
(642, 1369, 545, 5, '', 390, 0, '', '2017-09-11'),
(643, 1370, 545, 4.5, '', 390, 0, '', '2017-09-11'),
(644, 2655, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(645, 1371, 545, 4.5, '', 390, 0, '', '2017-09-11'),
(646, 2657, 545, 0, '', 390, 5, '', '2017-09-11'),
(647, 1373, 545, 5, '', 390, 0, '', '2017-09-11'),
(648, 2658, 545, 0, '', 390, 4, '', '2017-09-11'),
(649, 1374, 545, 5, '', 390, 0, '', '2017-09-11'),
(650, 2659, 545, 0, '', 390, 5, '', '2017-09-11'),
(651, 1375, 545, 5, '', 390, 0, '', '2017-09-11'),
(652, 2656, 586, 4.5, '', 422, 4.5, '', '2017-09-11'),
(653, 2660, 545, 0, '', 390, 5, '', '2017-09-11'),
(654, 1376, 545, 5, '', 390, 0, '', '2017-09-11'),
(655, 2661, 545, 0, '', 390, 5, '', '2017-09-11'),
(656, 2662, 545, 0, '', 390, 5, '', '2017-09-11'),
(657, 2663, 586, 3.5, '', 422, 5, '', '2017-09-11'),
(658, 2664, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(659, 2665, 586, 5, 'Dgdghdfh', 422, 4.5, '', '2017-09-11'),
(660, 2666, 586, 4.5, '', 422, 4.5, '', '2017-09-11'),
(661, 2667, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(662, 1384, 629, 5, '', 472, 0, '', '2017-09-11'),
(663, 2668, 629, 0, '', 472, 5, '', '2017-09-11'),
(664, 2669, 629, 0, '', 472, 5, '', '2017-09-11'),
(665, 1385, 629, 5, '', 472, 0, '', '2017-09-11'),
(666, 2670, 629, 0, '', 472, 5, '', '2017-09-11'),
(667, 2671, 629, 0, '', 472, 5, '', '2017-09-11'),
(668, 2672, 629, 0, '', 472, 5, '', '2017-09-11'),
(669, 1388, 629, 2.5, '', 472, 0, '', '2017-09-11'),
(670, 2673, 629, 0, '', 472, 5, '', '2017-09-11'),
(671, 1389, 629, 4.5, '', 472, 0, '', '2017-09-11'),
(672, 2674, 629, 0, '', 473, 5, '', '2017-09-11'),
(673, 2675, 629, 0, '', 473, 5, '', '2017-09-11'),
(674, 2677, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(675, 2678, 586, 0, 'Hhh', 422, 5, '', '2017-09-11'),
(676, 2676, 545, 0, '', 390, 4, '', '2017-09-11'),
(677, 2679, 586, 5, '', 422, 4.5, '', '2017-09-11'),
(678, 2685, 632, 0, '', 476, 3.5, '', '2017-09-12'),
(679, 2726, 640, 0, '', 477, 5, '', '2017-09-12'),
(680, 2727, 641, 0, '', 479, 5, '', '2017-09-12'),
(681, 2744, 642, 0, '', 477, 5, '', '2017-09-12'),
(682, 2745, 642, 0, '', 477, 5, '', '2017-09-12'),
(683, 2748, 630, 0, '', 477, 5, '', '2017-09-12'),
(684, 2747, 638, 5, '', 478, 0, '', '2017-09-12'),
(685, 2758, 538, 0, '', 480, 3, '', '2017-09-12'),
(686, 2759, 644, 0, '', 480, 2, '', '2017-09-12'),
(687, 2760, 645, 0, '', 481, 2, '', '2017-09-12'),
(688, 2757, 586, 0, '', 422, 5, '', '2017-09-12'),
(689, 2761, 647, 5, '', 482, 5, '', '2017-09-12'),
(690, 2762, 647, 4.5, '', 482, 4.5, '', '2017-09-12'),
(691, 2764, 647, 4.5, '', 482, 5, '', '2017-09-12'),
(692, 2765, 647, 2.5, '', 482, 5, '', '2017-09-12'),
(693, 2766, 647, 4.5, '', 482, 5, '', '2017-09-12'),
(694, 2767, 647, 4, '', 482, 4.5, '', '2017-09-12'),
(695, 2773, 545, 0, '', 444, 4, '', '2017-09-12'),
(696, 2783, 647, 4, '', 482, 4, '', '2017-09-12'),
(697, 1434, 436, 5, '', 483, 0, '', '2017-09-12'),
(698, 2791, 436, 0, '', 483, 5, '', '2017-09-12'),
(699, 1435, 436, 4.5, 'very nice', 483, 0, '', '2017-09-12'),
(700, 2792, 436, 0, '', 483, 5, 'good', '2017-09-12'),
(701, 2793, 436, 0, '', 483, 5, '', '2017-09-12'),
(702, 2787, 545, 0, '', 444, 5, '', '2017-09-12'),
(703, 2795, 647, 4, '', 482, 4.5, '', '2017-09-12'),
(704, 2796, 647, 3.5, '', 482, 5, '', '2017-09-12'),
(705, 2797, 647, 3, '', 482, 4.5, '', '2017-09-12'),
(706, 2799, 647, 3, '', 482, 4.5, '', '2017-09-12'),
(707, 2800, 647, 0, '', 444, 4.5, '', '2017-09-12'),
(708, 2801, 647, 4, '', 444, 4.5, '', '2017-09-12'),
(709, 2802, 647, 0, '', 482, 5, '', '2017-09-12'),
(710, 2803, 647, 4.5, '', 482, 4.5, '', '2017-09-12'),
(711, 1446, 545, 4.5, '', 444, 0, '', '2017-09-12'),
(712, 2804, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(713, 2805, 545, 0, '', 444, 4.5, '', '2017-09-12'),
(714, 2806, 647, 4.5, '', 482, 4, '', '2017-09-12'),
(715, 2807, 545, 0, '', 444, 5, '', '2017-09-12'),
(716, 1448, 545, 5, '', 444, 0, '', '2017-09-12'),
(717, 1450, 545, 5, '', 444, 0, '', '2017-09-12'),
(718, 2809, 545, 0, '', 444, 4.5, '', '2017-09-12'),
(719, 2808, 647, 0, '', 482, 5, '', '2017-09-12'),
(720, 2810, 545, 0, '', 444, 5, '', '2017-09-12'),
(721, 651, 415, 5, '', 258, 0, '', '2017-09-12'),
(722, 2813, 647, 0, '', 482, 4, '', '2017-09-12'),
(723, 2812, 415, 0, '', 390, 5, '', '2017-09-12'),
(724, 2811, 436, 0, '', 486, 5, '', '2017-09-12'),
(725, 2817, 545, 0, '', 444, 4.5, '', '2017-09-12'),
(726, 1458, 545, 4.5, '', 444, 0, '', '2017-09-12'),
(727, 1455, 415, 4.5, '', 110, 0, '', '2017-09-12'),
(728, 2815, 647, 0, '', 482, 4, '', '2017-09-12'),
(729, 2816, 436, 0, '', 486, 5, '', '2017-09-12'),
(730, 2814, 415, 0, '', 110, 5, '', '2017-09-12'),
(731, 1457, 436, 0, '', 486, 0, '', '2017-09-12'),
(732, 2818, 545, 0, '', 390, 5, '', '2017-09-12'),
(733, 1459, 545, 3, '', 390, 0, '', '2017-09-12'),
(734, 2819, 545, 0, '', 390, 5, '', '2017-09-12'),
(735, 2820, 545, 0, '', 390, 5, '', '2017-09-12'),
(736, 1462, 415, 5, '', 110, 0, '', '2017-09-12'),
(737, 2821, 415, 0, '', 110, 5, '', '2017-09-12'),
(738, 2823, 647, 4.5, '', 482, 4, '', '2017-09-12'),
(739, 2824, 545, 0, '', 486, 5, '', '2017-09-12'),
(740, 2825, 436, 0, '', 110, 5, '', '2017-09-12'),
(741, 2828, 545, 0, '', 390, 5, '', '2017-09-12'),
(742, 2834, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(743, 2836, 647, 4.5, '', 482, 4.5, '', '2017-09-12'),
(744, 1467, 545, 3.5, '', 390, 0, '', '2017-09-12'),
(745, 2838, 647, 0, '', 482, 4, '', '2017-09-12'),
(746, 2840, 647, 0, '', 482, 5, '', '2017-09-12'),
(747, 2841, 647, 4, '', 482, 4, '', '2017-09-12'),
(748, 2842, 545, 0, '', 390, 5, '', '2017-09-12'),
(749, 2843, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(750, 2844, 545, 0, '', 390, 5, '', '2017-09-12'),
(751, 1476, 545, 5, '', 390, 0, '', '2017-09-12'),
(752, 2846, 647, 4.5, '', 482, 5, '', '2017-09-12'),
(753, 2847, 647, 0, '', 482, 5, '', '2017-09-12'),
(754, 2848, 647, 0, '', 482, 4.5, 'H', '2017-09-12'),
(755, 2849, 647, 0, '', 482, 5, '', '2017-09-12'),
(756, 2850, 647, 0, '', 482, 5, '', '2017-09-12'),
(757, 2852, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(758, 2853, 664, 0, '', 494, 5, '', '2017-09-12'),
(759, 1486, 664, 5, '', 494, 0, '', '2017-09-12'),
(760, 2854, 664, 0, '', 494, 5, '', '2017-09-12'),
(761, 2855, 664, 0, '', 494, 5, '', '2017-09-12'),
(762, 2856, 664, 0, '', 494, 5, '', '2017-09-12'),
(763, 2859, 664, 0, '', 494, 5, '', '2017-09-12'),
(764, 2860, 647, 0, '', 482, 5, '', '2017-09-12'),
(765, 2851, 545, 0, '', 390, 5, '', '2017-09-12'),
(766, 2861, 664, 0, '', 494, 5, '', '2017-09-12'),
(767, 2862, 664, 0, '', 494, 5, '', '2017-09-12'),
(768, 2863, 664, 0, '', 496, 3.5, '', '2017-09-12'),
(769, 2865, 545, 0, '', 390, 5, '', '2017-09-12'),
(770, 2875, 603, 0, '', 501, 5, '', '2017-09-13'),
(771, 2878, 603, 0, '', 498, 1.5, '', '2017-09-13'),
(772, 2880, 673, 0, '', 505, 5, '', '2017-09-13'),
(773, 2881, 413, 0, '', 235, 5, '', '2017-09-13'),
(774, 2882, 603, 0, '', 498, 2, '', '2017-09-13'),
(775, 2884, 603, 0, '', 498, 3, '', '2017-09-13'),
(776, 2888, 436, 0, '', 441, 5, '', '2017-09-13'),
(777, 2889, 647, 0, '', 482, 4.5, '', '2017-09-13'),
(778, 2794, 648, 0, '', 485, 5, '', '2017-09-13'),
(779, 2893, 678, 0, '', 397, 5, '', '2017-09-13'),
(780, 1511, 545, 4.5, '', 494, 0, '', '2017-09-13'),
(781, 2894, 545, 0, '', 494, 5, '', '2017-09-13'),
(782, 2897, 436, 0, '', 409, 5, '', '2017-09-13'),
(783, 2899, 436, 0, '', 409, 5, '', '2017-09-13'),
(784, 2902, 680, 0, '', 509, 3, 'mmmm', '2017-09-13'),
(785, 2903, 655, 0, '', 391, 5, '', '2017-09-13'),
(786, 2907, 655, 0, '', 391, 5, '', '2017-09-13'),
(787, 506, 8, 5, 'ggghh', 255, 0, '', '2017-09-13'),
(788, 2917, 436, 0, '', 483, 5, '', '2017-09-13'),
(789, 2921, 687, 0, '', 514, 5, '', '2017-09-13'),
(790, 2923, 687, 0, '', 514, 5, '', '2017-09-13'),
(791, 2924, 687, 0, '', 514, 5, '', '2017-09-13'),
(792, 2925, 680, 0, '', 509, 5, 'mantap', '2017-09-13'),
(793, 2931, 545, 0, '', 390, 5, '', '2017-09-13'),
(794, 2934, 545, 0, '', 390, 5, '', '2017-09-13'),
(795, 2936, 689, 0, '', 235, 4.5, '', '2017-09-13'),
(796, 2942, 603, 0, '', 498, 2, '', '2017-09-13'),
(797, 2944, 689, 0, '', 235, 4.5, '', '2017-09-13'),
(798, 2948, 694, 0, '', 517, 5, '', '2017-09-13'),
(799, 2949, 694, 0, '', 517, 5, '', '2017-09-13'),
(800, 2955, 603, 0, '', 498, 1, '', '2017-09-14'),
(801, 2959, 646, 0, '', 522, 5, '', '2017-09-14'),
(802, 2965, 436, 0, '', 483, 5, '', '2017-09-14'),
(803, 1542, 436, 5, '', 483, 0, '', '2017-09-14'),
(804, 2968, 545, 0, '', 494, 4, '', '2017-09-14'),
(805, 1544, 545, 4.5, '', 494, 0, '', '2017-09-14'),
(806, 2966, 436, 0, '', 483, 5, '', '2017-09-14'),
(807, 1543, 436, 5, '', 483, 0, '', '2017-09-14'),
(808, 1546, 569, 5, '', 486, 0, '', '2017-09-14'),
(809, 2972, 569, 0, '', 486, 5, '', '2017-09-14'),
(810, 2975, 569, 0, '', 486, 5, '', '2017-09-14'),
(811, 2976, 569, 0, '', 486, 4.5, '', '2017-09-14'),
(812, 2969, 545, 0, '', 494, 5, '', '2017-09-14'),
(813, 1545, 545, 4, '', 494, 0, '', '2017-09-14'),
(814, 2979, 545, 0, '', 494, 4, '', '2017-09-14'),
(815, 2981, 569, 0, '', 486, 5, '', '2017-09-14'),
(816, 1550, 569, 5, '', 486, 0, '', '2017-09-14'),
(817, 2982, 569, 0, '', 486, 5, '', '2017-09-14'),
(818, 2985, 545, 0, '', 494, 4.5, '', '2017-09-14'),
(819, 1552, 545, 4.5, '', 494, 0, '', '2017-09-14'),
(820, 2986, 569, 0, '', 486, 5, '', '2017-09-14'),
(821, 2987, 545, 0, '', 494, 5, '', '2017-09-14'),
(822, 2988, 569, 0, '', 486, 5, '', '2017-09-14'),
(823, 2989, 545, 0, '', 494, 5, '', '2017-09-14'),
(824, 1556, 545, 5, '', 494, 0, '', '2017-09-14'),
(825, 2992, 439, 0, '', 322, 4.5, '', '2017-09-14'),
(826, 1564, 689, 5, '', 235, 0, '', '2017-09-14'),
(827, 3001, 689, 0, '', 235, 4.5, '', '2017-09-14'),
(828, 3000, 689, 0, '', 235, 5, '', '2017-09-14'),
(829, 3007, 603, 0, '', 498, 2, '', '2017-09-15'),
(830, 3010, 689, 0, '', 516, 5, '', '2017-09-15'),
(831, 1568, 689, 5, '', 516, 0, '', '2017-09-15'),
(832, 3012, 689, 0, '', 516, 5, '', '2017-09-15'),
(833, 3011, 689, 0, '', 516, 5, '', '2017-09-15'),
(834, 3018, 689, 0, '', 516, 5, '', '2017-09-15'),
(835, 1570, 689, 4, 'fare', 516, 0, '', '2017-09-15'),
(836, 363, 229, 5, '', 156, 5, '', '2017-09-15'),
(837, 3019, 229, 0, '', 534, 5, '', '2017-09-15'),
(838, 2937, 464, 0, '', 169, 1, '', '2017-09-15'),
(839, 3024, 436, 0, '', 95, 5, '', '2017-09-15'),
(840, 1573, 436, 4.5, '', 95, 0, '', '2017-09-15'),
(841, 3025, 707, 0, '', 538, 1.5, 'good customer', '2017-09-15'),
(842, 3027, 710, 0, '', 539, 5, '', '2017-09-15'),
(843, 3028, 689, 0, '', 516, 5, '', '2017-09-15'),
(844, 1576, 689, 5, 'nice ride', 516, 0, '', '2017-09-15'),
(845, 974, 464, 0, '', 169, 0, '', '2017-09-15'),
(846, 3032, 711, 0, '', 540, 4.5, 'Excelent', '2017-09-15'),
(847, 1578, 436, 3.5, '', 95, 0, '', '2017-09-15'),
(848, 3038, 464, 0, '', 169, 5, '', '2017-09-15'),
(849, 1582, 464, 5, 'ygg', 169, 0, '', '2017-09-16'),
(850, 3037, 689, 0, '', 516, 5, '', '2017-09-16'),
(851, 1585, 436, 5, '', 95, 0, '', '2017-09-16'),
(852, 3053, 436, 0, '', 95, 5, '', '2017-09-16'),
(853, 3033, 436, 0, '', 95, 5, '', '2017-09-16'),
(854, 1587, 718, 5, '', 486, 0, '', '2017-09-16'),
(855, 3055, 718, 0, '', 486, 5, '', '2017-09-16'),
(856, 1589, 718, 5, '', 486, 0, '', '2017-09-16'),
(857, 3057, 718, 0, '', 486, 4.5, '', '2017-09-16'),
(858, 1590, 718, 5, '', 486, 0, '', '2017-09-16'),
(859, 3058, 718, 0, '', 486, 5, '', '2017-09-16'),
(860, 3056, 689, 0, '', 516, 5, '', '2017-09-16'),
(861, 1592, 689, 5, '', 235, 0, '', '2017-09-16'),
(862, 3062, 689, 0, '', 235, 5, '', '2017-09-16'),
(863, 3063, 569, 0, '', 486, 5, '', '2017-09-16'),
(864, 3066, 545, 0, '', 390, 5, '', '2017-09-16'),
(865, 1586, 545, 0, '', 110, 0, '', '2017-09-16'),
(866, 3067, 436, 0, '', 549, 5, '', '2017-09-16'),
(867, 3078, 721, 0, '', 552, 5, '', '2017-09-16'),
(868, 3082, 721, 0, '', 552, 5, '', '2017-09-16'),
(869, 3101, 725, 0, '', 556, 4.5, '', '2017-09-16'),
(870, 1604, 725, 5, '', 556, 0, '', '2017-09-16'),
(871, 3105, 721, 0, '', 552, 5, '', '2017-09-17'),
(872, 1606, 721, 5, '', 552, 0, '', '2017-09-17'),
(873, 3106, 721, 0, '', 552, 5, '', '2017-09-17'),
(874, 1607, 721, 5, 'Excelente ', 552, 0, '', '2017-09-17'),
(875, 3107, 514, 0, '', 368, 5, '', '2017-09-17'),
(876, 3109, 721, 0, '', 552, 5, '', '2017-09-17'),
(877, 1609, 721, 5, '', 552, 0, '', '2017-09-17'),
(878, 1611, 731, 4.5, '', 95, 0, '', '2017-09-17'),
(879, 3115, 721, 0, '', 552, 5, '', '2017-09-18'),
(880, 1612, 721, 5, '', 552, 0, '', '2017-09-18'),
(881, 3111, 679, 0, '', 525, 4, '', '2017-09-18'),
(882, 3123, 7, 0, '', 8, 4.5, '', '2017-09-18'),
(883, 3125, 735, 0, '', 560, 5, '', '2017-09-18'),
(884, 3157, 735, 0, '', 562, 5, '', '2017-09-18'),
(885, 3158, 735, 0, '', 560, 5, '', '2017-09-18'),
(886, 3159, 735, 0, '', 560, 5, '', '2017-09-18'),
(887, 1618, 735, 4.5, '', 560, 0, '', '2017-09-18'),
(888, 3181, 739, 0, '', 466, 1, 'gente boa', '2017-09-18'),
(889, 1620, 739, 4.5, '', 466, 0, '', '2017-09-18'),
(890, 3185, 740, 0, '', 564, 4, 'muy bien', '2017-09-19'),
(891, 1623, 740, 4.5, '', 564, 0, '', '2017-09-19'),
(892, 1624, 689, 5, '', 516, 0, '', '2017-09-19'),
(893, 3187, 739, 0, '', 466, 5, 'xghggfcgh', '2017-09-19'),
(894, 1625, 739, 5, '', 466, 0, '', '2017-09-19'),
(895, 3126, 735, 0, '', 368, 5, '', '2017-09-19'),
(896, 3196, 540, 0, '', 9, 5, '', '2017-09-19'),
(897, 1630, 745, 5, '', 565, 0, '', '2017-09-19'),
(898, 3204, 745, 0, '', 565, 5, '', '2017-09-19'),
(899, 1631, 745, 5, '', 565, 0, '', '2017-09-19'),
(900, 3208, 745, 0, '', 565, 5, '', '2017-09-19'),
(901, 1632, 745, 5, '', 565, 0, '', '2017-09-19'),
(902, 3207, 745, 0, '', 565, 4.5, '', '2017-09-19'),
(903, 1633, 745, 5, '', 565, 0, '', '2017-09-19'),
(904, 3210, 745, 0, '', 565, 5, '', '2017-09-19'),
(905, 1634, 745, 5, '', 565, 0, '', '2017-09-19'),
(906, 3212, 745, 0, '', 565, 5, '', '2017-09-19'),
(907, 3215, 745, 0, '', 565, 5, '', '2017-09-19'),
(908, 3217, 749, 0, '', 565, 5, '', '2017-09-19'),
(909, 1636, 749, 5, '', 565, 0, '', '2017-09-19'),
(910, 3219, 748, 0, '', 565, 5, '', '2017-09-19'),
(911, 3218, 749, 0, '', 565, 5, '', '2017-09-19'),
(912, 3222, 200, 0, '', 111, 5, '', '2017-09-19'),
(913, 3223, 749, 0, '', 565, 5, '', '2017-09-19'),
(914, 1642, 749, 5, '', 565, 0, '', '2017-09-19'),
(915, 3220, 750, 0, '', 566, 1, '', '2017-09-19'),
(916, 2784, 602, 0, '', 441, 5, '', '2017-09-19'),
(917, 2940, 648, 0, '', 514, 5, '', '2017-09-19'),
(918, 3248, 11, 0, '', 569, 5, '', '2017-09-19'),
(919, 3249, 741, 5, '', 134, 2, '', '2017-09-20'),
(920, 3251, 689, 0, '', 516, 5, '', '2017-09-20'),
(921, 1650, 689, 5, 'tyyiuthhk', 516, 0, '', '2017-09-20'),
(922, 3254, 514, 0, '', 368, 5, 'good drive ', '2017-09-20'),
(923, 3253, 689, 0, '', 516, 5, '', '2017-09-20'),
(924, 3261, 11, 0, '', 569, 5, '', '2017-09-20'),
(925, 1653, 11, 5, '', 569, 0, '', '2017-09-20'),
(926, 1654, 739, 4.5, '', 478, 0, '', '2017-09-20'),
(927, 3262, 739, 0, '', 478, 3.5, 'gfjhh8j', '2017-09-20'),
(928, 3250, 741, 0, '', 134, 2, '', '2017-09-20'),
(929, 3274, 755, 0, '', 566, 5, '', '2017-09-20'),
(930, 1657, 755, 5, '', 566, 0, '', '2017-09-20'),
(931, 3299, 755, 0, '', 566, 5, '', '2017-09-20'),
(932, 3330, 7, 4.5, '', 8, 4.5, '', '2017-09-20'),
(933, 3331, 7, 0, '', 8, 4.5, '', '2017-09-20'),
(934, 1, 1, 0, '', 1, 5, '', '2017-09-20'),
(935, 2, 2, 4.5, '', 2, 5, '', '2017-09-22'),
(936, 3, 2, 0, '', 2, 5, '', '2017-09-22'),
(937, 4, 4, 0, '', 4, 5, '', '2017-09-22'),
(938, 5, 6, 0, '', 6, 4, '', '2017-09-22'),
(939, 6, 6, 5, '', 6, 4, '', '2017-09-22'),
(940, 9, 6, 0, '', 6, 3, '', '2017-09-22'),
(941, 12, 6, 4.5, '', 7, 3, '', '2017-09-22'),
(942, 13, 6, 5, '', 7, 5, '', '2017-09-22'),
(943, 18, 6, 5, '', 6, 5, '', '2017-09-22'),
(944, 24, 7, 0.5, '', 6, 3, '', '2017-09-24'),
(945, 26, 7, 2.5, '', 6, 3.5, '', '2017-09-24'),
(946, 34, 7, 2, '', 6, 2.5, '', '2017-09-24'),
(947, 35, 6, 3.5, '', 6, 2.5, '', '2017-09-25'),
(948, 36, 6, 0, '', 6, 3.5, '', '2017-09-25'),
(949, 38, 6, 4, 'Tfghgfg', 6, 3, '', '2017-09-28'),
(950, 45, 6, 5, '', 6, 0, '', '2017-09-28'),
(951, 46, 3, 0, '', 8, 5, 'Aggabsbs hahaha hahah', '2017-09-29'),
(952, 47, 3, 4, 'Ggfdggh fghhffh', 8, 3, 'Qdddggffhhcycjchucuc ycufuv', '2017-09-29'),
(953, 48, 6, 4, '', 11, 3, '', '2017-09-29'),
(954, 49, 6, 5, '', 11, 4, '', '2017-09-29'),
(955, 50, 7, 4.5, '', 11, 4, '', '2017-09-29'),
(956, 51, 7, 4, '', 11, 3, 'Gggg', '2017-09-29'),
(957, 52, 6, 4, '', 11, 2.5, 'Ffsgg', '2017-09-29'),
(958, 53, 7, 4.5, '', 11, 2, '', '2017-09-30'),
(959, 54, 7, 5, '', 11, 3, 'Foggy', '2017-09-30'),
(960, 55, 7, 0, '', 11, 1.5, '', '2017-09-30'),
(961, 56, 7, 5, '', 11, 3.5, '', '2017-09-30'),
(962, 57, 6, 5, '', 11, 3.5, 'Good ride', '2017-10-01'),
(963, 58, 6, 3.5, 'Fvvui', 11, 3, 'Fghfu', '2017-10-01'),
(964, 64, 6, 4, 'Hugging', 11, 3.5, '', '2017-10-03'),
(965, 66, 8, 5, '', 9, 5, '', '2017-10-03'),
(966, 67, 6, 5, 'Promo worked', 11, 2.5, 'Promo code test', '2017-10-03'),
(967, 68, 8, 4, '', 9, 5, 'Vkfkcjdjxir kfkckfofkrf', '2017-10-03'),
(968, 69, 6, 5, '', 11, 4, 'Highbrow', '2017-10-03'),
(969, 71, 9, 4, '', 17, 4, 'Hvjhggjjhv', '2017-10-03'),
(970, 76, 9, 5, '', 17, 3, 'Gryphon', '2017-10-03'),
(971, 78, 7, 4, 'hffggffgff', 17, 3.5, 'Hug', '2017-10-03'),
(972, 79, 7, 5, '', 17, 3, 'Hdhdhdhdhd', '2017-10-03'),
(973, 80, 7, 2.5, 'ghghhgh', 17, 4, 'Guhggcf', '2017-10-03'),
(974, 81, 9, 5, 'Cheers ', 17, 2.5, '', '2017-10-03'),
(975, 82, 9, 4, '', 17, 3, '', '2017-10-03'),
(976, 83, 9, 5, 'Thanks again ', 17, 0, '', '2017-10-03'),
(977, 84, 9, 4, '', 17, 2.5, '', '2017-10-03'),
(978, 86, 7, 4, '', 17, 3.5, '', '2017-10-04'),
(979, 87, 9, 0, '', 17, 4, 'Hgghhh', '2017-10-04'),
(980, 93, 12, 0, '', 28, 4.5, '', '2017-10-06'),
(981, 94, 12, 0, '', 28, 4.5, '', '2017-10-06'),
(982, 95, 12, 0, '', 30, 5, '', '2017-10-06'),
(983, 96, 11, 5, '', 30, 4.5, '', '2017-10-06'),
(984, 98, 11, 5, '', 30, 5, '', '2017-10-06'),
(985, 99, 9, 5, '', 17, 4.5, 'Gdvg', '2017-10-06'),
(986, 100, 13, 0, '', 31, 5, '', '2017-10-07');
INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(987, 101, 13, 0, '', 31, 5, '', '2017-10-08'),
(988, 102, 13, 5, '', 31, 4, '', '2017-10-08'),
(989, 59, 13, 4.5, '', 31, 0, '', '2017-10-08'),
(990, 103, 13, 5, 'good driver had enjoy riding with him', 31, 4, '', '2017-10-08'),
(991, 104, 13, 0, '', 31, 4, '', '2017-10-08'),
(992, 60, 13, 3.5, '', 31, 0, '', '2017-10-08'),
(993, 105, 13, 4, '', 31, 3.5, '', '2017-10-08'),
(994, 106, 13, 0, '', 31, 5, '', '2017-10-08'),
(995, 62, 13, 4, '', 31, 0, '', '2017-10-08'),
(996, 61, 13, 4, '', 31, 0, '', '2017-10-09'),
(997, 107, 8, 0, '', 9, 0, '', '2017-10-09'),
(998, 108, 8, 4.5, '', 9, 0, '', '2017-10-09'),
(999, 109, 8, 0, '', 9, 4, '', '2017-10-09'),
(1000, 110, 8, 0, '', 9, 4.5, '', '2017-10-09'),
(1001, 111, 8, 5, '', 9, 0, '', '2017-10-09'),
(1002, 112, 8, 0, '', 9, 4, '', '2017-10-09'),
(1003, 113, 8, 5, '', 9, 4, '', '2017-10-09'),
(1004, 114, 8, 3, '', 9, 5, '', '2017-10-09'),
(1005, 115, 13, 5, '', 31, 5, '', '2017-10-10'),
(1006, 116, 13, 0, '', 31, 4.5, '', '2017-10-10'),
(1007, 70, 13, 5, '', 31, 0, '', '2017-10-10'),
(1008, 117, 13, 4, '', 31, 5, '', '2017-10-10'),
(1009, 118, 13, 0, '', 31, 5, '', '2017-10-10'),
(1010, 72, 13, 5, '', 31, 0, '', '2017-10-10'),
(1011, 119, 13, 4, '', 31, 5, '', '2017-10-10'),
(1012, 120, 8, 4, 'jhhxdhd', 9, 4, '', '2017-10-10'),
(1013, 121, 8, 0, '', 9, 4.5, '', '2017-10-10'),
(1014, 73, 13, 4.5, '', 31, 0, '', '2017-10-11'),
(1015, 122, 13, 3.5, 'Thanks ', 31, 5, '', '2017-10-12'),
(1016, 123, 14, 0, '', 17, 5, 'First android', '2017-10-12'),
(1017, 77, 14, 4.5, 'my first android test', 17, 0, '', '2017-10-12'),
(1018, 124, 15, 5, '', 33, 5, '', '2017-10-12'),
(1019, 125, 14, 5, 'Thanks ', 17, 5, '', '2017-10-12'),
(1020, 126, 17, 5, '', 34, 5, 'good guy ', '2017-10-12'),
(1021, 130, 9, 5, 'Thanks ', 17, 5, '', '2017-10-12'),
(1022, 131, 9, 3.5, 'yy ', 17, 5, 'Ghfgvv', '2017-10-12'),
(1023, 132, 16, 5, '', 32, 4, '', '2017-10-12'),
(1024, 133, 9, 0, '', 17, 4, '', '2017-10-12'),
(1025, 134, 9, 0, '', 17, 4, '', '2017-10-12'),
(1026, 88, 9, 5, '', 17, 0, '', '2017-10-12'),
(1027, 139, 9, 4, '', 17, 4, '', '2017-10-12'),
(1028, 138, 16, 3.5, '', 32, 5, '', '2017-10-12'),
(1029, 90, 9, 0, '', 17, 0, '', '2017-10-12'),
(1030, 142, 9, 5, 'Gufhhh', 17, 4.5, 'Ghggvhhh', '2017-10-12'),
(1031, 143, 9, 4, '', 17, 5, '', '2017-10-12'),
(1032, 144, 9, 5, '', 17, 4, '', '2017-10-12'),
(1033, 145, 9, 3, '', 17, 4, '', '2017-10-12'),
(1034, 146, 9, 0, '', 17, 3.5, '', '2017-10-12'),
(1035, 147, 9, 4.5, '', 17, 4, 'Hjjh', '2017-10-12'),
(1036, 148, 16, 5, 'good hkk', 32, 4, '', '2017-10-12'),
(1037, 97, 16, 3.5, 'comment', 32, 0, '', '2017-10-12'),
(1038, 150, 9, 5, '', 32, 4, 'duzvi', '2017-10-12'),
(1039, 151, 9, 4, '', 17, 4, '', '2017-10-12'),
(1040, 152, 9, 3.5, '', 17, 4.5, 'Good one', '2017-10-12'),
(1041, 153, 9, 5, '', 17, 5, '', '2017-10-12'),
(1042, 155, 17, 0, '', 34, 5, '', '2017-10-12'),
(1043, 157, 16, 5, 'Nice trip ', 17, 3.5, '', '2017-10-12'),
(1044, 160, 17, 5, '', 34, 5, '', '2017-10-12'),
(1045, 169, 20, 5, '', 38, 5, '', '2017-10-13'),
(1046, 170, 17, 5, '', 34, 5, '', '2017-10-13'),
(1047, 171, 20, 5, '', 38, 5, '', '2017-10-13'),
(1048, 172, 15, 5, '', 33, 5, '', '2017-10-13'),
(1049, 173, 9, 5, '', 17, 5, '', '2017-10-13'),
(1050, 176, 15, 5, '', 33, 5, '', '2017-10-13'),
(1051, 180, 13, 0, '', 40, 4, '', '2017-10-13'),
(1052, 181, 15, 0, '', 33, 5, '', '2017-10-13'),
(1053, 185, 9, 4.5, '', 17, 3.5, '', '2017-10-13'),
(1054, 186, 9, 4.5, '', 17, 3.5, '', '2017-10-13'),
(1055, 191, 9, 4.5, '', 17, 3, 'Jfjdhdhdhd', '2017-10-13'),
(1056, 193, 9, 0, '', 17, 3.5, 'Nice ', '2017-10-13'),
(1057, 187, 16, 5, '', 32, 4.5, '', '2017-10-13'),
(1058, 195, 23, 4, '', 33, 5, '', '2017-10-13'),
(1059, 197, 15, 5, 'great driver ', 33, 5, '', '2017-10-13'),
(1060, 199, 15, 5, '', 33, 5, 'thanks ', '2017-10-13'),
(1061, 202, 23, 4, '', 32, 5, '', '2017-10-13'),
(1062, 206, 15, 0, '', 33, 5, '', '2017-10-14'),
(1063, 127, 15, 5, '', 33, 0, '', '2017-10-14'),
(1064, 209, 6, 5, '', 41, 4, '', '2017-10-14'),
(1065, 208, 15, 5, '', 33, 5, '', '2017-10-14'),
(1066, 128, 15, 5, '', 33, 0, '', '2017-10-14'),
(1067, 210, 9, 5, '', 17, 5, 'Thank you', '2017-10-14'),
(1068, 211, 24, 5, '', 17, 4, '', '2017-10-14'),
(1069, 217, 24, 5, '', 17, 5, '', '2017-10-15'),
(1070, 231, 25, 5, '', 38, 5, '', '2017-10-16'),
(1071, 234, 13, 5, '', 44, 4, '', '2017-10-16'),
(1072, 135, 13, 4, '', 44, 0, '', '2017-10-16'),
(1073, 235, 27, 5, '', 38, 5, '', '2017-10-16'),
(1074, 236, 13, 5, '', 40, 4, '', '2017-10-16'),
(1075, 137, 13, 4, '', 40, 0, '', '2017-10-16'),
(1076, 237, 13, 0, '', 40, 5, '', '2017-10-16'),
(1077, 239, 13, 0, '', 40, 4, '', '2017-10-16'),
(1078, 244, 13, 0, '', 40, 3.5, '', '2017-10-17'),
(1079, 245, 27, 5, 'good driver ', 38, 5, 'Bananas banana', '2017-10-17'),
(1080, 250, 24, 4, '', 17, 5, 'Nice trip', '2017-10-21'),
(1081, 252, 24, 5, '', 17, 4, '', '2017-10-21'),
(1082, 253, 24, 5, 'Nice trip', 17, 0, '', '2017-10-21'),
(1083, 254, 13, 0, '', 29, 4.5, '', '2017-10-23'),
(1084, 255, 28, 0, '', 46, 5, '', '2017-10-23'),
(1085, 258, 28, 0, '', 47, 5, '', '2017-10-23'),
(1086, 149, 28, 5, '', 47, 0, '', '2017-10-23'),
(1087, 260, 28, 5, 'good', 47, 5, '', '2017-10-23'),
(1088, 262, 29, 0, '', 48, 4, 'jjbj', '2017-10-23'),
(1089, 263, 29, 0, '', 48, 3.5, '', '2017-10-23'),
(1090, 265, 28, 3.5, '', 47, 5, '', '2017-10-23'),
(1091, 267, 30, 0, '', 46, 5, '', '2017-10-23'),
(1092, 156, 30, 5, '', 46, 0, '', '2017-10-23'),
(1093, 268, 6, 5, '', 17, 5, 'Good one', '2017-10-23'),
(1094, 158, 15, 5, '', 33, 0, '', '2017-10-23'),
(1095, 271, 15, 5, '', 33, 5, '', '2017-10-23'),
(1096, 159, 33, 0, '', 34, 0, '', '2017-10-23'),
(1097, 272, 33, 0, '', 34, 5, '', '2017-10-23'),
(1098, 273, 33, 0, '', 34, 5, '', '2017-10-23'),
(1099, 162, 15, 5, '', 53, 0, '', '2017-10-23'),
(1100, 275, 15, 0, '', 53, 5, '', '2017-10-23'),
(1101, 274, 33, 0, '', 34, 5, '', '2017-10-23'),
(1102, 161, 33, 5, '', 34, 0, '', '2017-10-23'),
(1103, 163, 34, 5, '', 33, 0, '', '2017-10-23'),
(1104, 279, 34, 5, '', 33, 5, '', '2017-10-23'),
(1105, 164, 33, 5, '', 34, 0, '', '2017-10-23'),
(1106, 287, 33, 0, '', 34, 5, '', '2017-10-23'),
(1107, 286, 6, 5, '', 17, 5, '', '2017-10-23'),
(1108, 165, 6, 5, '', 17, 0, '', '2017-10-23'),
(1109, 167, 35, 4, '', 32, 0, '', '2017-10-23'),
(1110, 288, 35, 0, '', 32, 4, '', '2017-10-23'),
(1111, 291, 6, 0, '', 45, 5, '', '2017-10-23'),
(1112, 293, 35, 4.5, '', 32, 5, '', '2017-10-23'),
(1113, 298, 35, 5, '', 32, 5, '', '2017-10-23'),
(1114, 301, 6, 0, '', 17, 5, '', '2017-10-23'),
(1115, 300, 33, 0, '', 34, 5, '', '2017-10-23'),
(1116, 308, 33, 0, '', 34, 5, '', '2017-10-23'),
(1117, 311, 6, 5, '', 11, 0, '', '2017-10-23'),
(1118, 175, 6, 5, '', 11, 0, '', '2017-10-23'),
(1119, 319, 6, 0, '', 11, 4.5, '', '2017-10-23'),
(1120, 320, 15, 0, '', 33, 5, '', '2017-10-23'),
(1121, 325, 15, 0, '', 53, 5, '', '2017-10-23'),
(1122, 177, 15, 4.5, '', 53, 0, '', '2017-10-23'),
(1123, 330, 9, 0, '', 17, 5, '', '2017-10-23'),
(1124, 179, 9, 4, '', 17, 0, '', '2017-10-23'),
(1125, 332, 6, 4.5, '', 17, 5, 'Good morning ', '2017-10-24'),
(1126, 333, 6, 3.5, '', 17, 3, '', '2017-10-24'),
(1127, 182, 6, 5, 'Good luck ', 11, 0, '', '2017-10-24'),
(1128, 334, 6, 5, '', 11, 5, '', '2017-10-24'),
(1129, 183, 6, 5, '', 11, 0, '', '2017-10-24'),
(1130, 336, 6, 5, '', 11, 5, '', '2017-10-24'),
(1131, 329, 33, 4.5, '', 34, 5, '', '2017-10-24'),
(1132, 337, 15, 5, '', 33, 5, '', '2017-10-24'),
(1133, 184, 15, 5, '', 33, 0, '', '2017-10-24'),
(1134, 339, 15, 5, '', 33, 3, '', '2017-10-24'),
(1135, 340, 12, 0, '', 21, 5, '', '2017-10-24'),
(1136, 341, 12, 5, '', 21, 4.5, '', '2017-10-24'),
(1137, 343, 12, 0, '', 21, 5, '', '2017-10-24'),
(1138, 188, 12, 5, '', 21, 0, '', '2017-10-24'),
(1139, 345, 12, 0, '', 27, 3.5, '', '2017-10-24'),
(1140, 347, 26, 0, '', 27, 4, '', '2017-10-24'),
(1141, 350, 27, 5, '', 49, 5, '', '2017-10-24'),
(1142, 190, 12, 3.5, '', 27, 0, '', '2017-10-24'),
(1143, 359, 12, 0, '', 27, 5, '', '2017-10-24'),
(1144, 194, 12, 4.5, '', 27, 0, '', '2017-10-24'),
(1145, 362, 12, 0, '', 60, 5, '', '2017-10-24'),
(1146, 196, 35, 5, '', 32, 0, '', '2017-10-24'),
(1147, 364, 33, 0, '', 34, 5, 'good customer ', '2017-10-24'),
(1148, 370, 7, 0, '', 11, 5, '', '2017-10-24'),
(1149, 372, 15, 0, '', 53, 5, '', '2017-10-24'),
(1150, 200, 15, 4.5, '', 53, 0, '', '2017-10-24'),
(1151, 376, 13, 0, '', 66, 4.5, '', '2017-10-25'),
(1152, 201, 13, 4, '', 66, 0, '', '2017-10-25'),
(1153, 378, 13, 0, '', 66, 4, '', '2017-10-25'),
(1154, 387, 40, 0, '', 71, 4, '', '2017-10-25'),
(1155, 203, 40, 4, '', 71, 0, '', '2017-10-25'),
(1156, 388, 43, 4, '', 67, 5, 'Dhdhdh', '2017-10-25'),
(1157, 390, 43, 0, '', 67, 5, '', '2017-10-25'),
(1158, 391, 44, 0, '', 72, 5, '', '2017-10-25'),
(1159, 392, 44, 0, '', 72, 4, '', '2017-10-25'),
(1160, 207, 44, 4, '', 72, 0, '', '2017-10-25'),
(1161, 396, 7, 0, '', 11, 4.5, '', '2017-10-25'),
(1162, 398, 6, 0, '', 11, 5, '', '2017-10-25'),
(1163, 399, 7, 0, '', 11, 4.5, '', '2017-10-25'),
(1164, 212, 6, 5, '', 11, 0, '', '2017-10-25'),
(1165, 400, 6, 0, '', 11, 4.5, '', '2017-10-25'),
(1166, 401, 6, 0, '', 11, 4.5, '', '2017-10-25'),
(1167, 402, 6, 5, '', 11, 4.5, '', '2017-10-25'),
(1168, 394, 6, 0, '', 73, 5, '', '2017-10-26'),
(1169, 406, 46, 0, '', 75, 5, '', '2017-10-26'),
(1170, 218, 33, 5, '', 34, 0, '', '2017-10-26'),
(1171, 409, 15, 0, '', 33, 5, '', '2017-10-27'),
(1172, 219, 15, 5, '', 33, 0, '', '2017-10-27'),
(1173, 410, 6, 0, '', 73, 5, '', '2017-10-27'),
(1174, 413, 6, 5, '', 73, 4.5, '', '2017-10-27'),
(1175, 418, 6, 5, '', 73, 5, '', '2017-10-27'),
(1176, 223, 15, 5, '', 33, 0, '', '2017-10-27'),
(1177, 224, 15, 5, '', 33, 0, '', '2017-10-28'),
(1178, 420, 15, 0, '', 33, 5, '', '2017-10-28'),
(1179, 421, 15, 0, '', 33, 5, '', '2017-10-28'),
(1180, 422, 6, 5, 'Good driver ðŸ˜„', 33, 4, '', '2017-10-28'),
(1181, 428, 48, 5, '', 76, 5, '', '2017-10-28'),
(1182, 429, 15, 0, '', 33, 5, '', '2017-10-28'),
(1183, 229, 15, 5, '', 33, 0, '', '2017-10-28'),
(1184, 430, 33, 0, '', 34, 5, '', '2017-10-28'),
(1185, 230, 33, 4.5, '', 34, 0, '', '2017-10-28'),
(1186, 431, 7, 0, '', 73, 5, '', '2017-10-28'),
(1187, 432, 44, 0, '', 72, 4.5, '', '2017-10-28'),
(1188, 232, 44, 4.5, '', 72, 0, '', '2017-10-28'),
(1189, 434, 35, 0, '', 32, 5, '', '2017-10-28'),
(1190, 233, 35, 0, '', 32, 0, '', '2017-10-28'),
(1191, 436, 7, 0, '', 73, 4.5, '', '2017-10-28'),
(1192, 439, 35, 0, '', 32, 5, '', '2017-10-28'),
(1193, 440, 33, 0, '', 34, 5, '', '2017-10-28'),
(1194, 441, 33, 0, '', 34, 5, '', '2017-10-28'),
(1195, 442, 15, 0, '', 33, 5, '', '2017-10-28'),
(1196, 238, 15, 5, '', 33, 0, '', '2017-10-28'),
(1197, 444, 6, 5, '', 17, 5, '', '2017-10-29'),
(1198, 456, 33, 0, '', 34, 5, 'good customer ', '2017-10-29'),
(1199, 457, 6, 0, '', 17, 4.5, '', '2017-10-29'),
(1200, 461, 13, 0, '', 80, 5, '', '2017-10-30'),
(1201, 248, 13, 4.5, '', 80, 0, '', '2017-10-30'),
(1202, 462, 13, 0, '', 80, 5, '', '2017-10-30'),
(1203, 249, 13, 5, '', 80, 0, '', '2017-10-30'),
(1204, 466, 42, 0, '', 81, 4.5, '', '2017-10-30'),
(1205, 467, 7, 0, '', 17, 5, '', '2017-10-30'),
(1206, 469, 42, 0, '', 81, 5, '', '2017-10-31'),
(1207, 470, 42, 5, '', 81, 5, '', '2017-10-31'),
(1208, 472, 42, 5, '', 81, 5, '', '2017-10-31'),
(1209, 473, 42, 4, '', 81, 5, '', '2017-10-31'),
(1210, 474, 42, 3, '', 81, 4.5, '', '2017-10-31'),
(1211, 475, 42, 0, '', 81, 5, '', '2017-10-31'),
(1212, 476, 42, 0, '', 81, 4.5, '\n', '2017-10-31'),
(1213, 477, 35, 0, '', 32, 5, 'good', '2017-10-31'),
(1214, 479, 35, 0, '', 32, 5, '', '2017-10-31'),
(1215, 261, 35, 5, '', 32, 0, '', '2017-10-31'),
(1216, 482, 15, 0, '', 33, 5, '', '2017-10-31'),
(1217, 264, 15, 5, '', 33, 0, '', '2017-10-31'),
(1218, 484, 13, 0, '', 80, 4.5, '', '2017-10-31'),
(1219, 485, 13, 0, '', 80, 4.5, '', '2017-10-31'),
(1220, 266, 13, 3.5, '', 80, 0, '', '2017-10-31'),
(1221, 488, 35, 0, '', 17, 4.5, '', '2017-10-31'),
(1222, 489, 6, 4.5, '', 32, 5, '', '2017-10-31'),
(1223, 490, 6, 4.5, '', 32, 5, '', '2017-10-31'),
(1224, 491, 35, 0, '', 17, 4.5, '', '2017-10-31'),
(1225, 496, 42, 0, '', 81, 5, '', '2017-11-01'),
(1226, 497, 6, 5, '', 17, 5, '', '2017-11-01'),
(1227, 498, 6, 5, '', 17, 5, '', '2017-11-01'),
(1228, 501, 6, 5, 'Highbrow', 17, 5, '', '2017-11-01'),
(1229, 503, 15, 0, '', 33, 5, '', '2017-11-01'),
(1230, 509, 33, 0, '', 34, 5, '', '2017-11-01'),
(1231, 280, 33, 5, '', 34, 0, '', '2017-11-01'),
(1232, 510, 6, 5, '', 17, 5, '', '2017-11-01'),
(1233, 511, 15, 0, '', 33, 5, '', '2017-11-01'),
(1234, 512, 15, 0, '', 33, 5, '', '2017-11-01'),
(1235, 513, 6, 5, '', 17, 5, '', '2017-11-02'),
(1236, 514, 6, 0, '', 17, 5, '', '2017-11-02'),
(1237, 516, 6, 0, '', 17, 3, '', '2017-11-02'),
(1238, 520, 6, 0, '', 17, 5, '', '2017-11-02'),
(1239, 515, 15, 0, '', 33, 5, '', '2017-11-02'),
(1240, 521, 6, 4.5, '', 17, 5, '', '2017-11-03'),
(1241, 523, 13, 0, '', 83, 4.5, '', '2017-11-04'),
(1242, 290, 0, 4, '', 0, 0, '', '2017-11-04'),
(1243, 524, 13, 0, '', 83, 4.5, '', '2017-11-04'),
(1244, 525, 13, 0, '', 83, 4.5, '', '2017-11-04'),
(1245, 292, 13, 5, '', 83, 0, '', '2017-11-04'),
(1246, 526, 13, 0, '', 83, 4, '', '2017-11-04'),
(1247, 528, 13, 0, '', 83, 4.5, '', '2017-11-04'),
(1248, 294, 13, 5, '', 83, 0, '', '2017-11-04'),
(1249, 529, 13, 0, '', 83, 4.5, '', '2017-11-04'),
(1250, 295, 13, 5, '', 83, 0, '', '2017-11-04'),
(1251, 530, 13, 0, '', 83, 4, '', '2017-11-04'),
(1252, 531, 49, 0, '', 83, 4.5, '', '2017-11-04'),
(1253, 532, 49, 0, '', 83, 4.5, '', '2017-11-04'),
(1254, 533, 15, 0, '', 33, 4.5, '', '2017-11-04'),
(1255, 299, 15, 4.5, '', 33, 0, '', '2017-11-04'),
(1256, 534, 6, 5, '', 17, 5, '', '2017-11-05'),
(1257, 535, 6, 5, '', 17, 4.5, '', '2017-11-05'),
(1258, 302, 15, 5, '', 33, 0, '', '2017-11-06'),
(1259, 536, 15, 0, '', 33, 5, '', '2017-11-06'),
(1260, 538, 49, 0, '', 83, 5, '', '2017-11-06'),
(1261, 303, 49, 4.5, '', 83, 0, '', '2017-11-06'),
(1262, 297, 49, 4.5, '', 83, 0, '', '2017-11-06'),
(1263, 539, 49, 0, '', 83, 5, '', '2017-11-06'),
(1264, 304, 49, 5, '', 83, 0, '', '2017-11-06'),
(1265, 540, 49, 0, '', 83, 5, '', '2017-11-06'),
(1266, 305, 49, 4, '', 83, 0, '', '2017-11-06'),
(1267, 545, 25, 0, '', 84, 0, '', '2017-11-06'),
(1268, 546, 25, 0, '', 84, 5, '', '2017-11-06'),
(1269, 547, 25, 0, '', 84, 5, '', '2017-11-06'),
(1270, 554, 49, 0, '', 83, 4.5, '', '2017-11-06'),
(1271, 310, 49, 4, '', 83, 0, '', '2017-11-06'),
(1272, 553, 25, 0, '', 84, 0, '', '2017-11-06'),
(1273, 555, 25, 0, '', 84, 5, '', '2017-11-06'),
(1274, 559, 6, 5, '', 73, 4.5, '', '2017-11-06'),
(1275, 560, 49, 0, '', 83, 5, '', '2017-11-06'),
(1276, 315, 49, 0, '', 83, 0, '', '2017-11-06'),
(1277, 561, 49, 0, '', 83, 4.5, '', '2017-11-06'),
(1278, 562, 25, 4.5, '', 84, 4.5, '', '2017-11-06'),
(1279, 563, 25, 4.5, '', 84, 0, '', '2017-11-06'),
(1280, 564, 25, 0, '', 84, 4.5, '', '2017-11-06'),
(1281, 565, 25, 5, '', 84, 0, '', '2017-11-06'),
(1282, 566, 25, 0, '', 84, 4.5, '', '2017-11-06'),
(1283, 316, 49, 4.5, '', 83, 0, '', '2017-11-06'),
(1284, 567, 49, 0, '', 83, 5, '', '2017-11-06'),
(1285, 321, 49, 5, '', 83, 0, '', '2017-11-06'),
(1286, 568, 49, 0, '', 83, 4.5, '', '2017-11-06'),
(1287, 322, 49, 4.5, '', 83, 0, '', '2017-11-06'),
(1288, 569, 49, 0, '', 83, 4.5, '', '2017-11-06'),
(1289, 323, 49, 4.5, '', 83, 0, '', '2017-11-06'),
(1290, 570, 13, 0, '', 83, 4.5, '', '2017-11-06'),
(1291, 324, 13, 4.5, '', 83, 0, '', '2017-11-06'),
(1292, 571, 25, 5, '', 84, 0, '', '2017-11-06'),
(1293, 573, 35, 0, '', 32, 5, '', '2017-11-06'),
(1294, 572, 33, 0, '', 34, 5, '', '2017-11-06'),
(1295, 575, 6, 0, '', 73, 5, '', '2017-11-06'),
(1296, 326, 33, 5, '', 34, 0, '', '2017-11-06'),
(1297, 578, 33, 0, '', 34, 5, '', '2017-11-06'),
(1298, 331, 33, 4.5, '', 34, 0, '', '2017-11-06'),
(1299, 574, 25, 5, '', 84, 0, '', '2017-11-07'),
(1300, 580, 25, 0, '', 84, 5, '', '2017-11-07'),
(1301, 581, 25, 5, '', 84, 4.5, 'I', '2017-11-07'),
(1302, 582, 15, 0, '', 33, 5, '', '2017-11-07'),
(1303, 585, 13, 0, '', 83, 4, '', '2017-11-07'),
(1304, 335, 13, 4, '', 83, 0, '', '2017-11-07'),
(1305, 589, 33, 0, '', 34, 5, '', '2017-11-08'),
(1306, 590, 15, 0, '', 33, 5, '', '2017-11-09'),
(1307, 592, 15, 0, '', 33, 5, '', '2017-11-09'),
(1308, 338, 15, 5, '', 33, 0, '', '2017-11-09'),
(1309, 327, 35, 5, '', 32, 0, '', '2017-11-09'),
(1310, 595, 6, 5, '', 73, 5, '', '2017-11-11'),
(1311, 596, 15, 0, '', 86, 5, '', '2017-11-11'),
(1312, 597, 15, 0, '', 33, 5, '', '2017-11-11'),
(1313, 342, 15, 5, '', 33, 0, '', '2017-11-11'),
(1314, 600, 33, 0, '', 34, 5, '', '2017-11-11'),
(1315, 601, 33, 0, '', 34, 5, '', '2017-11-11'),
(1316, 602, 33, 0, '', 34, 5, '', '2017-11-11'),
(1317, 344, 33, 5, '', 34, 0, '', '2017-11-11'),
(1318, 603, 33, 0, '', 34, 5, '', '2017-11-13'),
(1319, 346, 33, 4.5, '', 34, 0, '', '2017-11-13'),
(1320, 604, 6, 5, '', 73, 5, '', '2017-11-13'),
(1321, 607, 33, 0, '', 34, 5, '', '2017-11-14'),
(1322, 608, 33, 0, '', 34, 5, '', '2017-11-14'),
(1323, 609, 33, 0, '', 34, 5, '', '2017-11-14'),
(1324, 351, 33, 5, '', 34, 0, '', '2017-11-14'),
(1325, 612, 15, 0, '', 33, 5, '', '2017-11-16'),
(1326, 349, 33, 5, '', 34, 0, '', '2017-11-17');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 2, 2, 2),
(3, 1, 2, 2, 3),
(4, 1, 4, 4, 4),
(5, 1, 6, 6, 5),
(6, 1, 6, 6, 6),
(7, 1, 6, 0, 7),
(8, 1, 6, 0, 8),
(9, 1, 6, 6, 9),
(10, 1, 6, 0, 10),
(11, 1, 6, 0, 11),
(12, 1, 6, 7, 12),
(13, 1, 6, 7, 13),
(14, 1, 6, 0, 14),
(15, 1, 6, 0, 15),
(16, 1, 6, 0, 16),
(17, 1, 6, 0, 17),
(18, 1, 6, 6, 18),
(19, 1, 6, 0, 19),
(20, 1, 6, 6, 20),
(21, 1, 7, 6, 21),
(22, 1, 7, 0, 22),
(23, 1, 7, 6, 23),
(24, 1, 7, 6, 24),
(25, 1, 7, 0, 25),
(26, 1, 7, 6, 26),
(27, 1, 6, 0, 27),
(28, 1, 6, 6, 28),
(29, 1, 6, 0, 29),
(30, 1, 6, 0, 30),
(31, 1, 6, 0, 31),
(32, 1, 6, 0, 32),
(33, 1, 6, 0, 33),
(34, 1, 6, 0, 0),
(35, 1, 6, 0, 0),
(36, 1, 7, 0, 0),
(37, 1, 7, 6, 34),
(38, 1, 6, 6, 35),
(39, 1, 6, 6, 36),
(40, 1, 6, 0, 37),
(41, 1, 6, 6, 38),
(42, 1, 6, 0, 39),
(43, 1, 6, 6, 40),
(44, 1, 6, 0, 41),
(45, 1, 6, 0, 42),
(46, 1, 6, 0, 43),
(47, 1, 6, 0, 44),
(48, 1, 6, 6, 45),
(49, 1, 3, 8, 46),
(50, 1, 3, 8, 47),
(51, 1, 6, 11, 48),
(52, 1, 6, 11, 49),
(53, 1, 7, 11, 50),
(54, 1, 7, 11, 51),
(55, 1, 6, 11, 52),
(56, 1, 7, 11, 53),
(57, 1, 7, 11, 54),
(58, 1, 7, 11, 55),
(59, 1, 7, 11, 56),
(60, 1, 6, 11, 57),
(61, 1, 6, 11, 58),
(62, 1, 6, 0, 59),
(63, 1, 6, 11, 60),
(64, 1, 6, 0, 61),
(65, 1, 6, 0, 62),
(66, 1, 6, 0, 63),
(67, 1, 6, 11, 64),
(68, 1, 8, 0, 65),
(69, 1, 8, 9, 66),
(70, 1, 6, 11, 67),
(71, 1, 8, 9, 68),
(72, 1, 6, 11, 69),
(73, 1, 9, 0, 70),
(74, 1, 9, 17, 71),
(75, 1, 9, 0, 72),
(76, 1, 9, 0, 73),
(77, 1, 9, 0, 74),
(78, 1, 9, 0, 75),
(79, 1, 9, 17, 76),
(80, 1, 9, 0, 77),
(81, 1, 7, 17, 78),
(82, 1, 7, 17, 79),
(83, 1, 7, 17, 80),
(84, 1, 9, 17, 81),
(85, 1, 9, 17, 82),
(86, 1, 9, 17, 83),
(87, 1, 9, 0, 84),
(88, 1, 9, 17, 85),
(89, 1, 7, 17, 86),
(90, 1, 9, 17, 87),
(91, 1, 10, 22, 88),
(92, 1, 10, 22, 89),
(93, 1, 10, 22, 90),
(94, 1, 10, 22, 91),
(95, 1, 11, 27, 92),
(96, 1, 12, 28, 93),
(97, 1, 12, 28, 94),
(98, 1, 12, 30, 95),
(99, 1, 11, 30, 96),
(100, 1, 11, 0, 97),
(101, 1, 11, 30, 98),
(102, 1, 9, 17, 99),
(103, 1, 13, 31, 100),
(104, 1, 13, 31, 101),
(105, 1, 13, 31, 102),
(106, 1, 13, 31, 103),
(107, 1, 13, 31, 104),
(108, 1, 13, 31, 105),
(109, 1, 13, 31, 106),
(110, 1, 8, 9, 107),
(111, 1, 8, 9, 108),
(112, 1, 8, 31, 109),
(113, 1, 8, 9, 110),
(114, 1, 8, 9, 111),
(115, 1, 8, 0, 112),
(116, 1, 8, 9, 113),
(117, 1, 8, 9, 114),
(118, 1, 13, 31, 115),
(119, 1, 13, 31, 116),
(120, 1, 13, 31, 117),
(121, 1, 13, 31, 118),
(122, 1, 13, 31, 119),
(123, 1, 8, 9, 120),
(124, 1, 8, 9, 121),
(125, 1, 13, 31, 122),
(126, 1, 14, 17, 123),
(127, 1, 15, 33, 124),
(128, 1, 14, 17, 125),
(129, 1, 17, 34, 126),
(130, 1, 17, 34, 127),
(131, 1, 17, 0, 128),
(132, 1, 9, 0, 129),
(133, 1, 9, 17, 130),
(134, 1, 9, 17, 131),
(135, 1, 16, 32, 132),
(136, 1, 9, 17, 133),
(137, 1, 9, 17, 134),
(138, 1, 15, 33, 135),
(139, 1, 17, 34, 136),
(140, 1, 9, 17, 137),
(141, 1, 16, 32, 138),
(142, 1, 9, 17, 139),
(143, 1, 9, 17, 140),
(144, 1, 9, 17, 141),
(145, 1, 9, 17, 142),
(146, 1, 9, 17, 143),
(147, 1, 9, 17, 144),
(148, 1, 9, 17, 145),
(149, 1, 9, 17, 146),
(150, 1, 9, 17, 147),
(151, 1, 16, 32, 148),
(152, 1, 9, 0, 149),
(153, 1, 9, 32, 150),
(154, 1, 9, 17, 151),
(155, 1, 9, 17, 152),
(156, 1, 9, 17, 153),
(157, 1, 17, 34, 154),
(158, 1, 17, 34, 155),
(159, 1, 9, 17, 156),
(160, 1, 16, 17, 157),
(161, 1, 16, 17, 158),
(162, 1, 17, 34, 159),
(163, 1, 17, 34, 160),
(164, 1, 16, 0, 161),
(165, 1, 15, 0, 162),
(166, 1, 16, 33, 163),
(167, 1, 15, 0, 164),
(168, 1, 20, 37, 165),
(169, 1, 20, 0, 166),
(170, 1, 20, 0, 167),
(171, 1, 20, 0, 168),
(172, 1, 20, 38, 169),
(173, 1, 17, 34, 170),
(174, 1, 20, 38, 171),
(175, 1, 15, 33, 172),
(176, 1, 9, 17, 173),
(177, 1, 21, 0, 174),
(178, 1, 21, 0, 175),
(179, 1, 15, 33, 176),
(180, 1, 9, 0, 177),
(181, 1, 9, 17, 178),
(182, 1, 13, 40, 179),
(183, 1, 13, 40, 180),
(184, 1, 15, 33, 181),
(185, 1, 9, 17, 182),
(186, 1, 9, 0, 183),
(187, 1, 9, 17, 184),
(188, 1, 9, 17, 185),
(189, 1, 9, 17, 186),
(190, 1, 16, 32, 187),
(191, 1, 9, 17, 188),
(192, 1, 9, 0, 189),
(193, 1, 9, 17, 190),
(194, 1, 9, 17, 191),
(195, 1, 9, 0, 192),
(196, 1, 9, 17, 193),
(197, 1, 15, 32, 194),
(198, 1, 23, 33, 195),
(199, 1, 15, 0, 196),
(200, 1, 15, 33, 197),
(201, 1, 23, 0, 198),
(202, 1, 15, 33, 199),
(203, 1, 9, 17, 200),
(204, 1, 23, 33, 201),
(205, 1, 23, 32, 202),
(206, 1, 9, 0, 203),
(207, 1, 9, 0, 204),
(208, 1, 9, 0, 205),
(209, 1, 15, 33, 206),
(210, 1, 16, 0, 207),
(211, 1, 15, 33, 208),
(212, 1, 6, 41, 209),
(213, 1, 9, 17, 210),
(214, 1, 24, 17, 211),
(215, 1, 24, 0, 212),
(216, 1, 24, 17, 213),
(217, 1, 24, 0, 214),
(218, 1, 24, 0, 215),
(219, 1, 15, 0, 216),
(220, 1, 24, 17, 217),
(221, 1, 24, 0, 218),
(222, 1, 24, 11, 219),
(223, 1, 6, 0, 220),
(224, 1, 6, 0, 221),
(225, 1, 15, 33, 222),
(226, 1, 9, 0, 223),
(227, 1, 24, 0, 224),
(228, 1, 24, 0, 225),
(229, 1, 6, 0, 226),
(230, 1, 6, 0, 227),
(231, 1, 6, 0, 228),
(232, 1, 6, 0, 229),
(233, 1, 6, 0, 230),
(234, 1, 25, 38, 231),
(235, 1, 6, 0, 232),
(236, 1, 13, 0, 233),
(237, 1, 13, 44, 234),
(238, 1, 27, 38, 235),
(239, 1, 13, 40, 236),
(240, 1, 13, 40, 237),
(241, 1, 13, 0, 238),
(242, 1, 13, 40, 239),
(243, 1, 13, 40, 240),
(244, 1, 13, 0, 241),
(245, 1, 13, 0, 242),
(246, 1, 13, 0, 243),
(247, 1, 13, 40, 244),
(248, 1, 27, 38, 245),
(249, 1, 15, 33, 246),
(250, 1, 24, 0, 247),
(251, 1, 24, 0, 248),
(252, 1, 24, 17, 249),
(253, 1, 24, 17, 250),
(254, 1, 24, 0, 251),
(255, 1, 24, 17, 252),
(256, 1, 24, 17, 253),
(257, 1, 13, 29, 254),
(258, 1, 28, 46, 255),
(259, 1, 28, 0, 256),
(260, 1, 27, 0, 257),
(261, 1, 28, 47, 258),
(262, 1, 28, 47, 259),
(263, 1, 28, 47, 260),
(264, 1, 29, 0, 261),
(265, 1, 29, 48, 262),
(266, 1, 29, 48, 263),
(267, 1, 28, 47, 264),
(268, 1, 28, 47, 265),
(269, 1, 28, 47, 266),
(270, 1, 30, 46, 267),
(271, 1, 6, 17, 268),
(272, 1, 24, 0, 269),
(273, 1, 15, 0, 270),
(274, 1, 15, 33, 271),
(275, 1, 33, 34, 272),
(276, 1, 33, 34, 273),
(277, 1, 33, 34, 274),
(278, 1, 15, 53, 275),
(279, 1, 35, 0, 276),
(280, 1, 35, 0, 277),
(281, 1, 33, 34, 278),
(282, 1, 34, 33, 279),
(283, 1, 34, 0, 280),
(284, 1, 34, 0, 281),
(285, 1, 15, 0, 282),
(286, 1, 15, 0, 283),
(287, 1, 33, 34, 284),
(288, 1, 15, 0, 285),
(289, 1, 6, 17, 286),
(290, 1, 33, 34, 287),
(291, 1, 35, 32, 288),
(292, 1, 33, 34, 289),
(293, 1, 34, 0, 290),
(294, 1, 6, 45, 291),
(295, 1, 33, 0, 292),
(296, 1, 35, 32, 293),
(297, 1, 33, 0, 294),
(298, 1, 33, 0, 295),
(299, 1, 6, 0, 296),
(300, 1, 15, 0, 297),
(301, 1, 35, 32, 298),
(302, 1, 15, 0, 299),
(303, 1, 33, 34, 300),
(304, 1, 6, 17, 301),
(305, 1, 35, 0, 302),
(306, 1, 34, 0, 303),
(307, 1, 6, 0, 304),
(308, 1, 35, 0, 305),
(309, 1, 6, 11, 306),
(310, 1, 15, 0, 307),
(311, 1, 33, 34, 308),
(312, 1, 34, 0, 309),
(313, 1, 34, 0, 310),
(314, 1, 6, 11, 311),
(315, 1, 15, 0, 312),
(316, 1, 34, 0, 313),
(317, 1, 15, 0, 314),
(318, 1, 15, 0, 315),
(319, 1, 15, 0, 316),
(320, 1, 15, 0, 317),
(321, 1, 15, 0, 318),
(322, 1, 6, 11, 319),
(323, 1, 15, 33, 320),
(324, 1, 15, 33, 321),
(325, 1, 15, 0, 322),
(326, 1, 34, 0, 323),
(327, 1, 34, 0, 324),
(328, 1, 15, 53, 325),
(329, 1, 33, 0, 326),
(330, 1, 33, 0, 327),
(331, 1, 33, 0, 328),
(332, 1, 33, 34, 329),
(333, 1, 9, 17, 330),
(334, 1, 6, 0, 331),
(335, 1, 6, 17, 332),
(336, 1, 6, 17, 333),
(337, 1, 6, 11, 334),
(338, 1, 6, 0, 335),
(339, 1, 6, 11, 336),
(340, 1, 15, 33, 337),
(341, 1, 6, 11, 338),
(342, 1, 15, 33, 339),
(343, 1, 12, 21, 340),
(344, 1, 12, 21, 341),
(345, 1, 12, 21, 342),
(346, 1, 12, 21, 343),
(347, 1, 12, 21, 344),
(348, 1, 12, 27, 345),
(349, 1, 20, 0, 346),
(350, 1, 26, 27, 347),
(351, 1, 27, 0, 348),
(352, 1, 26, 0, 349),
(353, 1, 27, 49, 350),
(354, 1, 26, 0, 351),
(355, 1, 27, 0, 352),
(356, 1, 27, 0, 353),
(357, 1, 27, 0, 354),
(358, 1, 27, 0, 355),
(359, 1, 27, 0, 356),
(360, 1, 27, 0, 357),
(361, 1, 27, 0, 358),
(362, 1, 12, 27, 359),
(363, 1, 12, 0, 360),
(364, 1, 12, 0, 361),
(365, 1, 12, 60, 362),
(366, 1, 35, 32, 363),
(367, 1, 33, 34, 364),
(368, 1, 33, 34, 365),
(369, 1, 27, 0, 366),
(370, 1, 27, 0, 367),
(371, 1, 27, 0, 368),
(372, 1, 7, 0, 369),
(373, 1, 7, 11, 370),
(374, 1, 7, 0, 371),
(375, 1, 15, 53, 372),
(376, 1, 36, 0, 373),
(377, 1, 36, 0, 374),
(378, 1, 27, 0, 375),
(379, 1, 13, 66, 376),
(380, 1, 13, 66, 377),
(381, 1, 13, 66, 378),
(382, 1, 37, 0, 379),
(383, 1, 37, 0, 380),
(384, 1, 15, 0, 381),
(385, 1, 37, 0, 382),
(386, 1, 37, 0, 383),
(387, 1, 37, 0, 384),
(388, 1, 37, 0, 385),
(389, 1, 37, 0, 386),
(390, 1, 40, 71, 387),
(391, 1, 43, 67, 388),
(392, 1, 43, 67, 389),
(393, 1, 43, 67, 390),
(394, 1, 44, 72, 391),
(395, 1, 44, 72, 392),
(396, 1, 6, 0, 393),
(397, 1, 6, 73, 394),
(398, 1, 15, 33, 395),
(399, 1, 7, 11, 396),
(400, 1, 35, 0, 397),
(401, 1, 6, 11, 398),
(402, 1, 7, 11, 399),
(403, 1, 6, 11, 400),
(404, 1, 6, 11, 401),
(405, 1, 6, 11, 402),
(406, 1, 6, 73, 403),
(407, 1, 15, 33, 404),
(408, 1, 24, 0, 405),
(409, 1, 46, 75, 406),
(410, 1, 33, 34, 407),
(411, 1, 33, 0, 408),
(412, 1, 15, 33, 409),
(413, 1, 6, 73, 410),
(414, 1, 6, 0, 411),
(415, 1, 6, 0, 412),
(416, 1, 6, 73, 413),
(417, 1, 6, 0, 414),
(418, 1, 6, 0, 415),
(419, 1, 6, 0, 416),
(420, 1, 6, 0, 417),
(421, 1, 6, 73, 418),
(422, 1, 15, 33, 419),
(423, 1, 15, 33, 420),
(424, 1, 15, 33, 421),
(425, 1, 6, 33, 422),
(426, 1, 47, 0, 423),
(427, 1, 47, 0, 424),
(428, 1, 35, 0, 425),
(429, 1, 48, 0, 426),
(430, 1, 48, 0, 427),
(431, 1, 48, 76, 428),
(432, 1, 15, 33, 429),
(433, 1, 33, 34, 430),
(434, 1, 7, 73, 431),
(435, 1, 44, 72, 432),
(436, 1, 35, 32, 433),
(437, 1, 35, 32, 434),
(438, 1, 35, 32, 435),
(439, 1, 7, 73, 436),
(440, 1, 35, 0, 437),
(441, 1, 15, 0, 438),
(442, 1, 35, 32, 439),
(443, 1, 33, 34, 440),
(444, 1, 33, 34, 441),
(445, 1, 15, 33, 442),
(446, 1, 33, 34, 443),
(447, 1, 6, 17, 444),
(448, 1, 7, 17, 445),
(449, 1, 33, 34, 446),
(450, 1, 6, 0, 447),
(451, 1, 7, 0, 448),
(452, 1, 7, 0, 449),
(453, 1, 7, 0, 450),
(454, 1, 33, 34, 451),
(455, 1, 33, 34, 452),
(456, 1, 33, 34, 453),
(457, 1, 15, 33, 454),
(458, 1, 33, 0, 455),
(459, 1, 33, 34, 456),
(460, 1, 6, 17, 457),
(461, 1, 25, 0, 458),
(462, 1, 25, 77, 459),
(463, 1, 13, 0, 460),
(464, 1, 13, 80, 461),
(465, 1, 13, 80, 462),
(466, 1, 13, 80, 463),
(467, 1, 42, 0, 464),
(468, 1, 42, 0, 465),
(469, 1, 42, 81, 466),
(470, 1, 7, 17, 467),
(471, 1, 13, 0, 468),
(472, 1, 42, 81, 469),
(473, 1, 42, 81, 470),
(474, 1, 42, 0, 471),
(475, 1, 42, 81, 472),
(476, 1, 42, 81, 473),
(477, 1, 42, 81, 474),
(478, 1, 42, 81, 475),
(479, 1, 42, 81, 476),
(480, 1, 35, 32, 477),
(481, 1, 6, 0, 478),
(482, 1, 35, 32, 479),
(483, 1, 15, 32, 480),
(484, 1, 15, 32, 481),
(485, 1, 15, 33, 482),
(486, 1, 13, 0, 483),
(487, 1, 13, 80, 484),
(488, 1, 13, 80, 485),
(489, 1, 13, 0, 486),
(490, 1, 6, 32, 487),
(491, 1, 35, 17, 488),
(492, 1, 6, 32, 489),
(493, 1, 6, 32, 490),
(494, 1, 35, 17, 491),
(495, 1, 15, 0, 492),
(496, 1, 15, 0, 493),
(497, 1, 33, 34, 494),
(498, 1, 42, 0, 495),
(499, 1, 42, 81, 496),
(500, 1, 6, 17, 497),
(501, 1, 6, 17, 498),
(502, 1, 15, 33, 499),
(503, 1, 33, 34, 500),
(504, 1, 6, 17, 501),
(505, 1, 15, 0, 502),
(506, 1, 15, 33, 503),
(507, 1, 33, 0, 504),
(508, 1, 33, 0, 505),
(509, 1, 33, 0, 506),
(510, 1, 33, 0, 507),
(511, 1, 33, 0, 508),
(512, 1, 33, 34, 509),
(513, 1, 6, 17, 510),
(514, 1, 15, 33, 511),
(515, 1, 15, 33, 512),
(516, 1, 6, 17, 513),
(517, 1, 6, 17, 514),
(518, 1, 15, 33, 515),
(519, 1, 6, 17, 516),
(520, 1, 6, 17, 517),
(521, 1, 6, 17, 518),
(522, 1, 6, 0, 519),
(523, 1, 6, 17, 520),
(524, 1, 6, 17, 521),
(525, 1, 13, 0, 522),
(526, 1, 13, 83, 523),
(527, 1, 13, 83, 524),
(528, 1, 13, 83, 525),
(529, 1, 13, 83, 526),
(530, 1, 13, 0, 527),
(531, 1, 13, 83, 528),
(532, 1, 13, 83, 529),
(533, 1, 13, 83, 530),
(534, 1, 49, 83, 531),
(535, 1, 49, 83, 532),
(536, 1, 15, 33, 533),
(537, 1, 6, 17, 534),
(538, 1, 6, 17, 535),
(539, 1, 15, 33, 536),
(540, 1, 49, 83, 537),
(541, 1, 49, 83, 538),
(542, 1, 49, 83, 539),
(543, 1, 49, 83, 540),
(544, 1, 25, 0, 541),
(545, 1, 25, 0, 542),
(546, 1, 25, 0, 543),
(547, 1, 25, 0, 544),
(548, 1, 25, 84, 545),
(549, 1, 25, 84, 546),
(550, 1, 25, 0, 547),
(551, 1, 49, 84, 548),
(552, 1, 49, 0, 549),
(553, 1, 49, 0, 550),
(554, 1, 49, 0, 551),
(555, 1, 25, 84, 552),
(556, 1, 25, 84, 553),
(557, 1, 49, 83, 554),
(558, 1, 25, 84, 555),
(559, 1, 25, 84, 556),
(560, 1, 6, 0, 557),
(561, 1, 6, 0, 558),
(562, 1, 6, 73, 559),
(563, 1, 49, 83, 560),
(564, 1, 49, 83, 561),
(565, 1, 25, 84, 562),
(566, 1, 25, 84, 563),
(567, 1, 25, 0, 564),
(568, 1, 25, 84, 565),
(569, 1, 25, 84, 566),
(570, 1, 49, 83, 567),
(571, 1, 49, 83, 568),
(572, 1, 49, 83, 569),
(573, 1, 13, 83, 570),
(574, 1, 25, 84, 571),
(575, 1, 33, 34, 572),
(576, 1, 35, 32, 573),
(577, 1, 25, 84, 574),
(578, 1, 6, 73, 575),
(579, 1, 6, 73, 576),
(580, 1, 33, 0, 577),
(581, 1, 33, 34, 578),
(582, 1, 33, 0, 579),
(583, 1, 25, 84, 580),
(584, 1, 25, 84, 581),
(585, 1, 15, 33, 582),
(586, 1, 13, 0, 583),
(587, 1, 13, 0, 584),
(588, 1, 13, 83, 585),
(589, 1, 31, 0, 586),
(590, 1, 31, 0, 587),
(591, 1, 31, 0, 588),
(592, 1, 33, 34, 589),
(593, 1, 15, 33, 590),
(594, 1, 6, 0, 591),
(595, 1, 15, 33, 592),
(596, 1, 35, 0, 593),
(597, 1, 15, 33, 594),
(598, 1, 6, 73, 595),
(599, 1, 15, 86, 596),
(600, 1, 15, 33, 597),
(601, 1, 6, 0, 598),
(602, 1, 6, 0, 599),
(603, 1, 33, 34, 600),
(604, 1, 33, 34, 601),
(605, 1, 33, 34, 602),
(606, 1, 33, 0, 0),
(607, 1, 33, 0, 0),
(608, 1, 33, 34, 603),
(609, 1, 6, 73, 604),
(610, 1, 15, 33, 605),
(611, 1, 33, 0, 606),
(612, 1, 33, 34, 607),
(613, 1, 33, 34, 608),
(614, 1, 33, 34, 609),
(615, 1, 33, 34, 610),
(616, 1, 6, 73, 611),
(617, 1, 15, 33, 612),
(618, 1, 33, 0, 613),
(619, 1, 33, 0, 614),
(620, 1, 33, 0, 615),
(621, 1, 33, 0, 616),
(622, 1, 33, 0, 617),
(623, 1, 33, 0, 618),
(624, 1, 33, 0, 619),
(625, 1, 33, 0, 620),
(626, 1, 33, 0, 621),
(627, 1, 33, 0, 622),
(628, 1, 33, 0, 623),
(629, 1, 35, 0, 624);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'user .', 'user12@g.com', '+911234561234', 'qwerty', 'http://apporio.org/TagYourRide/tagyourride/uploads/swift_file65.jpeg', '', 0, '100', 'Wednesday, Sep 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-09-20', 1),
(2, 1, 'user .', 'user34@g.com', '+911592648731', 'qwerty', '', '', 0, '0', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-09-22', 1),
(3, 1, 'register .', 'register@q.com', '+911234560000', 'qwerty', '', '', 0, '0', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4', 0, '', 1, '2017-09-22', 1),
(4, 1, 'fjdggfgg .', 'bgdhftyy45@t.com', '+919696969696', '123456', '', '', 0, '0', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-09-22', 1),
(5, 1, 'shilpa .', 'shilpa@apporio.com', '+918130039030', 'qwerty', '', '', 0, '0', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-22', 1),
(6, 1, 'David Satande .', 'user@tagyourride.co.za', '+27835156147', '', 'http://apporio.org/TagYourRide/tagyourride/uploads/swift_file68.jpeg', '', 0, '81', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '10214919884891101', '10214919884891101@facebook.com', 'https://graph.facebook.com/10214919884891101/picture?width=640&height=640', 'David Tatenda Satande', '', '101280090012019087408', 'David Satande', 'davidtatenda@gmail.com', 'https://plus.google.com/_/focus/photos/private/AIbEiAIAAABDCLCwsffW4vLhESILdmNhcmRfcGhvdG8qKDU4ZDU3MTlhNTg2MTU1YTMyMWMzYWFkMDkyOTA4NzRkMjQyYTRiZTEwAeMxkTTWeqX0t-xZfmHbdHpH_vhC', '', '', 0, 0, '3.85483870968', 0, '', 3, '2017-09-22', 1),
(7, 1, 'Tagyourride Tagyourride ', 'user@tyr.co.za', '+27624503508', '', '', '', 0, '0.00', 'Friday, Sep 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '103928517190887232138', 'Tagyourride Tagyourride', 'tagyourride1@gmail.com', 'https://lh4.googleusercontent.com/-T6rdkm5jn9E/AAAAAAAAAAI/AAAAAAAAAAA/ACnBePaWkmPo_qaKiuEdWQUAGo6FM44avw/s400/photo.jpg', '', '', 0, 0, '4.26829268293', 0, '', 3, '2017-09-22', 1),
(8, 1, 'driv .', 'driv@g.com', '+911234567000', 'qwerty', '', '', 0, '0', 'Tuesday, Oct 3', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3.07692307692', 0, '', 1, '2017-10-03', 1),
(9, 1, 'David Satande .', 'davidtatenda@gmail.com', '+27835156147', 'tatenda', 'http://apporio.org/TagYourRide/tagyourride/uploads/swift_file72.jpeg', '', 0, '80', 'Tuesday, Oct 3', '', 0, 0, 0, 0, 0, '10214919884891101', '10214919884891101@facebook.com', 'https://graph.facebook.com/10214919884891101/picture?width=640&height=640', 'David Tatenda Satande', '', '101280090012019087408', 'David Satande', 'davidtatenda@gmail.com', 'https://plus.google.com/_/focus/photos/private/AIbEiAIAAABDCLCwsffW4vLhESILdmNhcmRfcGhvdG8qKDU4ZDU3MTlhNTg2MTU1YTMyMWMzYWFkMDkyOTA4NzRkMjQyYTRiZTEwAeMxkTTWeqX0t-xZfmHbdHpH_vhC', '', '', 0, 1, '3.640625', 0, '', 3, '2017-10-03', 1),
(10, 1, 'Samir Apporio .', '', '+917015363199', '', '', '', 0, '0', 'Wednesday, Oct 4', '', 0, 0, 0, 0, 0, '', '', '', '', '', '104580775658666397259', 'Samir Apporio', 'samir@apporio.com', 'https://lh3.googleusercontent.com/-cT--P5Hqs10/AAAAAAAAAAI/AAAAAAAAAAU/jefhN8D_EXk/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-10-04', 1),
(11, 1, 'Madhu Goel .', '', '+919991617237', '', '', '', 0, '0', 'Wednesday, Oct 4', '', 0, 0, 0, 0, 0, '', '', '', '', '', '109515782763297886389', 'Madhu Goel', 'madhugoel1842@gmail.com', 'https://lh6.googleusercontent.com/-yv_u_bhAKKY/AAAAAAAAAAI/AAAAAAAAAAU/pKr_2rVzOHY/photo.jpg', '', '', 0, 0, '3.9', 0, '', 3, '2017-10-04', 1),
(12, 1, 'Pooja Apporio .', '', '+919898989898', '', '', '', 0, '0.00', 'Friday, Oct 6', '', 0, 0, 0, 0, 0, '', '', '', '', '', '117223531907833843873', 'Pooja Apporio', 'pooja@apporio.com', 'null', '', '', 0, 0, '3.5', 0, '', 3, '2017-10-06', 1),
(13, 1, 'Apporio devices .', 'kaka@gmail.com', '+918080808080', 'HKAULG', '', '', 0, '200', 'Saturday, Oct 7', '', 0, 0, 0, 0, 0, '', '', '', '', '', '111976537605501585996', 'Apporio devices', 'apporiodevices@gmail.com', 'null', '', '', 0, 0, '2.99038461538', 0, '', 1, '2017-10-07', 1),
(14, 1, 'tatenda@tyr.co.za  .', 'tatenda@tyr.co.za', '+270636238799', 'tatenda', '', '', 0, '0', 'Wednesday, Oct 11', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3.33333333333', 0, '', 1, '2017-10-11', 1),
(15, 1, 'Rogerant Tshibangu .', 'rtshibangu@gmail.com', '+27614950420', 'kapinga12', '', '', 0, '0', 'Wednesday, Oct 11', '', 0, 0, 0, 0, 0, '', '', '', '', '', '113504333689561338454', 'Rogerant Tshibangu', 'rtshibangu@gmail.com', 'https://lh4.googleusercontent.com/-IURineo7rrE/AAAAAAAAAAI/AAAAAAAAH_M/DtqWr27G614/photo.jpg', '', '', 0, 0, '3.25', 0, '', 1, '2017-10-11', 1),
(16, 1, 'Roy Gwindi .', 'roymakg@gmail.com', '+270786022244', 'P@ssword01', '', '', 0, '0', 'Wednesday, Oct 11', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '3.5', 0, '', 1, '2017-10-11', 1),
(17, 1, 'dred .', 'dredarsenic@gmail.com', '+27736721260', 'DRE988$d', '', '', 0, '0', 'Wednesday, Oct 11', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-10-11', 1),
(18, 1, 'Tapiwanashe Mushosho', '', '+263719302982', '', '', '', 0, '0', 'Wednesday, Oct 11', '', 0, 0, 0, 0, 0, '10209944516196358', 'timushosho@gmail.com', 'http://graph.facebook.com/10209944516196358/picture?type=large', 'Tapiwanashe', 'Mushosho', '', '', '', '', '', '', 0, 0, '', 0, '', 2, '2017-10-11', 1),
(20, 1, 'user .', 'user@t.com', '+271234567890', '58vuxp', 'http://apporio.org/TagYourRide/tagyourride/uploads/swift_file70.jpeg', '', 0, '0', 'Friday, Oct 13', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-10-13', 1),
(19, 1, 'tapiwa mushosho .', '', '+2630774302982', '', '', '', 0, '0', 'Thursday, Oct 12', '', 0, 0, 0, 0, 0, '', '', '', '', '', '102858735795933533282', 'tapiwa mushosho', 'timshosho@gmail.com', 'https://lh6.googleusercontent.com/-dDcgfH694yE/AAAAAAAAAAI/AAAAAAAABQQ/hLoJtG_nHR8/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-10-12', 1),
(21, 1, 'vccvv .', 'bbb@gmail.con', '+91606060606060', '123456', '', '', 0, '0', 'Friday, Oct 13', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-13', 1),
(22, 1, 'zznn .', 'bzbzh@gmail.com', '+9199999999999', '123456', '', '', 0, '0', 'Friday, Oct 13', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-13', 1),
(23, 1, 'Annet Matebwe', 'sonu26tanwar@gmail.com', '+27736226812', '4mwkkc', '', '', 0, '0', 'Friday, Oct 13', '', 0, 0, 0, 0, 0, '1745008585570895', 'annetmatebwe@gmail.com', 'http://graph.facebook.com/1745008585570895/picture?type=large', 'Annet', 'Matebwe', '', '', '', '', '', '', 0, 0, '5', 0, '', 2, '2017-10-13', 1),
(24, 1, 'Tagyourride Tagyourride ', '', '+270835156147', '', '', '', 0, '0', 'Saturday, Oct 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '103928517190887232138', 'Tagyourride Tagyourride', 'tagyourride1@gmail.com', 'https://lh4.googleusercontent.com/-T6rdkm5jn9E/AAAAAAAAAAI/AAAAAAAAAAA/ACnBePaWkmPo_qaKiuEdWQUAGo6FM44avw/s400/photo.jpg', '', '', 0, 0, '4.5', 0, '', 3, '2017-10-14', 2),
(25, 1, 'qwerty .', 'q@9.com', '+271234554321', 'qwerty', 'http://apporio.org/TagYourRide/tagyourride/uploads/swift_file73.jpeg', '', 0, '0.00', 'Monday, Oct 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '2.86666666667', 0, '', 1, '2017-10-16', 1),
(26, 1, 'Manish Apporio .', '', '+278888888888888', '', '', '', 0, '0', 'Monday, Oct 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '113228268447579718411', 'Manish Apporio', 'manishsharma@apporio.com', 'null', '', '', 0, 0, '4', 0, '', 3, '2017-10-16', 1),
(27, 1, 'asdf .', 'asd@9.com', '+271234561234', 'ZRKD6N', '', '', 0, '0', 'Monday, Oct 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-10-16', 1),
(28, 1, 'qwerty .', 'qwerty@gmail.com', '+278874531856', 'qwerty', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4', 0, '', 1, '2017-10-23', 1),
(29, 1, 'Manish kumar Yadav .', '', '+276363636363', '', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '114388820832634526838', 'Manish kumar Yadav', 'manishkr.yadav132@gmail.com', 'https://lh4.googleusercontent.com/-S_A8t_v8Wuc/AAAAAAAAAAI/AAAAAAAAACc/FrEXYgtS_oY/photo.jpg', '', '', 0, 0, '3.75', 0, '', 3, '2017-10-23', 2),
(30, 1, 'poiuyt .', 'poiuyt@gmail.com', '+27658486998', 'poiuyt', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-10-23', 1),
(31, 1, 'dhhxhz .', 'zhzvvzvz@gmail.com', '+27978787787699', '123456', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-23', 1),
(32, 1, 'dbdvdvv .', 'sbcbsv@g.com', '+27898898988', '123456', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-23', 1),
(33, 1, 'dred .', 'dredarsenic@gmail.com', '+270736721260', 'DRE988$d', 'http://apporio.org/TagYourRide/tagyourride/uploads/1509995148230.jpg', '', 0, '0.00', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '3.48484848485', 0, '', 1, '2017-10-23', 1),
(34, 1, 'Andiswa Sindane .', '', '+27731709711', '', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '1281381935301218', 'sindane92@gmail.com', 'http://graph.facebook.com/1281381935301218/picture?type=large', 'Andiswa', 'Tshibangu', '110750737893306962981', 'Andiswa Sindane', 'sindane92@gmail.com', 'null', '', '', 0, 0, '2.5', 0, '', 2, '2017-10-23', 1),
(35, 1, 'Roy .', 'roymakg@gmail.com', '+270786022243', 'P@ssword01', 'http://apporio.org/TagYourRide/tagyourride/uploads/1508783019184.jpg', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '3.42857142857', 0, '', 1, '2017-10-23', 1),
(36, 1, 'Aisha Ally Khamisi', '', '+27620661534', '', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '156878778246555', 'null', 'http://graph.facebook.com/156878778246555/picture?type=large', 'Aisha Ally', 'Khamisi', '', '', '', '', '', '', 0, 0, '', 0, '', 2, '2017-10-25', 1),
(37, 1, 'manish sharma .', 'manishsharma@apporio.com', '+915252525353', 'XXA22D', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-25', 1),
(38, 1, 'Vikalp .', 'vikalp@apporio.com', '+270987654321', '34A7O4', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-25', 1),
(39, 1, 'satish .', 'sati@gmail.com', '+276565656566', '123456', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-25', 1),
(40, 1, 'jatin .', 'jatin@gmail.com', '+278787878787', '123456', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4', 0, '', 1, '2017-10-25', 1),
(41, 1, 'gahaha .', 'afaga@gmail.com', '+271472580369', 'qwerty', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-25', 1),
(42, 1, 'fggga .', 'fgahh@9.com', '+273692580147', 'qwerty', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.83333333333', 0, '', 1, '2017-10-25', 1),
(43, 1, 'Atul .', 'atul1@8.com', '+271234567980', 'qwerty', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-10-25', 1),
(44, 1, 'Pooja Srivastava', '', '+278080808080', '', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '1733364200070427', 'pooja140294@gmail.com', 'http://graph.facebook.com/1733364200070427/picture?type=large', 'Pooja', 'Srivastava', '', '', '', '', '', '', 0, 0, '3.375', 0, '', 2, '2017-10-25', 1),
(45, 1, 'Annet Matebwe', '', '+270736226812', '', '', '', 0, '0', 'Wednesday, Oct 25', '', 0, 0, 0, 0, 0, '1759724374099316', 'annetmatebwe@gmail.com', 'http://graph.facebook.com/1759724374099316/picture?type=large', 'Annet', 'Matebwe', '', '', '', '', '', '', 0, 0, '', 0, '', 2, '2017-10-25', 1),
(46, 1, 'shilpa garg .', '', '+278130039030', '', '', '', 0, '0', 'Thursday, Oct 26', '', 0, 0, 0, 0, 0, '', '', '', '', '', '110962357007210962758', 'shilpa garg', 'garg26.shilpa@gmail.com', 'https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECMa-y57fs4eRmAEiC3ZjYXJkX3Bob3RvKig1ZDI1NzhjMWZjMGE4N2E2NjBhNTFjNDhjNjNmMTU3OWVmZGU5NGI4MAEortAaGezrYk9clKR9SrV0Xa_cqg', '', '', 0, 0, '3.21428571429', 0, '', 3, '2017-10-26', 1),
(47, 1, 'Tendai Maynard .', 'tendaichabika@gmail.com', '+27719434001', 'melodidy', '', '', 0, '0', 'Saturday, Oct 28', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-28', 1),
(48, 1, 'register .', 'reg@9.com', '+271234560987', 'qwerty', '', '', 0, '0', 'Saturday, Oct 28', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-10-28', 1),
(49, 1, 'jxjxnx .', 'xvzvbz@g.com', '+275445488478', '123456', '', '', 0, '150', 'Saturday, Nov 4', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2.6', 0, '', 1, '2017-11-04', 1),
(50, 1, 'Apporio Testing .', '', '+274555555555', '', '', '', 0, '545', 'Monday, Nov 6', '', 0, 0, 0, 0, 0, '', '', '', '', '', '114711588900652976168', 'Apporio Testing', 'apporio.testing@gmail.com', 'null', '', '', 0, 0, '', 0, '', 3, '2017-11-06', 1),
(51, 1, 'cedric kabongo .', '', '+27729220293', '', '', '', 0, '0', 'Tuesday, Nov 21', '', 0, 0, 0, 0, 0, '', '', '', '', '', '117646228376550228468', 'cedric kabongo', 'cedrickabongo@gmail.com', 'https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECPSDx83v5v_x9AEiC3ZjYXJkX3Bob3RvKigwYmE0ZTA3MWI3ODE0NjczNWQ3OTQzZWE1ZTlkZjMzMjZlMWZjZGUwMAGQKxvIC0knGMnxwgGyufPmzT1oSQ', '', '', 0, 0, '', 0, '', 3, '2017-11-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, '8EAEE8D5EE57AC40E109F0D3BCE794FB731E0FE8672CF5614530C508DD5F69D4', 1, '757CBC15-1DCA-42CA-B47A-B5273460E79A', 1),
(2, 2, '5FBA5C3DBB8A7E2F8D73B7D40AB433557509CA47D484E3889ED8DAA2795470BD', 1, '1A7DAF6A-7675-4A07-B66D-AF4DBEF9D4BD', 1),
(3, 3, '7C1E865802A968D1B1A88913E44808F8AAA136F144DF1CECA1169E95EEE9209D', 1, 'D19F5E2C-7310-4923-9C6D-036FEE995958', 1),
(4, 5, 'D99B4FDFEEE0DF08CC6EEE1324E6AF7FDC5CAF7D9ABDC6278BF20274FACD7C83', 1, '9B99213C-F6FB-42C2-9560-8D45DD33DBBE', 1),
(5, 6, 'D7F929B9C5ED40F65D81825E68FAB503FFCC4460D8E6DB853EB22555EFC1340B', 1, '0F99C8AE-9DE3-4B3E-BCA1-3066729EF84B', 1),
(6, 7, '2AC6A572036ADCCEF3F7E0501EF349C80BA3698731DBAE46212A364C835FA9E7', 1, '7278CD4D-8DF6-404F-BED9-15D7C4623E04', 1),
(7, 28, '07393BBF619F3772D003DE59520451E97BB91B7D3308A61AB7B35E51636F2AF0', 1, '0305798B-2B4D-4052-8F04-A9DA800E3C91', 0),
(8, 10, 'f9FaCC1PO48:APA91bFiyJh4C_uvp-oufTWhXKCcwWSNFYDRG_AEPce8rNrzaaI_RzrMhaJeOPOhZPyTnkjZQ6mcNc17TNUur_8vFosGH61tooekSiHCFoBCVFAjUvDqlCVMw84hlgsNSzcLNO5OOJbF', 0, '6e52b5b177922b25', 1),
(9, 11, 'ceQkiq9kaf8:APA91bGGp3ER8hFSMawBzC5wGwYuTXlbX0MXwNTCfCOccDxu-sqadHGBknAX5voZxx3Lg619dmBofb52hH_HMGzeukaEdx2bvYlEX5JFtPyA4jUKSLmAmrmZNsuhgFB5WPuZu7ETDfXR', 0, 'e17522e429399ef', 1),
(10, 26, 'eIZLucmkLK4:APA91bFcr_8XMfOnAKkzibminVL12QbTy8H6Om6-HpQ71t8Wo09stCw6xrO5DuXq2jIaCDY4VFr4IioLlqoF67APpsxrRXKe2PtxEGaarOR8zoUqhzLSABHJRFToTgqkvwzGvdMnciFw', 0, '4ea68652d5e2452c', 1),
(11, 12, 'cEOi1uzFyM4:APA91bEciH-6BC7fBQrtqkXsNp7XqyAbEvt5G8EU-w9niPnuQo01xfcVefUaVbfiTNJhzIy8VUdgSgTYvBljT8pqx1ThpFK0V5cNobrjILVcU5yk_8pVyFAiToW6rtgD2R4RM-8kJfWR', 0, 'c06c40037632ed39', 1),
(12, 31, 'crAfFsPDLKU:APA91bHTMckmpuvhGQU8pE69iQH_mOuVFbDJ2b_uW56d0OI_3N48bg3EOhicZBbcc9R1l0R_AgIQs3TjuwGS6qie2J3evwEjwf7y4SDAiS-WsZC67waVIGPqg8TbtV8lFanZ22MApvth', 0, 'a449f5c72f1dbcf8', 1),
(13, 6, 'dXpv6p0f3B0:APA91bEr8yu984E8qYrxFhZOu-xV035ydnEKIkjDCjX-m-fycy1vDWOAstTA71PybbeDKBDQ6GjegUhyIXEIPgISDYE4B3bk0Uct-tuQDHtAxssZPi0eC_X1GWcQkZJpTez5OXj6V55h', 2, 'c498a3e3f2eeb060', 1),
(14, 7, 'dEDbGdCQr00:APA91bHc5k3IuPD3oltKiG0KxB4Xyp15S108ucmq1nvCKPXdkquS9QLiT2-e-TtMZKFucCCb6-2ajnyYnEA3chasbrKi10ljbPQ4qKMVKgXvrF3IhtL1IRaKNo7eZXh6oSiKupefVF5x', 2, 'e1fa9f7c66bbebc1', 1),
(15, 15, 'eBRwwvamZQ0:APA91bGJDjUCU3jFKDyu9lMZECf0BFw8PrG3H6bufVXEKvywkUx8KgQy4KIJHODDBRdlfTYm3_iQUYEOwr_RP9o-_cVZ3zfXZ3FoppSpb_dqnfhLArfrCeuMys7HButFyvHkyJwQYUte', 2, '833764abb3f44c37', 1),
(16, 35, 'dQLWYXc7ssg:APA91bFX5-cHe2JD-IrWYoLSyZ8tEr_9KKtdIGIpwNfXb-29nHq7MwDqhEXeVsFNgEnrFlvHk-da6nDMdN1g8ov6MnPoh7gpdE5XJqHFEbU_m6oh52J5DREnJrOasLUDiRNUyBxsjJFQ', 2, 'bcc3cfdd11f7c297', 1),
(17, 33, 'eshSC8eKXuY:APA91bEtgxy8VQkYK4HFIbBClfXsYEBxktX1Mphad1CmvIhNU1R9RPuwTD39nTq7YOaoNe1cjXzJdHuFq0PqHm0HKoGll10eGcbKL8oitBgNJAZ_L5LAPtYaikRKQ6EtTzoUFJtx63Xm', 2, 'f339d9833b21c265', 0),
(18, 19, 'f_D-eM1QF-Y:APA91bEXtL8laBo2ji1iL2D17b1XC2o85UNB5Nl5-1w894hGmE0qo01U3aGUwCxMzk0lpNRNUe4SLKjdgyZMrtVxz2ArMXSeyXEZjFQ0LIO8EFMek94elirN89G9u0zKMVXriQjlWU_M', 2, '37654246cf811843', 1),
(19, 45, 'e8gkHH8l86o:APA91bHV4BQ5L4YSxYwqP0xgrG4_-BIoEHFF7Q70TqzwFyB_scFeylJaZBOSSJ_0zDvVeFuTFdFuv3UZyVEfkXhwmeSnHxwo9ih2kjboXLrExXHn7MdrJGtxiuLB2PJThZ6_vVQbQreY', 2, '79f09d90d49048e7', 1),
(20, 13, 'fC_VwmuHrr0:APA91bHzr6ChyaMbnzDPlQxev-4NPXpx_Qys7oQFDFLjXw_jvCFRfvNuZcbz6VnxVs5kPqzE8sIW54HX9YazthhnVGrwwd5Fy6hhzQW7xPmHBfpxd91f3XxaXt5zOMvS-flwZMfdYxtD', 0, '32c87e564d926edc', 1),
(21, 50, 'fG1gLINwNlg:APA91bGOamJOwOpc6iAzI21fT6uUsdQD08PfaPcof-mrAB9I31jjVaMcsJO91iXOYtTLe4NEa1scvammIFKg3eYHicFaNkv7VlBxSqEsZNbtJE2y-fy_5LbbCOoGsteAzDXgSUihtTob', 2, '96ac6773bae674b8', 1),
(22, 32, 'fF_vQgx1ons:APA91bHm2wTpYcjhYs3fhRYqvP4EOQA2DWfycCPJGBDcArGjTP86cai_kvBFg29_eJULMdPtKTTXA1RgeC4Av_pvLCWe-kV1xNMGU4D7bM1VuSv9a3---nwc4ykQLZnBQ99Ylle5RsZ6', 0, 'db5fe23cac18a4b7', 1),
(23, 42, '76EB6036967ED030168452E649F27F391B333E3A08CF3423C6B8F6C010404DF1', 1, 'FCD62764-61D7-47A7-9DA3-5992CA7F39E9', 1),
(24, 34, 'fkpPwbngZus:APA91bFP3qazNg29Z0Z7ZV6lGvMGBkb1NlO8bDHG2KJa0GPKNyTVorZZdvZoU_eunv8H3qtjI3yWJLjqS9DQ2MYWmO8skDgLNbbiJu3NksylZIRpWsBlOER9X6eEhDjT02G1Zixu8NPy', 0, '6f5762530a4ac6b2', 1),
(25, 36, 'fL4jbeBLSwE:APA91bH-vcPyuWMq14bswoVPPrWrvVD1z78p8Lvj9OtxltCOTAU2yccedzTkhctrMwhlG-0ZUjrH07JlpXZK7OkhQdXhdizBNqCvR50ShRUCKVA3MRvI7fLaWQ2r_T4K_wKC-3LI3XKr', 0, 'bfa4ddd21e87351', 1),
(26, 13, 'e58vqaGf6ME:APA91bGQqCyLP8-vBBkRLH5Ozi5O3VptIyCmh-7fcV3yTJQM8eZLdwVbeZW6tJqEGHmDXiRuMSOYQmiPPuzxUxApQgiLzdsvklGm_eP55GGSlns1R78bub4Rjj-ztChSqwPztxZCPajc', 0, 'e659dbb5fa6025fb', 1),
(27, 44, 'cyoeroCG1eU:APA91bHHTnje9Tt2a9-rcuUrfuvE2x3n5qD4oZ7yjt0eHmlkPqG_9_fdMPrjjjaQtheGJ3iD_AGRBCLHuo9wGMsfEnRvs0rpGfrsd4tVLmr9XGfra_8WrhBjZ9XZQl1AjRMU8ojXSMk9', 2, 'a4594951bbeaaafa', 1),
(28, 25, 'B8D1D3E53992C47D15BFA5D80EEE5A9D7A492F3E753578B3A178995011A2535F', 1, '34DA1F03-6F65-44D8-B93B-F82D6F850EFF', 1),
(29, 46, 'eh26KvLv3C4:APA91bHe1WVn5tokZqGDpQRH__1jAAqJtLpmc0PFbB68FZeMxqu4PoXz7_9LDJb34KPkZ_zFipgM_EVNM_WdeGVOdBWkiEcPV0pMJLLvGMf-iMx4CeeorW8-gO_mRfxv2ojiqhuoBjdz', 2, '9f1891df4b8e4e77', 0),
(30, 47, 'cx2-JoKBQXc:APA91bFJdG97on2YEAGC7-JX6yqZE7sVsEK5GnzRiGDCGdjYalXoy90QkZ5GZtNpP5r0J2KjY-km5QvTbPqNO8Gzauq_GrdiWGvUPqwqnF0bz4wrZqMU-4DUnno5LTPHyATw7AueWmNB', 2, 'ad689ce154c3e395', 1),
(31, 48, '1BA8CF89F8E3DAD259E5DC2F9A25F536B96F0D9F1925EDD8201F10B9F013BEE4', 1, '3D86D265-356B-46C5-8262-A2B80733D3E2', 1),
(32, 13, 'dYIB8gPJHdk:APA91bE1RIumDMrK9DUjA53xLgtcI98Ze5M7N8r3_brunVZx2lpE0CdHagnNerzoW-G_snqB4C6uBT-on4zXaRtKjfa0opYCut3HaoJwCY2iTcmj5DvYoaJz5CBd-ggGjn7hU8py5Cs2', 2, '2ba9bf317a129dea', 1),
(33, 51, 'dwhcznV7534:APA91bGtR2ibkjaBS68ZEUxgmnn3m_ssqU8d5gvsPkhVcowN_zoQd9rsZL_pv9xJp9MJU__IXXYW6y8nsSIP1Nv4zkZJMQ9LU2uptzQ9YiMKzzunXTqRDKK33Q7een8Xsgy5vMsLY9cG', 2, '95c48ea1683afa7', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transtion`
--

CREATE TABLE `wallet_transtion` (
  `wallet_transtion_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `checkout_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wallet_transtion`
--

INSERT INTO `wallet_transtion` (`wallet_transtion_id`, `user_id`, `checkout_id`, `amount`, `date`) VALUES
(1, 1, '63269E058A34D442CDEACE5317D038D0.sbg-vm-tx02', '100', '2017-11-16'),
(2, 51, '4638A1BF8002514477BB55EA7A7BB467.sbg-vm-tx01', '100', '2017-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `wallet_transtion`
--
ALTER TABLE `wallet_transtion`
  ADD PRIMARY KEY (`wallet_transtion_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=851;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=625;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1327;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=630;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet_transtion`
--
ALTER TABLE `wallet_transtion`
  MODIFY `wallet_transtion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
